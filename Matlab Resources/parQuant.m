function [std_0Q, std_1Q, V_read_ref_Q, std_0QI, std_1QI, V_read_ref_QI] = parQuant(std_0, std_1, V_read_ref, QuantStruct)

parameterMax = 2^(QuantStruct.Parameter_BitWidth-1)-1;      % Sets the maximum integer for representing the parameters
parameterMin = -(parameterMax+1);                           % Sets the minimum integer for representing the parameters
parameterMax_v = 2^(2*QuantStruct.Parameter_BitWidth-1)-1;      % Sets the maximum integer for representing the parameters
parameterMin_v = -(parameterMax_v+1);                           % Sets the minimum integer for representing the parameters

std_0Q          = std_0 * (2^QuantStruct.Parameter_Fra_BitWidth);   % Multiplying x by 2^n shifts the binary point in the binary representation of x, n digits to the right.
std_1Q          = std_1 * (2^QuantStruct.Parameter_Fra_BitWidth);
V_read_ref_Q    = V_read_ref * (2^(2*QuantStruct.Parameter_Fra_BitWidth));

std_0Q          = round(std_0Q);
std_1Q          = round(std_1Q);
V_read_ref_Q    = round(V_read_ref_Q);

if (std_0Q >= 0)
    std_0Q  = min(std_0Q, parameterMax);
else
    std_0Q  = max(std_0Q, parameterMin+1);
end
if (std_1Q >= 0)
    std_1Q  = min(std_1Q, parameterMax);
else
    std_1Q  = max(std_1Q, parameterMin+1);
end
if (V_read_ref_Q >= 0)
    V_read_ref_Q  = min(V_read_ref_Q, parameterMax_v);
else
    V_read_ref_Q  = max(V_read_ref_Q, parameterMin_v+1);
end

std_0QI         = std_0Q;                                           % Stores std_0Q in a new variable
std_0Q          = std_0Q/(2^QuantStruct.Parameter_Fra_BitWidth);    % Returns std_0
std_1QI         = std_1Q;
std_1Q          = std_1Q/(2^QuantStruct.Parameter_Fra_BitWidth);
V_read_ref_QI   = V_read_ref_Q;
V_read_ref_Q    = V_read_ref_Q/(2^(2*QuantStruct.Parameter_Fra_BitWidth));