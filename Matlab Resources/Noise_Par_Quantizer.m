clear; clc

%%
noise_standard  = -151;
load Vol_LLR_Ref_Rate_0.83_SNR0_4_dB_SNR1_4_dB.mat
disp('In RTL: noise_std_1 = -151, noise_0 = -4836832, noise_1 = -4836832, Y = -9031136, LLR = -13')
%
d_SNR       = 0.1;
SNR_dB_b0   = 4 : d_SNR : 4;                % SNR per bit for bit 0, i.e., SNR = Eb_over_No
SNR_dB_b1   = SNR_dB_b0 ;                   % SNR per bit for bit 1
H_col       = 72;                           % No of columns of the base matrix
H_row       = 12;                           % No of rows of the base matrix
QuantStruct.Parameter_Int_BitWidth = 5;     % Precision of the integer part of the rational number in bits
QuantStruct.Parameter_Fra_BitWidth = 11;    % Precision of the fraction part of the rational number in bits

%%
QuantStruct.Parameter_BitWidth = QuantStruct.Parameter_Int_BitWidth + QuantStruct.Parameter_Fra_BitWidth;
RATE    = 1 - H_row/H_col;
SNR_b0  = 10^(SNR_dB_b0/10);
SNR_b1  = 10^(SNR_dB_b1/10);
var_0   = 1.0/(2*SNR_b0*RATE);
var_1   = 1.0/(2*SNR_b1*RATE);
std_0   = sqrt(var_0);
std_1   = sqrt(var_1);
[std_0Q, std_1Q, V_read_ref_Q, std_0QI, std_1QI, V_read_ref_QI] = parQuant(std_0, std_1, V_read_ref, QuantStruct);
noise_0 = std_0QI * noise_standard;
noise_1 = std_1QI * noise_standard;

fprintf('\nstd_0 = %d, std_1 = %d\n', std_0QI, std_1QI)
V_read_ref_QI
