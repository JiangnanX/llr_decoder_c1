%% Decoder_Schedule_for_given_layer_schedule_C1_802_15.m
%% Kiran Gunnam

%% Computes the parameters for the layered decoder hardware architecture
%% for a given layer schedule(i.e. re-ordered H matrix). Does the step 2 which
%% is described below:

%% The decoder hardware architecture is proposed to support out-of-order
%% processing to remove pipeline and memory accesses or to satisfy any other performance or
%% hardware constraint. Remaining  hardware architectures won't support out-of-order
%% processing without further involving more logic and memory.
%% For the above hardware decoder architecture, the optimization of decoder 
%% schedule belongs to the class of NP-complete problems. So there are several clasic optimization 
%% algorithms such as dynamic programming that can be applied. We apply the
%% following classic approach of optimal substructure.

%% Step 1: We will try different layer schedules(M_b! if there are M_b layers). For simplicity,
%% we wil try only a subset of possible sequences. Since we can have step value ranging from 0 
%% to  M_b-2(or 1 to Mb-1 in matlab index), we will have M_b-1 unique layer schedules or layer
%% re-orderings. 

%% Step 2:Given a layer schedule or a re-ordered H matrix, we will optimize the processing 
%% schedule of each layer. For this, we use the clasic approach of optimal substructure 
%% i.e. the solution to a given optimization problem can be obtained by the combination 
%% of optimal solutions to its subproblems.  So first we optimize the processing order to mimimize the
%% pipeline conflicts. Then we optimize the resulting processing order to
%% minimize the memory conflicts. So for each layer schedule, we are measuring the
%% number of stall cycles(our cost function).

%% Step 3: We choose a layer schedule which minimizes the cost function i.e. meets the requirements with 
%% less stall cycles due to pipeline conflicts and memory conflicts and also minimizes the memory accesses
%% (such as FS memory accesses to minimize the number of ports needed and to save the access power and 
%% to minimize the more muxing requirement.

%usage:
%load MC;H=MC(1).F1.S;circulant_size=18;pipeline_depth=4;mem_org=1;[Decoder_Parameters]=Decoder_Schedule_for_given_layer_schedule_802_15(H,circulant_size,pipeline_depth,mem_org)
function [Decoder_Parameters]=Decoder_Schedule_for_given_layer_schedule_C1_802_15(H,circulant_size,pipeline_depth,mem_org)

%clc;clear all; load MC;H=MC(1).F1.S;circulant_size=96;pipeline_depth=7;mem_org=1;%[Decoder_Parameters]=Decoder_Schedule_for_given_layer_schedule(H,circulant_size,pipeline_depth,mem_org)
out_of_order_processing_flag =1;%%DO OUT_OF_ORDER PROCESSING ON CIRCULANTS as proposed in [1-2]
number_of_circulants_per_clk =1;
construction                 ='row_shift'; %%'col_shift'

[M_b, N_b]        = size(H);
if strcmp(construction,'row_shift')
    for i=1:M_b
      index2 = find(H(i,:)~=-1);
      H(i,index2)= mod(circulant_size-H(i,index2),circulant_size); 
    end
end
S= H;


clear Decoder_Parameters
Decoder_Parameters.M_b = M_b;
Decoder_Parameters.N_b = N_b;
Decoder_Parameters.Z     = circulant_size;


H_base                            =(S >=0);
Decoder_Parameters.H_base         = H_base;
Decoder_Parameters.S              =S;
Decoder_Parameters.row_degree     =(sum(H_base,2));
Decoder_Parameters.column_degree  = sum(H_base,1);

circulant_index =0;
%% Compute delta shift matrix for each block row/layer
%% also compute the layer of the circulant whose information need to be used for the
%% present circulant_num in the present layer.
    for layer = 1: M_b
     index_non_zero = find(S(layer,:) ~= -1); 
        for circulant_num = 1: Decoder_Parameters.row_degree(layer)
            block_col = index_non_zero(circulant_num);
            Decoder_Parameters.circulant_column(layer,circulant_num) = block_col;
           % delta shift
            layer_prev = layer -1;
            if layer_prev == 0 
              layer_prev = M_b;
            end
             while(S(layer_prev,block_col) == -1)
               layer_prev = layer_prev - 1;
               if layer_prev == 0 
                layer_prev = M_b;
               end
             end
           index_non_zero_prev                                             = find(S(layer_prev,:) ~= -1); 
           circulant_num_prev                                              = find(block_col ==index_non_zero_prev);
           circulant_index                                                 = circulant_index+1;
           Decoder_Parameters.dependent_layer(layer,circulant_num)         = layer_prev;
           Decoder_Parameters.dependent_circulant(layer,circulant_num)     = circulant_num_prev;
           Decoder_Parameters.circulant_number(layer,circulant_num)        = circulant_index;
           Decoder_Parameters.differential_shift(layer,circulant_num)      = S(layer, block_col) - S(layer_prev, block_col);
           Decoder_Parameters.shift(layer,circulant_num)                   = S(layer, block_col);                           
           %% convert -neg shift coefficent(upshift) to positive shift
           %% coefficent(down shift) using modulo shift property
           if(Decoder_Parameters.differential_shift(layer,circulant_num)<0), 
               Decoder_Parameters.differential_shift(layer,circulant_num)  = circulant_size- abs(Decoder_Parameters.differential_shift(layer,circulant_num)); 
           end
        end
    end

    for layer = 1: M_b
        for circulant_num = 1: Decoder_Parameters.row_degree(layer)
         Decoder_Parameters.dependent_circulant_number(layer,circulant_num)   =  ...
                                                                             Decoder_Parameters.circulant_number( ...
                                                                                Decoder_Parameters.dependent_layer(layer,circulant_num),...
                                                                                Decoder_Parameters.dependent_circulant(layer,circulant_num));
        end
    end
                                                                            
    
%% Compute shift matrix for the first iteration for each block row/layer
    Decoder_Parameters.shift_value_for_the_first_time = Decoder_Parameters.differential_shift;
    [mm nn]= size(Decoder_Parameters.differential_shift);
    Decoder_Parameters.ucvf = zeros(mm,nn);
    for block_col = 1: N_b
        layer_next =1;
        while(S(layer_next,block_col) == -1 && layer_next<=M_b)
               layer_next = layer_next + 1;
        end
        index_non_zero                                                     = find(S(layer_next,:) ~= -1); 
        circulant_num                                                      = find(block_col ==index_non_zero);
        Decoder_Parameters.shift_value_for_the_first_time(layer_next,circulant_num)...
                                                                           = S(layer_next, block_col);
        Decoder_Parameters.ucvf(layer_next,circulant_num)= 1;
    end

	Decoder_Parameters.Decoder_Parameters_before_oop_for_pipeline          = Decoder_Parameters;
	Decoder_Parameters.out_of_order_processing_flag                        = out_of_order_processing_flag;
%%
%% OUT OF ORDER PROCESSING TO MINIMIZE STALL CYCLES DUE TO PIPELINE, MEMORY
%% CONFLICTS and TO OPTIMIZE FS MEMORY ACCESSES
    if(out_of_order_processing_flag)    
        
        %% PIPELINE CONSTRAINT
        %%Re-order the Decoder_Parameters structure information according to the out-of-order
        %%processing of the blocks. %%pipeline constraint.
        for i=1:M_b
             current_layer                                                 = i;
             next_layer                                                    = mod(current_layer,Decoder_Parameters.M_b)+1;
             Decoder_Parameters.next_layer_vec(i)                          = next_layer;
             Decoder_Parameters.current_layer_vec(i)                       = current_layer;      
             [dummy no_conflict_list]                                      = find(Decoder_Parameters.dependent_layer(next_layer,1:Decoder_Parameters.row_degree(next_layer))~=current_layer);
             Decoder_Parameters.num_indepedent_circulants(next_layer)      = length(no_conflict_list);
             [dummy conflict_list]                                         = find(Decoder_Parameters.dependent_layer(next_layer,1:Decoder_Parameters.row_degree(next_layer))==current_layer);
             
             Decoder_Parameters.processing_order(next_layer).circulant_num = [no_conflict_list conflict_list]';
             
             Decoder_Parameters.circulant_column(next_layer,1:Decoder_Parameters.row_degree(next_layer)) ...
                                                                           =  Decoder_Parameters.circulant_column(next_layer,Decoder_Parameters.processing_order(next_layer).circulant_num);
             Decoder_Parameters.dependent_circulant(next_layer,1:Decoder_Parameters.row_degree(next_layer))...
                                                                           =  Decoder_Parameters.dependent_circulant(next_layer,Decoder_Parameters.processing_order(next_layer).circulant_num);
             Decoder_Parameters.dependent_layer(next_layer,1:Decoder_Parameters.row_degree(next_layer)) ...
                                                                           =  Decoder_Parameters.dependent_layer(next_layer,Decoder_Parameters.processing_order(next_layer).circulant_num);
             Decoder_Parameters.circulant_number(next_layer,1:Decoder_Parameters.row_degree(next_layer))...
                                                                           =  Decoder_Parameters.circulant_number(next_layer,Decoder_Parameters.processing_order(next_layer).circulant_num);
             Decoder_Parameters.ucvf(next_layer,1:Decoder_Parameters.row_degree(next_layer))...
                                                                           =  Decoder_Parameters.ucvf(next_layer,Decoder_Parameters.processing_order(next_layer).circulant_num);
             Decoder_Parameters.shift_value_for_the_first_time(next_layer,1:Decoder_Parameters.row_degree(next_layer))...
                                                                           =  Decoder_Parameters.shift_value_for_the_first_time(next_layer,Decoder_Parameters.processing_order(next_layer).circulant_num);
             Decoder_Parameters.differential_shift(next_layer,1:Decoder_Parameters.row_degree(next_layer))...
                                                                           =  Decoder_Parameters.differential_shift(next_layer,Decoder_Parameters.processing_order(next_layer).circulant_num);
             Decoder_Parameters.shift(next_layer,1:Decoder_Parameters.row_degree(next_layer))...
                                                                           =  Decoder_Parameters.shift(next_layer,Decoder_Parameters.processing_order(next_layer).circulant_num);
             Decoder_Parameters.dependent_circulant_number(next_layer,1:Decoder_Parameters.row_degree(next_layer))...
                                                                           =  Decoder_Parameters.dependent_circulant_number(next_layer,Decoder_Parameters.processing_order(next_layer).circulant_num);
                                                                       
        end
        
        for i=1:M_b
            nic=Decoder_Parameters.num_indepedent_circulants(i);
            nc=length(Decoder_Parameters.processing_order(i).circulant_num);
            Decoder_Parameters.processing_order2(i).circulant_num=[-2*ones(length(nic+1:pipeline_depth),1) 
                                                                    Decoder_Parameters.processing_order(i).circulant_num];
            Decoder_Parameters.clock_cycles_or_time_slots_layer_CCL(i)=length(Decoder_Parameters.processing_order2(i).circulant_num);
        end
        
         Decoder_Parameters.clock_cycles_or_time_slots_per_iteration_CCI=sum(Decoder_Parameters.clock_cycles_or_time_slots_layer_CCL);
         
          %%still need to re-order correctly based on the new processing
          %%order with -2 for stall cycles. -1 is donot care and need not
          %%process or stall. we want to create a matrix for easy view.
          %%so all the rows in matrix are filled with valid entries or -2
          %%or -1. At a later step, when we are generating the parameters for ROM, we will
          %%ignore -1 completely.
         D=Decoder_Parameters;
        for i=1:M_b
           for jj=1:number_of_circulants_per_clk*max(D.clock_cycles_or_time_slots_layer_CCL)
               if(jj<=number_of_circulants_per_clk*D.clock_cycles_or_time_slots_layer_CCL(i))
                   if(D.processing_order2(i).circulant_num(jj)>=0)
                       Decoder_Parameters.circulant_column(i,jj)               =  D.circulant_column(i,D.processing_order2(i).circulant_num(jj));
                       Decoder_Parameters.dependent_layer(i,jj)                =  D.dependent_layer(i,D.processing_order2(i).circulant_num(jj));
                       Decoder_Parameters.dependent_circulant(i,jj)            =  D.dependent_circulant(i,D.processing_order2(i).circulant_num(jj));
                       Decoder_Parameters.circulant_number(i,jj)               =  D.circulant_number(i,D.processing_order2(i).circulant_num(jj));
                       Decoder_Parameters.ucvf(i,jj)                           =  D.ucvf(i,D.processing_order2(i).circulant_num(jj));
                       Decoder_Parameters.shift_value_for_the_first_time(i,jj) =  D.shift_value_for_the_first_time(i,D.processing_order2(i).circulant_num(jj));
                       Decoder_Parameters.differential_shift(i,jj)             =  D.differential_shift(i,D.processing_order2(i).circulant_num(jj));
                       Decoder_Parameters.shift(i,jj)                          =  D.shift(i,D.processing_order2(i).circulant_num(jj));
                       Decoder_Parameters.dependent_circulant_number(i,jj)     =  D.dependent_circulant_number(i,D.processing_order2(i).circulant_num(jj));                                                                       
                   else
                       Decoder_Parameters.circulant_column(i,jj)               = -2;
                       Decoder_Parameters.dependent_circulant(i,jj)            = -2; 
                       Decoder_Parameters.dependent_layer(i,jj)                = -2;
                       Decoder_Parameters.circulant_number(i,jj)               = -2; 
                       Decoder_Parameters.ucvf(i,jj)                           = -2; 
                       Decoder_Parameters.shift_value_for_the_first_time(i,jj) = -2; 
                       Decoder_Parameters.differential_shift(i,jj)             = -2; 
                       Decoder_Parameters.shift(i,jj)                          = -2; 
                       Decoder_Parameters.dependent_circulant_number(i,jj)     = -2;
                   end
               else
                       Decoder_Parameters.circulant_column(i,jj)               = -1;
                       Decoder_Parameters.dependent_circulant(i,jj)            = -1; 
                       Decoder_Parameters.dependent_layer(i,jj)                = -1;
                       Decoder_Parameters.circulant_number(i,jj)               = -1; 
                       Decoder_Parameters.ucvf(i,jj)                           = -1; 
                       Decoder_Parameters.shift_value_for_the_first_time(i,jj) = -1; 
                       Decoder_Parameters.differential_shift(i,jj)             = -1; 
                       Decoder_Parameters.shift(i,jj)                          = -1; 
                       Decoder_Parameters.dependent_circulant_number(i,jj)     = -1;
               end
           end
        end


        %%processing order3:also such that most of the fs memory accesses
        %%(dependent layer belong to the same layer. to optimize the mux
        %%accesss and to reduce the number of fs memory ports. For this, treat this as
        %%soft constraint. no stall cycles are neeed even if the constraint is
        %%not met as we can easily handle part of this in hardware. So we
        %%do not this at this time.
        

    end % end of if(out_of_order_processing_flag)
%%

% Decoder_Parameters.S                   = Decoder_Parameters.shift;
% %% the following indices start from 0 in hardware. 
% Decoder_Parameters.circulant_column    = Decoder_Parameters.circulant_column-1;
% Decoder_Parameters.dependent_circulant = Decoder_Parameters.dependent_circulant-1;
% Decoder_Parameters.dependent_layer     = Decoder_Parameters.dependent_layer-1;
% Decoder_Parameters.circulant_number    = Decoder_Parameters.circulant_number-1;
% Decoder_Parameters.row_degree          = Decoder_Parameters.row_degree; 

%end
ClockCylesPerIteration=Decoder_Parameters.clock_cycles_or_time_slots_per_iteration_CCI;
for i=1:Decoder_Parameters.M_b
if i-1==0,
comp_l= Decoder_Parameters.M_b;
else
comp_l=i-1;
end
[dummy col_indx]=find(Decoder_Parameters.Decoder_Parameters_before_oop_for_pipeline.dependent_layer(i,:)==comp_l);
if(length(col_indx)>0)
    nic(i)=min(col_indx);
else
    nic(i)=length(Decoder_Parameters.Decoder_Parameters_before_oop_for_pipeline.dependent_layer(i,:));
end
niclk(i)=nic(i);
overhead_pipeline_per_layer(i)=max(0,pipeline_depth-niclk(i));
end
overhead_pipeline=sum(overhead_pipeline_per_layer);
ClockCylesPerIteration_with_overhead=ClockCylesPerIteration+overhead_pipeline;

ClockCylesPerIteration_with_overhead=ClockCylesPerIteration+sum(overhead_pipeline);
Savings_due_to_oop=((ClockCylesPerIteration_with_overhead-ClockCylesPerIteration)/ClockCylesPerIteration_with_overhead)*100;
Decoder_Parameters.ClockCylesPerIteration_with_no_OOP=ClockCylesPerIteration_with_overhead;
Decoder_Parameters.overhead_due_to_no_oop_for_pipeline=overhead_pipeline;
Decoder_Parameters.Savings_due_to_oop= Savings_due_to_oop;





