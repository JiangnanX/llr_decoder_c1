%% Decoder_Schedule_for_given_layer_schedule_802_15.m
%% Kiran Gunnam

%% Computes the parameters for the layered decoder hardware architecture
%% for a given layer schedule(i.e. re-ordered H matrix). Does the step 2 which
%% is described below:

%% The decoder hardware architecture is proposed to support out-of-order
%% processing to remove pipeline and memory accesses or to satisfy any other performance or
%% hardware constraint. Remaining  hardware architectures won't support out-of-order
%% processing without further involving more logic and memory.
%% For the above hardware decoder architecture, the optimization of decoder 
%% schedule belongs to the class of NP-complete problems. So there are several clasic optimization 
%% algorithms such as dynamic programming that can be applied. We apply the
%% following classic approach of optimal substructure.

%% Step 1: We will try different layer schedules(M_b! if there are M_b layers). For simplicity,
%% we wil try only a subset of possible sequences. Since we can have step value ranging from 0 
%% to  M_b-2(or 1 to Mb-1 in matlab index), we will have M_b-1 unique layer schedules or layer
%% re-orderings. 

%% Step 2:Given a layer schedule or a re-ordered H matrix, we will optimize the processing 
%% schedule of each layer. For this, we use the clasic approach of optimal substructure 
%% i.e. the solution to a given optimization problem can be obtained by the combination 
%% of optimal solutions to its subproblems.  So first we optimize the processing order to mimimize the
%% pipeline conflicts. Then we optimize the resulting processing order to
%% minimize the memory conflicts. So for each layer schedule, we are measuring the
%% number of stall cycles(our cost function).

%% Step 3: We choose a layer schedule which minimizes the cost function i.e. meets the requirements with 
%% less stall cycles due to pipeline conflicts and memory conflicts and also minimizes the memory accesses
%% (such as FS memory accesses to minimize the number of ports needed and to save the access power and 
%% to minimize the more muxing requirement.

%usage:
%load MC;H=MC(1).F1.S;circulant_size=18;pipeline_depth=4;mem_org=1;[Decoder_Parameters]=Decoder_Schedule_for_given_layer_schedule_802_15(H,circulant_size,pipeline_depth,mem_org)
function [Decoder_Parameters]=Decoder_Schedule_for_given_layer_schedule_802_15(H,circulant_size,pipeline_depth,mem_org)

%clc;clear all; load MC;H=MC(1).F1.S;circulant_size=96;pipeline_depth=7;mem_org=1;%[Decoder_Parameters]=Decoder_Schedule_for_given_layer_schedule(H,circulant_size,pipeline_depth,mem_org)
out_of_order_processing_flag =1;%%DO OUT_OF_ORDER PROCESSING ON CIRCULANTS as proposed in [1-2]
number_of_circulants_per_clk =2;
construction                 ='row_shift'; %%'col_shift'

[M_b, N_b]        = size(H);
if strcmp(construction,'row_shift')
    for i=1:M_b
      index2 = find(H(i,:)~=-1);
      H(i,index2)= mod(circulant_size-H(i,index2),circulant_size); 
    end
end
S= H;


clear Decoder_Parameters
Decoder_Parameters.M_b = M_b;
Decoder_Parameters.N_b = N_b;
Decoder_Parameters.Z     = circulant_size;


H_base                            =(S >=0);
Decoder_Parameters.row_degree     =(sum(H_base,2));
Decoder_Parameters.column_degree  = sum(H_base,1);

circulant_index =0;
%% Compute delta shift matrix for each block row/layer
%% also compute the layer of the circulant whose information need to be used for the
%% present circulant_num in the present layer.
    for layer = 1: M_b
     index_non_zero = find(S(layer,:) ~= -1); 
        for circulant_num = 1: Decoder_Parameters.row_degree(layer)
            block_col = index_non_zero(circulant_num);
            Decoder_Parameters.circulant_column(layer,circulant_num) = block_col;
           % delta shift
            layer_prev = layer -1;
            if layer_prev == 0 
              layer_prev = M_b;
            end
             while(S(layer_prev,block_col) == -1)
               layer_prev = layer_prev - 1;
               if layer_prev == 0 
                layer_prev = M_b;
               end
             end
           index_non_zero_prev                                             = find(S(layer_prev,:) ~= -1); 
           circulant_num_prev                                              = find(block_col ==index_non_zero_prev);
           circulant_index                                                 = circulant_index+1;
           Decoder_Parameters.dependent_layer(layer,circulant_num)         = layer_prev;
           Decoder_Parameters.dependent_circulant(layer,circulant_num)     = circulant_num_prev;
           Decoder_Parameters.circulant_number(layer,circulant_num)        = circulant_index;
           Decoder_Parameters.differential_shift(layer,circulant_num)      = S(layer, block_col) - S(layer_prev, block_col);
           Decoder_Parameters.shift(layer,circulant_num)                   = S(layer, block_col);                           
           %% convert -neg shift coefficent(upshift) to positive shift
           %% coefficent(down shift) using modulo shift property
           if(Decoder_Parameters.differential_shift(layer,circulant_num)<0), 
               Decoder_Parameters.differential_shift(layer,circulant_num)  = circulant_size- abs(Decoder_Parameters.differential_shift(layer,circulant_num)); 
           end
        end
    end

    for layer = 1: M_b
        for circulant_num = 1: Decoder_Parameters.row_degree(layer)
         Decoder_Parameters.dependent_circulant_number(layer,circulant_num)   =  ...
                                                                             Decoder_Parameters.circulant_number( ...
                                                                                Decoder_Parameters.dependent_layer(layer,circulant_num),...
                                                                                Decoder_Parameters.dependent_circulant(layer,circulant_num));
        end
    end
                                                                            
    
%% Compute shift matrix for the first iteration for each block row/layer
    Decoder_Parameters.shift_value_for_the_first_time = Decoder_Parameters.differential_shift;
    [mm nn]= size(Decoder_Parameters.differential_shift);
    Decoder_Parameters.ucvf = zeros(mm,nn);
    for block_col = 1: N_b
        layer_next =1;
        while(S(layer_next,block_col) == -1 && layer_next<=M_b)
               layer_next = layer_next + 1;
        end
        index_non_zero                                                     = find(S(layer_next,:) ~= -1); 
        circulant_num                                                      = find(block_col ==index_non_zero);
        Decoder_Parameters.shift_value_for_the_first_time(layer_next,circulant_num)...
                                                                           = S(layer_next, block_col);
        Decoder_Parameters.ucvf(layer_next,circulant_num)= 1;
    end

	Decoder_Parameters.Decoder_Parameters_before_oop_for_pipeline          = Decoder_Parameters;
	Decoder_Parameters.out_of_order_processing_flag                        = out_of_order_processing_flag;
%%
%% OUT OF ORDER PROCESSING TO MINIMIZE STALL CYCLES DUE TO PIPELINE, MEMORY
%% CONFLICTS and TO OPTIMIZE FS MEMORY ACCESSES
    if(out_of_order_processing_flag)    
        
        %% PIPELINE CONSTRAINT
        %%Re-order the Decoder_Parameters structure information according to the out-of-order
        %%processing of the blocks. %%pipeline constraint.
        for i=1:M_b
             current_layer                                                 = i;
             next_layer                                                    = mod(current_layer,Decoder_Parameters.M_b)+1;
             Decoder_Parameters.next_layer_vec(i)                          = next_layer;
             Decoder_Parameters.current_layer_vec(i)                       = current_layer;      
             [dummy no_conflict_list]                                      = find(Decoder_Parameters.dependent_layer(next_layer,1:Decoder_Parameters.row_degree(next_layer))~=current_layer);
             Decoder_Parameters.num_indepedent_circulants(next_layer)      = length(no_conflict_list);
             [dummy conflict_list]                                         = find(Decoder_Parameters.dependent_layer(next_layer,1:Decoder_Parameters.row_degree(next_layer))==current_layer);
             
             Decoder_Parameters.processing_order(next_layer).circulant_num = [no_conflict_list conflict_list]';
             
             Decoder_Parameters.circulant_column(next_layer,1:Decoder_Parameters.row_degree(next_layer)) ...
                                                                           =  Decoder_Parameters.circulant_column(next_layer,Decoder_Parameters.processing_order(next_layer).circulant_num);
             Decoder_Parameters.dependent_circulant(next_layer,1:Decoder_Parameters.row_degree(next_layer))...
                                                                           =  Decoder_Parameters.dependent_circulant(next_layer,Decoder_Parameters.processing_order(next_layer).circulant_num);
             Decoder_Parameters.dependent_layer(next_layer,1:Decoder_Parameters.row_degree(next_layer)) ...
                                                                           =  Decoder_Parameters.dependent_layer(next_layer,Decoder_Parameters.processing_order(next_layer).circulant_num);
             Decoder_Parameters.circulant_number(next_layer,1:Decoder_Parameters.row_degree(next_layer))...
                                                                           =  Decoder_Parameters.circulant_number(next_layer,Decoder_Parameters.processing_order(next_layer).circulant_num);
             Decoder_Parameters.ucvf(next_layer,1:Decoder_Parameters.row_degree(next_layer))...
                                                                           =  Decoder_Parameters.ucvf(next_layer,Decoder_Parameters.processing_order(next_layer).circulant_num);
             Decoder_Parameters.shift_value_for_the_first_time(next_layer,1:Decoder_Parameters.row_degree(next_layer))...
                                                                           =  Decoder_Parameters.shift_value_for_the_first_time(next_layer,Decoder_Parameters.processing_order(next_layer).circulant_num);
             Decoder_Parameters.differential_shift(next_layer,1:Decoder_Parameters.row_degree(next_layer))...
                                                                           =  Decoder_Parameters.differential_shift(next_layer,Decoder_Parameters.processing_order(next_layer).circulant_num);
             Decoder_Parameters.shift(next_layer,1:Decoder_Parameters.row_degree(next_layer))...
                                                                           =  Decoder_Parameters.shift(next_layer,Decoder_Parameters.processing_order(next_layer).circulant_num);
             Decoder_Parameters.dependent_circulant_number(next_layer,1:Decoder_Parameters.row_degree(next_layer))...
                                                                           =  Decoder_Parameters.dependent_circulant_number(next_layer,Decoder_Parameters.processing_order(next_layer).circulant_num);                                                                       
        end
        
        
        %%Memory access constraint for multi-circulant processing
        %%Re-order the Decoder_Parameters structure information according to the out-of-order
        %%processing of the blocks. 
        %%choose m distinct banks for m-circulant processing. for 2-circulant processing choose 2 distinct banks.
        %%insert dummy circulants here if needed 
        
        %%memory exact organization. 
         num_circulant_banks                    = 3;
         if(mem_org==1)
         Decoder_Parameters.circulant_bank  = 1*(Decoder_Parameters.circulant_column>0&Decoder_Parameters.circulant_column<=10)...
                                             +2*(Decoder_Parameters.circulant_column>10&Decoder_Parameters.circulant_column<=20)...
                                             +3*(Decoder_Parameters.circulant_column>20&Decoder_Parameters.circulant_column<=32);
         elseif(mem_org==2)
         Decoder_Parameters.circulant_bank  = 1*(Decoder_Parameters.circulant_column>0&Decoder_Parameters.circulant_column<=11)...
                                             +2*(Decoder_Parameters.circulant_column>11&Decoder_Parameters.circulant_column<=22)...
                                             +3*(Decoder_Parameters.circulant_column>22&Decoder_Parameters.circulant_column<=32);
         elseif(mem_org==3)
         Decoder_Parameters.circulant_bank  = 1*(Decoder_Parameters.circulant_column>0&Decoder_Parameters.circulant_column<=12)...
                                             +2*(Decoder_Parameters.circulant_column>12&Decoder_Parameters.circulant_column<=22)...
                                             +3*(Decoder_Parameters.circulant_column>22&Decoder_Parameters.circulant_column<=32);
                                         
         else
         Decoder_Parameters.circulant_bank  = (mod(Decoder_Parameters.circulant_column,num_circulant_banks)+1).*(Decoder_Parameters.circulant_column>0);
         end    
                                                  
         Decoder_Parameters.num_circulant_banks = num_circulant_banks;
         Decoder_Parameters.Decoder_Parameters_before_oop_for_memory_access = Decoder_Parameters;    
         %%first take care of phase1: in which there would be no
         %%dependence of the current layer on the previous layer
         phase1_duration       = pipeline_depth*ones(M_b,1);
         num_phase1_circulants = Decoder_Parameters.num_indepedent_circulants;
             
                                                          
        for i=1:M_b
             phase1_circulants_to_be_processed=-2*ones(1,max(num_phase1_circulants(i),2*phase1_duration(i)));
             phase1_process_flags=-2*ones(1,max(num_phase1_circulants(i),2*phase1_duration(i)));
             phase1_circulants_to_be_processed(1:num_phase1_circulants(i))=(1:num_phase1_circulants(i));
             phase1_process_flags(1:num_phase1_circulants(i))=zeros(1,num_phase1_circulants(i));
             circulant_bank_access1(i,:)=-2*ones(1,phase1_duration(i));
             circulant_bank_access2(i,:)=-2*ones(1,phase1_duration(i));
             Decoder_Parameters.processing_order2(i).circulant_num(1:2*phase1_duration(i))=-1;
             %%Try to prepare a preferred order for optimization so that
             %%all the banks are used to minimize the conflicts
             for ind=1:num_circulant_banks
              cb(ind).accesses=find(Decoder_Parameters.circulant_bank(i,phase1_circulants_to_be_processed(1:num_phase1_circulants(i)))==ind);
              num_cb_accesses(ind)=length(cb(ind).accesses);
              cb_access_cnt(ind)=0;
             end
             clear preferred_block_order_for_opti
             preferred_block_order_for_opti=[];
             ind=0;
             for block_ind_tmp=1:num_phase1_circulants(i)
                 ind_tmp1=mod(ind+1,num_circulant_banks);
                 if(ind_tmp1)==0
                    ind_tmp1=num_circulant_banks;
                 end
                 ind_tmp2=mod(ind_tmp1+1,num_circulant_banks);
                 if(ind_tmp2)==0
                    ind_tmp2=num_circulant_banks;
                 end
                 ind_tmp3=mod(ind_tmp2+1,num_circulant_banks);
                 if(ind_tmp3)==0
                    ind_tmp3=num_circulant_banks;
                 end

                 if(cb_access_cnt(ind_tmp1)<num_cb_accesses(ind_tmp1))
                 cb_access_cnt(ind_tmp1)=cb_access_cnt(ind_tmp1)+1;
                 preferred_block_order_for_opti(block_ind_tmp)=cb(ind_tmp1).accesses(cb_access_cnt(ind_tmp1));
                 ind=ind_tmp1;
                 elseif(cb_access_cnt(ind_tmp2)<num_cb_accesses(ind_tmp2))
                 cb_access_cnt(ind_tmp2)=cb_access_cnt(ind_tmp2)+1;
                 preferred_block_order_for_opti(block_ind_tmp)=cb(ind_tmp2).accesses(cb_access_cnt(ind_tmp2));
                 ind=ind_tmp2;
                 elseif(cb_access_cnt(ind_tmp3)<num_cb_accesses(ind_tmp3))
                 cb_access_cnt(ind_tmp3)=cb_access_cnt(ind_tmp3)+1;
                 preferred_block_order_for_opti(block_ind_tmp)=cb(ind_tmp3).accesses(cb_access_cnt(ind_tmp3));
                 ind=ind_tmp3;
                 end
             end
             %%try to reverse the order such that unpaired accessed bank is
             %%in the order first-so that we can get a pair easily in the later step of optimization.
             ff=sort(cb_access_cnt-min(cb_access_cnt),'descend');
             if(ff(1)-ff(2)>1) 
              preferred_block_order_for_opti=preferred_block_order_for_opti(end:-1:1);
             end
             
             time_slot=1;
             while(time_slot<=phase1_duration)
                %%first choose 1 circulant to be processed in the given time_slot.
               if(length(preferred_block_order_for_opti)>0)
                 for block_num=preferred_block_order_for_opti
                     if(phase1_process_flags(block_num)==0)
                      circulant_bank_access1(i,time_slot)=Decoder_Parameters.circulant_bank(i,block_num);
                      Decoder_Parameters.processing_order2(i).circulant_num(2*time_slot-1)=block_num;
                      phase1_process_flags(block_num) =1;
                      break;
                     end
                 end
                 %%now choose 2nd circulant to be processed in the given time_slot.
                 for block_num=preferred_block_order_for_opti
                     if((phase1_process_flags(block_num)==0)&(Decoder_Parameters.circulant_bank(i,block_num)~=circulant_bank_access1(i,time_slot)))
                      circulant_bank_access2(i,time_slot)=Decoder_Parameters.circulant_bank(i,block_num);
                      Decoder_Parameters.processing_order2(i).circulant_num(2*time_slot)=block_num;
                      phase1_process_flags(block_num) =1;
                      break;
                     end
                 end
               end
                time_slot=time_slot+1;
             end
        end
        
        %%now collect the remaining circulants in phase1 and form
        %%circulants for phase 2. repeat the similar procedure.
                                                         
        for i=1:M_b
             phase2_circulants_to_be_processed=setdiff(1:Decoder_Parameters.row_degree(i),Decoder_Parameters.processing_order2(i).circulant_num);
             num_phase2_circulants(i)=length(phase2_circulants_to_be_processed); 
             phase2_process_flags=ismember(1:Decoder_Parameters.row_degree(i),Decoder_Parameters.processing_order2(i).circulant_num);
             phase2_duration(i)=ceil(num_phase2_circulants(i)/2);%%expected duration. could be more if there are stall cycles
             %%Try to prepare a preferred order for optimization so that
             %%all the banks are used to minimize the conflicts
             for ind=1:num_circulant_banks
              cb(ind).accesses=phase2_circulants_to_be_processed(find(Decoder_Parameters.circulant_bank(i,phase2_circulants_to_be_processed)==ind));
              num_cb_accesses(ind)=length(cb(ind).accesses);
              cb_access_cnt(ind)=0;
             end
             clear preferred_block_order_for_opti
             ind=0;
             for block_ind_tmp=1:num_phase2_circulants(i)
                 ind_tmp1=mod(ind+1,num_circulant_banks);
                 if(ind_tmp1)==0
                    ind_tmp1=num_circulant_banks;
                 end
                 ind_tmp2=mod(ind_tmp1+1,num_circulant_banks);
                 if(ind_tmp2)==0
                    ind_tmp2=num_circulant_banks;
                 end
                 ind_tmp3=mod(ind_tmp2+1,num_circulant_banks);
                 if(ind_tmp3)==0
                    ind_tmp3=num_circulant_banks;
                 end

                 if(cb_access_cnt(ind_tmp1)<num_cb_accesses(ind_tmp1))
                 cb_access_cnt(ind_tmp1)=cb_access_cnt(ind_tmp1)+1;
                 preferred_block_order_for_opti(block_ind_tmp)=cb(ind_tmp1).accesses(cb_access_cnt(ind_tmp1));
                 ind=ind_tmp1;
                 elseif(cb_access_cnt(ind_tmp2)<num_cb_accesses(ind_tmp2))
                 cb_access_cnt(ind_tmp2)=cb_access_cnt(ind_tmp2)+1;
                 preferred_block_order_for_opti(block_ind_tmp)=cb(ind_tmp2).accesses(cb_access_cnt(ind_tmp2));
                 ind=ind_tmp2;
                 elseif(cb_access_cnt(ind_tmp3)<num_cb_accesses(ind_tmp3))
                 cb_access_cnt(ind_tmp3)=cb_access_cnt(ind_tmp3)+1;
                 preferred_block_order_for_opti(block_ind_tmp)=cb(ind_tmp3).accesses(cb_access_cnt(ind_tmp3));
                 ind=ind_tmp3;
                 end
             end
             %%try to reverse the order such that unpaired accessed bank is
             %%in the order first-so that we can get a pair easily in the later step of optimization.
             ff=sort(cb_access_cnt-min(cb_access_cnt),'descend');
             if(ff(1)-ff(2)>1) 
              preferred_block_order_for_opti=preferred_block_order_for_opti(end:-1:1);
             end
             time_slot=phase1_duration(i)+1;
             while(any(phase2_process_flags==0))
                 circulant_bank_access1(i,time_slot)=-2;
                 circulant_bank_access2(i,time_slot)=-2;
                 Decoder_Parameters.processing_order2(i).circulant_num(2*time_slot-1)=-2;
                 Decoder_Parameters.processing_order2(i).circulant_num(2*time_slot)=-2;
                %%first choose 1 circulant to be processed in the given time_slot.
                 for block_num=preferred_block_order_for_opti
                     if(phase2_process_flags(block_num)==0)
                      circulant_bank_access1(i,time_slot)=Decoder_Parameters.circulant_bank(i,block_num);
                      Decoder_Parameters.processing_order2(i).circulant_num(2*time_slot-1)=block_num;
                      phase2_process_flags(block_num) =1;
                      break;
                     end
                 end
                 %%now choose 2nd circulant to be processed in the given time_slot.
                 for block_num=preferred_block_order_for_opti
                     if((phase2_process_flags(block_num)==0)&(Decoder_Parameters.circulant_bank(i,block_num)~=circulant_bank_access1(i,time_slot)))
                      circulant_bank_access2(i,time_slot)=Decoder_Parameters.circulant_bank(i,block_num);
                      Decoder_Parameters.processing_order2(i).circulant_num(2*time_slot)=block_num;
                      phase2_process_flags(block_num) =1;
                      break;
                     end
                 end
                 time_slot=time_slot+1;
             end
               Decoder_Parameters.clock_cycles_or_time_slots_layer_CCL(i)=time_slot-1;
        end
         Decoder_Parameters.clock_cycles_or_time_slots_per_iteration_CCI=sum(Decoder_Parameters.clock_cycles_or_time_slots_layer_CCL);
         
          %%still need to re-order correctly based on the new processing
          %%order with -2 for stall cycles. -1 is donot care and need not
          %%process or stall. we want to create a matrix for easy view.
          %%so all the rows in matrix are filled with valid entries or -2
          %%or -1. At a later step, when we are generating the parameters for ROM, we will
          %%ignore -1 completely.
         D=Decoder_Parameters;
        for i=1:M_b
           for jj=1:number_of_circulants_per_clk*max(D.clock_cycles_or_time_slots_layer_CCL)
               if(jj<=number_of_circulants_per_clk*D.clock_cycles_or_time_slots_layer_CCL(i))
                   if(D.processing_order2(i).circulant_num(jj)>=0)
                       Decoder_Parameters.circulant_column(i,jj)               =  D.circulant_column(i,D.processing_order2(i).circulant_num(jj));
                       Decoder_Parameters.dependent_layer(i,jj)                =  D.dependent_layer(i,D.processing_order2(i).circulant_num(jj));
                       Decoder_Parameters.circulant_bank(i,jj)                 =  D.circulant_bank(i,D.processing_order2(i).circulant_num(jj));
                       Decoder_Parameters.dependent_circulant(i,jj)            =  D.dependent_circulant(i,D.processing_order2(i).circulant_num(jj));
                       Decoder_Parameters.circulant_number(i,jj)               =  D.circulant_number(i,D.processing_order2(i).circulant_num(jj));
                       Decoder_Parameters.ucvf(i,jj)                           =  D.ucvf(i,D.processing_order2(i).circulant_num(jj));
                       Decoder_Parameters.shift_value_for_the_first_time(i,jj) =  D.shift_value_for_the_first_time(i,D.processing_order2(i).circulant_num(jj));
                       Decoder_Parameters.differential_shift(i,jj)             =  D.differential_shift(i,D.processing_order2(i).circulant_num(jj));
                       Decoder_Parameters.shift(i,jj)                          =  D.shift(i,D.processing_order2(i).circulant_num(jj));
                       Decoder_Parameters.dependent_circulant_number(i,jj)     =  D.dependent_circulant_number(i,D.processing_order2(i).circulant_num(jj));                                                                       
                   else
                       Decoder_Parameters.circulant_column(i,jj)               = -2;
                       Decoder_Parameters.dependent_circulant(i,jj)            = -2; 
                       Decoder_Parameters.dependent_layer(i,jj)                = -2;
                       Decoder_Parameters.circulant_bank(i,jj)                 = -2;
                       Decoder_Parameters.circulant_number(i,jj)               = -2; 
                       Decoder_Parameters.ucvf(i,jj)                           = -2; 
                       Decoder_Parameters.shift_value_for_the_first_time(i,jj) = -2; 
                       Decoder_Parameters.differential_shift(i,jj)             = -2; 
                       Decoder_Parameters.shift(i,jj)                          = -2; 
                       Decoder_Parameters.dependent_circulant_number(i,jj)     = -2;
                   end
               else
                       Decoder_Parameters.circulant_column(i,jj)               = -1;
                       Decoder_Parameters.dependent_circulant(i,jj)            = -1; 
                       Decoder_Parameters.dependent_layer(i,jj)                = -1;
                       Decoder_Parameters.circulant_bank(i,jj)                 = -1;
                       Decoder_Parameters.circulant_number(i,jj)               = -1; 
                       Decoder_Parameters.ucvf(i,jj)                           = -1; 
                       Decoder_Parameters.shift_value_for_the_first_time(i,jj) = -1; 
                       Decoder_Parameters.differential_shift(i,jj)             = -1; 
                       Decoder_Parameters.shift(i,jj)                          = -1; 
                       Decoder_Parameters.dependent_circulant_number(i,jj)     = -1;
               end
           end
        end


        %%processing order3:also such that most of the fs memory accesses
        %%(dependent layer belong to the same layer. to optimize the mux
        %%accesss and to reduce the number of fs memory ports. For this, treat this as
        %%soft constraint. no stall cycles are neeed even if the constraint is
        %%not met as we can easily handle part of this in hardware. So we
        %%do not this at this time.
        

    end % end of if(out_of_order_processing_flag)
%%

% Decoder_Parameters.S                   = Decoder_Parameters.shift;
% %% the following indices start from 0 in hardware. 
% Decoder_Parameters.circulant_column    = Decoder_Parameters.circulant_column-1;
% Decoder_Parameters.dependent_circulant = Decoder_Parameters.dependent_circulant-1;
% Decoder_Parameters.dependent_layer     = Decoder_Parameters.dependent_layer-1;
% Decoder_Parameters.circulant_number    = Decoder_Parameters.circulant_number-1;
% Decoder_Parameters.row_degree          = Decoder_Parameters.row_degree; 

%end
ClockCylesPerIteration=Decoder_Parameters.clock_cycles_or_time_slots_per_iteration_CCI;
for i=1:Decoder_Parameters.M_b
if i-1==0,
comp_l= Decoder_Parameters.M_b;
else
comp_l=i-1;
end
[dummy col_indx]=find(Decoder_Parameters.Decoder_Parameters_before_oop_for_pipeline.dependent_layer(i,:)==comp_l);
if(isempty(min(col_indx)))
nic(i)=Decoder_Parameters.Decoder_Parameters_before_oop_for_pipeline.row_degree(i);
else
nic(i)=min(col_indx);
end
niclk(i)=floor(nic(i)/2);
overhead_pipeline_per_layer(i)=max(0,pipeline_depth-niclk(i));
end
overhead_pipeline=sum(overhead_pipeline_per_layer);
ClockCylesPerIteration_with_overhead=ClockCylesPerIteration+overhead_pipeline;

mm=mod(Decoder_Parameters.Decoder_Parameters_before_oop_for_pipeline.circulant_column,3);
mm_even=mm(:,1:2:end);
mm_odd=mm(:,2:2:end);
[dummy mm_col]=size(mm);
if(mod(mm_col,2)==1)
    mm_odd(:,end+1)=0;
end

%mm_odd(:,end+1)=0
overhead_mem_conflicts=sum(sum(mm_even==mm_odd));
ClockCylesPerIteration_with_overhead=ClockCylesPerIteration+sum(overhead_pipeline)+overhead_mem_conflicts;
Savings_due_to_oop=((ClockCylesPerIteration_with_overhead-ClockCylesPerIteration)/ClockCylesPerIteration_with_overhead)*100;
Decoder_Parameters.ClockCylesPerIteration_with_no_OOP=ClockCylesPerIteration_with_overhead;
Decoder_Parameters.overhead_due_to_no_oop_for_pipeline=overhead_pipeline;
Decoder_Parameters.overhead_due_to_no_oop_for_mem_conflicts=overhead_mem_conflicts;
Decoder_Parameters.overhead_due_to_no_oop=overhead_pipeline+overhead_mem_conflicts;
Decoder_Parameters.Savings_due_to_oop= Savings_due_to_oop;





