%% Decoder_Schedule_for_given_layer_schedule_802_15.m
%% Kiran Gunnam

%% Computes the parameters for the layered decoder hardware architecture
%% for a given layer schedule(i.e. re-ordered H matrix). Does the step 1 which
%% is described below:

%% The decoder hardware architecture is proposed to support out-of-order
%% processing to remove pipeline and memory accesses or to satisfy any other performance or
%% hardware constraint. Remaining  hardware architectures won't support out-of-order
%% processing without further involving more logic and memory.
%% For the above hardware decoder architecture, the optimization of decoder 
%% schedule belongs to the class of NP-complete problems. So there are several clasic optimization 
%% algorithms such as dynamic programming that can be applied. We apply the
%% following classic approach of optimal substructure.

%% Step 1: We will try different layer schedules(M_b! if there are M_b layers). For simplicity,
%% we wil try only a subset of possible sequences. Since we can have step value ranging from 0 
%% to  M_b-2(or 1 to Mb-1 in matlab index), we will have M_b-1 unique layer schedules or layer
%% re-orderings. 

%% Step 2:Given a layer schedule or a re-ordered H matrix, we will optimize the processing 
%% schedule of each layer. For this, we use the clasic approach of optimal substructure 
%% i.e. the solution to a given optimization problem can be obtained by the combination 
%% of optimal solutions to its subproblems.  So first we optimize the processing order to mimimize the
%% pipeline conflicts. Then we optimize the resulting processing order to
%% minimize the memory conflicts. So for each layer schedule, we are measuring the
%% number of stall cycles(our cost function).

%% Step 3: We choose a layer schedule which minimizes the cost function i.e. meets the requirements with 
%% less stall cycles due to pipeline conflicts and memory conflicts and also minimizes the memory accesses
%% (such as FS memory accesses to minimize the number of ports needed and to save the access power and 
%% to minimize the more muxing requirement.

%usage:
%clc;clear all;cd H_matrices\; H_802_15; cd ../;H=H_1_2;step_size=2;forced_reordering=[];[H_reordered reordering]=Decoder_Layer_Schedule(H,step_size,'modulo',forced_reordering)
function [H_reordered reordering]=Decoder_Layer_Schedule_802_15(H,step_size,mode,forced_reordering)

[M_b N_b]=size(H);

if(strcmp(mode,'modulo'))
  mapped_index=1;
  reordering(1)=1;
  for i=2:M_b
     mapped_index    = mod(mapped_index-1+step_size,M_b)+1;
     reordering(i)   = mapped_index;
  end
end

if(strcmp(mode,'random'))
    reordering=randperm(M_b);
end

if(strcmp(mode,'forced'))
    reordering=forced_reordering;
end

 
if(length(unique(reordering))~=M_b)
   reordering=[1:M_b];
end

 H_reordered=H(reordering,:);
    








