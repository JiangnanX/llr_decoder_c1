%% Decoder_Scheduler_Top.m
%% Top level script for 802.15 Codes. calls different optimization
%% functions.
%% Decoder Schedule Parameter for Layered Decoder Architecture based on the
%% on-the-fly computation and out-of-order processing.
%% Note that all these scheduling paramters are specific to the given
%% hardware architecture.
%% Kiran Gunnam

%% Computes the parameters for the layered decoder hardware architecture
%% for a given layer schedule(i.e. re-ordered H matrix). Does the steps 1-3 which
%% are described below:

%% The decoder hardware architecture is proposed to support out-of-order
%% processing to remove pipeline and memory accesses or to satisfy any other performance or
%% hardware constraint. Remaining  hardware architectures won't support out-of-order
%% processing without further involving more logic and memory.
%% For the above hardware decoder architecture, the optimization of decoder 
%% schedule belongs to the class of NP-complete problems. So there are several clasic optimization 
%% algorithms such as dynamic programming that can be applied. We apply the
%% following classic approach of optimal substructure.

%% Step 1: We will try different layer schedules(M_b! if there are M_b layers). For simplicity,
%% we wil try only a subset of possible sequences. Since we can have step value ranging from 0 
%% to  M_b-2(or 1 to Mb-1 in matlab index), we will have M_b-1 unique layer schedules or layer
%% re-orderings. 

%% Step 2:Given a layer schedule or a re-ordered H matrix, we will optimize the processing 
%% schedule of each layer. For this, we use the clasic approach of optimal substructure 
%% i.e. the solution to a given optimization problem can be obtained by the combination 
%% of optimal solutions to its subproblems.  So first we optimize the processing order to mimimize the
%% pipeline conflicts. Then we optimize the resulting processing order to
%% minimize the memory conflicts. So for each layer schedule, we are measuring the
%% number of stall cycles(our cost function).

%% Step 3: We choose a layer schedule which minimizes the cost function i.e. meets the requirements with 
%% less stall cycles due to pipeline conflicts and memory conflicts and also minimizes the memory accesses
%% (such as FS memory accesses to minimize the number of ports needed and to save the access power and 
%% to minimize the more muxing requirement.
clc
% clear all
close all
load ..\H_matrices\LDPCCodeData_Rate_0.83_(12,72)_Code_CirculantSize_140_C1_H_masked.mat


H = H_Shift;       % H=H_Shift
circulant_size=140;
pipeline_depth=8;
mem_org=0;
layer_reordering_mode='modulo'; %forced
[a b]= size(H);
forced_reordering = [1:b];
required_efficiency=100;
[DP.H1.Decoder_Parameters ClockCylesPerIteration]=Decoder_Scheduler_802_11(H,circulant_size,pipeline_depth,mem_org,...
                                                                        layer_reordering_mode,forced_reordering,required_efficiency);
save DP_Flash DP



DP.Architecture_Efficiency=[DP.H1.Decoder_Parameters.Architecture_Efficiency]

DP.Architecture_Efficiency_without_oop=[DP.H1.Decoder_Parameters.Architecture_Efficiency_without_oop]

DP.H1.Decoder_Parameters.reordering