%% Decoder_Schduler.m
%% Decoder Schedule Parameter for Layered Decoder Architecture based on the
%% on-the-fly computation and out-of-order processing.
%% Note that all these scheduling paramters are specific to the given
%% hardware architecture.
%% Kiran Gunnam

%% Computes the parameters for the layered decoder hardware architecture
%% for a given layer schedule(i.e. re-ordered H matrix). Does the steps 1-3 which
%% are described below:

%% The decoder hardware architecture was proposed to support out-of-order
%% processing to remove pipeline and memory accesses or to satisfy any other performance or
%% hardware constraint. Remaining  hardware architectures won't support out-of-order
%% processing without further involving more logic and memory.
%% For the above hardware decoder architecture, the optimization of decoder 
%% schedule belongs to the class of NP-complete problems. So there are several clasic optimization 
%% algorithms such as dynamic programming that can be applied. We apply the
%% following classic approach of optimal substructure.

%% Step 1: We will try different layer schedules(M_b! if there are M_b layers). For simplicity,
%% we wil try only a subset of possible sequences. Since we can have step value ranging from 0 
%% to  M_b-2(or 1 to Mb-1 in matlab index), we will have M_b-1 unique layer schedules or layer
%% re-orderings. 

%% Step 2:Given a layer schedule or a re-ordered H matrix, we will optimize the processing 
%% schedule of each layer. For this, we use the clasic approach of optimal substructure 
%% i.e. the solution to a given optimization problem can be obtained by the combination 
%% of optimal solutions to its subproblems.  So first we optimize the processing order to mimimize the
%% pipeline conflicts. Then we optimize the resulting processing order to
%% minimize the memory conflicts. So for each layer schedule, we are measuring the
%% number of stall cycles(our cost function).

%% Step 3: We choose a layer schedule which minimizes the cost function i.e. meets the requirements with 
%% less stall cycles due to pipeline conflicts and memory conflicts and also minimizes the memory accesses
%% (such as FS memory accesses to minimize the number of ports needed and to save the access power and 
%% to minimize the more muxing requirement.

%usage:
%load MC;H=H_1_2;circulant_size=18;pipeline_depth=4;mem_org=1;required_efficiency=99;[Decoder_Parameters ClockCylesPerIteration]=Decoder_Scheduler(H,circulant_size,pipeline_depth,mem_org,required_efficiency)

function [Decoder_Parameters ClockCylesPerIteration]=Decoder_Scheduler_802_15(H,circulant_size,pipeline_depth,mem_org,...
                                                                       layer_reordering_mode,forced_reordering,required_efficiency)

[M_b N_b]=size(H);
number_of_circulants_per_clk=1;%%hardcoded for other functions.so it is not really a parameter.
Decoder_Parameters.H_original=H;
for i=1:M_b-1
    step_size(i)=i;
    [Choice(i).H_reordered Choice(i).reordering]  = Decoder_Layer_Schedule_802_15(H,step_size(i),layer_reordering_mode,forced_reordering);
    [Choice(i).Decoder_Parameters]                = Decoder_Schedule_for_given_layer_schedule_C1_802_15(Choice(i).H_reordered,circulant_size,pipeline_depth,mem_org);
    CCI(i)                                        = Choice(i).Decoder_Parameters.clock_cycles_or_time_slots_per_iteration_CCI;
    %[min(CCI) max(CCI)];
    if(layer_reordering_mode=='forced')
        break;
    end
end

[ClockCylesPerIteration Selection] = min(CCI);
Decoder_Parameters                 = Choice(Selection).Decoder_Parameters;
Decoder_Parameters.reordering      = Choice(Selection).reordering;
Decoder_Parameters.reordering_mode = layer_reordering_mode;
Decoder_Parameters.modulo_step     = step_size(Selection);
Decoder_Parameters.mem_org         = mem_org;
Decoder_Parameters.H               = Choice(Selection).H_reordered;

Decoder_Parameters.clock_cycles_or_time_slots_per_iteration_CCI_IDEAL=sum(ceil(Decoder_Parameters.row_degree/number_of_circulants_per_clk));
Decoder_Parameters.Architecture_Efficiency= ((Decoder_Parameters.clock_cycles_or_time_slots_per_iteration_CCI_IDEAL)...
                                             /Decoder_Parameters.clock_cycles_or_time_slots_per_iteration_CCI)*100;
                                         
Decoder_Parameters.Architecture_Efficiency_without_oop= ((Decoder_Parameters.clock_cycles_or_time_slots_per_iteration_CCI_IDEAL)...
                                                 /Decoder_Parameters.ClockCylesPerIteration_with_no_OOP)*100;
Decoder_Parameters.overhead_if_no_oop= ((Decoder_Parameters.ClockCylesPerIteration_with_no_OOP)...
                                                 /Decoder_Parameters.clock_cycles_or_time_slots_per_iteration_CCI)*100;
                                         

                                         
achieved_efficiency=Decoder_Parameters.Architecture_Efficiency;
if(achieved_efficiency<required_efficiency)
 Number_of_random_trials=10000; %%could go upto M_b!
for i=1:Number_of_random_trials
    %pack
    [Choice2(i).H_reordered Choice2(i).reordering]  = Decoder_Layer_Schedule_802_15(H,0,'random');
    [Choice2(i).Decoder_Parameters]                = Decoder_Schedule_for_given_layer_schedule_C1_802_15(Choice2(i).H_reordered,circulant_size,pipeline_depth,mem_org);
    CCI2(i)                                        = Choice2(i).Decoder_Parameters.clock_cycles_or_time_slots_per_iteration_CCI;
    achieved_efficiency= ((Decoder_Parameters.clock_cycles_or_time_slots_per_iteration_CCI_IDEAL)...
                                             /Choice2(i).Decoder_Parameters.clock_cycles_or_time_slots_per_iteration_CCI)*100;
    %[min([CCI CCI2]) max([CCI CCI2])]
    if(achieved_efficiency>=required_efficiency)
        Choice2(i).Decoder_Parameters.Number_of_random_trials=i;
       break;
    end
end

[ClockCylesPerIteration_2 Selection2] = min(CCI2);
 if(ClockCylesPerIteration_2<ClockCylesPerIteration)
    Decoder_Parameters                 = Choice2(Selection2).Decoder_Parameters;
    Decoder_Parameters.reordering      = Choice2(Selection2).reordering;
    Decoder_Parameters.H               = Choice2(Selection).H_reordered;
    Decoder_Parameters.reordering_mode = 'random';
    Decoder_Parameters.modulo_step     = 0;

    Decoder_Parameters.clock_cycles_or_time_slots_per_iteration_CCI_IDEAL=sum(ceil(Decoder_Parameters.row_degree/number_of_circulants_per_clk));
    Decoder_Parameters.Architecture_Efficiency= ((Decoder_Parameters.clock_cycles_or_time_slots_per_iteration_CCI_IDEAL)...
                                                 /Decoder_Parameters.clock_cycles_or_time_slots_per_iteration_CCI)*100;
    Decoder_Parameters.Architecture_Efficiency_without_oop= ((Decoder_Parameters.clock_cycles_or_time_slots_per_iteration_CCI_IDEAL)...
                                                 /Decoder_Parameters.ClockCylesPerIteration_with_no_OOP)*100;
    Decoder_Parameters.overhead_if_no_oop= ((Decoder_Parameters.ClockCylesPerIteration_with_no_OOP)...
                                                 /Decoder_Parameters.clock_cycles_or_time_slots_per_iteration_CCI)*100;
                                             
 end

end

    Architecture_Efficiency=Decoder_Parameters.Architecture_Efficiency
    Savings_due_to_oop=Decoder_Parameters.Savings_due_to_oop
    Architecture_Efficiency_without_oop=Decoder_Parameters.Architecture_Efficiency_without_oop
    overhead_if_no_oop=Decoder_Parameters.overhead_if_no_oop


