clear; clc

load uy_data.mat
load monitor.txt;
load Std_noise0&1_V_ref.mat

awgn_rtl_q3f6       = monitor(:,1);
noise_0_rtl_q9f22   = monitor(:,2);
noise_1_rtl_q9f22   = monitor(:,3);
Y_rtl_q9f22         = monitor(:,4);
LLR_rtl             = monitor(:,5);

awgn_rtl            = awgn_rtl_q3f6 / 2^6;
awgn_matlab         = awgn_rtl;

awgn_matlab_q4f11   = awgn_matlab * 2^11;
std0_matlab_q4f11   = std_0QI;
std1_matlab_q4f11   = std_1QI;

noise_0_matlab_q9f22 = awgn_matlab_q4f11 * std0_matlab_q4f11;
noise_1_matlab_q9f22 = awgn_matlab_q4f11 * std1_matlab_q4f11;

Y_bpsk          = Y;
Y_bpsk_q9f22    = Y_bpsk*2^22;

if(1-2*Y_bpsk > 0)
    noise_matlab_q9f22 = noise_1_matlab_q9f22;
else
    noise_matlab_q9f22 = noise_0_matlab_q9f22;
end

Y_matlab_q9f22  = Y_bpsk_q9f22 + noise_matlab_q9f22; %%q10f22=q9f22+q9f22
V_ref_q9f22     = V_read_ref_QI;

cond_3	= Y_matlab_q9f22 > V_ref_q9f22(7);
cond_2	= Y_matlab_q9f22 > V_ref_q9f22(6);
cond_1	= Y_matlab_q9f22 > V_ref_q9f22(5);
cond_0	= Y_matlab_q9f22 > V_ref_q9f22(4);
cond_1p	= Y_matlab_q9f22 > V_ref_q9f22(3);
cond_2p	= Y_matlab_q9f22 > V_ref_q9f22(2);
cond_3p	= Y_matlab_q9f22 > V_ref_q9f22(1);

LLR_matlab = zeros(length(Y),1);

for i = 1 : length(Y)
    if cond_3(i)
        LLR_matlab(i) = 13;
    elseif cond_2(i)
        LLR_matlab(i) = 9;
    elseif cond_1(i)
        LLR_matlab(i) = 5;
    elseif cond_0(i)
        LLR_matlab(i) = 2;
    elseif cond_1p(i)
        LLR_matlab(i) = -2;
    elseif cond_2p(i)
        LLR_matlab(i) = -5;
    elseif cond_3p(i)
        LLR_matlab(i) = -9;
    else
        LLR_matlab(i) = -13;
    end
end

isequal(LLR_matlab, LLR_rtl)

save ('LLR_matlab.mat', 'LLR_matlab')

% % cond_onehot={dff_3[32],dff_2[32],dff_1[32],dff_0[32],
% %             dff_1p[32],dff_2p[32],dff_3p[32]};
% % casex (cond_onehot)
% %  7'b1xxxxxx : LLR = 5'b01101 ;
% %  7'b01xxxxx : LLR = 5'b01001 ;
% %  7'b001xxxx : LLR = 5'b00101 ;
% %  7'b0001xxx : LLR = 5'b00010 ;
% %  7'b00001xx : LLR = 5'b11110 ;
% %  7'b000001x : LLR = 5'b11011 ;
% %  7'b0000001 : LLR = 5'b10111 ;
% %  7'b0000000 : LLR = 5'b10011 ;
% %  default    : LLR = 5'b00000 ;
% % endcase
%%According to the Verilog-2001 spec, section 9.5:
%%The case item expressions shall be evaluated and compared in
%%the exact order in which they are given. During the linear search, if one of
%%the case item expressions matches the case expression given in parentheses,
%%then the statement associated with that case item shall be executed.
%%equivalent coding style:
% LLR = (dff_3[31] == 1'b0)?
% 		5'b01101 :
%        (dff_2[31] == 1'b0)?
%        5'b01001 :
%        (dff_1[31] == 1'b0)?
%        5'b00101 :
%        (dff_0[31] == 1'b0)?
% 	   5'b00010 :
%        (dff_1p[31] == 1'b0)?
% 	   5'b11110 :
%        (dff_2p[31] == 1'b0) ?
% 	   5'b11011 :
%       (dff_3p[31] == 1'b0) ?
% 	   5'b10111 :
%        5'b10011;

