function [YN]  = IMPAIRMENTS_LDPC(Y,SNR_DB,RATE);
%% ADD AWGN.

%SNR_DB   = SNR_DB+10*log10(RATE);
%sigma    = sqrt(10^(-SNR_DB/10)/2);
%YN       = Y+ sigma*randn(length(Y),1);       % Gaussian noise added

 
 x=10^(SNR_DB/10);
 var=1.0/(2*x*RATE);
 svar=sqrt(var);
 YN=-Y+svar*randn(length(Y),1);
 YN = -2*YN/var;