clear all
close all
clc
load ../rtl_dbg_data/list_exp.lst
TS=list_exp(:,2);
iterations=list_exp(:,3);
layer=list_exp(:,4);
BC=list_exp(:,5);
CI=list_exp(:,6);
DB=list_exp(:,7);
DL=list_exp(:,8);
GAMMA=list_exp(:,9);
UCVF=list_exp(:,10);
SHIFT=list_exp(:,11);
SHIFT_UCVF1=list_exp(:,12);
DELTA_SHIFT=list_exp(:,13);
P_SHIFT=list_exp(4:end,14);
dc=list_exp(:,15);
qra=list_exp(:,16);
Qs_old_5=list_exp(:,17);
P_5=list_exp(:,18);
R_new_5=list_exp(:,19);
block_num=list_exp(:,20);
srnz=list_exp(:,21);
Qs_5=list_exp(:,22);
Ps_5=list_exp(:,23);
R_old_5=list_exp(:,24);

load ../Matlab_RTL_CodeGen/MC.mat
iteration_at_cp=1;
for ll=1:MC(1).C.H_row
    if(ll==1)
        ts_start=1;
        ts_end=MC(1).C.ts_max(1);
    else
        ts_start=sum(MC(1).C.ts_max(1:ll-1))+1;
        ts_end=sum(MC(1).C.ts_max(1:ll));
    end
    num_stall_cycles=MC(1).C.ts_max(ll)-MC(1).C.dc(ll);
    last_val=MC(1).C.blockcol_LUT(ll,MC(1).C.dc(ll))-1;

    val_rtl=BC(ts_start:ts_end)';
    val_filler=ones(1,num_stall_cycles)*last_val;
    val_matlab= [MC(1).C.blockcol_LUT(ll,1:MC(1).C.dc(ll))-1 val_filler];
    bc_matlab=val_matlab;
    if(any(val_matlab-val_rtl))
        disp('mismatch in rtl vs matlab for BC');
        ll
    end
  
    last_val=MC(1).C.circulant_index_LUT(ll,MC(1).C.dc(ll))-1;
    val_rtl=CI(ts_start:ts_end)';
    val_filler=ones(1,num_stall_cycles)*last_val;
    val_matlab= [MC(1).C.circulant_index_LUT(ll,1:MC(1).C.dc(ll))-1 val_filler];
    if(any(val_matlab-val_rtl))
        disp('mismatch in rtl vs matlab for CI');
        ll
    end
    
    last_val=MC(1).C.dependendent_block_LUT(ll,MC(1).C.dc(ll))-1;
    val_rtl=DB(ts_start:ts_end)';
    val_filler=ones(1,num_stall_cycles)*last_val;
    val_matlab= [MC(1).C.dependendent_block_LUT(ll,1:MC(1).C.dc(ll))-1 val_filler];
    if(any(val_matlab-val_rtl))
        disp('mismatch in rtl vs matlab for DB');
        ll
    end
    
    
    last_val=MC(1).C.dependendent_layer_LUT(ll,MC(1).C.dc(ll))-1;
    val_rtl=DL(ts_start:ts_end)';
    val_filler=ones(1,num_stall_cycles)*last_val;
    val_matlab= [MC(1).C.dependendent_layer_LUT(ll,1:MC(1).C.dc(ll))-1 val_filler];
    if(any(val_matlab-val_rtl))
        disp('mismatch in rtl vs matlab for DL');
        ll
    end
    
    
    last_val=MC(1).C.UseChannelValue_Flag(ll,MC(1).C.dc(ll)) & (iteration_at_cp==1);
    val_rtl=UCVF(ts_start:ts_end)';
    val_filler=(ones(1,num_stall_cycles)*last_val*(iteration_at_cp==1));
    val_matlab= [MC(1).C.UseChannelValue_Flag(ll,1:MC(1).C.dc(ll))*(iteration_at_cp==1) val_filler];
    if(any(val_matlab-val_rtl))
        disp('mismatch in rtl vs matlab for UCVF');
        ll
    end
    
    
    val_rtl=GAMMA(ts_start:ts_end)';
    val_matlab=MC(1).GAMMA(MC(1).C.dv(bc_matlab+1)+1);
    if(any(val_matlab-val_rtl))
        disp('mismatch in rtl vs matlab for GAMMA');
        ll
    end

    last_val=MC(1).C.shift(ll,MC(1).C.dc(ll));
    val_rtl=SHIFT(ts_start:ts_end)';
    val_filler=ones(1,num_stall_cycles)*last_val;
    val_matlab= [MC(1).C.shift(ll,1:MC(1).C.dc(ll)) val_filler];
    if(any(val_matlab-val_rtl))
        disp('mismatch in rtl vs matlab for SHIFT');
        ll
    end
    
    last_val=MC(1).C.delta_shift(ll,MC(1).C.dc(ll));
    val_rtl=DELTA_SHIFT(ts_start:ts_end)';
    val_filler=ones(1,num_stall_cycles)*last_val;
    val_matlab= [MC(1).C.delta_shift(ll,1:MC(1).C.dc(ll)) val_filler];
    if(any(val_matlab-val_rtl))
        disp('mismatch in rtl vs matlab for DELTA_SHIFT');
        ll
    end
    
    last_val=MC(1).C.SHIFT_UCVF1(ll,MC(1).C.dc(ll));
    val_rtl=SHIFT_UCVF1(ts_start:ts_end)';
    val_filler=ones(1,num_stall_cycles)*last_val;
    val_matlab= [MC(1).C.SHIFT_UCVF1(ll,1:MC(1).C.dc(ll)) val_filler];
    if(any(val_matlab-val_rtl))
        disp('mismatch in rtl vs matlab for SHIFT_UCVF1');
        ll
    end
    
    if(iteration_at_cp==1 & MC(1).C.UseChannelValue_Flag(ll,MC(1).C.dc(ll)))
      last_val=MC(1).C.SHIFT_UCVF1(ll,MC(1).C.dc(ll));
    else
      last_val=MC(1).C.delta_shift(ll,MC(1).C.dc(ll));
    end
    val_rtl=P_SHIFT(ts_start:ts_end)';
    val_filler=ones(1,num_stall_cycles)*last_val;
    P_SHIFT_TMP=[];
    for kk=1:MC(1).C.dc(ll)
       if(iteration_at_cp==1 & MC(1).C.UseChannelValue_Flag(ll,kk))
           P_SHIFT_TMP(kk)=MC(1).C.SHIFT_UCVF1(ll,kk);
       else
           P_SHIFT_TMP(kk)=MC(1).C.delta_shift(ll,kk);
       end
    end   
    val_matlab= [P_SHIFT_TMP val_filler];
    if(any(val_matlab-val_rtl))
        disp('mismatch in rtl vs matlab for P_SHIFT');
        ll
    end
   
    
end

