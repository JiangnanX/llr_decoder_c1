% Kiran Gunnam
% This module does the cH' computation using delta circulant. Can converge at the
% circulant level after a minimum of 1 iteration or minimum update time
%%it is possible to generate another flag in the scheduler which tells when all the blocks with UCVF=1 are complete.
% This function in  general serves as matrix-vector 
% computation unit for the QC-LDPC codes(regular and irregular)
% however needs max(dv) shifters and max(dv) circulant size xor arrays
% Sub-block parallelism mode can be added later.

function [C_Mem_delta USC_per_layer USC]= cht_computation_delta(C,delta_d_circulant,block_col,C_Mem_delta);

for layer = 1:C.H_row
     if(C.H(layer,block_col)~=-1)
     delta_d_circulant_shifted  = circshift(delta_d_circulant,C.H(layer,block_col));
     C_Mem_delta(:,layer)       = bitxor(C_Mem_delta(:,layer),delta_d_circulant_shifted);
     end
end
USC_per_layer=sum(C_Mem_delta);
USC=sum(USC_per_layer);