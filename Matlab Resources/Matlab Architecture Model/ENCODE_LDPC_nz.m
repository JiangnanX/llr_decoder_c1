%% GENERATE CODE WORD by using simple inverse for now. optimized versio
%% using dual diagonal and backsubstituion -later.
%% ASSUME BPSK. 0 MAPS TO 1 AND 
%% 1 MAPS TO -1.
function [Y,U] = ENCODE_LDPC_nz(C);

u = gf(randint((C.H_col-C.H_row)*C.p,1,[0 1])); %GENERATE ALL ZERO CODE WORD for now.
[H_sparse]= expand_base_matrix(C.S,C.p);
%[H_sparse]= expand_base_matrix(C.H_original,C.p);
H_u=gf(H_sparse(:,1:(C.H_col-C.H_row)*C.p));
H_p=gf(H_sparse(:,(C.H_col-C.H_row)*C.p+1:end));
Hu_u=H_u*u;
p=H_p\Hu_u;
x=[u
   p];
Y = 1-2*(x==1);%pskmod(,2);  %BPSK
U=(x==1);
    
