% Kiran Gunnam
% Texas Engineering Experiment Station 
% Texas A&M University
function [FS Qsign_shift PS]   = CNU_PS_FS(FS_in,PS,Q_shift,C,TimeSlotCounter,block_num,dc,layer);
%CNU partial state processing

if (TimeSlotCounter <=dc )
    if(TimeSlotCounter == 1) % initialization of M1 and M2
        PS.M1         = abs(Q_shift);
        PS.M1_index   = ones(C.M,1)*block_num;
        PS.M2         = ones(C.M,1)*Inf;
        Qsign_shift   = 2*(Q_shift>=0)-1;
        PS.cum_sign   = Qsign_shift;
    else
        Qsign_shift         = 2*(Q_shift>=0)-1;
        PS.cum_sign         = PS.cum_sign.* Qsign_shift;
        %M1
        index_tmp              = find(abs(Q_shift)<PS.M1);
        PS.M1_index(index_tmp) = block_num;
        PS.M2(index_tmp)       = PS.M1(index_tmp);
        PS.M1(index_tmp)       = abs(Q_shift(index_tmp));

        %M2
        Q_shift(index_tmp)  = ones(size(index_tmp))*Inf;
        index_tmp           = find(abs(Q_shift)<PS.M2);
        PS.M2(index_tmp)    = abs(Q_shift(index_tmp));                    
    end
        
else
%idle
end

%Latch the Final state into FS storage
if (TimeSlotCounter == dc+1)
    FS.M1       = C.alpha(layer)* PS.M1; % max(M1-COffSet(layer),zeros(C.M,1));
    FS.M2       = C.alpha(layer)* PS.M2; %max(M2-COffSet(layer),zeros(C.M,1));
    FS.cum_sign = PS.cum_sign;
    FS.M1_index = PS.M1_index;
   % Qsign_shift = sign(Q_shift);% DONT CARES
   % PS          = PS; % DONT CARES 
else
    FS= FS_in;
end


%% DEBUG
if((TimeSlotCounter>dc+1) |(TimeSlotCounter<0))
    disp('error: TimeSlotCounter should be between 0 and check node degree of the block row +1');
end


