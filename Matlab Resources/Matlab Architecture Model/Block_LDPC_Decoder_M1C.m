% /---Confidentiality Notice Start--/
%   Texas A&M University Confidential.
%   Contact: Office of Technology Commercialization.
%   The material contained in this software and the associated papers
%   have features which are contained in a patent disclosure for LDPC decoding architectures 
%   filed by Gwan Choi, John Junkins , Kiran Gunnam. 
%   For more information about the licensing process, please contact:
%   Alex Garcia
%   Department: Licensing & Intellectual Property Management
%   Title: Licensing Associate
%   Email: alegarza@tamu.edu
%   Phone: (979) 458-2640
%   Fax: (979) 845-2684
% /---Confidentiality Notice End--/
% Kiran Gunnam
% Texas Engineering Experiment Station
% Texas A&M University
function [d,d_LDPC,iterations,iterations_struct]    = Block_LDPC_Decoder(YN,C,MAX_ITERATIONS,QuantStruct);

d = (YN<0);

Nb=C.H_col;
Mb=C.H_row;
it_max=MAX_ITERATIONS;
DC=C.dc;
QMAX=Inf;
converge=0;
num_col=0;


for n=1:Nb
    Qs(:,n)=YN((n-1)*C.Z0+1:n*C.Z0);
end

for i=1:it_max
    for l=1:Mb
        dc=DC(l);
        PS.M1=QMAX*ones(C.M,1);
        PS.M2=QMAX*ones(C.M,1);
        PS.cum_sign=zeros(C.M,1);
        PS.M1_index= zeros(C.M,1);
        for n=1:dc
            bn=C.blocknum_LUT(l,n);
            ci=C.circulant_index_LUT(l,n);
            bc=C.blockcol_LUT(l,n);
            dl=C.dependendent_layer_LUT(l,n);
            db=C.dependendent_block_LUT(l,n);
            dci=C.dependendent_circulant_index_LUT(l,n);
            sm= C.shift(l,n);
            dsm=C.delta_shift(l,n);
            ucvf=C.UseChannelValue_Flag(l,n);
            dv=C.dv_LUT(l,n);
            beta= C.beta_LUT_dv(dv);
            if(ucvf)
                shft=sm;
                num_col=num_col+1;
            else
                shft=dsm;
            end
            if((i==1 && ucvf==1)==0)
             Rnew=R_Selection_M1C(FS_mem(dl).FS,Qsign(:,dci),db);
            elseif((i==1 && ucvf==1)==1)
             Rnew=zeros(C.M,1);
            end
            Qold=Qs(:,bc);
            Pnew=Qold+Rnew;
            Pnew_shifted=circshift(Pnew,shft);
            Hd_shift2_currentlayer  =  (Pnew_shifted<0);
            Hd_Mem_shift1_layer_latched_signflipcheck(:,bc)=Hd_shift2_currentlayer;
            HD_shift(bc) = sm;
            if(sum(sum(Hd_Mem_shift1_layer_latched_signflipcheck))==0 && converge==0&& num_col>=C.H_col) %%simple check for zero codeword
                converge=1;
                bn_cc=n;
                layer_cc=l;
                iterations_cc=i;
                break;
            end
            if(i==1)
                Rold=zeros(C.M,1);
            else
                Rold=R_Selection_M1C(FS_mem(l).FS,Qsign(:,ci),bn);
            end
            Qnew=Pnew_shifted-Rold;
            Qs(:,bc)=Qnew;
            Qmag=abs(beta*Qs(:,bc));
            Qsign_tmp=Qnew<0;
            Qsign(:,ci)=Qsign_tmp;

            index_tmp              = find(Qmag<PS.M1);
            PS.M1_index(index_tmp) = bn;
            PS.M2(index_tmp)       = PS.M1(index_tmp);
            PS.M1                  = min(Qmag,PS.M1);
            
            Qmag(index_tmp)        = ones(size(index_tmp))*Inf;
            index_tmp              = find(Qmag<PS.M2);
            PS.M2(index_tmp)       = Qmag(index_tmp);    
            
            
            PS.cum_sign  = xor(PS.cum_sign,Qsign_tmp);
            
        end
            FS_mem(l).FS=PS;
            if(converge)
                break;
            end
    end
    if(converge)
      break;
    end
end

  
%%unshift the Hard decision data before sending out. %one bit cyclic
%%shifter is needed. note that in hardware we need to do this only when the
%%convergence check is satisfied

 for n=1:Nb
  Hd_Mem(:,n) = circshift(Hd_Mem_shift1_layer_latched_signflipcheck(:,n), -HD_shift(n));
 end

layer=Mb;
iterations=i;
iterations_struct.cht_layer_iterations=iterations;
iterations_struct.cht_layer_layer=layer;
if(converge)
iterations_struct.actual_iterations_needed= iterations_cc;
iterations_struct.actual_layer = layer_cc;
else
iterations_struct.actual_iterations_needed= iterations;
iterations_struct.actual_layer = layer;
end    
if(layer==Mb)
file_name= strcat('debug_data/debug_data_it',num2str(iterations));
save(file_name);
end

d_LDPC =  reshape(Hd_Mem,[],1);
file_name= strcat('debug_data/debug_data_it',num2str(iterations),'layer',num2str(layer));
save(file_name);

%i=1;dec2hex(bin2dec(num2str(d_LDPC(i*8:-1:(i-1)*8+1)')))
               
  

    
    

            
            