%% This Software was received from Texas A&M University and modified by
%  TexasLDPC to include the feature of LLR selection at the channel output
%  based on read reference LLRs. This software assume there are 7 read
%  reference LLRs that correspond to 7 read reference voltages.

%% ---Confidentiality Notice Start--
%
%  TexasLDPC Inc Confidential.
%  � Copyright 2014 to the present, TexasLDPC Inc.
%  All the software contained in these files are protected by United States
%  copyright law and may not be reproduced, distributed, transmitted,
%  displayed, published or broadcast without the prior written permission
%  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
%  or other notice from copies of the content.
%
%  ---Confidentiality Notice End--
%
%  Osso Vahabzadeh
%  TexasLDPC Inc
%  May 2016
%
%  This software aims to select the channel LLRs (YN) based on the BPSK
%  modulated signal 'Y', SNR per bit 'SNR_DB_0' observed by bit 0, SNR per
%  bit 'SNR_DB_1' observed by bit 1, rate 'RATE', and 7 read reference LLRs
%  'LLR_read_ref_ii'.
%%

function [YN]  = IMPAIRMENTS_LDPC_TL(Y, SNR_DB_0, SNR_DB_1, RATE, LLR_read_ref_ii)
%% ADD AWGN.

% SNR_DB   = SNR_DB+10*log10(RATE);
% sigma    = sqrt(10^(-SNR_DB/10)/2);
% YN       = Y+ sigma*randn(length(Y),1);       % Gaussian noise added

% load ../LLR_matlab.mat
% YN = LLR_matlab;

x_0             = 10^(SNR_DB_0/10);
var_0           = 1.0/(2*x_0*RATE);
svar_0          = sqrt(var_0);
x_1             = 10^(SNR_DB_1/10);
var_1           = 1.0/(2*x_1*RATE);
svar_1          = sqrt(var_1);
noise_normal    = randn(length(Y),1);
YN              = -2 * (( -Y.*(Y>0)+(Y>0).*svar_0.*noise_normal )/var_0 + ( -Y.*(Y<0)+(Y<0).*svar_1.*noise_normal )/var_1 );

for i = 1 : length(YN)
    if YN(i) < LLR_read_ref_ii(1)
        YN(i) = -13;
    elseif YN(i) < LLR_read_ref_ii(2)
        YN(i) = -9;
    elseif YN(i) < LLR_read_ref_ii(3)
        YN(i) = -5;
    elseif YN(i) < LLR_read_ref_ii(4)
        YN(i) = -2;
    elseif YN(i) < LLR_read_ref_ii(5)
        YN(i) = 2;
    elseif YN(i) < LLR_read_ref_ii(6)
        YN(i) = 5;
    elseif YN(i) < LLR_read_ref_ii(7)
        YN(i) = 9;
    else
        YN(i) = 13;
    end
end