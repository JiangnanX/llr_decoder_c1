function [d,d_LDPC,iterations]    = LDPC_DECODER_MS_ARRAY_v7(YN,C,MAX_ITERATIONS);

d          = (YN<0);
iterations = 0;

R               = zeros(C.p,C.dc);
R_old           = zeros(C.dv,C.p,C.dc);
Psum            = reshape(YN,C.p,C.dc);
converge        = 0;
sub_iter        = 0;
while(~converge)
    sub_iter    = mod((sub_iter+1), C.dv);
    if(sub_iter == 0) % sub-iteration
        sub_iter            = C.dv;
        iterations                = iterations+1;
    end 
    Block_row_prev = mod(sub_iter -1, C.dv);
    if Block_row_prev == 0, Block_row_prev = C.dv;end
    Block_row = mod(sub_iter, C.dv);
    if Block_row == 0, Block_row = C.dv;end    
    Block_row_next = mod(sub_iter, C.dv);
    if Block_row_next == 0, Block_row_next = C.dv;end
                
    if( (iterations == MAX_ITERATIONS) ) %reaching maximum decoding iteration
        converge            = 1;
    end
        
    for i = 1:C.dc %cyclic shift of variable node messages
        Psum_shift(:,i) = circshift(Psum(:,i),-C.S(Block_row,i));
    end    
    R_next              = squeeze(R_old(Block_row,:,:));
    Q                   = Psum_shift - R_next;
    
    
    
   sum_CHT = 0;
   if(sub_iter == C.dv) % sub-iteration
   	Hd = (Psum<0);
   	for ii =1:C.dv
	       %if syndrome of CH' is zero, stop iteration here  
	           for jj=1:C.dc
	               Hd_dec(:,jj)      = circshift(Hd(:,jj), -C.S(ii,jj));
	           end
	           sum_CHT             = sum_CHT + sum(mod(sum(Hd_dec,2),2));
	end
	       if sum_CHT ==0
	           converge = 1;
	           continue;
	       end
   end 
  
    
    
    
    %check node processing
    R_old(Block_row_prev,:,:)    = R(:,:);

    [min1,min1_index]       = min(abs(Q),[],2);
    for ii_temp = 1:C.p
        Q(ii_temp,min1_index(ii_temp))   = Inf*sign(Q(ii_temp,min1_index(ii_temp)));
    end    
    min2                    = min(abs(Q),[],2);
    min1                    = max(min1-C.MSO_BETA,0);
    min2                    = max(min2-C.MSO_BETA,0);
    Q_sign                  = (Q>=0);
    Q_sign                  = 2*Q_sign-1;
    cum_sign                = prod( Q_sign,2); %across row
    R                       = min1*ones(1,C.dc);
    for jj= 1:C.p
        R_sign(jj,:)            = Q_sign(jj,:)*cum_sign(jj);
        R(jj,min1_index(jj))    = min2(jj);
    end
    R                       = R.*R_sign;
    %end of check node processing
    R_old_temp              = squeeze(R_old(Block_row,:,:));
    R_delta                 = R - R_old_temp;
    for iii= 1:C.dc %cyclic shift of check node node messages  
        R_delta(:,iii)      = circshift(R_delta(:,iii),C.S(Block_row,iii)); %shifting R_delta, for calculation of Psum
    end
    Psum                    = Psum + R_delta;    
    
end %end of while, decoding iteration

d_LDPC                      = (Psum<0);
d_LDPC                      = reshape(d_LDPC,C.N,1); %%due to BPSK mapping
