%% Simulates the proposed 802.16e LDPC decoder
%% KIRAN GUNNAM
%%
%% This Software was received from Texas A&M University and modified by
%  TexasLDPC to model the LLR generating RTL for the case when 7 optimized
%  read reference LLRs are used and the channel is Gaussian with
%  non-symmetric distributions for stored (transmitted) bits 0 and 1. This
%  modified software also calls the software used to generate the test
%  vectors, i.e., Test_LDPC_Decoder_Flash.m.

%% ---Confidentiality Notice Start--
%
%  TexasLDPC Inc Confidential.
%  � Copyright 2014 to the present, TexasLDPC Inc.
%  All the software contained in these files are protected by United States
%  copyright law and may not be reproduced, distributed, transmitted,
%  displayed, published or broadcast without the prior written permission
%  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
%  or other notice from copies of the content.
%
%  ---Confidentiality Notice End--
%
%  Osso Vahabzadeh
%  TexasLDPC Inc
%  May 2016
%
%%
%%Run either populate_C_structure_for_irregular_LDPC.m or Create_Cstruct.m from
%%Matlab_RTL_CodeGen directory to create C structs needed for this file.
%clc
tic
clear all
close all
randn('state',0);
rand('state',0);
plot_figures=0;

%% SIMULATION CONSTANTS
Sim.Codeword                    = 'Non-zero Codeword';          % options: 'Zero Codeword' and 'Non-zero Codeword'
Create_test_vector              = 'Yes';                         % options: 'Yes' and 'No'
Sim.MAX_PACKETS_TO_BE_SIMULATED = 2e0;
Sim.MAX_PACKETS_IN_ERROR        = 2e0;
Sim.MAX_ITERATIONS              = 20;
Sim.EbNo_bit0                   = 3.4;                  % In dB
Sim.EbNo_bit1                   = Sim.EbNo_bit0;                % In dB
Sim.Channel                     = 'AWG';
Sim.BSC_p                       = [0.02:-0.003:0.01 0.01];
load Vol_LLR_Ref_Rate_0.83_SNR0_2.8_6.4_dB_SNR1_2.8_6.4_dB_SL0.1.mat

%%
Sim.BER                  = zeros(1,length(Sim.EbNo_bit0));
if(Sim.Channel=='BSC')
    Sim.points_to_simulate       = length(Sim.BSC_p);
else
    Sim.points_to_simulate       = length(Sim.EbNo_bit0);
end

%Quantization parameters%%note that these are the bitwidths of magnitude
%part.
QuantStruct.Decoder_Type   ='LLD';%'LLD' ;
QuantStruct.Y_HD   = 0;
if(Sim.Channel=='BSC')
    QuantStruct.Y_HD   = 1;
end
QuantStruct.Y_BitWidth   = 4;
QuantStruct.R_BitWidth   = 4;
QuantStruct.Q_BitWidth   = 4;
QuantStruct.P_BitWidth   = 7;
QuantStruct.Y_Scaling    = 1;%2;
QuantStruct.EnableQuantization.Input = 1;
QuantStruct.EnableQuantization.R_old = 1;
QuantStruct.EnableQuantization.R_new = 1;
QuantStruct.EnableQuantization.Q     = 1;
QuantStruct.EnableQuantization.P     = 1;
QuantStruct.LLR_Ch_ports             = 1;   %1%16 % 128



% CODE PARAMETERS

%%run
%%populate_C_structure_for_irregular_LDPC for Block_LDPC_Decoder
%%populate_C_structure_for_irregular_LDPC ==>no PS OOP. only OOP for Rnew.
%%no processing parameter reordering

%%populate_C_structure_for_irregular_LDPC_M1==>PS OOP. blocks in each
%%layer are reordered;  and numbered in processing order.
%%all the other processing parameters are reordered

%%populate_C_structure_for_irregular_LDPC_M2==>PS OOP. blocks in each
%%layer are reordered.
%%all the other processing parameters are not reordered.

%%populate_C_structure_for_irregular_LDPC_M3==>PS OOP. blocks in each
%%layer are reordered.all the other processing parameters are reordered.

%%intent: populate_C_structure_for_irregular_LDPC_M1/M2/M3
%%for Block_LDPC_Decoder_M1/M2/M3 respectively.

%%Block_LDPC_Decoder_M1(this is same as Block_LDPC_Decoder_M0 except
%for dci cosmetic change) can take populate_C_structure_for_irregular_LDPC
%and populate_C_structure_for_irregular_LDPC_M1/M2
%%wont support populate_C_structure_for_irregular_LDPC_M3

%%Block_LDPC_Decoder_M2 can take populate_C_structure_for_irregular_LDPC_M1/M2
%%wont support populate_C_structure_for_irregular_LDPC_M3

%%Block_LDPC_Decoder_M3 can take populate_C_structure_for_irregular_LDPC_M1/M3.
%%wont support populate_C_structure_for_irregular_LDPC_M2
%%tested for floating point.should work for fixed point also.

%load C_Structure_H_Matrices_802_16e_H_1_2
load ../H_matrices/C_Struct_CodeId_Flash1



C.H=C.S;
C.Iteration_Mode =0;          %%==1-->cht at the end of each iteration. Not supported now with Block_LDPC_Decoder_v10
%%=0-->cht using layered update and sign flag
%%check
%%==2 -->Maximum number of iterations. also
%%record converegence for modes 0 and 1, if
%it happens below the max iterations

C.CorrectionMethod='Scaling';
%C.CorrectionMethod='Offset_' ;%'Scaling'
C.Scaling_Method ='Q_GAMMA'; %R_GAMMA  %Q_GAMMA
% C.beta_LUT_dv = ones(1,16)*0.75;
% disp('using 0.75 as scaling factor');
%C.Scaling_Method ='R_GAMMA';
[C.H_row,C.H_col]   = size(C.S);
%      C.dv = 3;
%      C.dc = 5;
%      for j=1:C.dv
%        for k = 1: C.dc
%          C.S(j,k)= (j-1)*(k-1);
%        end
%      end

C.Z0   = C.p;
C.M    = C.Z0;
C.N    = C.p*C.H_col;
C.N_K  = C.p*C.H_row;
C.RATE = 1-C.N_K/C.N;
C.K    = C.N*C.RATE;

%% Codeword selection and modulation
if strcmp (Sim.Codeword, 'Zero Codeword')
    [Y,U]  = ENCODE_LDPC(C);%%BPSK for zero code word
elseif strcmp (Sim.Codeword, 'Non-zero Codeword')
    %[Y,U] = ENCODE_LDPC_nz(C);
    load uy_data.mat;
end
%%

randn('state',0);

for ii =1: Sim.points_to_simulate
    LLR_read_ref_ii     = LLR_read_ref(ii+6, :);
    
    Sim.packet_num       = 0;
    Sim.packets_in_error = 0;
    Sim.packets_in_error_uncoded = 0;
    %% Simulation start
    while(Sim.packets_in_error<Sim.MAX_PACKETS_IN_ERROR & Sim.packet_num<Sim.MAX_PACKETS_TO_BE_SIMULATED)
        Sim.packet_num                       = Sim.packet_num+1;
        packet_num                           = Sim.packet_num;
        
        if(Sim.Channel=='BSC')
            Sim.BSC_error_injected=rand(1,C.N)<=Sim.BSC_p(ii);
            YN = Y.*((-1).^(Sim.BSC_error_injected)');
        else
            % [YN]                                 = IMPAIRMENTS_LDPC(Y,Sim.EbNo(ii),C.RATE);
            [YN]                                 = IMPAIRMENTS_LDPC_TL(Y,Sim.EbNo_bit0(ii),Sim.EbNo_bit1(ii), C.RATE,LLR_read_ref_ii); % LLR selection based on 7 read reference LLRs
        end
        %   load L.txt
        %   YN=L;
        Sim.packet.iterations(packet_num)    = 0;
        Sim.packet.bits_in_error(packet_num) = 0;
        system('del debug_data\*.mat');
        %% LDPC DECODER BEGIN %%V9 --optimization for alpha based on Rold, Rnew and
        %% scaling of received LLR values
        
        [d,d_LDPC,Sim.packet.iterations(packet_num),Sim.packet.is(packet_num).is] = Block_LDPC_Decoder_M1(YN,C,Sim.MAX_ITERATIONS,QuantStruct);
        %% LDPC DECODER END
        Sim.packet.bits_in_error(packet_num) = sum(abs(d_LDPC(1:C.N_K)-U(1:C.N_K)));
        Sim.packet.bits_in_error_uncoded(packet_num) = sum(abs(d-U));
        
        if(Sim.packet.bits_in_error(packet_num)>0),
            Sim.packets_in_error = Sim.packets_in_error+1;
        end
        if(Sim.packet.bits_in_error_uncoded(packet_num)>0),
            Sim.packets_in_error_uncoded = Sim.packets_in_error_uncoded+1;
        end
        save Sim_C_Structure_H_Matrices_Flash.mat Sim
        Sim;
        for i=1:Sim.packet_num
            a(i)=(Sim.packet.is(i).is.actual_iterations_needed-1)+Sim.packet.is(i).is.actual_layer/C.H_row;
            b(i)=(Sim.packet.is(i).is.cht_layer_iterations-1)+Sim.packet.is(i).is.cht_layer_layer/C.H_row;
        end
        ave_iterations = mean(b);
        if(Y==1),%%all zero code word for BPSK
            extra_iterations = ave_iterations-mean(a);
            close all
            if(plot_figures)
                figure;
                plot(a,'r')
                hold on ;plot(b,'b')
                legend('iterations for zero code word','cht iterations')
            end
        end
        
        %% Create the test vector with information about the no of iterations and layers
        if strcmp (Create_test_vector, 'Yes')
            if strcmp (Sim.Codeword, 'Zero Codeword')
                filename = strcat('LDPC_P_0_CW', '_EbNo_bit0_', num2str(Sim.EbNo_bit0(ii)), '_EbNo_bit1_', num2str(Sim.EbNo_bit1(ii)), '_dB_packet_no_', num2str(Sim.packet_num), '.DO');
            elseif strcmp (Sim.Codeword, 'Non-zero Codeword')
                %[Y,U] = ENCODE_LDPC_nz(C);
                filename = strcat('LDPC_P_NZ_CW', '_EbNo_bit0_', num2str(Sim.EbNo_bit0(ii)), '_EbNo_bit1_', num2str(Sim.EbNo_bit0(ii)), '_dB_packet_no_', num2str(Sim.packet_num), '.DO');
            end            
            fid1 = fopen(filename,'w');
            fprintf(fid1,'#Iteration = %d, Layer = %d\n\n', Sim.packet.is(packet_num).is.cht_layer_iterations, Sim.packet.is(packet_num).is.cht_layer_layer);
            fclose(fid1);
            run Test_LDPC_Decoder_Flash.m 
        end
        
    end
    
    Sim.FER(ii)                 = Sim.packets_in_error/Sim.packet_num;
    Total_User_Bits_Simulated   = Sim.packet_num*C.N_K;
    Total_Bits_Simulated        = Sim.packet_num*C.N;
    Sim.BER(ii)                 = sum(Sim.packet.bits_in_error)/(Total_User_Bits_Simulated);
    Sim.BER_uncoded(ii)         = sum(Sim.packet.bits_in_error_uncoded)/(Total_Bits_Simulated);
    Sim.ave_iterations_each_snr_point(ii) = ave_iterations;
    
    Sim
end
save Sim.mat Sim
%      figure;
%     semilogy(Sim.EbNo_bit0,Sim.BER,'b-*',Sim.EbNo_bit0,Sim.BER_uncoded,'g-*');
%     x      = sqrt(2*10.^(Sim.EbNo_bit0/10));
%     Q_func = 1/2*erfc(x/sqrt(2));
%     BER_th= Q_func;
%     title('BER vs EbNo');
%     hold on;
%     semilogy(Sim.EbNo_bit0,BER_th,'r-*');
%     hold off;
%     legend('simulated','uncoded', 'theoretical uncoded');
%     BER(l)=Sim.BER;
%     BER(l)
if(plot_figures)
    if(Sim.Channel=='BSC')
        figure;
        semilogy(Sim.BER_uncoded,Sim.FER,'b-*');
        title('FER vs Raw BER');
        figure
        plot(Sim.BER_uncoded,Sim.ave_iterations_each_snr_point,'r-*');
        title('iterations vs Raw BER');
    else
        figure;
        semilogy(Sim.EbNo_bit0,Sim.FER,'b-*');
        title('FER vs EbNo');
        figure
        plot(Sim.EbNo_bit0,Sim.ave_iterations_each_snr_point,'r-*');
        title('iterations vs EbNo');
    end
    legend('layered decoding fixed point');
end



save ALL_Sim_C_Structure_H_Matrices_Flash.mat
toc