function jvec = bvec_complement2 ( n, ivec )

%% BVEC_COMPLEMENT2 computes the two's complement of a binary vector.
%
%  Modified:
%
%    11 June 2004
%
%  Author:
%
%    John Burkardt
%
%  Parameters:
%
%    Input, integer N, the length of the vectors.
%
%    Input, integer IVEC(N), the vector to be two's complemented.
%
%    Output, integer JVEC(N), the two's complemented vector.
%
  kvec1(1:n) = 1 - ivec(1:n);

  kvec2(1) = 1;
  kvec2(2:n) = 0;

  jvec = bvec_add ( n, kvec1, kvec2 );
