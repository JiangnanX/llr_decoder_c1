% /---Confidentiality Notice Start--/
%   Texas A&M University Confidential. 
%   Contact: Office of Technology Commercialization.
%   The material contained in this software and the associated papers
%   have features which are contained in a patent disclosure for LDPC decoding architectures 
%   filed by Gwan Choi, John Junkins , Kiran Gunnam. 
%   For more information about the licensing process, please contact:
%   Alex Garcia
%   Department: Licensing & Intellectual Property Management
%   Title: Licensing Associate
%   Email: alegarza@tamu.edu
%   Phone: (979) 458-2640
%   Fax: (979) 845-2684
% /---Confidentiality Notice End--/
% Kiran Gunnam
% Texas Engineering Experiment Station
% Texas A&M University
clear all
close all
load C_Structure_H_Matrices_802_16e_H_1_2
[H_row H_col] = size(C.H);
dc_max = max(C.dc);
%%determine the write cache depth to support multiple read accesses for layer
for jj=1:dc_max
    num_cache_access_per_layer = jj;
    ncal = num_cache_access_per_layer;
    for i=1:H_row
     [a b]=find(C.dependendent_layer_LUT(:,:)==i);
     a_m = a-i;
     for kk=1:length(a_m)
         if(a_m(kk)<0), 
             a_m(kk)=a_m(kk)+H_row;
         end
     end    
     as=sort(a_m,1);
     w_cache_depth(i)=as(min(ncal,C.dc(i)));
    end
    w_cache_max_depth(jj) = max(w_cache_depth);
    figure;
    plot(w_cache_depth)
    xlabel('layer');
    ylabel('write cache depth')
    title(strcat('write cache depth to support ',num2str(ncal),'read access for layer'));
end

figure
plot(w_cache_max_depth)
xlabel('number of read accesses for layer from cache');
ylabel('write cache maximum depth');
title(strcat('write cache maximum depth vs number read access for layer from cache'));



%%
%%determine the read cache depth to support 1 read accesses for layer
ll=1;
for i=1:H_row
    read_address(ll:ll+C.dc(i)-1)= C.dependendent_layer_LUT(i,1:C.dc(i));
    ll= ll+C.dc(i);
end
figure;
plot(read_address);
title('read address to FS memory');
hist(read_address,H_row);%g=hist(read_address,H_row); note that g is equal to C.dc
title('histogram of read address to FS memory');
[read_address_s sort_index]= sort(read_address);
%g=hist(read_address_s,H_row); note that g is equal to C.dc
ll=1;
for i=1:H_row
    r_cache_depth(i)=min(diff(sort_index(ll:ll+C.dc(i)-1)));
    ll= ll+C.dc(i);
end
r_cache_max_depth = max(r_cache_depth);

for hk=1:r_cache_max_depth
[bb]=find(r_cache_depth<=hk);
number_of_accesses_supported(hk) = length(bb);
end
figure;
plot(number_of_accesses_supported)
title('number of read accesses supported vs read cache depth');



%%
    