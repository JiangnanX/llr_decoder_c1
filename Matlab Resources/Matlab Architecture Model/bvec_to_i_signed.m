function value = bvec_to_i_signed ( n, ivec )

%% BVEC_TO_I_SIGNED makes an integer from a (signed) binary vector.
%
%  Example:
%
%         IVEC   binary  I
%    ----------  -----  --
%    1  2  3  4
%    ----------
%    1, 0, 0, 0       1  1
%    0, 1, 0, 0      10  2
%    0, 0, 1, 1    -100 -4
%    0, 0, 1, 0     100  4
%    1, 0, 0, 1    -111 -9
%    1, 1, 1, 1      -0  0
%
%
%  Modified:
%
%    11 June 2004
%
%  Author:
%
%    John Burkardt
%
%  Parameters:
%
%    Input, integer N, the dimension of the vector.
%
%    Input, integer IVEC(N), the binary representation.
%
%    Output, integer VALUE, the integer.
%
  i_sign = 1;

  if ( ivec(n) == 1 )
    i_sign = -1;
    jvec(1:n-1) = bvec_complement2 ( n-1, ivec );
  else
    jvec(1:n-1) = ivec(1:n-1);
  end

  value = 0;
  for j = n-1 : -1 : 1
    value = 2 * value + jvec(j);
  end

  value = i_sign * value;
