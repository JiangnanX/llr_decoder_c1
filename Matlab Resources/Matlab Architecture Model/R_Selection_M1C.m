% Kiran Gunnam 
% Texas Engineering Experiment Station
% Texas A&M University
% Does the R selection operation by index comparator and R sign
function [R]= R_Selection(FS,Qsign_shift,block_num);

 R                = FS.M1;
 index_tmp        = find(FS.M1_index == block_num);
 R(index_tmp)     = FS.M2(index_tmp);
 R_sign           = xor(Qsign_shift,FS.cum_sign);
 index_tmp        = find(R_sign==1);
 R(index_tmp)     = -R(index_tmp);  