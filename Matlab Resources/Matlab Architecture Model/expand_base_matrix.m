%Kiran Gunnam
%expand_base_matrix.m
%expands the base matrix with cyclic shifts into sparse matrix
function [H_sparse]= expand_base_matrix(H,z);

[H_row H_col] = size(H);
H_sparse      = [];
I             = eye(z,z);
for i = 1: H_row %%constrcut one block row at a time
    H_sparse_BR      = [];
    for j=1:H_col %%constrcut one block at a time
        if(H(i,j)>=0),
            circulant= circshift(I,H(i,j));
        else
            circulant= zeros(z,z);
        end
        H_sparse_BR= [H_sparse_BR circulant];
    end
     H_sparse= [H_sparse 
                H_sparse_BR];
end