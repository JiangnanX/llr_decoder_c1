% /---Confidentiality Notice Start--/
%   Texas A&M University Confidential.
%   Contact: Office of Technology Commercialization.
%   The material contained in this software and the associated papers
%   have features which are contained in a patent disclosure for LDPC decoding architectures 
%   filed by Gwan Choi, John Junkins , Kiran Gunnam. 
%   For more information about the licensing process, please contact:
%   Alex Garcia
%   Department: Licensing & Intellectual Property Management
%   Title: Licensing Associate
%   Email: alegarza@tamu.edu
%   Phone: (979) 458-2640
%   Fax: (979) 845-2684
% /---Confidentiality Notice End--/
% Kiran Gunnam
% Texas Engineering Experiment Station
% Texas A&M University
function [d,d_LDPC,iterations,iterations_struct]    = Block_LDPC_Decoder(YN,C,MAX_ITERATIONS,QuantStruct);
% clear all;
% % load data1.mat;
% load data_block_LDPC.mat

d           = (YN<0);
iterations  = 0;
iterations_struct.cht_layer_iterations = MAX_ITERATIONS;
iterations_struct.cht_layer_layer      = C.H_row;
iterations_struct.actual_iterations_needed=  MAX_ITERATIONS;;
iterations_struct.actual_layer = C.H_row;
YMax    = (2^QuantStruct.Y_BitWidth-1);
YMin    = -(YMax+1);
RMax    = (2^QuantStruct.R_BitWidth-1);
RMin    = -(RMax+1);
QMax    = (2^QuantStruct.Q_BitWidth-1);
QMin    = -(QMax+1);
PMax    = (2^QuantStruct.P_BitWidth-1);
PMin    = -(PMax+1);
cht_layer_loop_visit =0;
cht_test_loop_visit  =0;
Iteration_Mode= C.Iteration_Mode;
debug_figures = 0;

if(QuantStruct.Y_HD)
      YN_HD           = YN<0;
      YN              = (1-2*YN_HD)*YMax;
else  if(QuantStruct.EnableQuantization.Input)
      YN              = round(QuantStruct.Y_Scaling*YN);
      YN              = min(YN, YMax );
      YN              = max(YN, YMin+1);%%symmetric -31 to 31
end
          
      
end
[H_row, H_col] = size(C.S);


%% Memories in the Architecture. Initialize them to 0s for debug ease. 
%% No need to do this in the hardware.
P_buf           = zeros(C.M, H_col);

%FS_mem contains the FS storage for all the layers
for layer= 1:H_row
FS_mem(layer).FS.M1        = zeros(C.M,1);
FS_mem(layer).FS.M2        = zeros(C.M,1);
FS_mem(layer).FS.cum_sign  = zeros(C.M,1);
FS_mem(layer).FS.M1_index  = zeros(C.M,1);
end
Qsign_shift_Mem  = zeros(C.M,C.Num_circulants);
P_Mem            = zeros(C.M,H_col,H_row);
P_Mem_shift      = zeros(C.M,H_col,H_row);
C_Mem            = zeros(C.M,H_row); %% check memory
Hd_Mem           = NaN*ones(C.M,H_col);%%Hard decision memory initialization
d_block_s2       = zeros(C.M,1);
HD_shift         = zeros(H_col);%%this is a dont care condtion. no need to initialize in the hardware
PS.M1        = zeros(C.M,1);
PS.M2        = zeros(C.M,1);
PS.cum_sign  = zeros(C.M,1);
PS.M1_index  = zeros(C.M,1);

R_Mem        = zeros(max(C.dc),C.M,C.H_row);
Q_Mem        = zeros(max(C.dc),C.M,C.H_row);
%Hd_mem_cht                 = zeros(C.M, H_col);
C_Mem_delta =zeros(C.M, H_col);

signcheck_block            = zeros(1,H_col);
converge = 0;
layer    = 0;
layer_cht = 0;

% initialize Psum to channel value.
for i = 1:H_col
        block_col = i;
        start_col = C.Z0 * (block_col-1)+1;
        end_col   = C.Z0 * (block_col);
        % P_buffer
        P_buffer_tmp  =  YN(start_col:end_col); 
%         if(C.Scaling_Method =='Q_GAMMA')
%          P_buffer_tmp = P_buffer_tmp/C.beta(i);
%         end
        P_mem_wr_addr =  block_col;
        P_buf(:, P_mem_wr_addr) = squeeze(P_buffer_tmp); 
end

num_circulants_tmp =0;
converge_tag = 0;
sign_flip_violation =0;
while(~converge) % each sub-iteration
    % iteration & sub-iteration management
    layer       = mod((layer+1), H_row);
    layer_cht   = layer_cht+1;
    layer_cht   = min(layer_cht,H_row);%saturate layer_cht to H_row


    if(layer == 0) % sub-iteration
        layer            = H_row;
    end   
    
    
    if layer == 1
        iterations          = iterations+1; 
        circulant_index_read_Inorder_for_R_old_sign =0;
        circulant_index_write_Inorder_for_R_new_sign =0;
    end
    layer_dc                    = C.dc(layer);
    CNU_PS_FS_PROCESSING_CYCLES = layer_dc+1;

    
    
%% PS processing is in order. %FS processing is in order
%% FS processing is merged into PS processing as it takes 
%% only one clock cycle and block level parallelism is employed
    for TimeSlotCounter = 1: CNU_PS_FS_PROCESSING_CYCLES
         if(TimeSlotCounter<=layer_dc), 
            block_num                                    = TimeSlotCounter;
            blocknum_cn                                  =  C.blocknum_LUT(layer,TimeSlotCounter);
            circulant_index_read_Inorder_for_R_old_sign  = C.circulant_index_LUT(layer,block_num);
            circulant_index_write_Inorder_for_R_new_sign = C.circulant_index_LUT(layer,block_num);%

            if(iterations ==1 && C.UseChannelValue_Flag(layer,block_num)==1) 
                %%for the first iteration use channel values for the P. Also
                % make sure that absolute shift values are applied for the
                % first time in the first iteration
                P                       = P_buf(:,C.blockcol_LUT(layer,block_num)); 
                P_NLD                   = P;
                P_NLD_unshifted         = P_NLD;
                Hd_buffer =  P<0; 
                Hd_shift1_current_layer_signflipcheck = Hd_buffer; %%%this is not necessary. computation is done for debug purposes
                P_Mem(:,C.blockcol_LUT(layer,block_num),layer)  = P;
                P_shift                 = circshift(P,C.Shift_iteration1(layer,block_num));
                P_Mem_shift(:,C.blockcol_LUT(layer,block_num),layer)  = P_shift;
                Hd_shift2_currentlayer  = (P_shift<0);
                
                Hd_Mem_shift1_layer_latched_signflipcheck(:,C.blockcol_LUT(layer,block_num))  = Hd_shift2_currentlayer;
                HD_shift(C.blockcol_LUT(layer,block_num)) = C.Shift_iteration1(layer,block_num);
                Q_shift     = P_shift;
                Q_shift_tmp = Q_shift;
                num_circulants_tmp = num_circulants_tmp+1;
                
            else 
                %% R new select is out of order and on the fly. P and Q update are on the fly.            
%% Vector Data path operations: out of order operations
            if(C.Scaling_Method =='Q_GAMMA')
            R_old_beta = 1;%C.beta(C.blockcol_LUT(layer,block_num));  
            R_new_beta = 1;%R_old_beta;
            else
            %R_old_beta = C.beta(C.blockcol_LUT(layer,block_num));  
            R_old_beta  = C.beta_LUT_dv(C.dv(C.blockcol_LUT(layer,block_num)));%%this is how it is done in hw
                         %above two statements are same.
            R_new_beta = R_old_beta;
            end
            dci=C.dependendent_circulant_index_LUT(layer,block_num);    
            R_new      = R_new_beta*R_Selection...
                     (FS_mem(C.dependendent_layer_LUT(layer,block_num)).FS,...
                      Qsign_shift_Mem(:,dci),...
                      C.dependendent_block_LUT(layer,block_num));
            
                if(QuantStruct.EnableQuantization.R_new)
                       %R_new   = round(R_new/QuantStruct.stepSize)*QuantStruct.stepSize;
                       %R_new   = floor(R_new+0.5);
                       R_new   = round(R_new);%%floor is chagned back to round for the consistency with the hardware 
                                              %%simulation
                       R_new   = min(R_new, RMax);
                       R_new   = max(R_new, RMin);
                end    
                
             
%% Vector Data path operations: In order operations
           
           
            P_LD          = R_new + Q_shift_Mem(:,C.blockcol_LUT(layer,block_num));
            if(QuantStruct.Decoder_Type=='NLD' & C.UseChannelValue_Flag(layer,block_num)==1)
               P_NLD         = P_LD;
               P_NLD_unshifted =  circshift(P_NLD ,-C.shift(C.dependendent_layer_LUT(layer,block_num),...
                                                                                   C.dependendent_block_LUT(layer,block_num))); 
            elseif(QuantStruct.Decoder_Type=='NLD') 
               P_NLD         =  circshift(P_NLD_unshifted ,C.shift(C.dependendent_layer_LUT(layer,block_num),...
                                                                                   C.dependendent_block_LUT(layer,block_num))); 
            end
            
            if(QuantStruct.Decoder_Type=='NLD')
               P         = P_NLD;
            else
               P         = P_LD;
            end
                   
                
            Q_old      = Q_shift_Mem(:,C.blockcol_LUT(layer,block_num));
            
                if(QuantStruct.EnableQuantization.P)
                   %P              = round(P/QuantStruct.stepSize)*QuantStruct.stepSize;
                   %P              = floor(P+0.5);
                    P              = round(P);%%floor is chagned back to round for the consistency with the hardware 
                                              %%simulation
                   P              = min(P, PMax);
                   P              = max(P, PMin);
                end

            %%this memory and this shifter are not needed in the architecture. kept for debug purposes   
            P_Mem(:,C.blockcol_LUT(layer,block_num),layer)  = circshift(P ,-C.shift(C.dependendent_layer_LUT(layer,block_num),...
                                                                                   C.dependendent_block_LUT(layer,block_num))); 
            R_old      = R_old_beta*R_Selection...
                         (FS_mem(layer).FS,Qsign_shift_Mem(:,circulant_index_read_Inorder_for_R_old_sign),blocknum_cn);
                if(QuantStruct.EnableQuantization.R_old)
                       %R_old   = round(R_old/QuantStruct.stepSize)*QuantStruct.stepSize;
                       %R_old   = floor(R_old+0.5);
                       R_old   = round(R_old);%%floor is chagned back to round for the consistency with the hardware 
                                              %%simulation
                       R_old   = min(R_old, RMax);
                       R_old   = max(R_old, RMin);
                end 
            %%this memory is not needed in the architecture. kept for debug purposes       
            %R_Mem(layer,:,block_num)= R_old;
            R_Mem(block_num,:,layer)= R_old;
            
            Hd_buffer =  P<0; 
            %%not needed in the architecture
%              Hd_Mem(:,C.blockcol_LUT(layer,block_num)) = circshift(Hd_buffer ,-C.S(C.dependendent_layer_LUT(layer,block_num),...
%                                                                                    C.dependendent_block_LUT(layer,block_num))); 
% %                                                                               %one bit cyclic shifter
            Hd_shift1_current_layer_signflipcheck = Hd_buffer;
%             if(block_num ==1),
%                 sign_flip_violation = 0;%%reset this at the beginning of each layer processing and then do the sign flip check
%             end
             d_block_s2=Hd_Mem_shift1_layer_latched_signflipcheck(:,C.blockcol_LUT(layer,block_num));
             
            if(any(Hd_shift1_current_layer_signflipcheck ~= Hd_Mem_shift1_layer_latched_signflipcheck(:,C.blockcol_LUT(layer,block_num))))
                %sign_flip_violation = 1;
                layer_cht_temp           = layer -C.dependendent_layer_LUT(layer,block_num); %%set this to the number of "good" rows that don't have
                                                                                        %%the sign violation
                if(layer_cht_temp<0),
                    layer_cht_temp = layer_cht_temp+H_row;
                end   
                layer_cht = min(layer_cht,layer_cht_temp);% take the minimum number of good rows for all the blocks

            end 
            P_shift                 = circshift(P,C.delta_shift(layer,block_num));
            P_Mem_shift(:,C.blockcol_LUT(layer,block_num),layer)  = P_shift;
            Hd_shift2_currentlayer  =  (P_shift<0);
            Hd_Mem_shift1_layer_latched_signflipcheck(:,C.blockcol_LUT(layer,block_num))  = Hd_shift2_currentlayer;
            HD_shift(C.blockcol_LUT(layer,block_num)) = C.shift(layer,block_num);
            Q_shift    = P_shift- R_old;
            Q_shift_tmp = Q_shift;%*Q_scaling_factor; %%scaling on Q
                if(QuantStruct.EnableQuantization.Q)
                   
                   %Q_shift   = round(Q_shift/QuantStruct.stepSize)*QuantStruct.stepSize;
                   %Q_shift   = floor(Q_shift+0.5);
                   Q_shift   = round(Q_shift);%%floor is chagned back to round for the consistency with the hardware 
                                              %%simulation
                   Q_shift   = min(Q_shift, PMax);
                   Q_shift   = max(Q_shift, PMin);
                   Q_shift_tmp = Q_shift; 
                   %%do not do another quantization here as the scaling module
                   % accepts does conversion from 2's complement to signed
                   % magnitude in a differnt way.
                    Q_shift_tmp   = round(Q_shift_tmp);
                    Q_shift_tmp   = min(Q_shift_tmp, PMax);
                    Q_shift_tmp   = max(Q_shift_tmp, -PMax);%%INTENTIONAL. should be -PMax.
                     %%due to the 2's complement implementation of QMin=-QMax
                end 
            end 
          Q_shift_Mem(:,C.blockcol_LUT(layer,block_num)) = Q_shift;
         end
         if(C.Scaling_Method =='Q_GAMMA')
         %Q_scaling_factor =  C.beta(C.blockcol_LUT(layer,block_num));
         Q_scaling_factor  = C.beta_LUT_dv(C.dv(C.blockcol_LUT(layer,block_num)));%%this is how it is done in hw
                         %above two statements are same.
         else
         Q_scaling_factor =   1;
         end
         %Q_offset_parameter = C.beta_o(C.blockcol_LUT(layer,block_num))*(2^(QuantStruct.Q_BitWidth-3)-1);
         Q_offset_parameter = C.beta_o_LUT_dv(C.dv(C.blockcol_LUT(layer,block_num)));
       if(C.CorrectionMethod=='Scaling')
         Q_corrected        = Q_scaling_factor*Q_shift_tmp;
         if(debug_figures==1)
         figure
         hist(Q_corrected);
         title('Q_corrected')
         end
         %notes on offset mode: also uncomment the quantization on Q_shift_tmp so that it is quantized to QMin and
         %QMax(not PMin and PMax)
%          Qsign_shift_tmp    = 2*(Q_shift_tmp>0)-1; 
%          Q_corrected        = Qsign_shift_tmp .*max(abs(Q_shift_tmp)-Q_offset_parameter,zeros(C.M,1)+2^(-QuantStruct.Q_BitWidth+1)); %%to preserve the sign
%         
          if(QuantStruct.EnableQuantization.Q)
            %Q_corrected   = floor(Q_corrected+0.5);
            Q_corrected   = round(Q_corrected);%%floor is chagned back to round for the consistency with the hardware 
                                              %%simulation
            Q_corrected   = min(Q_corrected, QMax);
            Q_corrected   = max(Q_corrected, -QMax);%%INTENTIONAL. should be -QMax.
                                                  %%due to the 2's complement implementation of QMin=-QMax
          end
        elseif(C.CorrectionMethod=='Offset_')
          %notes on offset mode: also uncomment the quantization on Q_shift_tmp so that it is quantized to QMin and
          %QMax(not PMin and PMax)
          Qsign_shift_tmp    = 2*(Q_shift_tmp>0)-1; 
          Q_corrected        = Qsign_shift_tmp .*max(abs(Q_shift_tmp)-Q_offset_parameter,zeros(C.M,1)); 
          if(QuantStruct.EnableQuantization.Q)
            Q_corrected   = min(Q_corrected, QMax);
            Q_corrected   = max(Q_corrected, -QMax);%%INTENTIONAL. should be -QMax.
                                                  %%due to the 2's complement implementation of QMin=-QMax
          end
       end 
          %%this memory is not needed in the architecture. kept for debug purposes       
            Q_Mem(block_num,:,layer)= Q_corrected;

%          if(iterations ==1 && C.UseChannelValue_Flag(layer,block_num)==1) 
%          [FS_mem(layer).FS Qsign_shift_Mem(:,circulant_index_write_Inorder_for_R_new_sign) PS ] = ...
%                         CNU_PS_FS(FS_mem(layer).FS,PS,Q_shift_tmp,C,TimeSlotCounter,block_num,layer_dc,layer);
%          else
         if(TimeSlotCounter<=layer_dc)
         [FS_mem(layer).FS Qsign_shift_Mem(:,circulant_index_write_Inorder_for_R_new_sign) PS ] = ...
                        CNU_PS_FS(FS_mem(layer).FS,PS,Q_corrected,C,TimeSlotCounter,blocknum_cn,layer_dc,layer);
         elseif(TimeSlotCounter==layer_dc+1)
         [FS_mem(layer).FS] = CNU_PS_FS(FS_mem(layer).FS,PS,Q_corrected,C,TimeSlotCounter,blocknum_cn,layer_dc,layer);
         end
         if(debug_figures==1)
         figure
         hist(Qsign_shift_Mem(:,circulant_index_write_Inorder_for_R_new_sign))
         title('Q_sign of a block')    
         pause
         disp('press any button to continue');
         end
         %end
          


%% cHt layered based
          
          if(TimeSlotCounter<=layer_dc)
            if(block_num==1)
                     C_Mem(:,layer)         = Hd_shift2_currentlayer;         
            else
                     C_Mem(:,layer)         = bitxor(C_Mem(:,layer),Hd_shift2_currentlayer);         
            end 
          end
            
    end

   
%sign_flip_condition = (sum((signcheck_block >= C.dv))==H_col);
sign_flip_condition = (layer_cht==H_row); %above and this statement are equal.
sfc(iterations,layer) = sign_flip_condition;
cht_sum(iterations,layer)= (sum(sum(C_Mem)));
layer_cht_mat(iterations,layer) = layer_cht;
if(sign_flip_condition & (sum(sum(C_Mem)) ==0)& cht_layer_loop_visit ==0)
   
          if(Iteration_Mode==0)
           converge = 1;
           layer_cht
          end
        % disp('cht layer')
         iterations_struct.cht_layer_iterations = iterations;
         iterations_struct.cht_layer_layer = layer;
        % disp(iterations)
        % disp(layer)
         cht_layer_loop_visit =1;
     end

if(iterations>1 & Hd_Mem==0&converge~=1 &    cht_test_loop_visit ==0)
    disp('Possible mismatch in convergence function if the input data vector is all zero code word');
    iterations_struct.actual_iterations_needed= iterations;
    iterations_struct.actual_layer = layer;
    cht_test_loop_visit =1;
elseif(cht_layer_loop_visit ==1 & cht_test_loop_visit ==0)
    iterations_struct.actual_iterations_needed= iterations;
    iterations_struct.actual_layer = layer;
    cht_test_loop_visit =1;
end
    

if(iterations== MAX_ITERATIONS),
     converge = 1; 
     iterations_struct.iterations = iterations;
end

 %%unshift the Hard decision data before sending out. %one bit cyclic
%%shifter is needed. note that in hardware we need to do this only when the
%%convergence check is satisfied
    if(iterations>1 |converge==1)
     for i=1:H_col
     Hd_Mem(:,i) = circshift(Hd_Mem_shift1_layer_latched_signflipcheck(:,i), -HD_shift(i));
     end
    end

    if(layer==H_row)
    file_name= strcat('debug_data/debug_data_it',num2str(iterations));
    save(file_name);
    end
end

d_LDPC =  reshape(Hd_Mem,[],1);
file_name= strcat('debug_data/debug_data_it',num2str(iterations),'layer',num2str(layer));
save(file_name);

%i=1;dec2hex(bin2dec(num2str(d_LDPC(i*8:-1:(i-1)*8+1)')))
               
  

    
    
            