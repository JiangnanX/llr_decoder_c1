function bvec = i_to_bvec_signed ( i, n )

%% I_TO_BVEC_SIGNED makes a signed binary vector from an integer.
%
%  Example:
%
%     I       IVEC         binary
%    --  ----------------  ------
%     1  1, 0, 0, 0, 0, 0      1
%     2  0, 1, 0, 0, 0, 0     10
%     3  1, 1, 0, 0, 0, 0     11
%     4  0, 0, 1, 0, 0, 0    100
%     9  1, 0, 0, 1, 0, 0   1001
%    -9  1, 1, 1, 0, 1, 1  -1001 = 110111 (2's complement)
%
%  Discussion:
%
%    Negative values have a two's complement operation applied.
%
%    To guarantee that there will be enough space for any
%    value of I, it would be necessary to set N = 32.
%
%  Modified:
%
%    11 June 2004
%
%  Author:
%
%    John Burkardt
%
%  Parameters:
%
%    Input, integer I, an integer to be represented.
%
%    Input, integer N, the dimension of the vector.
%
%    Output, integer BVEC(N), the signed binary representation.
%
  i2 = abs ( i );

  for j = 1 : n-1
    bvec(j) = mod ( i2, 2 );
    i2 = floor ( i2 / 2 );
  end

  bvec(n) = 0;

  if ( i < 0 )
    bvec = bvec_complement2 ( n, bvec );
  end
