function bvec = i_to_bvec_unsigned ( i, n )

%% I_TO_BVEC_UNSIGNED makes a unsigned binary vector from an integer.
%
%  Example:
%
%     I       IVEC         binary
%    --  ----------------  ------
%     1  1, 0, 0, 0, 0, 0       1
%     2  0, 1, 0, 0, 0, 0      10
%     3  1, 1, 0, 0, 0, 0      11
%     4  0, 0, 1, 0, 0, 0     100
%     9  1, 0, 0, 1, 0, 0    1001
%    -9  1, 1, 1, 0, 1, 1  110111
%
%  Discussion:
%
%    Negative values have a two's complement operation applied.
%
%    To guarantee that there will be enough space for any
%    value of I, it would be necessary to set N = 32.
%
%  Modified:
%
%    05 August 2004
%
%  Author:
%
%    John Burkardt
%
%  Parameters:
%
%    Input, integer I, an integer to be represented.
%
%    Input, integer N, the dimension of the vector.
%
%    Output, integer BVEC(N), the signed binary representation.
%
  for j = 1 : n
    bvec(j) = mod ( i, 2 );
    i = floor ( i / 2 );
  end
