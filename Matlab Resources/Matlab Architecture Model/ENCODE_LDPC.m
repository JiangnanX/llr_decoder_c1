%% GENERATE ALL ZERO CODE WORD for now. ASSUME BPSK. 0 MAPS TO 1 AND 
%% 1 MAPS TO -1.
function [Y,U] = ENCODE_LDPC(C);

U = zeros(C.N,1); %GENERATE ALL ZERO CODE WORD for now.
Y = 1-2*U;%pskmod(U,2);  %BPSK
    
