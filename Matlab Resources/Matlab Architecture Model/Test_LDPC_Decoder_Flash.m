%% Test_LDPC_Decoder.m
%% Identical to Test_DigitalRX_802_11b_1MBPS.m except for few changes
%% Based on Test_DigitalRX_802_11b.m
%% Kiran Gunnam
%%
%% This Software was received from Texas A&M University and modified by
%  TexasLDPC to work in the case when it is aimed to generate the test
%  vectors by running 'LDPC_SIMULATION_TOP_Flash.m'.

%% ---Confidentiality Notice Start--
%
%  TexasLDPC Inc Confidential.
%  � Copyright 2014 to the present, TexasLDPC Inc.
%  All the software contained in these files are protected by United States
%  copyright law and may not be reproduced, distributed, transmitted,
%  displayed, published or broadcast without the prior written permission
%  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
%  or other notice from copies of the content.
%
%  ---Confidentiality Notice End--
%
%  Osso Vahabzadeh
%  TexasLDPC Inc
%  May 2016
%
%  This software aims to estimate generate the test vectors that can be
%  used in RTL simulations.
%%
% clear all
% close all
%% Initialization
Data_print_mode     = 'a+'; % If running this file within LDPC_SIMULATION_TOP_Flash.m, use 'a+' (i.e., append). If running as a standalone file, use 'w' (i.e., overwrite)
PMD_clock_period    = 5000;%==>200 MHz
if strcmp(Data_print_mode, 'a+')
    wl              = QuantStruct.Q_BitWidth + 1;
    H_col           = C.H_col;
    p               = C.Z;
    LLR_Ch_ports    = QuantStruct.LLR_Ch_ports;     %1%16 % 128
elseif strcmp(Data_print_mode, 'w')
    wl              = 5;
    H_col           = 72;
    p               = 140;
    LLR_Ch_ports    = 1;
    filename        = strcat('LDPC_P', '.DO');
    load debug_data/debug_data_it1.mat
end
%%
LLR_Ch_sub_streams  = p/LLR_Ch_ports;
N     = p*H_col;%number of bits
new_data_prep = 1;
if(new_data_prep==1)
%% Prepare the input vectors
clear bvec_I_str II bvec_I
data_length =N;
data_I     = YN;
for i=1:N
bvec_I(i,:)=i_to_bvec_signed(data_I(i), wl );
end

i=0;
code_stream_num = 1;
for col=1:H_col
 for sub_stream= 1: LLR_Ch_sub_streams 
    II_MV=[];
    for cs= 1:LLR_Ch_ports
    i=i+1;
    II=num2str(bvec_I(i,wl));%% MSB first
        for j=wl-1:-1:1
        II =strcat(II,num2str(bvec_I(i,j)));
        end
    II_MV =strcat(II,II_MV);    %%
    end
    bvec_I_str(code_stream_num,:) =II_MV;
    code_stream_num =  code_stream_num+1;%%goes from 1 to H_col*LLR_Ch_sub_streams
 end
end

time =PMD_clock_period*[0:H_col*LLR_Ch_sub_streams-1]; %
%fid1 = fopen('../testvectors/input/LDPC_P.DO','w');
fid1 = fopen(filename, Data_print_mode);
fprintf(fid1,'##Automatic Test Vector generation by Test_LDPC_Decoder.m: BEGIN');
fprintf(fid1,'#Number of test vectors for Test_LDPC_Decoder=%d\n',H_col*LLR_Ch_sub_streams);
fprintf(fid1,'#Clock Period =%d in ps \n',PMD_clock_period);
for i=1:H_col*LLR_Ch_sub_streams
fprintf(fid1,'force -freeze sim:/LDPC_Decoder_Top/LLR_ch  %d''b%s %d \n',LLR_Ch_ports*wl,bvec_I_str(i,:),time(i));
end
fprintf(fid1,'##Automatic Test Vector generation by Test_LDPC_Decoder.m: END');
fclose(fid1);
end %if(new_data_prep==1)


