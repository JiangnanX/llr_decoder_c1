function [ kvec, overflow ] = bvec_add ( n, ivec, jvec )

%% BVEC_ADD adds two binary vectors.
%
%  Example:
%
%    N = 4
%
%    IVEC     dec  JVEC     dec  KVEC     dec
%    -------  ---  -------  ---  -------  ---
%    1 0 0 0   1   1 1 0 0   3   0 0 1 0   4
%
%  Modified:
%
%    09 June 2004
%
%  Author:
%
%    John Burkardt
%
%  Parameters:
%
%    Input, integer N, the length of the vectors.
%
%    Input, integer IVEC(N), JVEC(N), the vectors to be added.
%
%    Output, integer KVEC(N), the sum of the two input vectors.
%
%    Output, logical OVERFLOW, is true if the sum overflows.
%
  overflow = 0;
%
  ivec(1:n) = floor ( ivec(1:n) );
  ivec(1:n) = mod ( ivec(1:n), 2 );

  jvec(1:n) = floor ( jvec(1:n) );
  jvec(1:n) = mod ( jvec(1:n), 2 );

  kvec(1:n) = ivec(1:n) + jvec(1:n);

  for i = 1 : n
    if ( kvec(i) == 2 )
      kvec(i) = 0;
      if ( i < n )
        kvec(i+1) = kvec(i+1) + 1;
      else
        overflow = 1;
      end
    end
  end
