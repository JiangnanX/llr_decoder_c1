function [d,d_LDPC, iterations]    = LDPC_DECODER_MS_ARRAY_STD_V2(YN,C,MAX_ITERATIONS);

d          = (YN<0);
iterations = 0;


R               = zeros(C.dv,C.p,C.dc);
for block_row = 1:C.dv
    Q(block_row,:,:) = reshape(YN,C.p,C.dc);
end
% converge        = 0;
% sub_iter        = 0;
% iterations      = 0;
converge = 0;
while (~converge)
  
    if(iterations == MAX_ITERATIONS) %reaching maximum decoding iteration
        converge            = 1;
        continue;
    end
%     else
        iterations          = iterations +1;
%         converge            = 0;
%     end
        
    %check node processing
    for block_row = 1: C.dv
        for i = 1:C.dc %cyclic shift of variable node messages
            Q_shift(:,i) = circshift(Q(block_row,:,i)',-C.S(block_row,i));
        end    
%         
%         %if syndrome of CH' is zero, stop iteration here.   
%         CHT = 0;
%         for blockRow = 1:C.dv
%             for i=1:C.dc
%                 Hd_dec      = circshif(Q(i,:), - (C.S(blockRow,i) - C.S(sub_iter,i));
%             end
%             Hd_dec          = (Hd_dec<0);
%             CHT             = CHT + mod(sum(Hd_dec,2),2);
%         end
%         if CHT ==0
%             converge = 1;
%             continue;
%         end

        [min1,min1_index]       = min(abs(Q_shift(:,:)),[],2);
        for ii_temp = 1:C.p
            Q_shift(ii_temp,min1_index(ii_temp))   = Inf*sign(Q_shift(ii_temp,min1_index(ii_temp)));
        end
        min2                    = min(abs(Q_shift(:,:)),[],2);
        min1                    = max(min1-C.MSO_BETA,0);
        min2                    = max(min2-C.MSO_BETA,0);
        Q_sign                  = (Q_shift(:,:)>=0);
        Q_sign                  = 2*Q_sign-1;
        cum_sign                = prod( Q_sign,2);
        R(block_row,:,:)        = min1*ones(1,C.dc);   
        R_sign                  = (cum_sign*ones(1,C.dc)).*Q_sign;
        for jj= 1:C.p
%             R_sign(jj,:)            = Q_sign(jj,:)*cum_sign(jj);
            R(block_row, jj,min1_index(jj))    = min2(jj);
        end
        R(block_row,:,:)                       = squeeze(R(block_row,:,:)).*R_sign;
    end  %end of check node processing
     
    %variable node processing  
    Tsum                        = reshape(YN,C.p,C.dc);
    for block_row = 1:C.dv 
        for i = 1:C.dc
            R_temp                 = squeeze(R(block_row,:,i))';
            R_shift(block_row,:,i) = circshift(R_temp,C.S(block_row,i));
            Tsum(:,i)              = Tsum(:,i) + squeeze(R_shift(block_row,:,i))';
        end
    end 
    for block_row = 1:C.dv
        Q(block_row,:,:)        = Tsum - squeeze(R_shift(block_row,:,:));
    end
    %end of variable processing
  
%     hist(reshape(R,C.dc*C.dv*C.p,1))


   sum_CHT = 0;

   	Hd = (Tsum<0);
   	for ii =1:C.dv
	       %if syndrome of CH' is zero, stop iteration here  
	           for jj=1:C.dc
	               Hd_dec(:,jj)      = circshift(Hd(:,jj), -C.S(ii,jj));
	           end
	           sum_CHT             = sum_CHT + sum(mod(sum(Hd_dec,2),2));
	end
	       if sum_CHT ==0
	           converge = 1;
	           continue;
           end
           

  
    




end %end of while, decoding iteration

d_LDPC          = reshape(Tsum, C.p*C.dc, 1);
d_LDPC          = (d_LDPC<0);
