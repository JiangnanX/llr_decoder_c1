%% /---Confidentiality Notice Start--/
%  TexasLDPC Inc Confidential.
%  � Copyright 2015 to the present, TexasLDPC Inc.
%  All the software contained in these files are protected by United States
%  copyright law and may not be reproduced, distributed, transmitted,
%  displayed, published or broadcast without the prior written permission
%  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
%  or other notice from copies of the content.
%  /---Confidentiality Notice End--/
%
%  Software to compute and plot the PDF of the received signals in a BPSK
%  system with asymmetric SNRs
%  Osso Vahabzadeh
%  TexasLDPC Inc
%  August 2015

%%

close all; clear; clc

%  This m-file plots the distributions of the received signals y0 and y1 in
%  a system with BPSK signaling where bit 0 is mapped to m_0 = 1 and bit 1
%  is mapped to m_1 = -1.
%
%  SNR_dB_b0 : SNR per bit for m_0
%  SNR_dB_b1 : SNR per bit for m_1
%  R         : Code rate

%% Initialization:

SNR_dB_b0   = 2.8;
SNR_dB_b1   = 2.8;
R           = 0.83;

%%

m_0         = 1;%1;
m_1         = -1;%-1;
delta_x     = 0.05;
SNR0        = 10^(SNR_dB_b0/10);
var0        = 1.0/(2*SNR0*R);
svar0       = sqrt(var0);
SNR1        = 10^(SNR_dB_b1/10);
var1        = 1.0/(2*SNR1*R);
svar1       = sqrt(var1);

%% Plot the PDFs:

[y0, x0]    = normal_pdf(m_0, svar0, delta_x);
[y1, x1]    = normal_pdf(m_1, svar1, delta_x);
plot(x0, y0, x1, y1, 'LineWidth', 1.5)
legend('y_0(x) (for m_0 tramsmitted)','y_1(x) (for m_1 tramsmitted)')
xlabel('x'); ylabel('y(x)')
title('PDF of the received signals in a BPSK system with asymmetric SNRs')
grid