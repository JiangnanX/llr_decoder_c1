Given the channel's operating SNRs for bits 0 and 1, this software computes 
the optimal read reference voltages and their associated reference LLRs for
1, 3, 5, and 7 channel reads.
