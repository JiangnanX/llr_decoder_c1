%% /---Confidentiality Notice Start--/
%  TexasLDPC Inc Confidential.
%  � Copyright 2015 to the present, TexasLDPC Inc.
%  All the software contained in these files are protected by United States
%  copyright law and may not be reproduced, distributed, transmitted,
%  displayed, published or broadcast without the prior written permission
%  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
%  or other notice from copies of the content.
%  /---Confidentiality Notice End--/
%
%  Software to compute the Gaussian probability density function (PDF)
%  Osso Vahabzadeh
%  TexasLDPC Inc
%  August 2015

%%

function [pdf, x] = normal_pdf(m, std, x_step)

%  This function computes the normal (Gaussian) PDF of a random variable x.
%  mgiven its mean, standard deviation stddev and the step size x_step.
%  m      : Mean of x
%  std    : Standard deviation of x
%  x_step : Step size for x

if nargin == 2
    x_step = 0.001;     % Default step size
end

x   = (m-4*std) : x_step : (m+4*std);
pdf = 1/sqrt(2*pi)/std*exp(-(x-m).^2/2/std^2);