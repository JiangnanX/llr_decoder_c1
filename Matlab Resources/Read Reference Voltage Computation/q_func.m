%% /---Confidentiality Notice Start--/
%  TexasLDPC Inc Confidential.
%  � Copyright 2015 to the present, TexasLDPC Inc.
%  All the software contained in these files are protected by United States
%  copyright law and may not be reproduced, distributed, transmitted,
%  displayed, published or broadcast without the prior written permission
%  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
%  or other notice from copies of the content.
%  /---Confidentiality Notice End--/
%
%  Software to compute the Q function
%  Osso Vahabzadeh
%  TexasLDPC Inc
%  August 2015

%%

function y = q_func(x)

%  This function computes the Q function for a given number x.

func = @(t)1/sqrt(2*pi)*exp(-t.^2/2);
y    = integral(func, x, Inf);