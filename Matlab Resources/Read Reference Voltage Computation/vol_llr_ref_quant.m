%% /---Confidentiality Notice Start--/
%  TexasLDPC Inc Confidential.
%  � Copyright 2015 to the present, TexasLDPC Inc.
%  All the software contained in these files are protected by United States
%  copyright law and may not be reproduced, distributed, transmitted,
%  displayed, published or broadcast without the prior written permission
%  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
%  or other notice from copies of the content.
%  /---Confidentiality Notice End--/
%
%  Software to compute the reference LLRs for the LDPC decoder
%  Osso Vahabzadeh
%  TexasLDPC Inc
%  August 2015

%%

function [Vol_ref_QI, Vol_ref_QI_bin, LLR_ref, LLR_ref_QI, LLR_ref_QI_bin] = vol_llr_ref_quant(Vol_ref, m_0, m_1, SNR_dB_b0, SNR_dB_b1, RATE, QuantStruct)

%  This function computes the reference LLRs used in hard-decision (HD) and
%  soft-decision (SD) decoding of LDPC codes, based on the following inputs:
%
%  q_ref             : Vector of read reference voltages
%  m_0               : Bit 0 is mapped to m_0
%  m_1               : Bit 1 is mapped to m_1
%  SNR_dB_b0         : SNR per bit for bit 0 (dB)
%  SNR_dB_b1         : SNR per bit for bit 1 (dB)
%  RATE              : Code rate
%  QuantStruct       : Contains the details about the number of quantization bits
%
%  The output variables are the following:
%
%  LLR_ref           : Vector of LLRs corresponding to q_ref
%  LLR_ref_QI        : Decimal quantized representation of LLR_ref
%  LLR_ref_QI_bin    : Signed binary representation of LLR_ref_QI

%%
parameterMax = 2^(QuantStruct.Parameter_BitWidth-1)-1;                      % Sets the maximum integer for representing the parameter where 1 bit is reduced for the sign.
parameterMin = -(parameterMax+1);                                           % Sets the minimum integer for representing the parameter

%%
SNR_b0          = 10^(SNR_dB_b0/10);
SNR_b1          = 10^(SNR_dB_b1/10);
var_0           = 1.0/(2*SNR_b0*RATE);
var_1           = 1.0/(2*SNR_b1*RATE);
sigma_0         = sqrt(var_0);
sigma_1         = sqrt(var_1);

t1 = log(sigma_1/sigma_0) + (var_0*m_1^2-var_1*m_0^2)/(2*var_0*var_1);
t2 = (var_1*m_0-var_0*m_1)/(var_0*var_1);
t3 = (var_0-var_1)/(2*var_0*var_1);

%%
Vol_ref_Q       = zeros(length(Vol_ref),1);
Vol_ref_QI      = zeros(length(Vol_ref),1);
Vol_ref_QI_bin  = [];

LLR_ref         = zeros(length(Vol_ref),1);
LLR_ref_Q       = zeros(length(Vol_ref),1);
LLR_ref_QI      = zeros(length(Vol_ref),1);
LLR_ref_QI_bin  = [];

for i = 1 : length(Vol_ref)
    %%
    Vol_ref_Q(i)    = Vol_ref(i) * (2^QuantStruct.Parameter_Fra_BitWidth);  % Multiplying x by 2^n shifts the binary point in the binary representation of x, n digits to the right.
    Vol_ref_Q(i)    = round(Vol_ref_Q(i));
    if (Vol_ref_Q(i) >= 0)
        Vol_ref_Q(i)  = min(Vol_ref_Q(i), parameterMax);                    % Restricts LLR_ref_Q to be <= parameterMax
    else
        Vol_ref_Q(i)  = max(Vol_ref_Q(i), parameterMin+1);                  % Restricts LLR_ref_Q to be >= parameterMin ???????? (why +1)
    end
    Vol_ref_QI(i)   = Vol_ref_Q(i);                                         % Stores LLR_ref_Q in a new variable
    Vol_ref_Q(i)    = Vol_ref_Q(i)/(2^QuantStruct.Parameter_Fra_BitWidth);  % Returns LLR_ref_Q
    Vol_ref_QI_bin_i= dec2sbin(Vol_ref_QI(i), QuantStruct.Parameter_BitWidth);
    Vol_ref_QI_bin  = [Vol_ref_QI_bin; Vol_ref_QI_bin_i];
    
    LLR_ref(i)      = t1 + t2*Vol_ref(i) + t3*Vol_ref(i)^2;
    LLR_ref_Q(i)    = LLR_ref(i) * (2^QuantStruct.Parameter_Fra_BitWidth);
    LLR_ref_Q(i)    = round(LLR_ref_Q(i));                                  % Makes LLR_ref_Q an integer
    if (LLR_ref_Q(i) >= 0)
        LLR_ref_Q(i)  = min(LLR_ref_Q(i), parameterMax);                    % Restricts LLR_ref_Q to be <= parameterMax
    else
        LLR_ref_Q(i)  = max(LLR_ref_Q(i), parameterMin+1);                  % Restricts LLR_ref_Q to be >= parameterMin ???????? (why +1)
    end
    LLR_ref_QI(i)   = LLR_ref_Q(i);                                         % Stores LLR_ref_Q in a new variable
    LLR_ref_Q(i)    = LLR_ref_Q(i)/(2^QuantStruct.Parameter_Fra_BitWidth);  % Returns LLR_ref_Q    
    LLR_ref_QI_bin_i= dec2sbin(LLR_ref_QI(i), QuantStruct.Parameter_BitWidth);
    LLR_ref_QI_bin = [LLR_ref_QI_bin; LLR_ref_QI_bin_i];
end