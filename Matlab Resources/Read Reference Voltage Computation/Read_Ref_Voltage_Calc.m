%% /---Confidentiality Notice Start--/
%  TexasLDPC Inc Confidential.
%  � Copyright 2015 to the present, TexasLDPC Inc.
%  All the software contained in these files are protected by United States
%  copyright law and may not be reproduced, distributed, transmitted,
%  displayed, published or broadcast without the prior written permission
%  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
%  or other notice from copies of the content.
%  /---Confidentiality Notice End--/
%
%  Software to compute the optimum read reference voltages and their
%  corresponding LLRs for LDPC decoders using a mutual information (MI)
%  based method
%  Osso Vahabzadeh
%  TexasLDPC Inc
%  August 2015

%%
tic
close all; clear;

%  This software computes the optimum read reference voltages so that the
%  mutual information between the inputs and outputs of a system with BPSK
%  signaling and asymmetric AWGN is maximized. Moreover, the corresponding
%  LLRs and their signed binary representations are computed for a
%  given precision. The following parameters are computed for 1, 3, 5, and
%  7 channel reads:
%
%  q_opt            : Vector of optimum read reference voltages storing 7 values
%  I_i_max          : Maximum achievable mutual information when we have i channel reads where i=1,3,5,7
%  LLR_ref          : Vector of LLRs corresponding to the optimized read reference voltages
%  LLR_ref_QI       : Quantized values of LLR_ref in decimal format
%  LLR_ref_QI_bin   : Signed binary representation of LLR_ref_QI

%% Initialization:

m_0                     =  1;                           % Mean of the distribution of the noise samples for bit 0
m_1                     = -1;                           % Mean of the distribution of the noise samples for bit 1
d_SNR                   = 0.1;                          % Step size for SNR range
SNR_dB_b0               = 2.8 : d_SNR : 6.4;            % SNR per dB for bit 0
SNR_dB_b1               = SNR_dB_b0;                    % SNR per dB for bit 1
No_reads                = 7;                            % 1, 3, 5, or 7
q_step                  = 0.05;                         % Step size for the read reference voltages
C.H_col                 = 72;                           % No of columns of the base matrix
C.H_row                 = 12;                           % No of rows of the base matrix
plot_figure             = 0;                            % 0 for not to plot & 1 for plot
Extended_result_display = 'No';
QuantStruct.Parameter_Int_BitWidth  = 10;               % Bit precision of the integer part of the read reference voltages
QuantStruct.Parameter_Fra_BitWidth  = 22;               % Bit precision of the fraction part of the read reference voltages

%% Calculation of Noise Parameters:

RATE            = 1 - C.H_row/C.H_col;
V_read_ref      = zeros(length(SNR_dB_b0), No_reads);
LLR_read_ref    = zeros(length(SNR_dB_b0), No_reads);
QuantStruct.Parameter_BitWidth      = QuantStruct.Parameter_Int_BitWidth+QuantStruct.Parameter_Fra_BitWidth;

if size(SNR_dB_b0, 2) == 1
    file_name   = ['Rate_0.', num2str(round(100*RATE)), '_SNR0_', num2str(SNR_dB_b0), '_dB', '_SNR1_', num2str(SNR_dB_b1), '_dB'];        
%     file_name_llr     = ['LLR_Ref_', 'Rate_0.', num2str(round(100*RATE)), '_SNR0_', num2str(SNR_dB_b0), '_dB', '_SNR1_', num2str(SNR_dB_b1), '_dB'];
else    
    file_name   = ['Rate_0.', num2str(round(100*RATE)), '_SNR0_', num2str(SNR_dB_b0(1)), '_', num2str(SNR_dB_b0(end)), '_dB', '_SNR1_', ...
        num2str(SNR_dB_b1(1)), '_', num2str(SNR_dB_b1(end)), '_dB', '_SL', num2str(d_SNR)];
%     file_name_llr=['LLR_Ref_', 'Rate_0.', num2str(round(100*RATE)), '_SNR0_', num2str(SNR_dB_b0(1)), '_', num2str(SNR_dB_b0(end)), '_dB', '_SNR1_', ...
%         num2str(SNR_dB_b1(1)), '_', num2str(SNR_dB_b1(end)), '_dB', '_SL', num2str(d_SNR)];
end

file_name_vol               = strcat('Vol_Ref_', file_name)         ;
file_name_llr               = strcat('LLR_Ref_', file_name)         ;

file_name_vol_coe           = strcat(file_name_vol, '.coe')         ;
file_name_llr_coe           = strcat(file_name_llr, '.coe')         ;
file_name_vol_llr_matlab    = strcat('Vol_', file_name_llr, '.mat') ;

fid_vol_coe = fopen(file_name_vol_coe, 'wt');
fprintf(fid_vol_coe, 'memory_initialization_radix=2;\n');
fprintf(fid_vol_coe, 'memory_initialization_vector=\n');

fid_llr_coe = fopen(file_name_llr_coe, 'wt');
fprintf(fid_llr_coe, 'memory_initialization_radix=2;\n');
fprintf(fid_llr_coe, 'memory_initialization_vector=\n');

for i_snr = 1 : length(SNR_dB_b0)
    
    SNR_b0      = 10^(SNR_dB_b0(i_snr)/10);
    SNR_b1      = 10^(SNR_dB_b1(i_snr)/10);
    var_0       = 1.0/(2*SNR_b0*RATE);          % Variance
    var_1       = 1.0/(2*SNR_b1*RATE);
    sigma_0     = sqrt(var_0);                  % Standard deviation
    sigma_1     = sqrt(var_1);
    q0_low      = min(m_1-4*sigma_1, m_0-4*sigma_0);
    q0_up       = max(m_0+4*sigma_1, m_1+4*sigma_1);
    q0_max_abs  = max(abs(q0_low),abs(q0_up));
    range_pos   = 0 : q_step : q0_max_abs;
    q0          = [-range_pos(end:-1:1), range_pos(2:end)];
    
    LLR_1r      = zeros(1, 2);
    LLR_3r      = zeros(1, 4);
    LLR_5r      = zeros(1, 6);
    LLR_7r      = zeros(1, 8);
    
    %% 1 Channel Read:
    
    I_1         = zeros(1, length(q0));
    
    for i = 1 : length(q0)
        p_01    = 1 - q_func((m_0-q0(i))/sigma_0);   p_02 = q_func((m_0-q0(i))/sigma_0);
        p_11    = 1 - q_func((-m_1+q0(i))/sigma_1);  p_12 = q_func((-m_1+q0(i))/sigma_1);
        
        I_1(i)  = entropy((p_01+p_12)/2, (p_02+p_11)/2) - 0.5*entropy(p_01, p_12, p_02, p_11);
    end
    
    i_I_1_max   = find(I_1(:) == max(I_1));
    % For very high SNRs, it is possible that highest mutual information is
    % obtained for more than one voltage. In such cases:
%     if length (i_I_1_max) > 1
%         if mod(length (i_I_1_max),2) == 1
%             i_I_1_max = i_I_1_max ((1+length(i_I_1_max))/2);
%         else
%             i_I_1_max = i_I_1_max (length(i_I_1_max)/2);
%         end
%     end
    
    q0_opt      = q0(i_I_1_max);
    q_opt       = q0_opt;
    I_1_max     = I_1(i_I_1_max);
    
    % Computing the LLRs corresponding to each read region in the case of 1 reads:
    
    LLR_1r (1) = log2((1-q_func((q0_opt-m_0)/sigma_0))  / (1-q_func((q0_opt-m_1)/sigma_1)));
    LLR_1r (2) = log2(q_func((q0_opt-m_0)/sigma_0)      / q_func((q0_opt-m_1)/sigma_1));
    
    %% 3 Channel Reads
    
    q1                      = q0( find(q0 == q0_opt)+1 : end );
    q1_p                    = q0( 1 : find(q0 == q0_opt)-1 );
    I_3                     = zeros(length(q1), length(q1_p));
    
    for i = 1 : length(q1)
        for j = 1 : length(q1_p)
            p_01 = 1 - q_func((m_0-q1(i))/sigma_0)                              ;   p_11 = 1 - q_func((-m_1+q1_p(j))/sigma_1)                               ;               
            p_02 = q_func((m_0-q1(i))/sigma_0) - q_func((m_0-q0_opt)/sigma_0)   ;   p_12 = q_func((-m_1+q1_p(j))/sigma_1) - q_func((-m_1+q0_opt)/sigma_1)   ;
            p_03 = q_func((m_0-q0_opt)/sigma_0) - q_func((m_0-q1_p(j))/sigma_0) ;   p_13 = q_func((-m_1+q0_opt)/sigma_1) - q_func((-m_1+q1(i))/sigma_1)     ;
            p_04 = q_func((m_0-q1_p(j))/sigma_0)                                ;   p_14 = q_func((-m_1+q1(i))/sigma_1)                                     ;
            
            I_3(i,j)        = entropy((p_01+p_14)/2, (p_02+p_13)/2, (p_03+p_12)/2, (p_04+p_11)/2) - 0.5*entropy(p_01, p_02, p_03, p_04, p_14, p_13, p_12, p_11);
        end
    end
    
    [i_I_3_max, j_I_3_max]  = find(I_3 == max(I_3(:)));
    % For very high SNRs, it is possible that highest mutual information is
    % obtained for more than one voltage. In such cases:
%     if length (i_I_3_max) > 1
%         if mod(length (i_I_3_max),2) == 1
%             i_I_3_max = i_I_3_max ((1+length(i_I_3_max))/2);
%             j_I_3_max = j_I_3_max ((1+length(j_I_3_max))/2);
%         else
%             i_I_3_max = i_I_3_max (length(i_I_3_max)/2);
%             j_I_3_max = j_I_3_max (length(j_I_3_max)/2+1);
%         end
%     end
    
    
    q1_opt                  = q1(i_I_3_max);
    q1_p_opt                = q1_p(j_I_3_max);
    q_opt                   = [q1_p_opt; q_opt; q1_opt];
    I_3_max                 = I_3(i_I_3_max, j_I_3_max);
    
    % Computing the LLRs corresponding to each read region in the case of 3 reads:
    
    LLR_3r (1) = log2((1-q_func((q1_p_opt-m_0)/sigma_0))                               / (1-q_func((q1_p_opt-m_1)/sigma_1)));
    LLR_3r (2) = log2((q_func((q1_p_opt-m_0)/sigma_0)-q_func((q0_opt-m_0)/sigma_0))    / (q_func((q1_p_opt-m_1)/sigma_1)-q_func((q0_opt-m_1)/sigma_1)));
    LLR_3r (3) = log2((q_func((q0_opt-m_0)/sigma_0)-q_func((q1_opt-m_0)/sigma_0))      / (q_func((q0_opt-m_1)/sigma_1)-q_func((q1_opt-m_1)/sigma_1)));
    LLR_3r (4) = log2(q_func((q1_opt-m_0)/sigma_0)                                     / q_func((q1_opt-m_1)/sigma_1));
    
    %% 5 Channel Reads:
    
    q2                      = q1( find(q1 == q1_opt)+1 : end );
    q2_p                    = q1_p( 1 : find(q1_p == q1_p_opt)-1 );
    I_5                     = zeros(length(q2), length(q2_p));
    
    for i = 1 : length(q2)
        for j = 1 : length(q2_p)
            p_01            = 1 - q_func((m_0-q2(i))/sigma_0);                                     p_04 = q_func((m_0-q0_opt)/sigma_0) - q_func((m_0-q1_p_opt)/sigma_0);
            p_02            = q_func((m_0-q2(i))/sigma_0) - q_func((m_0-q1_opt)/sigma_0);          p_05 = q_func((m_0-q1_p_opt)/sigma_0) - q_func((m_0-q2_p(j))/sigma_0);
            p_03            = q_func((m_0-q1_opt)/sigma_0) - q_func((m_0-q0_opt)/sigma_0);         p_06 = q_func((m_0-q2_p(j))/sigma_0);
            p_11            = 1 - q_func((-m_1+q2_p(j))/sigma_1);                                  p_14 = q_func((-m_1+q0_opt)/sigma_1) - q_func((-m_1+q1_opt)/sigma_1);
            p_12            = q_func((-m_1+q2_p(j))/sigma_1) - q_func((-m_1+q1_p_opt)/sigma_1);    p_15 = q_func((-m_1+q1_opt)/sigma_1) - q_func((-m_1+q2(i))/sigma_1);
            p_13            = q_func((-m_1+q1_p_opt)/sigma_1) - q_func((-m_1+q0_opt)/sigma_1);     p_16 = q_func((-m_1+q2(i))/sigma_1);
            
            I_5(i,j)        = entropy((p_01+p_16)/2, (p_02+p_15)/2, (p_03+p_14)/2, (p_04+p_13)/2, (p_05+p_12)/2, (p_06+p_11)/2) - ...
                0.5*entropy(p_01, p_02, p_03, p_04, p_05, p_06, p_16, p_15, p_14, p_13, p_12, p_11);
        end
    end
    
    [i_I_5_max, j_I_5_max]  = find(I_5 == max(I_5(:)));
    q2_opt                  = q2(i_I_5_max);
    q2_p_opt                = q2_p(j_I_5_max);
    q_opt                   = [q2_p_opt; q_opt; q2_opt];
    I_5_max                 = I_5(i_I_5_max, j_I_5_max);
    
    % Computing the LLRs corresponding to each read region in the case of 5 reads:
    
    LLR_5r (1) = log2((1-q_func((q2_p_opt-m_0)/sigma_0))                               / (1-q_func((q2_p_opt-m_1)/sigma_1)));
    LLR_5r (2) = log2((q_func((q2_p_opt-m_0)/sigma_0)-q_func((q1_p_opt-m_0)/sigma_0))  / (q_func((q2_p_opt-m_1)/sigma_1)-q_func((q1_p_opt-m_1)/sigma_1)));
    LLR_5r (3) = log2((q_func((q1_p_opt-m_0)/sigma_0)-q_func((q0_opt-m_0)/sigma_0))    / (q_func((q1_p_opt-m_1)/sigma_1)-q_func((q0_opt-m_1)/sigma_1)));
    LLR_5r (4) = log2((q_func((q0_opt-m_0)/sigma_0)-q_func((q1_opt-m_0)/sigma_0))      / (q_func((q0_opt-m_1)/sigma_1)-q_func((q1_opt-m_1)/sigma_1)));
    LLR_5r (5) = log2((q_func((q1_opt-m_0)/sigma_0)-q_func((q2_opt-m_0)/sigma_0))      / (q_func((q1_opt-m_1)/sigma_1)-q_func((q2_opt-m_1)/sigma_1)));
    LLR_5r (6) = log2(q_func((q2_opt-m_0)/sigma_0)                                     / q_func((q2_opt-m_1)/sigma_1));
    
    %% 7 Channel Reads:
    
    q3                      = q2( find(q2 == q2_opt)+1 : end );
    q3_p                    = q2_p( 1 : find(q2_p == q2_p_opt)-1 );
    I_7                     = zeros(length(q3), length(q3_p));
    
    for i = 1 : length(q3)
        for j = 1 : length(q3_p)
            p_01            = 1 - q_func((m_0-q3(i))/sigma_0);                                     p_05 = q_func((m_0-q0_opt)/sigma_0)   - q_func((m_0-q1_p_opt)/sigma_0);
            p_02            = q_func((m_0-q3(i))/sigma_0)  - q_func((m_0-q2_opt)/sigma_0);         p_06 = q_func((m_0-q1_p_opt)/sigma_0) - q_func((m_0-q2_p_opt)/sigma_0);
            p_03            = q_func((m_0-q2_opt)/sigma_0) - q_func((m_0-q1_opt)/sigma_0);         p_07 = q_func((m_0-q2_p_opt)/sigma_0) - q_func((m_0-q3_p(j))/sigma_0);
            p_04            = q_func((m_0-q1_opt)/sigma_0) - q_func((m_0-q0_opt)/sigma_0);         p_08 = q_func((m_0-q3_p(j))/sigma_0);
            p_11            = 1 - q_func((-m_1+q3_p(j))/sigma_1);                                  p_15 = q_func((-m_1+q0_opt)/sigma_1) - q_func((-m_1+q1_opt)/sigma_1);
            p_12            = q_func((-m_1+q3_p(j))/sigma_1)  - q_func((-m_1+q2_p_opt)/sigma_1);   p_16 = q_func((-m_1+q1_opt)/sigma_1) - q_func((-m_1+q2_opt)/sigma_1);
            p_13            = q_func((-m_1+q2_p_opt)/sigma_1) - q_func((-m_1+q1_p_opt)/sigma_1);   p_17 = q_func((-m_1+q2_opt)/sigma_1) - q_func((-m_1+q3(i))/sigma_1);
            p_14            = q_func((-m_1+q1_p_opt)/sigma_1) - q_func((-m_1+q0_opt)/sigma_1);     p_18 = q_func((-m_1+q3(i))/sigma_1);
            
            I_7(i,j)        = entropy((p_01+p_18)/2, (p_02+p_17)/2, (p_03+p_16)/2, (p_04+p_15)/2, (p_05+p_14)/2, (p_06+p_13)/2, (p_07+p_12)/2, (p_08+p_11)/2) - ...
                0.5*entropy(p_01, p_02, p_03, p_04, p_05, p_06, p_07, p_08, p_18, p_17, p_16, p_15, p_14, p_13, p_12, p_11);
        end
    end
    
    [i_I_7_max, j_I_7_max]  = find(I_7 == max(I_7(:)));
    q3_opt                  = q3(i_I_7_max);
    q3_p_opt                = q3_p(j_I_7_max);
    q_opt                   = [q3_p_opt; q_opt; q3_opt];
    I_7_max                 = I_7(i_I_7_max, j_I_7_max);
    
    % Computing the LLRs corresponding to each read region in the case of 7 reads:
    
    LLR_7r (1) = log2((1-q_func((q3_p_opt-m_0)/sigma_0))                               / (1-q_func((q3_p_opt-m_1)/sigma_1)));
    LLR_7r (2) = log2((q_func((q3_p_opt-m_0)/sigma_0)-q_func((q2_p_opt-m_0)/sigma_0))  / (q_func((q3_p_opt-m_1)/sigma_1)-q_func((q2_p_opt-m_1)/sigma_1)));
    LLR_7r (3) = log2((q_func((q2_p_opt-m_0)/sigma_0)-q_func((q1_p_opt-m_0)/sigma_0))  / (q_func((q2_p_opt-m_1)/sigma_1)-q_func((q1_p_opt-m_1)/sigma_1)));
    LLR_7r (4) = log2((q_func((q1_p_opt-m_0)/sigma_0)-q_func((q0_opt-m_0)/sigma_0))    / (q_func((q1_p_opt-m_1)/sigma_1)-q_func((q0_opt-m_1)/sigma_1)));
    LLR_7r (5) = log2((q_func((q0_opt-m_0)/sigma_0)-q_func((q1_opt-m_0)/sigma_0))      / (q_func((q0_opt-m_1)/sigma_1)-q_func((q1_opt-m_1)/sigma_1)));
    LLR_7r (6) = log2((q_func((q1_opt-m_0)/sigma_0)-q_func((q2_opt-m_0)/sigma_0))      / (q_func((q1_opt-m_1)/sigma_1)-q_func((q2_opt-m_1)/sigma_1)));
    LLR_7r (7) = log2((q_func((q2_opt-m_0)/sigma_0)-q_func((q3_opt-m_0)/sigma_0))      / (q_func((q2_opt-m_1)/sigma_1)-q_func((q3_opt-m_1)/sigma_1)));
    LLR_7r (8) = log2(q_func((q3_opt-m_0)/sigma_0)                                     / q_func((q3_opt-m_1)/sigma_1));
    
    %% Quantization of the read reference voltages and LLRs
    
    [Vol_ref_QI, Vol_ref_QI_bin, LLR_ref, LLR_ref_QI, LLR_ref_QI_bin]   = vol_llr_ref_quant(q_opt, m_0, m_1, SNR_dB_b0(i_snr), SNR_dB_b1(i_snr), RATE, QuantStruct);
    
    %% Record the LLRs corresponding to the optimum read reference voltages
    
    if i_snr ~= length(SNR_dB_b0)
        fprintf(fid_vol_coe, '%s%s%s%s%s%s%s \n', num2str(Vol_ref_QI_bin(7,:)), num2str(Vol_ref_QI_bin(6,:)), num2str(Vol_ref_QI_bin(5,:)),...
            num2str(Vol_ref_QI_bin(4,:)), num2str(Vol_ref_QI_bin(3,:)), num2str(Vol_ref_QI_bin(2,:)), num2str(Vol_ref_QI_bin(1,:)));
        fprintf(fid_llr_coe, '%s%s%s%s%s%s%s \n', num2str(LLR_ref_QI_bin(7,:)), num2str(LLR_ref_QI_bin(6,:)), num2str(LLR_ref_QI_bin(5,:)),...
            num2str(LLR_ref_QI_bin(4,:)), num2str(LLR_ref_QI_bin(3,:)), num2str(LLR_ref_QI_bin(2,:)), num2str(LLR_ref_QI_bin(1,:)));
    else
        fprintf(fid_vol_coe, '%s%s%s%s%s%s%s;', num2str(Vol_ref_QI_bin(7,:)), num2str(Vol_ref_QI_bin(6,:)), num2str(Vol_ref_QI_bin(5,:)),...
            num2str(Vol_ref_QI_bin(4,:)), num2str(Vol_ref_QI_bin(3,:)), num2str(Vol_ref_QI_bin(2,:)), num2str(Vol_ref_QI_bin(1,:)));
        fprintf(fid_llr_coe, '%s%s%s%s%s%s%s;', num2str(LLR_ref_QI_bin(7,:)), num2str(LLR_ref_QI_bin(6,:)), num2str(LLR_ref_QI_bin(5,:)),...
            num2str(LLR_ref_QI_bin(4,:)), num2str(LLR_ref_QI_bin(3,:)), num2str(LLR_ref_QI_bin(2,:)), num2str(LLR_ref_QI_bin(1,:)));
    end
    
    %% Display the results:
    fprintf('SNR_b0 = %1.2f (dB), SNR_b1 = %1.2f (dB)\n\n', SNR_dB_b0(i_snr), SNR_dB_b1(i_snr))
    
    fprintf('Optimized read reference voltages for 1, 3, 5, and 7 reads where the corresponding read vectors are:\n\n1 read  :  q0_opt, \n3 reads : [q1''_opt, q0_opt,  q1_opt], \n5 reads : [q2''_opt, q1''_opt, q0_opt,  q1_opt, q2_opt], \n7 reads : [q3''_opt, q2''_opt, q1''_opt, q0_opt, q1_opt, q2_opt, q3_opt]\n\nwhere:\n\n')
    
    fprintf('q3''_opt =  %4.4f,\nq2''_opt =  %4.4f,\nq1''_opt =  %4.4f,\nq0_opt  =  %4.4f,\nq1_opt  =  %4.4f,\nq2_opt  =  %4.4f,\nq3_opt  =  %4.4f\n\n', ...
        q3_p_opt, q2_p_opt, q1_p_opt, q0_opt, q1_opt, q2_opt, q3_opt)
    
    fprintf('Signed binary representation of the optimum read reference voltages:\n\n')
    fprintf('V_q3'' =  %s\nV_q2'' =  %s\nV_q1'' =  %s\nV_q0  =  %s\nV_q1  =  %s\nV_q2  =  %s\nV_q3  =  %s\n\n',...
        Vol_ref_QI_bin(1,:), Vol_ref_QI_bin(2,:), Vol_ref_QI_bin(3,:), Vol_ref_QI_bin(4,:), Vol_ref_QI_bin(5,:),...
        Vol_ref_QI_bin(6,:), Vol_ref_QI_bin(7,:))
    
    fprintf('LLRs corresponding to the optimum read reference voltages:\n\nLLR_ref = ')
    disp(num2str(LLR_ref'))
    
    fprintf('\nSigned binary representation of the LLRs corresponding to the optimum read reference voltages:\n\n')
    fprintf('LLR_q3'' =  %s\nLLR_q2'' =  %s\nLLR_q1'' =  %s\nLLR_q0  =  %s\nLLR_q1  =  %s\nLLR_q2  =  %s\nLLR_q3  =  %s\n\n',...
        LLR_ref_QI_bin(1,:), LLR_ref_QI_bin(2,:), LLR_ref_QI_bin(3,:), LLR_ref_QI_bin(4,:), LLR_ref_QI_bin(5,:),...
        LLR_ref_QI_bin(6,:), LLR_ref_QI_bin(7,:))
    
    if strcmp(Extended_result_display, 'Yes')
        fprintf('Mutual Information (MI) maximized for 1, 3, 5, and 7 channel reads:\n\n')
        
        fprintf('I_1_max =  %4.4f,\nI_3_max =  %4.4f,\nI_5_max =  %4.4f,\nI_7_max =  %4.4f\n\n', I_1_max, I_3_max, I_5_max, I_7_max)
        
        fprintf('LLRs corresponding to the 2 read regions generated as a result of 1 read:\n\nLLR \t= ')
        disp(num2str(LLR_1r))
        
        fprintf('\nLLRs corresponding to the 4 read regions generated as a result of 3 reads:\n\nLLR \t= ')
        disp(num2str(LLR_3r))
        
        fprintf('\nLLRs corresponding to the 6 read regions generated as a result of 5 reads:\n\nLLR \t= ')
        disp(num2str(LLR_5r))
        
        fprintf('\nLLRs corresponding to the 8 read regions generated as a result of 7 reads:\n\nLLR \t= ')
        disp(num2str(LLR_7r))
        fprintf('\n')
    end
    
    V_read_ref(i_snr, :)    = q_opt;
    LLR_read_ref(i_snr, :)  = LLR_ref;    
    save (file_name_vol_llr_matlab, 'V_read_ref', 'LLR_read_ref')
    
    if plot_figure == 1
        
        [y0, x0] = normal_pdf(m_0, sigma_0, q_step);
        [y1, x1] = normal_pdf(m_1, sigma_1, q_step);
        plot(x0, y0, x1, y1, 'LineWidth', 1.5)
        legend('y_0(q)','y_1(q)')
        xlabel('q');
        ylabel('y(q)')
        title('Voltage Distributions in the BPSK Signaling')
        grid on
        
        figure
        subplot(2, 2, 1)
        plot(q0, I_1, 'r', 'LineWidth', 1.5)
        xlabel('q_0');
        ylabel('I_1(q_0)')
        title('Mutual Information for 1 Read (q_0)')
        grid on
        
        subplot(2, 2, 2)
        mesh(q1, q1_p, I_3')
        xlabel('q_1');
        ylabel('q_1''');
        zlabel('I_3(q_1'', q_1)')
        title('Mutual Information for 3 Reads as a Function of Reads 2 and 3 (q_1'' and q_1)')
        
        subplot(2, 2, 3)
        mesh(q2, q2_p, I_5')
        xlabel('q_2');
        ylabel('q_2''');
        zlabel('I_5(q_2'', q_2)')
        title('Mutual Information for 5 Reads as a Function of Reads 4 and 5 (q_2'' and q_2)')
        
        subplot(2, 2, 4)
        mesh(q3, q3_p, I_7')
        xlabel('q_3');
        ylabel('q_3''');
        zlabel('I_7(q_3'', q_3)')
        title('Mutual Information for 7 Reads as a Function of Reads 6 and 7 (q_3'' and q_3)')
    end
end

fclose(fid_vol_coe);
fclose(fid_llr_coe);

toc