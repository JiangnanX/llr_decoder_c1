%% /---Confidentiality Notice Start--/
%  TexasLDPC Inc Confidential.
%  � Copyright 2015 to the present, TexasLDPC Inc.
%  All the software contained in these files are protected by United States
%  copyright law and may not be reproduced, distributed, transmitted,
%  displayed, published or broadcast without the prior written permission
%  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
%  or other notice from copies of the content.
%  /---Confidentiality Notice End--/
%
%  Software to compute the entropy of an array of probabilities
%  Osso Vahabzadeh
%  TexasLDPC Inc
%  August 2015

%%

function H = entropy(varargin)

%  This function computes the entropy of an arbitrary number of
%  probabilities.

p = zeros(1, nargin);

for i = 1 : nargin
    p(i) = varargin{i};
end

if nnz(p<0) || nnz(p>1)
    warning('Probabilities should always be between 0 and 1!')
else    
    H = 0;    
    for i = 1 : nargin
        if p(i) ~=0
            H = H - p(i)*log2(p(i));
        end
    end    
end

%%

% function H = entropy(p)
%
% %  This function computes the entropy of an array of probabilities p that
% %  correspond to a discrete random variable. If p has only one element, the
% %  function computes the binary entropy function for p and 1-p.
%
% if nnz(p<0) || nnz(p>1)
%     warning('Probabilities should always be between 0 and 1!')
% else
%     if numel(p) == 1
%         if p == 0 || p == 1
%             H = 0;
%         else
%             H = - p*log2(p) - (1-p)*log2(1-p);
%         end
%     else
%         if sum(p(:)) ~= 1
%             warning('Sum of probabilities should be 1!')
%         else
%             H = 0;
%             [r, c] = size(p);
%             for i = 1 : r
%                 for j = 1 : c
%                     if p(i,j) ~= 0
%                         H = H - p(i,j)*log2(p(i,j));
%                     end
%                 end
%             end
%         end
%     end
% end