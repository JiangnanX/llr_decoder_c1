%% /---Confidentiality Notice Start--/
%  TexasLDPC Inc Confidential.
%  � Copyright 2015 to the present, TexasLDPC Inc.
%  All the software contained in these files are protected by United States
%  copyright law and may not be reproduced, distributed, transmitted,
%  displayed, published or broadcast without the prior written permission
%  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
%  or other notice from copies of the content.
%  /---Confidentiality Notice End--/
%
%  Software to compute and plot the binary entropy function for a random
%  variable with two possible outcomes
%  Osso Vahabzadeh
%  TexasLDPC Inc
%  August 2015

%%

close all; clear; clc

%  For a random variable with two possible outcomes with probabilties p and 
%  1 - p, this m-file computes and plots the binary entropy function H

%%

p = 0 : 1e-3 : 1;
H = zeros(1, length(p));
for i = 1 : length(p)
    H(i) = entropy([p(i), 1-p(i)]);
end
plot(p, H)
xlabel('p')
ylabel('H(p)')