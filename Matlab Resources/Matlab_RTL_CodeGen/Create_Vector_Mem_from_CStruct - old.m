% /---Confidentiality Notice Start--/
%   Texas A&M University Confidential.
%   Contact: Office of Technology Commercialization.
%   The material contained in this software and the associated papers
%   have features which are contained in a patent disclosure for LDPC decoding architectures 
%   filed by Gwan Choi, John Junkins , Kiran Gunnam. 
%   For more information about the licensing process, please contact:
%   Alex Garcia
%   Department: Licensing & Intellectual Property Management
%   Title: Licensing Associate
%   Email: alegarza@tamu.edu
%   Phone: (979) 458-2640
%   Fax: (979) 845-2684
% /---Confidentiality Notice End--/
%%Function: Create_Vector_Mem_from_CStruct  
%%Kiran Gunnam 
%%Texas A&M University 
%%This function creates a vector memory from different vectors
function [tmp_ROM tmp_base_ROM tmp_layer_base_ROM tmp_layer_base_MCbase_ROM] = ...
               Create_Vector_Mem_from_CStruct(MC,var_name,layer_base_flag,share_flag,special_case,clk_flag,wl,memory_type,combo)

%MC.Nc = Number_of_Codes_supported

if(nargin ==2)
    layer_base_flag           = 0;
    share_flag                = 0; %Information is not shared among different H matrices
    special_case              = 0;
    clk_flag                  = 0;
    wl                        = 0;
    memory_type               = 'au';%au-auto, pb-pipeline dist. ram
    tmp_layer_base_ROM        = [];
    tmp_layer_base_MCbase_ROM = [];
    combo                     =  0;
end

if(nargin ==3)
    share_flag                = 0; %Information is not shared among different H matrices
    special_case              = 0;
    clk_flag                  = 0;
    wl                        = 0;
    memory_type               = 'au';%au-auto, pb-pipeline dist. ram
    tmp_layer_base_ROM        = [];
    tmp_layer_base_MCbase_ROM = [];
    combo                     =  0;
end

if(nargin ==4)
    special_case              = 0;
    clk_flag                  = 0;
    wl                        = 0;
    memory_type               = 'au';%au-auto, pb-pipeline dist. ram
    combo                     =  0;
end

if(nargin ==5)
    clk_flag                  = 0;
    wl                        = 0;
    memory_type               = 'au';%au-auto, pb-pipeline dist. ram
    combo                     =  0;
end

if(nargin ==6)
    wl                        = 0;
    memory_type               = 'au';%au-auto, pb-pipeline dist. ram
    combo                     =  0;
end

if(nargin ==7)
    memory_type               = 'au';%au-auto, pb-pipeline dist. ram
    combo                     =  0;
end

if(nargin ==8)
    combo                     =  0; 
end

if(share_flag ==0)
    %% Create data for tmp ROM and the base information for different H matrices
    i =1;
    tmp_base_ROM(i)  = 0;
    j =1;
    tmp_vector =MC(i).C.tmp;
    [mm nn] = size(tmp_vector);
    if(nn~=1) %%i.e. the variable is a matrix. so convert into matrix -neglect the entries for non-valid elements
    tmp_vector  = (MC(i).C.tmp(j,1:MC(i).C.dc(j)))';
    for j = 2:MC(i).C.H_row
        tmp          = (MC(i).C.tmp(j,1:MC(i).C.dc(j)))' ;
        tmp_vector  = [tmp_vector
                       tmp];
    end
    end
    %this simple reshape is not valid as C struct matrices have non-valid elements in some
    %positions.
    %convert the tmp matrix to column vector take the elements from
    %row order. since reshape takes the elements from column order, 
    %transpose the tmp matrix before reshape.
    %tmp_vector       = reshape((MC(i).C.tmp)',[],1); 


    tmp_ROM          = tmp_vector; 
    for i= 2:MC(1).Nc
     tmp_base_ROM(i)  = length(tmp_ROM); %%note that indexing in hardware starts from 0
     tmp_vector =MC(i).C.tmp;
     j =1;
     if(nn~=1) %%i.e. the variable is a matrix. so convert into matrix -neglect the entries for non-valid elements
         tmp_vector  = (MC(i).C.tmp(j,1:MC(i).C.dc(j)))';
             for j = 2:MC(i).C.H_row
                tmp          = (MC(i).C.tmp(j,1:MC(i).C.dc(j)))'; 
                tmp_vector  = [tmp_vector
                               tmp];
             end
     end

     tmp_ROM          = [tmp_ROM
                        tmp_vector];
    end
    addr_bus_name = 'addr';
    addr_bus_name = strcat(var_name,addr_bus_name);
    addr_bus_name_of_var = addr_bus_name;
    addr_bus_name_of_var_first =addr_bus_name_of_var;
    addr_bus_name_is_CodeId =0;
    base_flag    = 1;
    look_ahead_steps=0;
    latch_addr=1;
    if(combo)
     Gen_OFC_ROM_verilog(MC,var_name,addr_bus_name,addr_bus_name_is_CodeId,base_flag,clk_flag,wl,memory_type,look_ahead_steps,latch_addr,combo);
    else
     Gen_OFC_ROM_verilog(tmp_ROM,var_name,addr_bus_name,addr_bus_name_is_CodeId,base_flag,clk_flag,wl,memory_type);
    end
    %%Gen_ROM_verilog(tmp_ROM,var_name);
    %%Gen_ROM_verilog(tmp_base_ROM,strcat(var_name,'_base'));
    addr_bus_name = 'CodeId';
    addr_bus_name_is_CodeId =1;
    base_flag    = 1;
    clk_flag_for_base = 1;
    var_base= strcat(var_name,'_base');
    var_base_first = var_base;
    wl =0;
    Gen_OFC_ROM_verilog(tmp_base_ROM,var_base,addr_bus_name,addr_bus_name_is_CodeId,base_flag,clk_flag_for_base,wl,memory_type);
    var_rom_file_name      = strcat('../hdl_gen/','OFC_ROM.v');
    if(layer_base_flag == 0)
    fid1 = fopen(var_rom_file_name,'a');
    if(nn~=1) %%for the data which is originally matrix
        fprintf(fid1, 'assign %s = linear_index + %s ;\n',addr_bus_name_of_var,var_base); 
    else %% %%for the data which is originally vector
        if(special_case==2)fprintf(fid1, 'assign %s = BC + %s ;\n',addr_bus_name_of_var,var_base); 
        elseif(special_case==5) 
        fprintf(fid1, 'assign %s = valid_entry_rom_address + %s ;\n',addr_bus_name_of_var,var_base); 
        else
        fprintf(fid1, 'assign %s = layer + %s ;\n',addr_bus_name_of_var,var_base); 
        end
    end
    fclose(fid1);
    end


    %% Create data for the base information(tmp_layer_ROM) for different layers in each H
    %% matrix and then create a master base information(tmp_layer_base_MCbaseROM) for this
    if(layer_base_flag == 1)
        i =1;
        tmp_layer_base_MCbase_ROM(i)  = 0;
        tmp_layer_base_ROM           = (cumsum([0 (MC(i).C.dc(1:end-1))']))';
        for i = 2:MC(1).Nc
            tmp                      = (cumsum([0 (MC(i).C.dc(1:end-1))']))';
            tmp_layer_base_ROM       = [tmp_layer_base_ROM 
                                        tmp];
            tmp_layer_base_MCbase_ROM(i)= tmp_layer_base_MCbase_ROM(i-1)+ MC(i-1).C.H_row; 
                                           %%note that indexing in hardware starts from 0
        end
        var_name1 = strcat(var_name,'layer_base_ROM');
        addr_bus_name = 'layer';
        addr_bus_name = strcat(var_name1,addr_bus_name);
        addr_bus_name_of_var = addr_bus_name;
        addr_bus_name_is_CodeId =0;
        base_flag =1;
        wl=0;
        Gen_OFC_ROM_verilog(tmp_layer_base_ROM ,var_name1,addr_bus_name,addr_bus_name_is_CodeId,base_flag,clk_flag,wl,memory_type);
        %%Gen_ROM_verilog(tmp_layer_base_ROM ,var_name1);
        var_name2 = strcat(var_name,'layer_base_MCbase_ROM');
        var_base  = var_name2;
        addr_bus_name = 'CodeId';
        addr_bus_name_is_CodeId =1;
        base_flag    = 1;
        clk_flag_for_base = 1;
        wl =0;
        Gen_OFC_ROM_verilog(tmp_layer_base_MCbase_ROM,var_name2,addr_bus_name,addr_bus_name_is_CodeId,base_flag, clk_flag_for_base,wl, memory_type);
        %%Gen_ROM_verilog(tmp_layer_base_MCbase_ROM,var_name2);
        fid1 = fopen(var_rom_file_name,'a');
        if(special_case==0)
         fprintf(fid1, 'assign %s = layer + %s; \n',addr_bus_name_of_var,var_base); 
         fprintf(fid1, 'assign %s = %s+%s + block_num ;\n',addr_bus_name_of_var_first,var_base_first,var_name1); 
        elseif(special_case==1) %to take care of the DCI address generation
         fprintf(fid1, 'assign %s = DL + %s; \n',addr_bus_name_of_var,var_base); 
         fprintf(fid1, 'assign %s = %s+%s + DB_2L ;\n',addr_bus_name_of_var_first,var_base_first,var_name1); 
        elseif(special_case==4) %to take care of the CI address generation
         fprintf(fid1, 'assign %s = layer + %s; \n',addr_bus_name_of_var,var_base); 
         fprintf(fid1, 'assign %s = %s+%s + block_num_2L ;\n',addr_bus_name_of_var_first,var_base_first,var_name1); 
        end
        
        fclose(fid1);


    end
end

if(share_flag ==1)
    tmp_base_ROM              = [];
    tmp_layer_base_ROM        = [];
    tmp_layer_base_MCbase_ROM = [];
    tmp_ROM       = MC(1).C.tmp;
    addr_bus_name = 'addr';
    addr_bus_name = strcat(var_name,addr_bus_name);
    addr_bus_name_of_var = addr_bus_name;
    addr_bus_name_is_CodeId =0;
    base_flag               =1;
    Gen_OFC_ROM_verilog(tmp_ROM,var_name,addr_bus_name,addr_bus_name_is_CodeId,base_flag,clk_flag,wl,memory_type);
    var_rom_file_name      = strcat('../hdl_gen/','OFC_ROM.v');
    fid1 = fopen(var_rom_file_name,'a');
    fprintf(fid1, 'assign %s = dv;\n',addr_bus_name_of_var); 
    fclose(fid1);
end

if(share_flag ==2)
    tmp_base_ROM              = [];
    tmp_layer_base_ROM        = [];
    tmp_layer_base_MCbase_ROM = [];
    tmp_ROM       = MC(1).C.tmp;
    addr_bus_name = 'addr';
    addr_bus_name = strcat(var_name,addr_bus_name);
    addr_bus_name_of_var = addr_bus_name;
    addr_bus_name_is_CodeId =0;
    base_flag               =1;
    Gen_OFC_ROM_verilog(tmp_ROM,var_name,addr_bus_name,addr_bus_name_is_CodeId,base_flag,clk_flag,wl,memory_type);
    var_rom_file_name      = strcat('../hdl_gen/','OFC_ROM.v');
    fid1 = fopen(var_rom_file_name,'a');
    fprintf(fid1, 'assign %s = CodeId_L;\n',addr_bus_name_of_var); 
    fclose(fid1);
end


