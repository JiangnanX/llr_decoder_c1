% /---Confidentiality Notice Start--/
%   Texas A&M University Confidential.
%   Contact: Office of Technology Commercialization.
%   The material contained in this software and the associated papers
%   have features which are contained in a patent disclosure for LDPC decoding architectures 
%   filed by Gwan Choi, John Junkins , Kiran Gunnam. 
%   For more information about the licensing process, please contact:
%   Alex Garcia
%   Department: Licensing & Intellectual Property Management
%   Title: Licensing Associate
%   Email: alegarza@tamu.edu
%   Phone: (979) 458-2640
%   Fax: (979) 845-2684
% � Copyright 2005 to the present, Texas A&M University System. 
%  All the software contained in these files are protected by United States copyright law
%  and may not be reproduced, distributed, transmitted, displayed, published or broadcast 
%  without the prior written permission of Texas A&M University System. 
%  You may not alter or remove any trademark, copyright or other notice from copies 
%  of the content.
% /---Confidentiality Notice End--/
%%KIRAN GUNNAM 
%%Texas A&M University 
%%December 2005
%%This script calls the other matlab function which generate the 
%%verilog files for the Decoder of irregular LDPC based on the On-the-fly computation
%%paradigm

clear all
close all
clc
%% %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% USER CONFIGURATION START %%%%%%%%%%%%%%%%%%%%%%%%%%% 
Parallelization                     = 140;
M                                   = Parallelization; 

%%Code Specific parameters

%Nc = Number_of_Codes_supported
MC(1).Nc                           = 1;%12;
MC(1).MaxNc                        = 4;
MC(1).MaxH_row                     = 12;
MC(1).MaxH_col                     = 72;
MC(1).Maxdc                        = 24;
MC(1).Maxdv                        = 4;
MC(1).MaxCirculants                = 300;
MC(1).MaxZ                         = Parallelization;%8; %Maximum expansion factor of the circulant
MC(1).MaxCirculantShift            = M;
MC(1).MAX_NUM_ITERATIONS_SUPPORTED = 128;
%%may need even number due to cc block receving inputs to its xor block. giving same input to other blocks wot matter.
MC(1).MaxCodeWordLength            = MC(1).MaxZ* MC(1).MaxH_col;

MC(1).CodeClass                     = 1;%//1==> 802.11n,2 ==>802.16e
CodeClass_wl                        = 4;
MC(1).Sub_Version                   = 1;
Sub_version_wl                      = 4;
MC(1).Version_wl                    = Sub_version_wl+CodeClass_wl;
MC(1).Version                       = strcat(num2str(MC(1).Version_wl),'''b', ...
                                               dec2bin(MC(1).CodeClass,CodeClass_wl),dec2bin(MC(1).Sub_Version,Sub_version_wl));
                                           






MC(1).MAX_NUM_ITERATIONS_SUPPORTED = 126;%%note that this need to a number that is atleast 2^x-2. other wise there will
                                         %%be issues with the iteration
                                         %%counter logic 
MC(1).Parallelization              = Parallelization;
MC(1).N_HD_Buff                    = 2; 
MC(1).N_Qs_Buff                    = 2;
MC(1).LLR_ch_ports                 = 3;%16%16;%16%MC(1).Parallelization;  %16;
MC(1).LLR_Ch_wl                    = 5;
%%Extend factor when doing clock domain change for the input and output interfaces must be long enough so that
%%infc_clk can catch it. Here this is based on the decoder clock is 150 MHz interface clock is 75 MHz
MC(1).Extend_Factor_CDC            = 4; 
%%Fixed Point parameters
P_wl                               = 8;
P_shifter_wl                       = P_wl;
HD_shifter_wl                      = 1;
Qs_wl                              = P_wl;
Q_wl                               = 5;
R_wl                               = 5;
M_wl                               = Q_wl-1;
GammaV_wl                          = 4;%M_wl;
GammaOffset_wl                     = 2;%%see the table on gamma_offset at line 135. if this table changes, then this need to change.
iteration_wl                       = ceil(log2(MC(1).MAX_NUM_ITERATIONS_SUPPORTED));
correction_method                  = 1 % 1 for SCALING;% 2 for OFFSET
%% Create the Master C Struct(MC) for all the codes supported
for i = 1:MC(1).Nc
    file_name  = strcat('../H_Matrices/C_Struct_CodeId_Flash',num2str(i));
    load(file_name);
    MC(i).C    = C;
    C.Z=M;
    if(C.Z ~= M), disp('Though the OFC architecture can support different circulant sizes with');
        disp('the same parallelization, this matlab script can not presently support automated');
        disp('code generation when M~=Z');
        disp('Make sure that you executed Create_Cstruct.m on the codes you want');
        error('Desired combination of parallelization with the given code is not supported presently.');
    end
end
MC(1).gamma_argument_type    = 'dv';% CI
MC(1).gamma_correction_method =  correction_method;
if(MC(1).gamma_argument_type    == 'dv')
    if(correction_method==2) 
    gamma_offset        = [0 0 0  1  2 3 1 1 1 1 1   1   1   1   1   1   1]; 
    MC(1).GAMMA         = gamma_offset; 
    elseif(correction_method==1) 
    gamma_actual            = 1-[1/2^GammaV_wl 1/2^GammaV_wl 0.1250 0.1875 0.1875 0.2500 0.2500 0.2500 0.2500 0.2500 0.3125  0.3125 0.3125 0.3125 0.5000 0.5000 0.7500];%%TEMP. FILL WITH SIMUALTED VALUES
    %1.0000    1.0000    0.9063    0.8438    0.8125    0.7500    0.7813 0.7500
    MC(1).GAMMA             = (gamma_actual*2^GammaV_wl); 
    end
    MC(1).gamma_argument    = [0:MC(1).Maxdv];
   
     if(any(round(MC(1).GAMMA)~=MC(1).GAMMA))
         error('quantization error in Gamma.');
     end
elseif(MC(1).gamma_argument_type    == 'CI')
    disp('Please calculate gamma values for each circulant in the Create_CStructROM.m');    
    gamma_actual            = [];%circulant based.=gamma=beta*alpha
    MC(1).gamma_argument    = [1:MC(1).MaxCirculants];
end

 % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% USER CONFIGURATION END %%%%%%%%%%%%%%%%% 

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%AUTOMATED CODE GENERATION START %%%%%%%%%
MC(1).QuantStruct.P_wl             = P_wl;
MC(1).QuantStruct.P_shifter_wl     = P_shifter_wl;
MC(1).QuantStruct.HD_shifter_wl    = HD_shifter_wl;
MC(1).QuantStruct.Qs_wl            = Qs_wl;
MC(1).QuantStruct.Q_wl             = Q_wl;
MC(1).QuantStruct.R_wl             = R_wl;
MC(1).QuantStruct.M_wl             = M_wl;
MC(1).GammaV_wl                    = GammaV_wl;
MC(1).GammaOffset_wl               = GammaOffset_wl;
MC(1).PIPELINE_DEPTH               = 8;%% 
MC(1).CONFIGURABLE_PIPELINE_DEPTHS = [8 9 10 11 12];%% all these 5 tested OK
if(MC(1).PIPELINE_DEPTH == 12);
   MC(1).FS_MEM_TYPE                  = 1;
 %%write of new content to FS Memory in cycle 1. read address to FS_Memory in cycle 2. read out of new content to FS_meory in cycle 3.
 %%addtional 2 cycles of delay after FS is ready from CNU
  %MC(1).FS_MEM_TYPE                  = 2; %this is not needed if FS_MEM_TYPE=1 works. mostly for debug.
 %%can be selected also when distributed registers for FS_memory are preferrred.
 MC(1).CORRECTION_UNIT_PIPELINE     = 2;
 p_shifter_pipeline_flag =2 %2 
 hd_shifter_pipeline_flag =2 %2
elseif(MC(1).PIPELINE_DEPTH == 11)
   MC(1).FS_MEM_TYPE                  = 3;%
%write of new content to FS Memory in cycle 1. read address to FS_Memory in cycle 2. read out of new content to FS_memory in cycle 2 using 
%bypass register/write cache register. addtional 1 cycles of delay after FS is ready from CNU 1 clock cycle access delay for FS_new access from FS_memory
MC(1).CORRECTION_UNIT_PIPELINE     = 2;
 p_shifter_pipeline_flag =2 %2 
 hd_shifter_pipeline_flag =2 %2
elseif(MC(1).PIPELINE_DEPTH == 10)
   MC(1).FS_MEM_TYPE                  = 4;
%%write of new content to FS Memory in cycle 1. read address to FS_Memory in cycle 1. read out of new content to FS_memory in cycle 1 using 
%%bypass register for FS_in and bypass register/write cache register for registered version of FS_new addtional 1 cycles of delay
%%after FS is ready from CNU 1 clock cycle access delay for FS_new access from FS_memory
MC(1).CORRECTION_UNIT_PIPELINE     = 2;
p_shifter_pipeline_flag =2 %2 
hd_shifter_pipeline_flag =2 %2
elseif(MC(1).PIPELINE_DEPTH == 9)
   MC(1).FS_MEM_TYPE                  = 4;
   MC(1).CORRECTION_UNIT_PIPELINE     = 1;
%%write of new content to FS Memory in cycle 1. read address to FS_Memory in cycle 1. read out of new content to FS_memory in cycle 1 using 
%%bypass register for FS_in and bypass register/write cache register for registered version of FS_new addtional 1 cycles of delay
%%after FS is ready from CNU 1 clock cycle access delay for FS_new access from FS_memory
 p_shifter_pipeline_flag =2 %2 
 hd_shifter_pipeline_flag =2 %2
elseif(MC(1).PIPELINE_DEPTH == 8)
   MC(1).FS_MEM_TYPE                  = 4;
   MC(1).CORRECTION_UNIT_PIPELINE     = 1;
%%write of new content to FS Memory in cycle 1. read address to FS_Memory in cycle 1. read out of new content to FS_memory in cycle 1 using 
%%bypass register for FS_in and bypass register/write cache register for registered version of FS_new addtional 1 cycles of delay
%%after FS is ready from CNU 1 clock cycle access delay for FS_new access from FS_memory
   p_shifter_pipeline_flag =0 %2 
   hd_shifter_pipeline_flag =2 %2
else
   disp('supported pipeline depths');
   MC(1).CONFIGURABLE_PIPELINE_DEPTHS
    error('PIPELINE DEPTH is not supported');
end

MC(1).P_SHIFTER_PIPELINE  = (p_shifter_pipeline_flag>0);
MC(1).HD_SHIFTER_PIPELINE  = (hd_shifter_pipeline_flag>0);

MC(1).NumFreeze_Slots              = 0;
MC(1).Freeze_Slot                  = (MC(1).Maxdc-1)+1;
%though dc not a index, we will store 1 as 0 2 as 1 and so on. 
%the reason is that we will never have dc equal to 0. So if dc=20,
%then we can use 20 to represent a Freeze_Slot
MC(1).Num_FS_Write_Slots           = 0;
MC(1).Num_FS_Read_Slots            = 0;
%FS_Write_Slot equal to dc and varies based on the layer and the code.
%MC(1).Overhead_per_layer           = MC(1).Num_FS_Write_Slots+MC(1).Num_FS_Read_Slots;
%%MC(1).Overhead_per_layer           = 8;%%==>Make this code specific and
%%layer specific.
%% have idle cycles that are needed at the end of layer due to pipeline dependency.
%%program. dc+overhead per layer<max_dc while having freeze slot as
%%max_dc.

%%If you change the following four parameters, please change Gen_LDPC_Decoder_parameters.m so that
%%LDPC_Decoder_Parameters.h is updated
%TimeSlotCounter_wl                 = ceil(log2(MC(1).Maxdc+MC(1).Overhead_per_layer+MC(1).NumFreeze_Slots));
TimeSlotCounter_wl                 = ceil(log2(MC(1).Maxdc+MC(1).PIPELINE_DEPTH));
%%assuming Overhead_per_layer is less than pipeline depth-which is normal.
%if there are other stall cycles due to memory then may need to modify
%this.
dc_wl                              = ceil(log2(MC(1).Maxdc));
PS_wl                              = 2*M_wl+dc_wl+1;
FS_wl                              = 2*M_wl+dc_wl+1;
layer_wl                           = ceil(log2(MC(1).MaxH_row));

MC(1).FS_wl                        = FS_wl;
MC(1).Max_FanIn_of_convergence_logic = 7; %%maximum of 8 inputs to OR gate in convergence logic

%% Delete all the previous files
disp('Deleting all the previous verilog files in ../hdl_gen/')
delete_prev_files = 0;
a                 = 1;
if(delete_prev_files==0)
    a=input('Type 1 for Yes or 0 for No:')
end

if(a==1)
  delete ../hdl_gen/*
  disp('deleted all the files in ../hdl_gen/');
end
disp('Generating the files in ../hdl_gen/');

%% Create the header file LDPC_Decoder_parameters.h based on the template
%% file LDPC_Decoder_parameters_template.h
[message_str] =Gen_LDPC_Decoder_parameters_header(MC);

%% Generate the OFC ROM. This is the most important module for the architecture.
MC=Create_CStructROM(MC);
save MC.mat MC
disp('MC structure is stored in MC.mat ../matlab/MC.mat');

%% Generate the cyclic shifter for P

module_name = 'P_shifter';freeze_capability =0;
Gen_logshifter_verilog(M,P_shifter_wl,p_shifter_pipeline_flag,module_name,freeze_capability);

%% Generate the cyclic shifter for Hard decisions
module_name = 'HD_shifter';freeze_capability =1;
Gen_logshifter_verilog(M,HD_shifter_wl,hd_shifter_pipeline_flag,module_name,freeze_capability);

%% Generate the adder array for P
cmdString = '..\perl_scripts\P_adder_array.pl';
[perl_result perl_status] = perl(cmdString,num2str(M),num2str(R_wl),num2str(Qs_wl),...
                                  num2str(P_wl),'..\hdl_gen\P_adder_array.v'); 
if(perl_status ~=0), 
    error('Error in Perl script');
else
    disp('P adder array verilog file is created');
end

%% Generate the mux array for P
%%Mux for P. P= (UCVF==1)?LLR_ch:P_in;when iterations ==0;: MODE 1. Iterative decoding. Supported 
%%Mux for P. MODE 2. Iterative synchronization+ Iterative decoding
%%P= (UCVF==1)?LLR_ch:P_in;when iterations ==0
%%P= (UCVF==1)?P_in+delta_LLR_ch:P_in;when iterations >0==>(UCVF==1)?P_in+LLR_ch:P_in;when iterations >0;;
%%not supported as of now. Support can be added easily for this iterarative
%%synchronization by having another adder array for P = P+ delta_LLR_ch
%%delta_LLR_ch comes from the channel from the second iteration to final iteration on the same bus as LLR_ch";

% cmdString = '..\perl_scripts\P_mux_array.pl';
% [perl_result perl_status] = perl(cmdString,num2str(M),num2str(P_wl),num2str(iteration_wl),...
%                                   '..\hdl_gen\P_mux_array.v'); 
% if(perl_status ~=0), 
%     error('Error in Perl script');
% else
%     disp('P mux array verilog file is created');
% end

%% Generate the subtractor array for Q
cmdString = '..\perl_scripts\Q_subtractor_array.pl';
[perl_result perl_status] = perl(cmdString,num2str(M),num2str(P_shifter_wl),num2str(R_wl),...
                                  num2str(Qs_wl),'..\hdl_gen\Q_subtractor_array.v'); 
if(perl_status ~=0), 
    error('Error in Perl script');
else
    disp('Q subtractor array verilog file is created');
end

%% Generate the CNU_PS array 
cmdString = '..\perl_scripts\CNU_PS_array.pl';

[perl_result perl_status] = perl(cmdString,num2str(M),num2str(Q_wl),num2str(layer_wl),num2str(TimeSlotCounter_wl),...
                                 num2str(dc_wl),num2str(PS_wl),'..\hdl_gen\CNU_PS_array.v'); 
if(perl_status ~=0), 
    error('Error in Perl script');
else
    disp('CNU_PS_array verilog file is created');
end

%% Generate the R selection array 
cmdString = '..\perl_scripts\R_Selection_array.pl';

[perl_result perl_status] = perl(cmdString,num2str(M),num2str(FS_wl),num2str(TimeSlotCounter_wl),...
                                 num2str(dc_wl),num2str(R_wl),'..\hdl_gen\R_Selection_array.v'); 
if(perl_status ~=0), 
    error('Error in Perl script');
else
    disp('R_Selection_array verilog file is created');
end


%% Generate the Circulant_Scaling array 
if(correction_method==1) 
cmdString = '..\perl_scripts\Circulant_Scaling_array.pl';
[perl_result perl_status] = perl(cmdString,num2str(M),num2str(Qs_wl),num2str(GammaV_wl),...
                                 num2str(Q_wl),'..\hdl_gen\Circulant_correction_array.v'); 
elseif(correction_method==2) 
cmdString = '..\perl_scripts\Circulant_correction_offset_array.pl';
[perl_result perl_status] = perl(cmdString,num2str(M),num2str(Qs_wl),num2str(GammaOffset_wl),...
                                 num2str(Q_wl),'..\hdl_gen\Circulant_correction_array.v'); 
end


if(perl_status ~=0), 
    error('Error in Perl script');
else
    disp('Circulant_correction_array verilog file is created');
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%AUTOMATED CODE GENERATION END %%%%%%%%%
s=sprintf('\n******************\n');
disp(s)
disp('THINGS THAT CAN NOT BE DONE AUTOMATICALLY AT THIS TIME');
disp(message_str)
disp('Please note that you should change ../hdl/FS_mem.v based on the parallelization and word length of R');
disp('You may also need to create Xilinx cores for the desired dual port RAMs and adders');
if(MC(1).N_HD_Buff > 2 | MC(1).N_Qs_Buff > 2)
disp('You may also need to modify the buffer control modules for QsMem and HDMem as part of their logic is tuned for double buffering');
end
s=sprintf('\n******************\n');
disp(s);