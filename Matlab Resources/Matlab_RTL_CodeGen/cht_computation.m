% /---Confidentiality Notice Start--/
%   Texas A&M University Confidential.
%   Contact: Office of Technology Commercialization.
%   The material contained in this software and the associated papers
%   have features which are contained in a patent disclosure for LDPC decoding architectures 
%   filed by Gwan Choi, John Junkins , Kiran Gunnam. 
%   For more information about the licensing process, please contact:
%   Alex Garcia
%   Department: Licensing & Intellectual Property Management
%   Title: Licensing Associate
%   Email: alegarza@tamu.edu
%   Phone: (979) 458-2640
%   Fax: (979) 845-2684
% /---Confidentiality Notice End--/
% Kiran Gunnam
% Texas A&M University 
% This module does the cH' computation using block-serial matrix-vector
% operation. This function in  general serves as matrix-vector 
% computation unit for the QC-LDPC codes(regular and irregular)
% Sub-block parallelism mode can be added later.
function [cht]= cht_computation(C,Hd_in);

C_Mem           = zeros(C.Z,C.H_row);
Hd              = reshape(Hd_in,[],C.H_col);
for layer = 1:C.H_row
    for block_num = 1: C.dc(layer)
     Hd_buffer               = Hd(:,C.BC(layer,block_num));   
     Hd_shift2_currentlayer  = circshift(Hd_buffer,C.S(layer,block_num));
     C_Mem(:,layer)          = bitxor(C_Mem(:,layer),Hd_shift2_currentlayer);
    end
end

cht = C_Mem;