%%Function: Create_Vector_Mem_from_CStruct 
%%Kiran Gunnam  
%%Texas A&M University  
%%This function creates a vector memory from different vectors
function [] = Create_Partitioned_Vector_Mem_from_CStruct(MC,var_name,clk_flag,base_flag,wl,memory_type,look_ahead_steps)

%MC.Nc = Number_of_Codes_supported

if(nargin < 3)
    clk_flag                  = 0;
end

if(nargin < 4)
    base_flag                 = 0;
end

if(nargin < 5)
    wl                        = 0;
end

if(nargin <6)
    memory_type               = 'au';%au-auto, pb-pipeline dist. ram
end

if(nargin <7)
    look_ahead_steps =0; %0 ==>normal memory. var =mem(addr);
                         % any other non zero positve value, say ==>look ahead memory. 
                         %==>var =mem(addr+look_ahead_steps);
                         %addr+look_ahead_steps is calculated in modulo fashion
end

    %% Create data for tmp ROM and the base information for different H matrices
 for i= 1:MC(1).Nc
     tmp_vector       = MC(i).C.tmp;
     [mm nn] = size(tmp_vector);
     j =1;
     if(nn~=1) %%i.e. the variable is a matrix. so convert into matrix -neglect the entries for non-valid elements
         tmp_vector  = (MC(i).C.tmp(j,1:MC(i).C.dc(j)))';
             for j = 2:MC(i).C.H_row
                tmp          = (MC(i).C.tmp(j,1:MC(i).C.dc(j)))'; 
                tmp_vector  = [tmp_vector
                               tmp];
             end
     end

     tmp_ROM                  = [tmp_vector];
     max_var_tmp_ROM(i)       = max(tmp_ROM);
    addr_bus_name = 'addr';
    addr_bus_name = strcat(var_name,addr_bus_name,'Id',num2str(i));
    addr_bus_name_is_CodeId =0;
    base_flag    = 1;
    latch_addr   =0; %for UCVF. since we are using linear index(changes every clock)
    %+CodeId(which changes only during the reset) as the address, no latching is needed
    var_name_partition = strcat(var_name,'Id',num2str(i));
    Gen_OFC_ROM_verilog(tmp_ROM,var_name_partition,addr_bus_name,addr_bus_name_is_CodeId,base_flag,clk_flag,wl,memory_type,look_ahead_steps,latch_addr);

    var_rom_file_name      = strcat('../hdl_gen/','OFC_ROM.v');
    fid1 = fopen(var_rom_file_name,'a');
    if(look_ahead_steps>0),
        fprintf(fid1, '//The above memory is constructed such that it is a look ahead memory\n'); 
        fprintf(fid1,'//var_original =mem(addr+look_ahead_steps); \n');
        fprintf(fid1,'// addr+look_ahead_steps is calculated in modulo fashion\n');
        fprintf(fid1,'//look_ahead_steps= %d\n',look_ahead_steps);
    end
    if(nn~=1) %%for the data which is originally matrix
        fprintf(fid1, 'assign %s = linear_index ;\n',addr_bus_name); 
    else %% %%for the data which is originally vector
        fprintf(fid1, 'assign %s = layer + %s ;\n',addr_bus_name); 
    end
    fclose(fid1);

 end
 
 %%Now do the multiplexing of data based on the CodeID.
 var_rom_file_name      = strcat('../hdl_gen/','OFC_ROM.v');
 fid1 = fopen(var_rom_file_name,'a');
 addr_bus_name = 'CodeId';%%no need to declare CodeId again.
 max_var                = max(max_var_tmp_ROM);
 if(base_flag ==0)
 data_bus_width         = ceil(log2(max_var));
 else
 data_bus_width         = ceil(log2(max_var+1));
 end
 data_bus_width         = max(data_bus_width,1);
 if(wl>0), data_bus_width =wl; %%override the data bus width definition if it is specified from outside and is greater than 0
 end
 fprintf(fid1, 'reg [%d:0] %s;\n', data_bus_width-1,var_name);    
 fprintf(fid1, 'always @(posedge clk)\n');    
 fprintf(fid1, '   case (%s)\n',addr_bus_name); 
 for i= 1:MC(1).Nc
  fprintf(fid1,'      %d: %s = %s;\n', i-1,var_name, strcat(var_name,'Id',num2str(i)));
 end
fprintf(fid1,'      default:  %s = %s;\n', var_name, strcat(var_name,'Id',num2str(1)));
fprintf(fid1,'   endcase \n'); 

fclose(fid1);
    

