% /---Confidentiality Notice Start--/
%   Texas A&M University Confidential.
%   Contact: Office of Technology Commercialization.
%   The material contained in this software and the associated papers
%   have features which are contained in a patent disclosure for LDPC decoding architectures 
%   filed by Gwan Choi, John Junkins , Kiran Gunnam. 
%   For more information about the licensing process, please contact:
%   Alex Garcia
%   Department: Licensing & Intellectual Property Management
%   Title: Licensing Associate
%   Email: alegarza@tamu.edu
%   Phone: (979) 458-2640 
%   Fax: (979) 845-2684
% /---Confidentiality Notice End--/
%%Kiran Gunnam
%%Texas A&M University
%%December 2005 
clc
clear all 
close all

 %load ../H_matrices/H_Matrices_802_16e
 %circulant_size        = 96;
 %number_of_codes       = 2;%116; % for wimax. note that only shift coeffients need to be stored for all the 116 matrices.
%                                % all other parameters need to be stored
%                                % once per base matrix.
% 
 load ..\H_matrices\LDPCCodeData_Rate_0.83_(12,72)_Code_CirculantSize_140_C1_H_masked.mat
 circulant_size        = 140;
 number_of_codes       = 1;
 construction          ='col_shift';

DO_OOP_BLOCK_FLAG     = 1; %Does out-of-order processing with in a layer. Note that this flag 
%%affects only the partial state processing. The R new selection is always
%%done out-of-order for irregular LDPC codes- so this is not affected by
%%this flag
 Nc = number_of_codes


% f='../H_matrices/H_array.txt';
% H_1= load(f);
% H_2= load(f);
% DO_OOP_BLOCK_FLAG     = 0;
% circulant_size        = 8;
% number_of_codes       = 2;
% Nc                    = number_of_codes ;



for CodeId = 1:Nc
    switch(CodeId)
         case 1, H = H_Shift;
              [H_row, H_col] = size(H);
              H_ori= H;
              C.H_original = H;
              reordering = [1 6 11 4 9 2 7 12 5 10 3 8];
              H=H(reordering,:);
              
         case 2, H = H_2_3_B;
              [H_row, H_col] = size(H);
              H_ori= H;
              C.H_original = H;
              H=H([1 2 3 4 5 6 7 8],:);
         case 3, H = H_3_4_B;
              [H_row, H_col] = size(H);
              H_ori= H;
              C.H_original = H;
              H=H([1 2 3 4 5 6],:);
         case 4, H =H_5_6;
              [H_row, H_col] = size(H);
              H_ori= H;
              C.H_original = H;
              H=H([1 2 4 3],:);
              
    end 
C_struct_file_name = strcat('../H_matrices/C_Struct_CodeId_Flash',num2str(CodeId));
            
C.H_row = H_row;
C.H_col = H_col;
C.Z     = circulant_size;

if(construction=='row_shift')
S= H;
    for i=1:H_row
      index2 = find(H(i,:)~=-1);
      S(i,index2)= mod(circulant_size-H(i,index2),circulant_size); 
    end
%H;%%row_shift
else
S = H;
end

C.shift_type_original = construction;
C.shift_type          = 'col_shift';
C.H = S;


a    =(S >=0);
C.dc =(sum(a,2));
C.dv = sum(a,1);
C.S  = S;
C.p  = circulant_size;





%% 2-D Correction factors for min-sum normalized. 
C.alpha = ones(1,H_row);
C.beta  = ones(1,H_col);

%% Each entry corresponds to the normalization factor for the variable node degree
dv_argument = [1:16];
GammaV_wl   =4;
beta_LUT_dv = [1.0000-1/2^GammaV_wl 0.8750 0.8125 0.8125 0.7500 0.7500 0.7500 0.7500 ...
               0.7500 0.6875 0.6875 0.6875 0.6875 0.5000 0.5000 0.5000];
beta_o_LUT_dv = [0      0.2250 0.2875 0.2875 0.3500 0.3500 0.3500 0.3500 0.3500 ...
                 0.4125 0.4125 0.4125 0.4125 0.6000 0.6000 0.6000];           

C.beta        = beta_LUT_dv(C.dv);   
C.beta_o      = beta_o_LUT_dv(C.dv);
           
C.beta_LUT_dv        = beta_LUT_dv;   
C.beta_o_LUT_dv      = beta_o_LUT_dv;

circulant_index =0;
%% Compute delta shift matrix for each block row/layer
%% also compute the layer of the block whose information need to be used for the
%% present block_num in the present layer.
for layer = 1: C.H_row
 index_non_zero = find(S(layer,:) ~= -1); 
    for block_num = 1: C.dc(layer)
        block_col = index_non_zero(block_num);
        C.BC(layer,block_num) = block_col;
       % delta shift
        layer_prev = layer -1;
        if layer_prev == 0 
          layer_prev = C.H_row;
        end
         while(S(layer_prev,block_col) == -1)
           layer_prev = layer_prev - 1;
           if layer_prev == 0 
            layer_prev = C.H_row;
           end
         end
       index_non_zero_prev                       = find(S(layer_prev,:) ~= -1); 
       block_num_prev                            = find(block_col ==index_non_zero_prev);
       circulant_index                           = circulant_index+1;
       C.DL(layer,block_num)                     = layer_prev;
       C.DB(layer,block_num)                     = block_num_prev;
       C.CI(layer,block_num)                     = circulant_index;
       C.DELTA_SHIFT_UCVF0(layer,block_num)      = S(layer, block_col) - S(layer_prev, block_col);
       C.SHIFT(layer,block_num)                  = S(layer, block_col);                           
       %% convert -neg shift coefficent(upshift) to positive shift
       %% coefficent(down shift) using modulo shift property
       if(C.DELTA_SHIFT_UCVF0(layer,block_num)<0), 
           C.DELTA_SHIFT_UCVF0(layer,block_num) =  circulant_size- abs(C.DELTA_SHIFT_UCVF0(layer,block_num)); 
       end
       C.dv_LUT(layer,block_num)                   = C.dv(block_col);
    end
     C.BN(layer,1:C.dc(layer))                   =  1:C.dc(layer);
end

for layer = 1: C.H_row
   for block_num = 1: C.dc(layer)
       C.DCI(layer,block_num)   = C.CI(C.DL(layer,block_num),C.DB(layer,block_num));
   end
end

num_circulants = circulant_index;
C.Num_circulants = num_circulants;
%% Compute shift matrix for the first iteration for each block row/layer
C.SHIFT_UCVF1 = C.DELTA_SHIFT_UCVF0;
[mm nn]= size(C.DELTA_SHIFT_UCVF0);
C.UCVF = zeros(mm,nn);
for block_col = 1: H_col
    layer_next =1;
    while(S(layer_next,block_col) == -1 && layer_next<=H_row)
           layer_next = layer_next + 1;
    end
    index_non_zero = find(S(layer_next,:) ~= -1); 
    block_num      = find(block_col ==index_non_zero);
    C.SHIFT_UCVF1(layer_next,block_num) =  S(layer_next, block_col);
    C.UCVF(layer_next,block_num)         = 1;
end
dc_max = max(C.dc);
%% Do the re-ordering of C struct information according to the layered
%% scheduling and Out of order processing on blocks/circulants in a layer
if(DO_OOP_BLOCK_FLAG==1)
C.CStructOriginal      = C;
C.DO_OOP_BLOCK_FLAG     =DO_OOP_BLOCK_FLAG;
%%Re-order the C structure information according to the out-of-order
%%processing of the blocks
next_layer =1;
for i=1:C.H_row
       current_layer                                = i;
       next_layer                                   = mod(current_layer,C.H_row)+1;
       C.next_layer_vec(i)                          = next_layer;
       C.current_layer_vec(i)                       = current_layer;      
       [dummy no_conflict_list]                     = find(C.DL(next_layer,1:C.dc(next_layer))~=current_layer);
       C.tolerable_latency_per_layer(next_layer)    = length(no_conflict_list);
       [dummy conflict_list]                        = find(C.DL(next_layer,1:C.dc(next_layer))==current_layer);
       C.processing_order(next_layer).block_num     = [no_conflict_list conflict_list]';
       C.BC(next_layer,1:C.dc(next_layer))          =  C.BC(next_layer,C.processing_order(next_layer).block_num);
       C.BN(next_layer,1:C.dc(next_layer))          =  C.BN(next_layer,C.processing_order(next_layer).block_num);
       C.DB(next_layer,1:C.dc(next_layer))          =  C.DB(next_layer,C.processing_order(next_layer).block_num);
       C.DL(next_layer,1:C.dc(next_layer))          =  C.DL(next_layer,C.processing_order(next_layer).block_num);
       C.CI(next_layer,1:C.dc(next_layer))          =  C.CI(next_layer,C.processing_order(next_layer).block_num);
       C.UCVF(next_layer,1:C.dc(next_layer))        =  C.UCVF(next_layer,C.processing_order(next_layer).block_num);
       C.SHIFT_UCVF1(next_layer,1:C.dc(next_layer)) =  C.SHIFT_UCVF1(next_layer,C.processing_order(next_layer).block_num);
       C.DELTA_SHIFT_UCVF0(next_layer,1:C.dc(next_layer)) =  C.DELTA_SHIFT_UCVF0(next_layer,C.processing_order(next_layer).block_num);
       C.SHIFT(next_layer,1:C.dc(next_layer))             =  C.SHIFT(next_layer,C.processing_order(next_layer).block_num);
       C.DCI(next_layer,1:C.dc(next_layer))               =  C.DCI(next_layer,C.processing_order(next_layer).block_num);     
       C.dv_LUT(next_layer,1:C.dc(next_layer))            =  C.dv_LUT(next_layer,C.processing_order(next_layer).block_num);
       %this is not used in the architecture.just to see how long is the time
       %before completing processing on a block column
       for i=1:C.H_col,
        for j=1:C.H_row,
         if((S(j,i)>0)),
          C.lastblock_in_BC(i)=j;
         end
        end
       end
end 

%renumber the blocks in each layer in the processing order.
%only blocknum_LUT and dependendent_block_LUT will be affected.
%note that these values are only used for index of min1
for i=1:C.H_row
       current_layer                                = i;
       next_layer                                   = mod(current_layer,C.H_row)+1;
       C.next_layer_vec(i)                          = next_layer;
       C.current_layer_vec(i)                       = current_layer;      
       C.BN(next_layer,1:C.dc(next_layer))          = 1:C.dc(next_layer);
       for jj=1:1:C.dc(next_layer)
           dl_tmp=C.DL(next_layer,jj);
           dci_tmp=C.DCI(next_layer,jj);
           [dummy C.DB(next_layer,jj)]  = find(C.CI(dl_tmp,:)==dci_tmp);
       end
end 

end
%% END of if(DO_OOP_BLOCK_FLAG==1)

%%duplicate the parameters to the expanded names used in matlab decoder model
C.blockcol_LUT            = C.BC;
C.blocknum_LUT            = C.BN;
C.dependendent_layer_LUT  = C.DL;
C.circulant_index_LUT     = C.CI;
C.dependendent_block_LUT  = C.DB;
C.UseChannelValue_Flag    = C.UCVF;
C.Shift_iteration1        = C.SHIFT_UCVF1;
C.delta_shift             = C.DELTA_SHIFT_UCVF0;
C.shift                   = C.SHIFT;
C.dependendent_circulant_index_LUT = C.DCI;





%% the following indices start from 0 in hardware. 
C.BC = C.BC-1;
C.BN = C.BN-1;
C.DB = C.DB-1;
C.DL = C.DL-1;
C.CI = C.CI-1;
C.DCI = C.DCI-1;
C.dc = C.dc; 
if(DO_OOP_BLOCK_FLAG)
C.CStructOriginal.BC= C.CStructOriginal.BC-1;
C.CStructOriginal.DB= C.CStructOriginal.DB-1;
C.CStructOriginal.DL= C.CStructOriginal.DL-1;
C.CStructOriginal.CI= C.CStructOriginal.CI-1;
C.CStructOriginal.DCI= C.CStructOriginal.DCI-1;
C.tolerable_latency_per_layer
average_tolerable_latency_per_layer=mean(C.tolerable_latency_per_layer)
end



v1 = 'C';
save(C_struct_file_name,v1); 
disp('C structure is stored into ')
C_struct_file_name
clear C
end


%end

