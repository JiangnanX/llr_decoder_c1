%% /---Confidentiality Notice Start--/
%  TexasLDPC Inc Confidential.
%  � Copyright 2015 to the present, TexasLDPC Inc.
%  All the software contained in these files are protected by United States
%  copyright law and may not be reproduced, distributed, transmitted,
%  displayed, published or broadcast without the prior written permission
%  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
%  or other notice from copies of the content.
%  /---Confidentiality Notice End--/
%
%  Software to compute the signed binary representation of a decimal number
%  Osso Vahabzadeh
%  TexasLDPC Inc
%  August 2015

%%

function sbin = dec2sbin(d, n)

%  dec2sbin(d, n) computes the n-bit signed binary representation of an 
%  integer d. If more than n bits are required for this purpose, a warning 
%  will be displayed that indicates the minimum number of bits that is 
%  necessary to represent d as a signed binary number.

parameterMax = 2^(n-1)-1;       % The largest integer that has an n-bit signed binary representation
parameterMin = - parameterMax;  % The smallest integer that has an n-bit signed binary representation

if (parameterMin <= d) && (d <= parameterMax)
    if d >= 0
        sbin = dec2bin(d, n);
    else
        sbin = dec2bin(2^n+d, n);
    end
else
    warning('At least %d bits are required for a signed binary representation of %d!', floor(log2(d))+2, d)
end