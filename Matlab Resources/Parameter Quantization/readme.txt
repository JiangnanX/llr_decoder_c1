Given the channel's operating SNRs for bits 0 and 1, this software computes 
the parameters used for computing the received (stored) noisy signals or their corresponding LLRs.
