clear; clc

%  The detection of the channel output with BPSK signaling can be done in
%  two ways. Either the output, i.e., the transmitted (stored) signal + 
%  noise is compared with some read reference vlotage(s) and a decision is 
%  made to determine the channel input, or the LLR corresponding to the 
%  channel output is computed and compared with some reference LLR(s). In
%  general, the noise observed by bits 0 and 1 may be different. In
%  hardware simulations, usually a noise sample with standard normal
%  distribution is generated. Then based on the operating SNRs per bit, 
%  i.e., the SNRs observed when bits 0 or 1 are transmitted, the 
%  appropriate noise samples are computed. Here the appropriate noise
%  parameters are the standard deviations of the two noise distributions.
%  It is also possible to compute the corresponding LLRs using more 
%  elaborate equations. The necessary parameters in this case are denoted
%  by P1, P2, and P3. In the case of identical noise distributions, i.e., 
%  when m_0 = -m_1 and SNR_dB_b0 = SNR_dB_b1, we have P1 = 2/var = vari, 
%  P2 = 2/std = svari, and P3 = 0. For each SNR, the m-file generates the 
%  signed binary representation of these parameters. Depending on whether
%  the RTL model uses 'Noise' or 'LLR' parameters, the correct option
%  should be selected. Also the precision of the parameters, as well as
%  some other parameters should be determined. The results are written in
%  .coe file which is used in RTL simulations.


%% Initialization:

d_SNR                               = 0.1;                      % Step size for SNR range
SNR_dB_b0                           = 2.8 : d_SNR : 6.4;        % SNR per bit for bit 0, i.e., SNR = Eb_over_No
SNR_dB_b1                           = SNR_dB_b0;                % SNR per bit for bit 1
m_0                                 =  1;                       % Mean of the distribution of the noise samples corrupting bit 0
m_1                                 = -1;                       % Mean of the distribution of the noise samples corrupting bit 1
C.H_col                             = 72;                       % No of columns of the base matrix
C.H_row                             = 12;                       % No of rows of the base matrix
Parameter_to_Quantize               = 'Noise';
% Parameter_to_Quantize              = 'LLR';
QuantStruct.Parameter_Int_BitWidth = 5;                         % Precision of the integer part of the rational number in bits
QuantStruct.Parameter_Fra_BitWidth = 11;                        % Precision of the fraction part of the rational number in bits
QuantStruct.Parameter_BitWidth     = QuantStruct.Parameter_Int_BitWidth+QuantStruct.Parameter_Fra_BitWidth;
C.RATE      = 1 - C.H_row/C.H_col;

%%
if strcmp(Parameter_to_Quantize, 'Noise')
    
    if size(SNR_dB_b0, 2) == 1
        file_name=['Noise_Par_', 'Rate_0.', num2str(round(100*C.RATE)), '_SNR0_', ...
            num2str(SNR_dB_b0), '_dB', '_SNR1_', num2str(SNR_dB_b1), '_dB', '.coe'];
    else
        file_name=['Noise_Par_', 'Rate_0.', num2str(round(100*C.RATE)), '_SNR0_', ...
            num2str(SNR_dB_b0(1)), '_', num2str(SNR_dB_b0(end)), '_dB', '_SNR1_', ...
            num2str(SNR_dB_b1(1)), '_', num2str(SNR_dB_b1(end)), '_dB', '_SL', num2str(d_SNR), '.coe'];
        std_0   = zeros(length(SNR_dB_b0),1);
        std_1   = zeros(length(SNR_dB_b0),1);
        std_0Q  = zeros(length(SNR_dB_b0),1);
        std_1Q  = zeros(length(SNR_dB_b0),1);
        std_0QI = zeros(length(SNR_dB_b0),1);
        std_1QI = zeros(length(SNR_dB_b0),1);
    end
    
elseif strcmp(Parameter_to_Quantize, 'LLR')
    
    if size(SNR_dB_b0, 2) == 1
        file_name=['LLR_Par_', 'Rate_0.', num2str(round(100*C.RATE)), '_SNR0_', ...
            num2str(SNR_dB_b0), '_dB', '_SNR1_', num2str(SNR_dB_b1), '_dB', '.coe'];
    else
        file_name=['LLR_Par_', 'Rate_0.', num2str(round(100*C.RATE)), '_SNR0_', ...
            num2str(SNR_dB_b0(1)), '_', num2str(SNR_dB_b0(end)), '_dB', '_SNR1_', ...
            num2str(SNR_dB_b1(1)), '_', num2str(SNR_dB_b1(end)), '_dB', '_SL', num2str(d_SNR), '.coe'];
        P1      = zeros(length(SNR_dB_b0),1);
        P2      = zeros(length(SNR_dB_b0),1);
        P3      = zeros(length(SNR_dB_b0),1);
        P1Q     = zeros(length(SNR_dB_b0),1);       % Approaximately P1
        P2Q     = zeros(length(SNR_dB_b0),1);       % Approaximately P2
        P3Q     = zeros(length(SNR_dB_b0),1);       % Approaximately P3
        P1QI    = zeros(length(SNR_dB_b0),1);
        P2QI    = zeros(length(SNR_dB_b0),1);
        P3QI    = zeros(length(SNR_dB_b0),1);
    end
    
end

fid = fopen(file_name, 'wt');
fprintf(fid, 'memory_initialization_radix=2;\n');
fprintf(fid, 'memory_initialization_vector=\n');

for i = 1:length(SNR_dB_b0)
    
    SNR_b0 = 10^(SNR_dB_b0(i)/10);
    SNR_b1 = 10^(SNR_dB_b1(i)/10);
    var_0  = 1.0/(2*SNR_b0*C.RATE);
    var_1  = 1.0/(2*SNR_b1*C.RATE);
    std_0(i)  = sqrt(var_0);
    std_1(i)  = sqrt(var_1);
%     dec2bin(round(std_0(i)*2^11),16)
%     dec2bin(round(std_1(i)*2^11),16)
    
    if strcmp(Parameter_to_Quantize, 'Noise')
        
        [std_0(i), std_1(i), std_0Q(i), std_1Q(i), std_0QI(i), std_1QI(i)] = noiseParQuant(std_0(i), std_1(i), QuantStruct);
        
        std_0QI_bin = dec2sbin(std_0QI(i), QuantStruct.Parameter_BitWidth);
        std_1QI_bin = dec2sbin(std_1QI(i), QuantStruct.Parameter_BitWidth);
        
        if i ~= length(SNR_dB_b0)
            fprintf(fid, '%s%s \n', num2str(std_1QI_bin), num2str(std_0QI_bin));
        else
            fprintf(fid, '%s%s;', num2str(std_1QI_bin), num2str(std_0QI_bin));
        end
        
        fprintf('SNR_b0 = %.2f dB, SNR_b1 = %.2f dB:\n\n', SNR_dB_b0(i), SNR_dB_b1(i))
        fprintf('std_0QI_bin(%d)  = %s\n', i, std_0QI_bin)
        fprintf('std_1QI_bin(%d)  = %s\n', i, std_1QI_bin)
        fprintf('[std_0QI_bin(%d), std_1QI_bin(%d)] = %s%s\n\n', i, i, std_0QI_bin, std_1QI_bin)
        
    elseif strcmp(Parameter_to_Quantize, 'LLR')
        
        P1(i) = log(std_1(i)/std_0(i)) + (var_0*m_1^2-var_1*m_0^2)/(2*var_0*var_1); % Same as t1 in the document
        P2(i) = (var_1*m_0-var_0*m_1)/(var_0*var_1);                                % Same as t2 in the document
        P3(i) = (var_0-var_1)/(2*var_0*var_1);                                      % Same as t3 in the document
        [P1(i),P2(i),P3(i),P1Q(i),P2Q(i),P3Q(i),P1QI(i),P2QI(i),P3QI(i)] = parLLRQuant(P1(i), P2(i), P3(i), QuantStruct);
        
        P1QI_bin = dec2sbin(P1QI(i), QuantStruct.Parameter_BitWidth);
        P2QI_bin = dec2sbin(P2QI(i), QuantStruct.Parameter_BitWidth);
        P3QI_bin = dec2sbin(P3QI(i), QuantStruct.Parameter_BitWidth);
        
        if i ~= length(SNR_dB_b0)
            fprintf(fid, '%s%s%s \n', num2str(P1QI_bin), num2str(P2QI_bin), num2str(P3QI_bin));
        else
            fprintf(fid, '%s%s%s;', num2str(P1QI_bin), num2str(P2QI_bin), num2str(P3QI_bin));
        end
        
        fprintf('SNR_b0 = %.2f dB, SNR_b1 = %.2f dB:\n\n', SNR_dB_b0(i), SNR_dB_b1(i))
        fprintf('P1QI_bin(%d)  = %s\n', i, P1QI_bin)
        fprintf('P2QI_bin(%d)  = %s\n', i, P2QI_bin)
        fprintf('P3QI_bin(%d)  = %s\n', i, P3QI_bin)
        fprintf('[P1QI_bin(%d), P2QI_bin(%d), P3QI_bin(%d)] = %s%s%s\n\n', i, i, i, P1QI_bin, P2QI_bin, P3QI_bin)
    end
end

fclose(fid);