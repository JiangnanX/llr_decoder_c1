function [P1,P2,P3,P1Q,P2Q,P3Q,P1QI,P2QI,P3QI] = llrParQuant(P1, P2, P3, QuantStruct)

parameterMax = 2^(QuantStruct.Parameter_BitWidth-1)-1;  % Sets the maximum integer for representing the parameters
parameterMin = -(parameterMax+1);                       % Sets the minimum integer for representing the parameters

%%
P1Q     = P1 * (2^QuantStruct.Parameter_Fra_BitWidth);
P2Q     = P2 * (2^QuantStruct.Parameter_Fra_BitWidth);
P3Q     = P3 * (2^QuantStruct.Parameter_Fra_BitWidth);

P1Q     = round(P1Q);
P2Q     = round(P2Q);
P3Q     = round(P3Q);

if (P1Q >= 0)
    P1Q  = min(P1Q, parameterMax);                      % Restricts P1Q to be <= parameterMax
else
    P1Q  = max(P1Q, parameterMin+1);                    % Restricts P1Q to be >= parameterMin ???????? (why +1)
end

if (P2Q >= 0)
    P2Q  = min(P2Q, parameterMax);
else
    P2Q  = max(P2Q, parameterMin+1);
end

if (P3Q >= 0)
    P3Q  = min(P3Q, parameterMax);
else
    P3Q  = max(P3Q, parameterMin+1);
end

P1QI = P1Q;                                             % Stores P1Q in a new variable
P1Q  = P1Q/(2^QuantStruct.Parameter_Fra_BitWidth);      % Returns P1

P2QI = P2Q;
P2Q  = P2Q/(2^QuantStruct.Parameter_Fra_BitWidth);

P3QI = P3Q;
P3Q  = P3Q/(2^QuantStruct.Parameter_Fra_BitWidth);