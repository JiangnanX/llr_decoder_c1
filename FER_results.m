% Initilialization:
Rate = 1 - 12/72;
EbNo_dB = 2.8 : 0.1 : 2.8+26*0.1;

error_no_nz_cw  =  100*ones(1,11);
frame_no_nz_cw  = [100*ones(1,3), 101, 109, 126, 306, 996, 8858, 525690, 24049243];

error_no_z_cw   =  100*ones(1,11);
frame_no_z_cw   = [100*ones(1,4), 102, 128, 188, 590, 6782, 383573, 12767523];

%%
FER_nz_cw       = error_no_nz_cw ./ frame_no_nz_cw;
FER_z_cw        = error_no_z_cw  ./ frame_no_z_cw ;
RBER            = ebno2p(EbNo_dB(1:max(length(FER_nz_cw),length(FER_z_cw))),Rate);

figure
semilogy(EbNo_dB(1:length(FER_nz_cw)), FER_nz_cw, '-o', ...
         EbNo_dB(1:length(FER_z_cw )), FER_z_cw , '-*', 'LineWidth', 1.5)

legend('Nonzero Codeword', 'Zero Codeword')

grid
xlabel('E_b/N_0 (dB)')
ylabel('FER')

figure
semilogy(RBER(1:length(FER_nz_cw)), FER_nz_cw, '-o', ...
         RBER(1:length(FER_z_cw )), FER_z_cw , '-*', 'LineWidth', 1.5)

legend('Nonzero Codeword', 'Zero Codeword')

grid
xlabel('RBER')
ylabel('FER')