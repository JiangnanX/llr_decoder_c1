#add_wave  {{/LDPC_SIMULATION_TOP/*}}
#add_wave -recursive {{/LDPC_SIMULATION_TOP/LG1/*}}
add_force {/LDPC_SIMULATION_TOP/clk} -radix bin {1 0ns} {0 5000ps} -repeat_every 10000ps
add_force {/LDPC_SIMULATION_TOP/rst_n} -radix bin {1 0ns}
add_force {/LDPC_SIMULATION_TOP/start_simulation} -radix bin {0 0ns}
run 80 ns
add_force {/LDPC_SIMULATION_TOP/rst_n} -radix bin {0 0ns}
run 80 ns
add_force {/LDPC_SIMULATION_TOP/rst_n} -radix bin {1 0ns}
run 4000 ns
add_force {/LDPC_SIMULATION_TOP/start_simulation} -radix bin {1 0ns}
run 80 ns
run 80 ns
run 403200 ns
run 50000 ns
add_force {/LDPC_SIMULATION_TOP/start_simulation} -radix bin {0 0ns}
run 10 us
#run 200 us