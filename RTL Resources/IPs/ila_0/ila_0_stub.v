// Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2014.2 (win64) Build 932637 Wed Jun 11 13:33:10 MDT 2014
// Date        : Fri Oct 07 18:11:58 2016
// Host        : TexasLDPC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               {C:/Users/jiangnan/Desktop/v1.3_sim_instructions-20161005T221036Z/v1.3_sim_instructions/RTL
//               Resources/IPs/ila_0/ila_0_stub.v}
// Design      : ila_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "ila,Vivado 2014.2" *)
module ila_0(clk, probe0, probe1, probe2, probe3, probe4, probe5, probe6, probe7)
/* synthesis syn_black_box black_box_pad_pin="clk,probe0[0:0],probe1[0:0],probe2[4:0],probe3[39:0],probe4[39:0],probe5[0:0],probe6[5:0],probe7[42:0]" */;
  input clk;
  input [0:0]probe0;
  input [0:0]probe1;
  input [4:0]probe2;
  input [39:0]probe3;
  input [39:0]probe4;
  input [0:0]probe5;
  input [5:0]probe6;
  input [42:0]probe7;
endmodule
