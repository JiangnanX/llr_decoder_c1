// Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2014.2 (win64) Build 932637 Wed Jun 11 13:33:10 MDT 2014
// Date        : Fri Oct 07 18:12:26 2016
// Host        : TexasLDPC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               {C:/Users/jiangnan/Desktop/v1.3_sim_instructions-20161005T221036Z/v1.3_sim_instructions/RTL
//               Resources/IPs/dist_mem_gen_2/dist_mem_gen_2_funcsim.v}
// Design      : dist_mem_gen_2
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "dist_mem_gen_v8_0,Vivado 2014.2" *) (* CHECK_LICENSE_TYPE = "dist_mem_gen_2,dist_mem_gen_v8_0,{}" *) 
(* core_generation_info = "dist_mem_gen_2,dist_mem_gen_v8_0,{x_ipProduct=Vivado 2014.2,x_ipVendor=xilinx.com,x_ipLibrary=ip,x_ipName=dist_mem_gen,x_ipVersion=8.0,x_ipCoreRevision=5,x_ipLanguage=VERILOG,C_FAMILY=artix7,C_ADDR_WIDTH=5,C_DEFAULT_DATA=0,C_DEPTH=32,C_HAS_CLK=1,C_HAS_D=1,C_HAS_DPO=0,C_HAS_DPRA=0,C_HAS_I_CE=0,C_HAS_QDPO=0,C_HAS_QDPO_CE=0,C_HAS_QDPO_CLK=0,C_HAS_QDPO_RST=0,C_HAS_QDPO_SRST=0,C_HAS_QSPO=0,C_HAS_QSPO_CE=0,C_HAS_QSPO_RST=0,C_HAS_QSPO_SRST=0,C_HAS_SPO=1,C_HAS_WE=1,C_MEM_INIT_FILE=no_coe_file_loaded,C_ELABORATION_DIR=./,C_MEM_TYPE=1,C_PIPELINE_STAGES=0,C_QCE_JOINED=0,C_QUALIFY_WE=0,C_READ_MIF=0,C_REG_A_D_INPUTS=0,C_REG_DPRA_INPUT=0,C_SYNC_ENABLE=1,C_WIDTH=43,C_PARSER_TYPE=1}" *) 
(* NotValidForBitStream *)
module dist_mem_gen_2
   (a,
    d,
    clk,
    we,
    spo);
  input [4:0]a;
  input [42:0]d;
  input clk;
  input we;
  output [42:0]spo;

  wire [4:0]a;
  wire clk;
  wire [42:0]d;
  wire [42:0]spo;
  wire we;
  wire [42:0]NLW_U0_dpo_UNCONNECTED;
  wire [42:0]NLW_U0_qdpo_UNCONNECTED;
  wire [42:0]NLW_U0_qspo_UNCONNECTED;

(* C_FAMILY = "artix7" *) 
   (* C_HAS_CLK = "1" *) 
   (* C_HAS_D = "1" *) 
   (* C_HAS_DPO = "0" *) 
   (* C_HAS_DPRA = "0" *) 
   (* C_HAS_QDPO = "0" *) 
   (* C_HAS_QDPO_CE = "0" *) 
   (* C_HAS_QDPO_CLK = "0" *) 
   (* C_HAS_QDPO_RST = "0" *) 
   (* C_HAS_QDPO_SRST = "0" *) 
   (* C_HAS_WE = "1" *) 
   (* C_MEM_TYPE = "1" *) 
   (* C_QCE_JOINED = "0" *) 
   (* C_REG_DPRA_INPUT = "0" *) 
   (* DONT_TOUCH *) 
   (* c_addr_width = "5" *) 
   (* c_default_data = "0" *) 
   (* c_depth = "32" *) 
   (* c_elaboration_dir = "./" *) 
   (* c_has_i_ce = "0" *) 
   (* c_has_qspo = "0" *) 
   (* c_has_qspo_ce = "0" *) 
   (* c_has_qspo_rst = "0" *) 
   (* c_has_qspo_srst = "0" *) 
   (* c_has_spo = "1" *) 
   (* c_mem_init_file = "no_coe_file_loaded" *) 
   (* c_parser_type = "1" *) 
   (* c_pipeline_stages = "0" *) 
   (* c_qualify_we = "0" *) 
   (* c_read_mif = "0" *) 
   (* c_reg_a_d_inputs = "0" *) 
   (* c_sync_enable = "1" *) 
   (* c_width = "43" *) 
   dist_mem_gen_2_dist_mem_gen_v8_0__parameterized0 U0
       (.a(a),
        .clk(clk),
        .d(d),
        .dpo(NLW_U0_dpo_UNCONNECTED[42:0]),
        .dpra({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .i_ce(1'b1),
        .qdpo(NLW_U0_qdpo_UNCONNECTED[42:0]),
        .qdpo_ce(1'b1),
        .qdpo_clk(1'b0),
        .qdpo_rst(1'b0),
        .qdpo_srst(1'b0),
        .qspo(NLW_U0_qspo_UNCONNECTED[42:0]),
        .qspo_ce(1'b1),
        .qspo_rst(1'b0),
        .qspo_srst(1'b0),
        .spo(spo),
        .we(we));
endmodule

(* ORIG_REF_NAME = "dist_mem_gen_v8_0" *) (* C_FAMILY = "artix7" *) (* C_ADDR_WIDTH = "5" *) 
(* C_DEFAULT_DATA = "0" *) (* C_DEPTH = "32" *) (* C_HAS_CLK = "1" *) 
(* C_HAS_D = "1" *) (* C_HAS_DPO = "0" *) (* C_HAS_DPRA = "0" *) 
(* C_HAS_I_CE = "0" *) (* C_HAS_QDPO = "0" *) (* C_HAS_QDPO_CE = "0" *) 
(* C_HAS_QDPO_CLK = "0" *) (* C_HAS_QDPO_RST = "0" *) (* C_HAS_QDPO_SRST = "0" *) 
(* C_HAS_QSPO = "0" *) (* C_HAS_QSPO_CE = "0" *) (* C_HAS_QSPO_RST = "0" *) 
(* C_HAS_QSPO_SRST = "0" *) (* C_HAS_SPO = "1" *) (* C_HAS_WE = "1" *) 
(* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_ELABORATION_DIR = "./" *) (* C_MEM_TYPE = "1" *) 
(* C_PIPELINE_STAGES = "0" *) (* C_QCE_JOINED = "0" *) (* C_QUALIFY_WE = "0" *) 
(* C_READ_MIF = "0" *) (* C_REG_A_D_INPUTS = "0" *) (* C_REG_DPRA_INPUT = "0" *) 
(* C_SYNC_ENABLE = "1" *) (* C_WIDTH = "43" *) (* C_PARSER_TYPE = "1" *) 
module dist_mem_gen_2_dist_mem_gen_v8_0__parameterized0
   (a,
    d,
    dpra,
    clk,
    we,
    i_ce,
    qspo_ce,
    qdpo_ce,
    qdpo_clk,
    qspo_rst,
    qdpo_rst,
    qspo_srst,
    qdpo_srst,
    spo,
    dpo,
    qspo,
    qdpo);
  input [4:0]a;
  input [42:0]d;
  input [4:0]dpra;
  input clk;
  input we;
  input i_ce;
  input qspo_ce;
  input qdpo_ce;
  input qdpo_clk;
  input qspo_rst;
  input qdpo_rst;
  input qspo_srst;
  input qdpo_srst;
  output [42:0]spo;
  output [42:0]dpo;
  output [42:0]qspo;
  output [42:0]qdpo;

  wire \<const0> ;
  wire [4:0]a;
  wire clk;
  wire [42:0]d;
  wire [42:0]spo;
  wire we;

  assign dpo[42] = \<const0> ;
  assign dpo[41] = \<const0> ;
  assign dpo[40] = \<const0> ;
  assign dpo[39] = \<const0> ;
  assign dpo[38] = \<const0> ;
  assign dpo[37] = \<const0> ;
  assign dpo[36] = \<const0> ;
  assign dpo[35] = \<const0> ;
  assign dpo[34] = \<const0> ;
  assign dpo[33] = \<const0> ;
  assign dpo[32] = \<const0> ;
  assign dpo[31] = \<const0> ;
  assign dpo[30] = \<const0> ;
  assign dpo[29] = \<const0> ;
  assign dpo[28] = \<const0> ;
  assign dpo[27] = \<const0> ;
  assign dpo[26] = \<const0> ;
  assign dpo[25] = \<const0> ;
  assign dpo[24] = \<const0> ;
  assign dpo[23] = \<const0> ;
  assign dpo[22] = \<const0> ;
  assign dpo[21] = \<const0> ;
  assign dpo[20] = \<const0> ;
  assign dpo[19] = \<const0> ;
  assign dpo[18] = \<const0> ;
  assign dpo[17] = \<const0> ;
  assign dpo[16] = \<const0> ;
  assign dpo[15] = \<const0> ;
  assign dpo[14] = \<const0> ;
  assign dpo[13] = \<const0> ;
  assign dpo[12] = \<const0> ;
  assign dpo[11] = \<const0> ;
  assign dpo[10] = \<const0> ;
  assign dpo[9] = \<const0> ;
  assign dpo[8] = \<const0> ;
  assign dpo[7] = \<const0> ;
  assign dpo[6] = \<const0> ;
  assign dpo[5] = \<const0> ;
  assign dpo[4] = \<const0> ;
  assign dpo[3] = \<const0> ;
  assign dpo[2] = \<const0> ;
  assign dpo[1] = \<const0> ;
  assign dpo[0] = \<const0> ;
  assign qdpo[42] = \<const0> ;
  assign qdpo[41] = \<const0> ;
  assign qdpo[40] = \<const0> ;
  assign qdpo[39] = \<const0> ;
  assign qdpo[38] = \<const0> ;
  assign qdpo[37] = \<const0> ;
  assign qdpo[36] = \<const0> ;
  assign qdpo[35] = \<const0> ;
  assign qdpo[34] = \<const0> ;
  assign qdpo[33] = \<const0> ;
  assign qdpo[32] = \<const0> ;
  assign qdpo[31] = \<const0> ;
  assign qdpo[30] = \<const0> ;
  assign qdpo[29] = \<const0> ;
  assign qdpo[28] = \<const0> ;
  assign qdpo[27] = \<const0> ;
  assign qdpo[26] = \<const0> ;
  assign qdpo[25] = \<const0> ;
  assign qdpo[24] = \<const0> ;
  assign qdpo[23] = \<const0> ;
  assign qdpo[22] = \<const0> ;
  assign qdpo[21] = \<const0> ;
  assign qdpo[20] = \<const0> ;
  assign qdpo[19] = \<const0> ;
  assign qdpo[18] = \<const0> ;
  assign qdpo[17] = \<const0> ;
  assign qdpo[16] = \<const0> ;
  assign qdpo[15] = \<const0> ;
  assign qdpo[14] = \<const0> ;
  assign qdpo[13] = \<const0> ;
  assign qdpo[12] = \<const0> ;
  assign qdpo[11] = \<const0> ;
  assign qdpo[10] = \<const0> ;
  assign qdpo[9] = \<const0> ;
  assign qdpo[8] = \<const0> ;
  assign qdpo[7] = \<const0> ;
  assign qdpo[6] = \<const0> ;
  assign qdpo[5] = \<const0> ;
  assign qdpo[4] = \<const0> ;
  assign qdpo[3] = \<const0> ;
  assign qdpo[2] = \<const0> ;
  assign qdpo[1] = \<const0> ;
  assign qdpo[0] = \<const0> ;
  assign qspo[42] = \<const0> ;
  assign qspo[41] = \<const0> ;
  assign qspo[40] = \<const0> ;
  assign qspo[39] = \<const0> ;
  assign qspo[38] = \<const0> ;
  assign qspo[37] = \<const0> ;
  assign qspo[36] = \<const0> ;
  assign qspo[35] = \<const0> ;
  assign qspo[34] = \<const0> ;
  assign qspo[33] = \<const0> ;
  assign qspo[32] = \<const0> ;
  assign qspo[31] = \<const0> ;
  assign qspo[30] = \<const0> ;
  assign qspo[29] = \<const0> ;
  assign qspo[28] = \<const0> ;
  assign qspo[27] = \<const0> ;
  assign qspo[26] = \<const0> ;
  assign qspo[25] = \<const0> ;
  assign qspo[24] = \<const0> ;
  assign qspo[23] = \<const0> ;
  assign qspo[22] = \<const0> ;
  assign qspo[21] = \<const0> ;
  assign qspo[20] = \<const0> ;
  assign qspo[19] = \<const0> ;
  assign qspo[18] = \<const0> ;
  assign qspo[17] = \<const0> ;
  assign qspo[16] = \<const0> ;
  assign qspo[15] = \<const0> ;
  assign qspo[14] = \<const0> ;
  assign qspo[13] = \<const0> ;
  assign qspo[12] = \<const0> ;
  assign qspo[11] = \<const0> ;
  assign qspo[10] = \<const0> ;
  assign qspo[9] = \<const0> ;
  assign qspo[8] = \<const0> ;
  assign qspo[7] = \<const0> ;
  assign qspo[6] = \<const0> ;
  assign qspo[5] = \<const0> ;
  assign qspo[4] = \<const0> ;
  assign qspo[3] = \<const0> ;
  assign qspo[2] = \<const0> ;
  assign qspo[1] = \<const0> ;
  assign qspo[0] = \<const0> ;
GND GND
       (.G(\<const0> ));
dist_mem_gen_2_dist_mem_gen_v8_0_synth \synth_options.dist_mem_inst 
       (.a(a),
        .clk(clk),
        .d(d),
        .spo(spo),
        .we(we));
endmodule

(* ORIG_REF_NAME = "dist_mem_gen_v8_0_synth" *) 
module dist_mem_gen_2_dist_mem_gen_v8_0_synth
   (spo,
    a,
    d,
    clk,
    we);
  output [42:0]spo;
  input [4:0]a;
  input [42:0]d;
  input clk;
  input we;

  wire [4:0]a;
  wire clk;
  wire [42:0]d;
  wire [42:0]spo;
  wire we;

dist_mem_gen_2_spram__parameterized0 \gen_sp_ram.spram_inst 
       (.a(a),
        .clk(clk),
        .d(d),
        .spo(spo),
        .we(we));
endmodule

(* ORIG_REF_NAME = "spram" *) 
module dist_mem_gen_2_spram__parameterized0
   (spo,
    a,
    d,
    clk,
    we);
  output [42:0]spo;
  input [4:0]a;
  input [42:0]d;
  input clk;
  input we;

  wire [4:0]a;
  wire clk;
  wire [42:0]d;
(* RTL_KEEP = "true" *)   wire [42:0]qspo_int;
  wire [42:0]spo;
  wire we;

(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[0]),
        .Q(qspo_int[0]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[10]),
        .Q(qspo_int[10]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[11]),
        .Q(qspo_int[11]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[12]),
        .Q(qspo_int[12]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[13]),
        .Q(qspo_int[13]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[14]),
        .Q(qspo_int[14]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[15]),
        .Q(qspo_int[15]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[16] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[16]),
        .Q(qspo_int[16]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[17] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[17]),
        .Q(qspo_int[17]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[18] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[18]),
        .Q(qspo_int[18]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[19] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[19]),
        .Q(qspo_int[19]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[1]),
        .Q(qspo_int[1]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[20] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[20]),
        .Q(qspo_int[20]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[21] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[21]),
        .Q(qspo_int[21]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[22] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[22]),
        .Q(qspo_int[22]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[23] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[23]),
        .Q(qspo_int[23]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[24] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[24]),
        .Q(qspo_int[24]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[25] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[25]),
        .Q(qspo_int[25]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[26] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[26]),
        .Q(qspo_int[26]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[27] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[27]),
        .Q(qspo_int[27]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[28] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[28]),
        .Q(qspo_int[28]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[29] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[29]),
        .Q(qspo_int[29]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[2]),
        .Q(qspo_int[2]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[30] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[30]),
        .Q(qspo_int[30]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[31] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[31]),
        .Q(qspo_int[31]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[32] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[32]),
        .Q(qspo_int[32]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[33] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[33]),
        .Q(qspo_int[33]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[34] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[34]),
        .Q(qspo_int[34]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[35] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[35]),
        .Q(qspo_int[35]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[36] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[36]),
        .Q(qspo_int[36]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[37] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[37]),
        .Q(qspo_int[37]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[38] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[38]),
        .Q(qspo_int[38]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[39] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[39]),
        .Q(qspo_int[39]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[3]),
        .Q(qspo_int[3]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[40] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[40]),
        .Q(qspo_int[40]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[41] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[41]),
        .Q(qspo_int[41]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[42] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[42]),
        .Q(qspo_int[42]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[4]),
        .Q(qspo_int[4]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[5]),
        .Q(qspo_int[5]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[6]),
        .Q(qspo_int[6]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[7]),
        .Q(qspo_int[7]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[8]),
        .Q(qspo_int[8]),
        .R(1'b0));
(* KEEP = "yes" *) 
   (* equivalent_register_removal = "no" *) 
   FDRE #(
    .INIT(1'b0)) 
     \qspo_int_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(spo[9]),
        .Q(qspo_int[9]),
        .R(1'b0));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_0_0
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[0]),
        .O(spo[0]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_10_10
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[10]),
        .O(spo[10]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_11_11
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[11]),
        .O(spo[11]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_12_12
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[12]),
        .O(spo[12]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_13_13
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[13]),
        .O(spo[13]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_14_14
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[14]),
        .O(spo[14]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_15_15
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[15]),
        .O(spo[15]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_16_16
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[16]),
        .O(spo[16]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_17_17
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[17]),
        .O(spo[17]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_18_18
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[18]),
        .O(spo[18]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_19_19
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[19]),
        .O(spo[19]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_1_1
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[1]),
        .O(spo[1]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_20_20
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[20]),
        .O(spo[20]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_21_21
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[21]),
        .O(spo[21]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_22_22
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[22]),
        .O(spo[22]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_23_23
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[23]),
        .O(spo[23]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_24_24
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[24]),
        .O(spo[24]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_25_25
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[25]),
        .O(spo[25]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_26_26
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[26]),
        .O(spo[26]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_27_27
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[27]),
        .O(spo[27]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_28_28
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[28]),
        .O(spo[28]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_29_29
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[29]),
        .O(spo[29]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_2_2
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[2]),
        .O(spo[2]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_30_30
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[30]),
        .O(spo[30]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_31_31
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[31]),
        .O(spo[31]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_32_32
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[32]),
        .O(spo[32]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_33_33
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[33]),
        .O(spo[33]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_34_34
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[34]),
        .O(spo[34]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_35_35
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[35]),
        .O(spo[35]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_36_36
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[36]),
        .O(spo[36]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_37_37
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[37]),
        .O(spo[37]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_38_38
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[38]),
        .O(spo[38]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_39_39
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[39]),
        .O(spo[39]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_3_3
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[3]),
        .O(spo[3]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_40_40
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[40]),
        .O(spo[40]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_41_41
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[41]),
        .O(spo[41]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_42_42
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[42]),
        .O(spo[42]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_4_4
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[4]),
        .O(spo[4]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_5_5
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[5]),
        .O(spo[5]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_6_6
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[6]),
        .O(spo[6]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_7_7
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[7]),
        .O(spo[7]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_8_8
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[8]),
        .O(spo[8]),
        .WCLK(clk),
        .WE(we));
RAM32X1S #(
    .INIT(32'h00000000)) 
     ram_reg_0_31_9_9
       (.A0(a[0]),
        .A1(a[1]),
        .A2(a[2]),
        .A3(a[3]),
        .A4(a[4]),
        .D(d[9]),
        .O(spo[9]),
        .WCLK(clk),
        .WE(we));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
