// Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2014.2 (win64) Build 932637 Wed Jun 11 13:33:10 MDT 2014
// Date        : Fri Oct 07 18:12:26 2016
// Host        : TexasLDPC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               {C:/Users/jiangnan/Desktop/v1.3_sim_instructions-20161005T221036Z/v1.3_sim_instructions/RTL
//               Resources/IPs/dist_mem_gen_2/dist_mem_gen_2_stub.v}
// Design      : dist_mem_gen_2
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "dist_mem_gen_v8_0,Vivado 2014.2" *)
module dist_mem_gen_2(a, d, clk, we, spo)
/* synthesis syn_black_box black_box_pad_pin="a[4:0],d[42:0],clk,we,spo[42:0]" */;
  input [4:0]a;
  input [42:0]d;
  input clk;
  input we;
  output [42:0]spo;
endmodule
