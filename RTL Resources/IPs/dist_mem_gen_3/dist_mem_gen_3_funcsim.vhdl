-- Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2014.2 (win64) Build 932637 Wed Jun 11 13:33:10 MDT 2014
-- Date        : Wed Oct 05 18:22:26 2016
-- Host        : TexasLDPC running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               {C:/Users/jiangnan/Downloads/v1.3_sim_instructions-20161005T221036Z/v1.3_sim_instructions/RTL
--               Resources/IPs/dist_mem_gen_3/dist_mem_gen_3_funcsim.vhdl}
-- Design      : dist_mem_gen_3
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \dist_mem_gen_3_rom__parameterized0\ is
  port (
    spo : out STD_LOGIC_VECTOR ( 130 downto 0 );
    a : in STD_LOGIC_VECTOR ( 4 downto 0 );
    clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \dist_mem_gen_3_rom__parameterized0\ : entity is "rom";
end \dist_mem_gen_3_rom__parameterized0\;

architecture STRUCTURE of \dist_mem_gen_3_rom__parameterized0\ is
  signal a_reg : STD_LOGIC_VECTOR ( 4 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of g0_b0 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of g0_b1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of g0_b10 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of g0_b11 : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of g0_b12 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of g0_b128 : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of g0_b129 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of g0_b13 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of g0_b130 : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of g0_b131 : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of g0_b132 : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of g0_b133 : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of g0_b134 : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of g0_b135 : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of g0_b136 : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of g0_b137 : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of g0_b138 : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of g0_b139 : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of g0_b14 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of g0_b140 : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of g0_b141 : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of g0_b142 : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of g0_b143 : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of g0_b144 : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of g0_b145 : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of g0_b146 : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of g0_b147 : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of g0_b15 : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of g0_b16 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of g0_b160 : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of g0_b161 : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of g0_b162 : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of g0_b163 : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of g0_b164 : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of g0_b165 : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of g0_b166 : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of g0_b167 : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of g0_b168 : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of g0_b169 : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of g0_b17 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of g0_b170 : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of g0_b171 : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of g0_b172 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of g0_b173 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of g0_b174 : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of g0_b175 : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of g0_b176 : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of g0_b177 : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of g0_b178 : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of g0_b179 : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of g0_b18 : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of g0_b180 : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of g0_b181 : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of g0_b19 : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of g0_b192 : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of g0_b193 : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of g0_b194 : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of g0_b195 : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of g0_b196 : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of g0_b197 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of g0_b198 : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of g0_b199 : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of g0_b2 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of g0_b20 : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of g0_b200 : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of g0_b201 : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of g0_b202 : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of g0_b203 : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of g0_b204 : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of g0_b205 : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of g0_b206 : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of g0_b207 : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of g0_b208 : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of g0_b209 : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of g0_b21 : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of g0_b210 : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of g0_b211 : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of g0_b212 : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of g0_b213 : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of g0_b22 : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of g0_b3 : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of g0_b32 : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of g0_b33 : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of g0_b34 : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of g0_b35 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of g0_b36 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of g0_b37 : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of g0_b38 : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of g0_b39 : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of g0_b4 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of g0_b40 : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of g0_b41 : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of g0_b42 : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of g0_b43 : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of g0_b44 : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of g0_b45 : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of g0_b46 : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of g0_b47 : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of g0_b48 : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of g0_b49 : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of g0_b5 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of g0_b50 : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of g0_b51 : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of g0_b52 : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of g0_b53 : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of g0_b6 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of g0_b64 : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of g0_b65 : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of g0_b66 : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of g0_b67 : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of g0_b68 : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of g0_b69 : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of g0_b7 : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of g0_b70 : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of g0_b71 : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of g0_b72 : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of g0_b73 : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of g0_b74 : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of g0_b75 : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of g0_b76 : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of g0_b77 : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of g0_b78 : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of g0_b79 : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of g0_b8 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of g0_b80 : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of g0_b81 : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of g0_b82 : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of g0_b83 : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of g0_b84 : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of g0_b9 : label is "soft_lutpair3";
begin
\a_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => a(0),
      Q => a_reg(0),
      R => '0'
    );
\a_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => a(1),
      Q => a_reg(1),
      R => '0'
    );
\a_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => a(2),
      Q => a_reg(2),
      R => '0'
    );
\a_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => a(3),
      Q => a_reg(3),
      R => '0'
    );
\a_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => a(4),
      Q => a_reg(4),
      R => '0'
    );
g0_b0: unisim.vcomponents.LUT5
    generic map(
      INIT => X"600C01F3"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(0)
    );
g0_b1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E003FFF0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(1)
    );
g0_b10: unisim.vcomponents.LUT5
    generic map(
      INIT => X"800C3E03"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(10)
    );
g0_b11: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0381"
    )
    port map (
      I0 => a_reg(1),
      I1 => a_reg(2),
      I2 => a_reg(3),
      I3 => a_reg(4),
      O => spo(11)
    );
g0_b12: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6003C1F0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(12)
    );
g0_b128: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FF80000"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(66)
    );
g0_b129: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
    port map (
      I0 => a_reg(1),
      I1 => a_reg(2),
      I2 => a_reg(3),
      I3 => a_reg(4),
      O => spo(67)
    );
g0_b13: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E0003FF0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(13)
    );
g0_b130: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0007FE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(68)
    );
g0_b131: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000001FF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(69)
    );
g0_b132: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FF801FF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(70)
    );
g0_b133: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FFFFE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(71)
    );
g0_b134: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0007FE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(72)
    );
g0_b135: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000001FF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(73)
    );
g0_b136: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FF801FF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(74)
    );
g0_b137: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FFFFE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(75)
    );
g0_b138: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0007FE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(76)
    );
g0_b139: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000001FF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(77)
    );
g0_b14: unisim.vcomponents.LUT5
    generic map(
      INIT => X"800C3E03"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(14)
    );
g0_b140: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FF801FF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(78)
    );
g0_b141: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FFFFE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(79)
    );
g0_b142: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0007FE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(80)
    );
g0_b143: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000001FF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(81)
    );
g0_b144: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FF801FF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(82)
    );
g0_b145: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FFFFE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(83)
    );
g0_b146: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0007FE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(84)
    );
g0_b147: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000001FF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(85)
    );
g0_b15: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0381"
    )
    port map (
      I0 => a_reg(1),
      I1 => a_reg(2),
      I2 => a_reg(3),
      I3 => a_reg(4),
      O => spo(15)
    );
g0_b16: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6003C1F0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(16)
    );
g0_b160: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9FF801FC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(86)
    );
g0_b161: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FFFFE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(87)
    );
g0_b162: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8007C1FC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(88)
    );
g0_b163: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80003FFC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(89)
    );
g0_b164: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FF83E00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(90)
    );
g0_b165: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FFFC000"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(91)
    );
g0_b166: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8007C1FC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(92)
    );
g0_b167: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80003FFC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(93)
    );
g0_b168: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FF83E00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(94)
    );
g0_b169: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FFFC000"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(95)
    );
g0_b17: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E0003FF0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(17)
    );
g0_b170: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8007C1FC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(96)
    );
g0_b171: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80003FFC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(97)
    );
g0_b172: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FF83E00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(98)
    );
g0_b173: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FFFC000"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(99)
    );
g0_b174: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8007C1FC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(100)
    );
g0_b175: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80003FFC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(101)
    );
g0_b176: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FF83E00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(102)
    );
g0_b177: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FFFC000"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(103)
    );
g0_b178: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8007C1FC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(104)
    );
g0_b179: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80003FFC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(105)
    );
g0_b18: unisim.vcomponents.LUT5
    generic map(
      INIT => X"800C3E03"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(18)
    );
g0_b180: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000003"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(106)
    );
g0_b181: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(107)
    );
g0_b19: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0381"
    )
    port map (
      I0 => a_reg(1),
      I1 => a_reg(2),
      I2 => a_reg(3),
      I3 => a_reg(4),
      O => spo(19)
    );
g0_b192: unisim.vcomponents.LUT5
    generic map(
      INIT => X"600C01F3"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(108)
    );
g0_b193: unisim.vcomponents.LUT5
    generic map(
      INIT => X"800FFE03"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(109)
    );
g0_b194: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6003C1F0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(110)
    );
g0_b195: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E0003FF0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(111)
    );
g0_b196: unisim.vcomponents.LUT5
    generic map(
      INIT => X"800C3E03"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(112)
    );
g0_b197: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0381"
    )
    port map (
      I0 => a_reg(1),
      I1 => a_reg(2),
      I2 => a_reg(3),
      I3 => a_reg(4),
      O => spo(113)
    );
g0_b198: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6003C1F0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(114)
    );
g0_b199: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E0003FF0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(115)
    );
g0_b2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"800C3E03"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(2)
    );
g0_b20: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FC01"
    )
    port map (
      I0 => a_reg(1),
      I1 => a_reg(2),
      I2 => a_reg(3),
      I3 => a_reg(4),
      O => spo(20)
    );
g0_b200: unisim.vcomponents.LUT5
    generic map(
      INIT => X"800C3E03"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(116)
    );
g0_b201: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0381"
    )
    port map (
      I0 => a_reg(1),
      I1 => a_reg(2),
      I2 => a_reg(3),
      I3 => a_reg(4),
      O => spo(117)
    );
g0_b202: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6003C1F0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(118)
    );
g0_b203: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E0003FF0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(119)
    );
g0_b204: unisim.vcomponents.LUT5
    generic map(
      INIT => X"800C3E03"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(120)
    );
g0_b205: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0381"
    )
    port map (
      I0 => a_reg(1),
      I1 => a_reg(2),
      I2 => a_reg(3),
      I3 => a_reg(4),
      O => spo(121)
    );
g0_b206: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6003C1F0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(122)
    );
g0_b207: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E0003FF0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(123)
    );
g0_b208: unisim.vcomponents.LUT5
    generic map(
      INIT => X"800C3E03"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(124)
    );
g0_b209: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0381"
    )
    port map (
      I0 => a_reg(1),
      I1 => a_reg(2),
      I2 => a_reg(3),
      I3 => a_reg(4),
      O => spo(125)
    );
g0_b21: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => a_reg(1),
      I1 => a_reg(2),
      I2 => a_reg(3),
      I3 => a_reg(4),
      O => spo(21)
    );
g0_b210: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6003C1F0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(126)
    );
g0_b211: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E0003FF0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(127)
    );
g0_b212: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FFFFFF0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(128)
    );
g0_b213: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => a_reg(2),
      I1 => a_reg(3),
      I2 => a_reg(4),
      O => spo(129)
    );
g0_b214: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
    port map (
      I0 => a_reg(2),
      I1 => a_reg(3),
      I2 => a_reg(4),
      O => spo(130)
    );
g0_b22: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      I0 => a_reg(1),
      I1 => a_reg(2),
      I2 => a_reg(3),
      I3 => a_reg(4),
      O => spo(22)
    );
g0_b3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0381"
    )
    port map (
      I0 => a_reg(1),
      I1 => a_reg(2),
      I2 => a_reg(3),
      I3 => a_reg(4),
      O => spo(3)
    );
g0_b32: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9FF801FC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(23)
    );
g0_b33: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8007FFFC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(24)
    );
g0_b34: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FF83E00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(25)
    );
g0_b35: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FFFC000"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(26)
    );
g0_b36: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8007C1FC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(27)
    );
g0_b37: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80003FFC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(28)
    );
g0_b38: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FF83E00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(29)
    );
g0_b39: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FFFC000"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(30)
    );
g0_b4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6003C1F0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(4)
    );
g0_b40: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8007C1FC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(31)
    );
g0_b41: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80003FFC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(32)
    );
g0_b42: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FF83E00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(33)
    );
g0_b43: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FFFC000"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(34)
    );
g0_b44: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8007C1FC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(35)
    );
g0_b45: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80003FFC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(36)
    );
g0_b46: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FF83E00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(37)
    );
g0_b47: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FFFC000"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(38)
    );
g0_b48: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8007C1FC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(39)
    );
g0_b49: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80003FFC"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(40)
    );
g0_b5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E0003FF0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(5)
    );
g0_b50: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FF83E00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(41)
    );
g0_b51: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FFFC000"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(42)
    );
g0_b52: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1FFFFFFF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(43)
    );
g0_b53: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E0000000"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(44)
    );
g0_b6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"800C3E03"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(6)
    );
g0_b64: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FF80000"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(45)
    );
g0_b65: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0007FFFF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(46)
    );
g0_b66: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FF801FF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(47)
    );
g0_b67: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FFFFE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(48)
    );
g0_b68: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0007FE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(49)
    );
g0_b69: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000001FF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(50)
    );
g0_b7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0381"
    )
    port map (
      I0 => a_reg(1),
      I1 => a_reg(2),
      I2 => a_reg(3),
      I3 => a_reg(4),
      O => spo(7)
    );
g0_b70: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FF801FF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(51)
    );
g0_b71: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FFFFE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(52)
    );
g0_b72: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0007FE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(53)
    );
g0_b73: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000001FF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(54)
    );
g0_b74: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FF801FF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(55)
    );
g0_b75: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FFFFE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(56)
    );
g0_b76: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0007FE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(57)
    );
g0_b77: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000001FF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(58)
    );
g0_b78: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FF801FF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(59)
    );
g0_b79: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FFFFE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(60)
    );
g0_b8: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6003C1F0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(8)
    );
g0_b80: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0007FE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(61)
    );
g0_b81: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000001FF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(62)
    );
g0_b82: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FF801FF"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(63)
    );
g0_b83: unisim.vcomponents.LUT5
    generic map(
      INIT => X"3FFFFE00"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(64)
    );
g0_b84: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => a_reg(1),
      I1 => a_reg(2),
      I2 => a_reg(3),
      I3 => a_reg(4),
      O => spo(65)
    );
g0_b9: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E0003FF0"
    )
    port map (
      I0 => a_reg(0),
      I1 => a_reg(1),
      I2 => a_reg(2),
      I3 => a_reg(3),
      I4 => a_reg(4),
      O => spo(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity dist_mem_gen_3_dist_mem_gen_v8_0_synth is
  port (
    spo : out STD_LOGIC_VECTOR ( 130 downto 0 );
    a : in STD_LOGIC_VECTOR ( 4 downto 0 );
    clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of dist_mem_gen_3_dist_mem_gen_v8_0_synth : entity is "dist_mem_gen_v8_0_synth";
end dist_mem_gen_3_dist_mem_gen_v8_0_synth;

architecture STRUCTURE of dist_mem_gen_3_dist_mem_gen_v8_0_synth is
begin
\gen_rom.rom_inst\: entity work.\dist_mem_gen_3_rom__parameterized0\
    port map (
      a(4 downto 0) => a(4 downto 0),
      clk => clk,
      spo(130 downto 0) => spo(130 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ is
  port (
    a : in STD_LOGIC_VECTOR ( 4 downto 0 );
    d : in STD_LOGIC_VECTOR ( 223 downto 0 );
    dpra : in STD_LOGIC_VECTOR ( 4 downto 0 );
    clk : in STD_LOGIC;
    we : in STD_LOGIC;
    i_ce : in STD_LOGIC;
    qspo_ce : in STD_LOGIC;
    qdpo_ce : in STD_LOGIC;
    qdpo_clk : in STD_LOGIC;
    qspo_rst : in STD_LOGIC;
    qdpo_rst : in STD_LOGIC;
    qspo_srst : in STD_LOGIC;
    qdpo_srst : in STD_LOGIC;
    spo : out STD_LOGIC_VECTOR ( 223 downto 0 );
    dpo : out STD_LOGIC_VECTOR ( 223 downto 0 );
    qspo : out STD_LOGIC_VECTOR ( 223 downto 0 );
    qdpo : out STD_LOGIC_VECTOR ( 223 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is "dist_mem_gen_v8_0";
  attribute C_FAMILY : string;
  attribute C_FAMILY of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is "artix7";
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 5;
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 32;
  attribute C_HAS_CLK : integer;
  attribute C_HAS_CLK of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 1;
  attribute C_HAS_D : integer;
  attribute C_HAS_D of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_HAS_DPO : integer;
  attribute C_HAS_DPO of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_HAS_DPRA : integer;
  attribute C_HAS_DPRA of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_HAS_I_CE : integer;
  attribute C_HAS_I_CE of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_HAS_QDPO : integer;
  attribute C_HAS_QDPO of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_HAS_QDPO_CE : integer;
  attribute C_HAS_QDPO_CE of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_HAS_QDPO_CLK : integer;
  attribute C_HAS_QDPO_CLK of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_HAS_QDPO_RST : integer;
  attribute C_HAS_QDPO_RST of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_HAS_QDPO_SRST : integer;
  attribute C_HAS_QDPO_SRST of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_HAS_QSPO : integer;
  attribute C_HAS_QSPO of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_HAS_QSPO_CE : integer;
  attribute C_HAS_QSPO_CE of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_HAS_QSPO_RST : integer;
  attribute C_HAS_QSPO_RST of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_HAS_QSPO_SRST : integer;
  attribute C_HAS_QSPO_SRST of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_HAS_SPO : integer;
  attribute C_HAS_SPO of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 1;
  attribute C_HAS_WE : integer;
  attribute C_HAS_WE of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is "dist_mem_gen_3.mif";
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is "./";
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_PIPELINE_STAGES : integer;
  attribute C_PIPELINE_STAGES of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_QCE_JOINED : integer;
  attribute C_QCE_JOINED of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_QUALIFY_WE : integer;
  attribute C_QUALIFY_WE of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 1;
  attribute C_REG_A_D_INPUTS : integer;
  attribute C_REG_A_D_INPUTS of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 1;
  attribute C_REG_DPRA_INPUT : integer;
  attribute C_REG_DPRA_INPUT of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 0;
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 224;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ : entity is 1;
end \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\;

architecture STRUCTURE of \dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\ is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^spo\ : STD_LOGIC_VECTOR ( 214 downto 0 );
begin
  dpo(223) <= \<const0>\;
  dpo(222) <= \<const0>\;
  dpo(221) <= \<const0>\;
  dpo(220) <= \<const0>\;
  dpo(219) <= \<const0>\;
  dpo(218) <= \<const0>\;
  dpo(217) <= \<const0>\;
  dpo(216) <= \<const0>\;
  dpo(215) <= \<const0>\;
  dpo(214) <= \<const0>\;
  dpo(213) <= \<const0>\;
  dpo(212) <= \<const0>\;
  dpo(211) <= \<const0>\;
  dpo(210) <= \<const0>\;
  dpo(209) <= \<const0>\;
  dpo(208) <= \<const0>\;
  dpo(207) <= \<const0>\;
  dpo(206) <= \<const0>\;
  dpo(205) <= \<const0>\;
  dpo(204) <= \<const0>\;
  dpo(203) <= \<const0>\;
  dpo(202) <= \<const0>\;
  dpo(201) <= \<const0>\;
  dpo(200) <= \<const0>\;
  dpo(199) <= \<const0>\;
  dpo(198) <= \<const0>\;
  dpo(197) <= \<const0>\;
  dpo(196) <= \<const0>\;
  dpo(195) <= \<const0>\;
  dpo(194) <= \<const0>\;
  dpo(193) <= \<const0>\;
  dpo(192) <= \<const0>\;
  dpo(191) <= \<const0>\;
  dpo(190) <= \<const0>\;
  dpo(189) <= \<const0>\;
  dpo(188) <= \<const0>\;
  dpo(187) <= \<const0>\;
  dpo(186) <= \<const0>\;
  dpo(185) <= \<const0>\;
  dpo(184) <= \<const0>\;
  dpo(183) <= \<const0>\;
  dpo(182) <= \<const0>\;
  dpo(181) <= \<const0>\;
  dpo(180) <= \<const0>\;
  dpo(179) <= \<const0>\;
  dpo(178) <= \<const0>\;
  dpo(177) <= \<const0>\;
  dpo(176) <= \<const0>\;
  dpo(175) <= \<const0>\;
  dpo(174) <= \<const0>\;
  dpo(173) <= \<const0>\;
  dpo(172) <= \<const0>\;
  dpo(171) <= \<const0>\;
  dpo(170) <= \<const0>\;
  dpo(169) <= \<const0>\;
  dpo(168) <= \<const0>\;
  dpo(167) <= \<const0>\;
  dpo(166) <= \<const0>\;
  dpo(165) <= \<const0>\;
  dpo(164) <= \<const0>\;
  dpo(163) <= \<const0>\;
  dpo(162) <= \<const0>\;
  dpo(161) <= \<const0>\;
  dpo(160) <= \<const0>\;
  dpo(159) <= \<const0>\;
  dpo(158) <= \<const0>\;
  dpo(157) <= \<const0>\;
  dpo(156) <= \<const0>\;
  dpo(155) <= \<const0>\;
  dpo(154) <= \<const0>\;
  dpo(153) <= \<const0>\;
  dpo(152) <= \<const0>\;
  dpo(151) <= \<const0>\;
  dpo(150) <= \<const0>\;
  dpo(149) <= \<const0>\;
  dpo(148) <= \<const0>\;
  dpo(147) <= \<const0>\;
  dpo(146) <= \<const0>\;
  dpo(145) <= \<const0>\;
  dpo(144) <= \<const0>\;
  dpo(143) <= \<const0>\;
  dpo(142) <= \<const0>\;
  dpo(141) <= \<const0>\;
  dpo(140) <= \<const0>\;
  dpo(139) <= \<const0>\;
  dpo(138) <= \<const0>\;
  dpo(137) <= \<const0>\;
  dpo(136) <= \<const0>\;
  dpo(135) <= \<const0>\;
  dpo(134) <= \<const0>\;
  dpo(133) <= \<const0>\;
  dpo(132) <= \<const0>\;
  dpo(131) <= \<const0>\;
  dpo(130) <= \<const0>\;
  dpo(129) <= \<const0>\;
  dpo(128) <= \<const0>\;
  dpo(127) <= \<const0>\;
  dpo(126) <= \<const0>\;
  dpo(125) <= \<const0>\;
  dpo(124) <= \<const0>\;
  dpo(123) <= \<const0>\;
  dpo(122) <= \<const0>\;
  dpo(121) <= \<const0>\;
  dpo(120) <= \<const0>\;
  dpo(119) <= \<const0>\;
  dpo(118) <= \<const0>\;
  dpo(117) <= \<const0>\;
  dpo(116) <= \<const0>\;
  dpo(115) <= \<const0>\;
  dpo(114) <= \<const0>\;
  dpo(113) <= \<const0>\;
  dpo(112) <= \<const0>\;
  dpo(111) <= \<const0>\;
  dpo(110) <= \<const0>\;
  dpo(109) <= \<const0>\;
  dpo(108) <= \<const0>\;
  dpo(107) <= \<const0>\;
  dpo(106) <= \<const0>\;
  dpo(105) <= \<const0>\;
  dpo(104) <= \<const0>\;
  dpo(103) <= \<const0>\;
  dpo(102) <= \<const0>\;
  dpo(101) <= \<const0>\;
  dpo(100) <= \<const0>\;
  dpo(99) <= \<const0>\;
  dpo(98) <= \<const0>\;
  dpo(97) <= \<const0>\;
  dpo(96) <= \<const0>\;
  dpo(95) <= \<const0>\;
  dpo(94) <= \<const0>\;
  dpo(93) <= \<const0>\;
  dpo(92) <= \<const0>\;
  dpo(91) <= \<const0>\;
  dpo(90) <= \<const0>\;
  dpo(89) <= \<const0>\;
  dpo(88) <= \<const0>\;
  dpo(87) <= \<const0>\;
  dpo(86) <= \<const0>\;
  dpo(85) <= \<const0>\;
  dpo(84) <= \<const0>\;
  dpo(83) <= \<const0>\;
  dpo(82) <= \<const0>\;
  dpo(81) <= \<const0>\;
  dpo(80) <= \<const0>\;
  dpo(79) <= \<const0>\;
  dpo(78) <= \<const0>\;
  dpo(77) <= \<const0>\;
  dpo(76) <= \<const0>\;
  dpo(75) <= \<const0>\;
  dpo(74) <= \<const0>\;
  dpo(73) <= \<const0>\;
  dpo(72) <= \<const0>\;
  dpo(71) <= \<const0>\;
  dpo(70) <= \<const0>\;
  dpo(69) <= \<const0>\;
  dpo(68) <= \<const0>\;
  dpo(67) <= \<const0>\;
  dpo(66) <= \<const0>\;
  dpo(65) <= \<const0>\;
  dpo(64) <= \<const0>\;
  dpo(63) <= \<const0>\;
  dpo(62) <= \<const0>\;
  dpo(61) <= \<const0>\;
  dpo(60) <= \<const0>\;
  dpo(59) <= \<const0>\;
  dpo(58) <= \<const0>\;
  dpo(57) <= \<const0>\;
  dpo(56) <= \<const0>\;
  dpo(55) <= \<const0>\;
  dpo(54) <= \<const0>\;
  dpo(53) <= \<const0>\;
  dpo(52) <= \<const0>\;
  dpo(51) <= \<const0>\;
  dpo(50) <= \<const0>\;
  dpo(49) <= \<const0>\;
  dpo(48) <= \<const0>\;
  dpo(47) <= \<const0>\;
  dpo(46) <= \<const0>\;
  dpo(45) <= \<const0>\;
  dpo(44) <= \<const0>\;
  dpo(43) <= \<const0>\;
  dpo(42) <= \<const0>\;
  dpo(41) <= \<const0>\;
  dpo(40) <= \<const0>\;
  dpo(39) <= \<const0>\;
  dpo(38) <= \<const0>\;
  dpo(37) <= \<const0>\;
  dpo(36) <= \<const0>\;
  dpo(35) <= \<const0>\;
  dpo(34) <= \<const0>\;
  dpo(33) <= \<const0>\;
  dpo(32) <= \<const0>\;
  dpo(31) <= \<const0>\;
  dpo(30) <= \<const0>\;
  dpo(29) <= \<const0>\;
  dpo(28) <= \<const0>\;
  dpo(27) <= \<const0>\;
  dpo(26) <= \<const0>\;
  dpo(25) <= \<const0>\;
  dpo(24) <= \<const0>\;
  dpo(23) <= \<const0>\;
  dpo(22) <= \<const0>\;
  dpo(21) <= \<const0>\;
  dpo(20) <= \<const0>\;
  dpo(19) <= \<const0>\;
  dpo(18) <= \<const0>\;
  dpo(17) <= \<const0>\;
  dpo(16) <= \<const0>\;
  dpo(15) <= \<const0>\;
  dpo(14) <= \<const0>\;
  dpo(13) <= \<const0>\;
  dpo(12) <= \<const0>\;
  dpo(11) <= \<const0>\;
  dpo(10) <= \<const0>\;
  dpo(9) <= \<const0>\;
  dpo(8) <= \<const0>\;
  dpo(7) <= \<const0>\;
  dpo(6) <= \<const0>\;
  dpo(5) <= \<const0>\;
  dpo(4) <= \<const0>\;
  dpo(3) <= \<const0>\;
  dpo(2) <= \<const0>\;
  dpo(1) <= \<const0>\;
  dpo(0) <= \<const0>\;
  qdpo(223) <= \<const0>\;
  qdpo(222) <= \<const0>\;
  qdpo(221) <= \<const0>\;
  qdpo(220) <= \<const0>\;
  qdpo(219) <= \<const0>\;
  qdpo(218) <= \<const0>\;
  qdpo(217) <= \<const0>\;
  qdpo(216) <= \<const0>\;
  qdpo(215) <= \<const0>\;
  qdpo(214) <= \<const0>\;
  qdpo(213) <= \<const0>\;
  qdpo(212) <= \<const0>\;
  qdpo(211) <= \<const0>\;
  qdpo(210) <= \<const0>\;
  qdpo(209) <= \<const0>\;
  qdpo(208) <= \<const0>\;
  qdpo(207) <= \<const0>\;
  qdpo(206) <= \<const0>\;
  qdpo(205) <= \<const0>\;
  qdpo(204) <= \<const0>\;
  qdpo(203) <= \<const0>\;
  qdpo(202) <= \<const0>\;
  qdpo(201) <= \<const0>\;
  qdpo(200) <= \<const0>\;
  qdpo(199) <= \<const0>\;
  qdpo(198) <= \<const0>\;
  qdpo(197) <= \<const0>\;
  qdpo(196) <= \<const0>\;
  qdpo(195) <= \<const0>\;
  qdpo(194) <= \<const0>\;
  qdpo(193) <= \<const0>\;
  qdpo(192) <= \<const0>\;
  qdpo(191) <= \<const0>\;
  qdpo(190) <= \<const0>\;
  qdpo(189) <= \<const0>\;
  qdpo(188) <= \<const0>\;
  qdpo(187) <= \<const0>\;
  qdpo(186) <= \<const0>\;
  qdpo(185) <= \<const0>\;
  qdpo(184) <= \<const0>\;
  qdpo(183) <= \<const0>\;
  qdpo(182) <= \<const0>\;
  qdpo(181) <= \<const0>\;
  qdpo(180) <= \<const0>\;
  qdpo(179) <= \<const0>\;
  qdpo(178) <= \<const0>\;
  qdpo(177) <= \<const0>\;
  qdpo(176) <= \<const0>\;
  qdpo(175) <= \<const0>\;
  qdpo(174) <= \<const0>\;
  qdpo(173) <= \<const0>\;
  qdpo(172) <= \<const0>\;
  qdpo(171) <= \<const0>\;
  qdpo(170) <= \<const0>\;
  qdpo(169) <= \<const0>\;
  qdpo(168) <= \<const0>\;
  qdpo(167) <= \<const0>\;
  qdpo(166) <= \<const0>\;
  qdpo(165) <= \<const0>\;
  qdpo(164) <= \<const0>\;
  qdpo(163) <= \<const0>\;
  qdpo(162) <= \<const0>\;
  qdpo(161) <= \<const0>\;
  qdpo(160) <= \<const0>\;
  qdpo(159) <= \<const0>\;
  qdpo(158) <= \<const0>\;
  qdpo(157) <= \<const0>\;
  qdpo(156) <= \<const0>\;
  qdpo(155) <= \<const0>\;
  qdpo(154) <= \<const0>\;
  qdpo(153) <= \<const0>\;
  qdpo(152) <= \<const0>\;
  qdpo(151) <= \<const0>\;
  qdpo(150) <= \<const0>\;
  qdpo(149) <= \<const0>\;
  qdpo(148) <= \<const0>\;
  qdpo(147) <= \<const0>\;
  qdpo(146) <= \<const0>\;
  qdpo(145) <= \<const0>\;
  qdpo(144) <= \<const0>\;
  qdpo(143) <= \<const0>\;
  qdpo(142) <= \<const0>\;
  qdpo(141) <= \<const0>\;
  qdpo(140) <= \<const0>\;
  qdpo(139) <= \<const0>\;
  qdpo(138) <= \<const0>\;
  qdpo(137) <= \<const0>\;
  qdpo(136) <= \<const0>\;
  qdpo(135) <= \<const0>\;
  qdpo(134) <= \<const0>\;
  qdpo(133) <= \<const0>\;
  qdpo(132) <= \<const0>\;
  qdpo(131) <= \<const0>\;
  qdpo(130) <= \<const0>\;
  qdpo(129) <= \<const0>\;
  qdpo(128) <= \<const0>\;
  qdpo(127) <= \<const0>\;
  qdpo(126) <= \<const0>\;
  qdpo(125) <= \<const0>\;
  qdpo(124) <= \<const0>\;
  qdpo(123) <= \<const0>\;
  qdpo(122) <= \<const0>\;
  qdpo(121) <= \<const0>\;
  qdpo(120) <= \<const0>\;
  qdpo(119) <= \<const0>\;
  qdpo(118) <= \<const0>\;
  qdpo(117) <= \<const0>\;
  qdpo(116) <= \<const0>\;
  qdpo(115) <= \<const0>\;
  qdpo(114) <= \<const0>\;
  qdpo(113) <= \<const0>\;
  qdpo(112) <= \<const0>\;
  qdpo(111) <= \<const0>\;
  qdpo(110) <= \<const0>\;
  qdpo(109) <= \<const0>\;
  qdpo(108) <= \<const0>\;
  qdpo(107) <= \<const0>\;
  qdpo(106) <= \<const0>\;
  qdpo(105) <= \<const0>\;
  qdpo(104) <= \<const0>\;
  qdpo(103) <= \<const0>\;
  qdpo(102) <= \<const0>\;
  qdpo(101) <= \<const0>\;
  qdpo(100) <= \<const0>\;
  qdpo(99) <= \<const0>\;
  qdpo(98) <= \<const0>\;
  qdpo(97) <= \<const0>\;
  qdpo(96) <= \<const0>\;
  qdpo(95) <= \<const0>\;
  qdpo(94) <= \<const0>\;
  qdpo(93) <= \<const0>\;
  qdpo(92) <= \<const0>\;
  qdpo(91) <= \<const0>\;
  qdpo(90) <= \<const0>\;
  qdpo(89) <= \<const0>\;
  qdpo(88) <= \<const0>\;
  qdpo(87) <= \<const0>\;
  qdpo(86) <= \<const0>\;
  qdpo(85) <= \<const0>\;
  qdpo(84) <= \<const0>\;
  qdpo(83) <= \<const0>\;
  qdpo(82) <= \<const0>\;
  qdpo(81) <= \<const0>\;
  qdpo(80) <= \<const0>\;
  qdpo(79) <= \<const0>\;
  qdpo(78) <= \<const0>\;
  qdpo(77) <= \<const0>\;
  qdpo(76) <= \<const0>\;
  qdpo(75) <= \<const0>\;
  qdpo(74) <= \<const0>\;
  qdpo(73) <= \<const0>\;
  qdpo(72) <= \<const0>\;
  qdpo(71) <= \<const0>\;
  qdpo(70) <= \<const0>\;
  qdpo(69) <= \<const0>\;
  qdpo(68) <= \<const0>\;
  qdpo(67) <= \<const0>\;
  qdpo(66) <= \<const0>\;
  qdpo(65) <= \<const0>\;
  qdpo(64) <= \<const0>\;
  qdpo(63) <= \<const0>\;
  qdpo(62) <= \<const0>\;
  qdpo(61) <= \<const0>\;
  qdpo(60) <= \<const0>\;
  qdpo(59) <= \<const0>\;
  qdpo(58) <= \<const0>\;
  qdpo(57) <= \<const0>\;
  qdpo(56) <= \<const0>\;
  qdpo(55) <= \<const0>\;
  qdpo(54) <= \<const0>\;
  qdpo(53) <= \<const0>\;
  qdpo(52) <= \<const0>\;
  qdpo(51) <= \<const0>\;
  qdpo(50) <= \<const0>\;
  qdpo(49) <= \<const0>\;
  qdpo(48) <= \<const0>\;
  qdpo(47) <= \<const0>\;
  qdpo(46) <= \<const0>\;
  qdpo(45) <= \<const0>\;
  qdpo(44) <= \<const0>\;
  qdpo(43) <= \<const0>\;
  qdpo(42) <= \<const0>\;
  qdpo(41) <= \<const0>\;
  qdpo(40) <= \<const0>\;
  qdpo(39) <= \<const0>\;
  qdpo(38) <= \<const0>\;
  qdpo(37) <= \<const0>\;
  qdpo(36) <= \<const0>\;
  qdpo(35) <= \<const0>\;
  qdpo(34) <= \<const0>\;
  qdpo(33) <= \<const0>\;
  qdpo(32) <= \<const0>\;
  qdpo(31) <= \<const0>\;
  qdpo(30) <= \<const0>\;
  qdpo(29) <= \<const0>\;
  qdpo(28) <= \<const0>\;
  qdpo(27) <= \<const0>\;
  qdpo(26) <= \<const0>\;
  qdpo(25) <= \<const0>\;
  qdpo(24) <= \<const0>\;
  qdpo(23) <= \<const0>\;
  qdpo(22) <= \<const0>\;
  qdpo(21) <= \<const0>\;
  qdpo(20) <= \<const0>\;
  qdpo(19) <= \<const0>\;
  qdpo(18) <= \<const0>\;
  qdpo(17) <= \<const0>\;
  qdpo(16) <= \<const0>\;
  qdpo(15) <= \<const0>\;
  qdpo(14) <= \<const0>\;
  qdpo(13) <= \<const0>\;
  qdpo(12) <= \<const0>\;
  qdpo(11) <= \<const0>\;
  qdpo(10) <= \<const0>\;
  qdpo(9) <= \<const0>\;
  qdpo(8) <= \<const0>\;
  qdpo(7) <= \<const0>\;
  qdpo(6) <= \<const0>\;
  qdpo(5) <= \<const0>\;
  qdpo(4) <= \<const0>\;
  qdpo(3) <= \<const0>\;
  qdpo(2) <= \<const0>\;
  qdpo(1) <= \<const0>\;
  qdpo(0) <= \<const0>\;
  qspo(223) <= \<const0>\;
  qspo(222) <= \<const0>\;
  qspo(221) <= \<const0>\;
  qspo(220) <= \<const0>\;
  qspo(219) <= \<const0>\;
  qspo(218) <= \<const0>\;
  qspo(217) <= \<const0>\;
  qspo(216) <= \<const0>\;
  qspo(215) <= \<const0>\;
  qspo(214) <= \<const0>\;
  qspo(213) <= \<const0>\;
  qspo(212) <= \<const0>\;
  qspo(211) <= \<const0>\;
  qspo(210) <= \<const0>\;
  qspo(209) <= \<const0>\;
  qspo(208) <= \<const0>\;
  qspo(207) <= \<const0>\;
  qspo(206) <= \<const0>\;
  qspo(205) <= \<const0>\;
  qspo(204) <= \<const0>\;
  qspo(203) <= \<const0>\;
  qspo(202) <= \<const0>\;
  qspo(201) <= \<const0>\;
  qspo(200) <= \<const0>\;
  qspo(199) <= \<const0>\;
  qspo(198) <= \<const0>\;
  qspo(197) <= \<const0>\;
  qspo(196) <= \<const0>\;
  qspo(195) <= \<const0>\;
  qspo(194) <= \<const0>\;
  qspo(193) <= \<const0>\;
  qspo(192) <= \<const0>\;
  qspo(191) <= \<const0>\;
  qspo(190) <= \<const0>\;
  qspo(189) <= \<const0>\;
  qspo(188) <= \<const0>\;
  qspo(187) <= \<const0>\;
  qspo(186) <= \<const0>\;
  qspo(185) <= \<const0>\;
  qspo(184) <= \<const0>\;
  qspo(183) <= \<const0>\;
  qspo(182) <= \<const0>\;
  qspo(181) <= \<const0>\;
  qspo(180) <= \<const0>\;
  qspo(179) <= \<const0>\;
  qspo(178) <= \<const0>\;
  qspo(177) <= \<const0>\;
  qspo(176) <= \<const0>\;
  qspo(175) <= \<const0>\;
  qspo(174) <= \<const0>\;
  qspo(173) <= \<const0>\;
  qspo(172) <= \<const0>\;
  qspo(171) <= \<const0>\;
  qspo(170) <= \<const0>\;
  qspo(169) <= \<const0>\;
  qspo(168) <= \<const0>\;
  qspo(167) <= \<const0>\;
  qspo(166) <= \<const0>\;
  qspo(165) <= \<const0>\;
  qspo(164) <= \<const0>\;
  qspo(163) <= \<const0>\;
  qspo(162) <= \<const0>\;
  qspo(161) <= \<const0>\;
  qspo(160) <= \<const0>\;
  qspo(159) <= \<const0>\;
  qspo(158) <= \<const0>\;
  qspo(157) <= \<const0>\;
  qspo(156) <= \<const0>\;
  qspo(155) <= \<const0>\;
  qspo(154) <= \<const0>\;
  qspo(153) <= \<const0>\;
  qspo(152) <= \<const0>\;
  qspo(151) <= \<const0>\;
  qspo(150) <= \<const0>\;
  qspo(149) <= \<const0>\;
  qspo(148) <= \<const0>\;
  qspo(147) <= \<const0>\;
  qspo(146) <= \<const0>\;
  qspo(145) <= \<const0>\;
  qspo(144) <= \<const0>\;
  qspo(143) <= \<const0>\;
  qspo(142) <= \<const0>\;
  qspo(141) <= \<const0>\;
  qspo(140) <= \<const0>\;
  qspo(139) <= \<const0>\;
  qspo(138) <= \<const0>\;
  qspo(137) <= \<const0>\;
  qspo(136) <= \<const0>\;
  qspo(135) <= \<const0>\;
  qspo(134) <= \<const0>\;
  qspo(133) <= \<const0>\;
  qspo(132) <= \<const0>\;
  qspo(131) <= \<const0>\;
  qspo(130) <= \<const0>\;
  qspo(129) <= \<const0>\;
  qspo(128) <= \<const0>\;
  qspo(127) <= \<const0>\;
  qspo(126) <= \<const0>\;
  qspo(125) <= \<const0>\;
  qspo(124) <= \<const0>\;
  qspo(123) <= \<const0>\;
  qspo(122) <= \<const0>\;
  qspo(121) <= \<const0>\;
  qspo(120) <= \<const0>\;
  qspo(119) <= \<const0>\;
  qspo(118) <= \<const0>\;
  qspo(117) <= \<const0>\;
  qspo(116) <= \<const0>\;
  qspo(115) <= \<const0>\;
  qspo(114) <= \<const0>\;
  qspo(113) <= \<const0>\;
  qspo(112) <= \<const0>\;
  qspo(111) <= \<const0>\;
  qspo(110) <= \<const0>\;
  qspo(109) <= \<const0>\;
  qspo(108) <= \<const0>\;
  qspo(107) <= \<const0>\;
  qspo(106) <= \<const0>\;
  qspo(105) <= \<const0>\;
  qspo(104) <= \<const0>\;
  qspo(103) <= \<const0>\;
  qspo(102) <= \<const0>\;
  qspo(101) <= \<const0>\;
  qspo(100) <= \<const0>\;
  qspo(99) <= \<const0>\;
  qspo(98) <= \<const0>\;
  qspo(97) <= \<const0>\;
  qspo(96) <= \<const0>\;
  qspo(95) <= \<const0>\;
  qspo(94) <= \<const0>\;
  qspo(93) <= \<const0>\;
  qspo(92) <= \<const0>\;
  qspo(91) <= \<const0>\;
  qspo(90) <= \<const0>\;
  qspo(89) <= \<const0>\;
  qspo(88) <= \<const0>\;
  qspo(87) <= \<const0>\;
  qspo(86) <= \<const0>\;
  qspo(85) <= \<const0>\;
  qspo(84) <= \<const0>\;
  qspo(83) <= \<const0>\;
  qspo(82) <= \<const0>\;
  qspo(81) <= \<const0>\;
  qspo(80) <= \<const0>\;
  qspo(79) <= \<const0>\;
  qspo(78) <= \<const0>\;
  qspo(77) <= \<const0>\;
  qspo(76) <= \<const0>\;
  qspo(75) <= \<const0>\;
  qspo(74) <= \<const0>\;
  qspo(73) <= \<const0>\;
  qspo(72) <= \<const0>\;
  qspo(71) <= \<const0>\;
  qspo(70) <= \<const0>\;
  qspo(69) <= \<const0>\;
  qspo(68) <= \<const0>\;
  qspo(67) <= \<const0>\;
  qspo(66) <= \<const0>\;
  qspo(65) <= \<const0>\;
  qspo(64) <= \<const0>\;
  qspo(63) <= \<const0>\;
  qspo(62) <= \<const0>\;
  qspo(61) <= \<const0>\;
  qspo(60) <= \<const0>\;
  qspo(59) <= \<const0>\;
  qspo(58) <= \<const0>\;
  qspo(57) <= \<const0>\;
  qspo(56) <= \<const0>\;
  qspo(55) <= \<const0>\;
  qspo(54) <= \<const0>\;
  qspo(53) <= \<const0>\;
  qspo(52) <= \<const0>\;
  qspo(51) <= \<const0>\;
  qspo(50) <= \<const0>\;
  qspo(49) <= \<const0>\;
  qspo(48) <= \<const0>\;
  qspo(47) <= \<const0>\;
  qspo(46) <= \<const0>\;
  qspo(45) <= \<const0>\;
  qspo(44) <= \<const0>\;
  qspo(43) <= \<const0>\;
  qspo(42) <= \<const0>\;
  qspo(41) <= \<const0>\;
  qspo(40) <= \<const0>\;
  qspo(39) <= \<const0>\;
  qspo(38) <= \<const0>\;
  qspo(37) <= \<const0>\;
  qspo(36) <= \<const0>\;
  qspo(35) <= \<const0>\;
  qspo(34) <= \<const0>\;
  qspo(33) <= \<const0>\;
  qspo(32) <= \<const0>\;
  qspo(31) <= \<const0>\;
  qspo(30) <= \<const0>\;
  qspo(29) <= \<const0>\;
  qspo(28) <= \<const0>\;
  qspo(27) <= \<const0>\;
  qspo(26) <= \<const0>\;
  qspo(25) <= \<const0>\;
  qspo(24) <= \<const0>\;
  qspo(23) <= \<const0>\;
  qspo(22) <= \<const0>\;
  qspo(21) <= \<const0>\;
  qspo(20) <= \<const0>\;
  qspo(19) <= \<const0>\;
  qspo(18) <= \<const0>\;
  qspo(17) <= \<const0>\;
  qspo(16) <= \<const0>\;
  qspo(15) <= \<const0>\;
  qspo(14) <= \<const0>\;
  qspo(13) <= \<const0>\;
  qspo(12) <= \<const0>\;
  qspo(11) <= \<const0>\;
  qspo(10) <= \<const0>\;
  qspo(9) <= \<const0>\;
  qspo(8) <= \<const0>\;
  qspo(7) <= \<const0>\;
  qspo(6) <= \<const0>\;
  qspo(5) <= \<const0>\;
  qspo(4) <= \<const0>\;
  qspo(3) <= \<const0>\;
  qspo(2) <= \<const0>\;
  qspo(1) <= \<const0>\;
  qspo(0) <= \<const0>\;
  spo(223) <= \<const0>\;
  spo(222) <= \<const0>\;
  spo(221) <= \<const0>\;
  spo(220) <= \<const0>\;
  spo(219) <= \<const0>\;
  spo(218) <= \<const0>\;
  spo(217) <= \<const0>\;
  spo(216) <= \<const0>\;
  spo(215) <= \<const0>\;
  spo(214 downto 192) <= \^spo\(214 downto 192);
  spo(191) <= \<const0>\;
  spo(190) <= \<const0>\;
  spo(189) <= \<const0>\;
  spo(188) <= \<const0>\;
  spo(187) <= \<const0>\;
  spo(186) <= \<const0>\;
  spo(185) <= \<const0>\;
  spo(184) <= \<const0>\;
  spo(183) <= \<const0>\;
  spo(182) <= \<const0>\;
  spo(181 downto 160) <= \^spo\(181 downto 160);
  spo(159) <= \<const0>\;
  spo(158) <= \<const0>\;
  spo(157) <= \<const0>\;
  spo(156) <= \<const0>\;
  spo(155) <= \<const0>\;
  spo(154) <= \<const0>\;
  spo(153) <= \<const0>\;
  spo(152) <= \<const0>\;
  spo(151) <= \<const0>\;
  spo(150) <= \<const0>\;
  spo(149) <= \<const0>\;
  spo(148) <= \<const1>\;
  spo(147 downto 128) <= \^spo\(147 downto 128);
  spo(127) <= \<const0>\;
  spo(126) <= \<const0>\;
  spo(125) <= \<const0>\;
  spo(124) <= \<const0>\;
  spo(123) <= \<const0>\;
  spo(122) <= \<const0>\;
  spo(121) <= \<const0>\;
  spo(120) <= \<const0>\;
  spo(119) <= \<const0>\;
  spo(118) <= \<const0>\;
  spo(117) <= \<const0>\;
  spo(116) <= \<const0>\;
  spo(115) <= \<const0>\;
  spo(114) <= \<const0>\;
  spo(113) <= \<const0>\;
  spo(112) <= \<const0>\;
  spo(111) <= \<const0>\;
  spo(110) <= \<const0>\;
  spo(109) <= \<const0>\;
  spo(108) <= \<const0>\;
  spo(107) <= \<const0>\;
  spo(106) <= \<const0>\;
  spo(105) <= \<const0>\;
  spo(104) <= \<const0>\;
  spo(103) <= \<const0>\;
  spo(102) <= \<const0>\;
  spo(101) <= \<const0>\;
  spo(100) <= \<const0>\;
  spo(99) <= \<const0>\;
  spo(98) <= \<const0>\;
  spo(97) <= \<const0>\;
  spo(96) <= \<const0>\;
  spo(95) <= \<const1>\;
  spo(94) <= \<const1>\;
  spo(93) <= \<const1>\;
  spo(92) <= \<const1>\;
  spo(91) <= \<const1>\;
  spo(90) <= \<const1>\;
  spo(89) <= \<const1>\;
  spo(88) <= \<const1>\;
  spo(87) <= \<const1>\;
  spo(86) <= \<const1>\;
  spo(85) <= \<const1>\;
  spo(84 downto 64) <= \^spo\(84 downto 64);
  spo(63) <= \<const1>\;
  spo(62) <= \<const1>\;
  spo(61) <= \<const1>\;
  spo(60) <= \<const1>\;
  spo(59) <= \<const1>\;
  spo(58) <= \<const1>\;
  spo(57) <= \<const1>\;
  spo(56) <= \<const1>\;
  spo(55) <= \<const1>\;
  spo(54) <= \<const1>\;
  spo(53 downto 32) <= \^spo\(53 downto 32);
  spo(31) <= \<const1>\;
  spo(30) <= \<const1>\;
  spo(29) <= \<const1>\;
  spo(28) <= \<const1>\;
  spo(27) <= \<const1>\;
  spo(26) <= \<const1>\;
  spo(25) <= \<const1>\;
  spo(24) <= \<const1>\;
  spo(23) <= \<const1>\;
  spo(22 downto 0) <= \^spo\(22 downto 0);
GND: unisim.vcomponents.GND
    port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
    port map (
      P => \<const1>\
    );
\synth_options.dist_mem_inst\: entity work.dist_mem_gen_3_dist_mem_gen_v8_0_synth
    port map (
      a(4 downto 0) => a(4 downto 0),
      clk => clk,
      spo(130 downto 108) => \^spo\(214 downto 192),
      spo(107 downto 86) => \^spo\(181 downto 160),
      spo(85 downto 66) => \^spo\(147 downto 128),
      spo(65 downto 45) => \^spo\(84 downto 64),
      spo(44 downto 23) => \^spo\(53 downto 32),
      spo(22 downto 0) => \^spo\(22 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity dist_mem_gen_3 is
  port (
    a : in STD_LOGIC_VECTOR ( 4 downto 0 );
    clk : in STD_LOGIC;
    spo : out STD_LOGIC_VECTOR ( 223 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of dist_mem_gen_3 : entity is true;
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of dist_mem_gen_3 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of dist_mem_gen_3 : entity is "dist_mem_gen_v8_0,Vivado 2014.2";
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of dist_mem_gen_3 : entity is "dist_mem_gen_3,dist_mem_gen_v8_0,{}";
  attribute core_generation_info : string;
  attribute core_generation_info of dist_mem_gen_3 : entity is "dist_mem_gen_3,dist_mem_gen_v8_0,{x_ipProduct=Vivado 2014.2,x_ipVendor=xilinx.com,x_ipLibrary=ip,x_ipName=dist_mem_gen,x_ipVersion=8.0,x_ipCoreRevision=5,x_ipLanguage=VERILOG,C_FAMILY=artix7,C_ADDR_WIDTH=5,C_DEFAULT_DATA=0,C_DEPTH=32,C_HAS_CLK=1,C_HAS_D=0,C_HAS_DPO=0,C_HAS_DPRA=0,C_HAS_I_CE=0,C_HAS_QDPO=0,C_HAS_QDPO_CE=0,C_HAS_QDPO_CLK=0,C_HAS_QDPO_RST=0,C_HAS_QDPO_SRST=0,C_HAS_QSPO=0,C_HAS_QSPO_CE=0,C_HAS_QSPO_RST=0,C_HAS_QSPO_SRST=0,C_HAS_SPO=1,C_HAS_WE=0,C_MEM_INIT_FILE=dist_mem_gen_3.mif,C_ELABORATION_DIR=./,C_MEM_TYPE=0,C_PIPELINE_STAGES=0,C_QCE_JOINED=0,C_QUALIFY_WE=0,C_READ_MIF=1,C_REG_A_D_INPUTS=1,C_REG_DPRA_INPUT=0,C_SYNC_ENABLE=1,C_WIDTH=224,C_PARSER_TYPE=1}";
end dist_mem_gen_3;

architecture STRUCTURE of dist_mem_gen_3 is
  signal NLW_U0_dpo_UNCONNECTED : STD_LOGIC_VECTOR ( 223 downto 0 );
  signal NLW_U0_qdpo_UNCONNECTED : STD_LOGIC_VECTOR ( 223 downto 0 );
  signal NLW_U0_qspo_UNCONNECTED : STD_LOGIC_VECTOR ( 223 downto 0 );
  attribute C_FAMILY : string;
  attribute C_FAMILY of U0 : label is "artix7";
  attribute C_HAS_D : integer;
  attribute C_HAS_D of U0 : label is 0;
  attribute C_HAS_DPO : integer;
  attribute C_HAS_DPO of U0 : label is 0;
  attribute C_HAS_DPRA : integer;
  attribute C_HAS_DPRA of U0 : label is 0;
  attribute C_HAS_I_CE : integer;
  attribute C_HAS_I_CE of U0 : label is 0;
  attribute C_HAS_QDPO : integer;
  attribute C_HAS_QDPO of U0 : label is 0;
  attribute C_HAS_QDPO_CE : integer;
  attribute C_HAS_QDPO_CE of U0 : label is 0;
  attribute C_HAS_QDPO_CLK : integer;
  attribute C_HAS_QDPO_CLK of U0 : label is 0;
  attribute C_HAS_QDPO_RST : integer;
  attribute C_HAS_QDPO_RST of U0 : label is 0;
  attribute C_HAS_QDPO_SRST : integer;
  attribute C_HAS_QDPO_SRST of U0 : label is 0;
  attribute C_HAS_WE : integer;
  attribute C_HAS_WE of U0 : label is 0;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of U0 : label is 0;
  attribute C_PIPELINE_STAGES : integer;
  attribute C_PIPELINE_STAGES of U0 : label is 0;
  attribute C_QCE_JOINED : integer;
  attribute C_QCE_JOINED of U0 : label is 0;
  attribute C_QUALIFY_WE : integer;
  attribute C_QUALIFY_WE of U0 : label is 0;
  attribute C_REG_DPRA_INPUT : integer;
  attribute C_REG_DPRA_INPUT of U0 : label is 0;
  attribute DONT_TOUCH : boolean;
  attribute DONT_TOUCH of U0 : label is std.standard.true;
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 5;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 32;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_clk : integer;
  attribute c_has_clk of U0 : label is 1;
  attribute c_has_qspo : integer;
  attribute c_has_qspo of U0 : label is 0;
  attribute c_has_qspo_ce : integer;
  attribute c_has_qspo_ce of U0 : label is 0;
  attribute c_has_qspo_rst : integer;
  attribute c_has_qspo_rst of U0 : label is 0;
  attribute c_has_qspo_srst : integer;
  attribute c_has_qspo_srst of U0 : label is 0;
  attribute c_has_spo : integer;
  attribute c_has_spo of U0 : label is 1;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "dist_mem_gen_3.mif";
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 1;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 1;
  attribute c_reg_a_d_inputs : integer;
  attribute c_reg_a_d_inputs of U0 : label is 1;
  attribute c_sync_enable : integer;
  attribute c_sync_enable of U0 : label is 1;
  attribute c_width : integer;
  attribute c_width of U0 : label is 224;
begin
U0: entity work.\dist_mem_gen_3_dist_mem_gen_v8_0__parameterized0\
    port map (
      a(4 downto 0) => a(4 downto 0),
      clk => clk,
      d(223) => '0',
      d(222) => '0',
      d(221) => '0',
      d(220) => '0',
      d(219) => '0',
      d(218) => '0',
      d(217) => '0',
      d(216) => '0',
      d(215) => '0',
      d(214) => '0',
      d(213) => '0',
      d(212) => '0',
      d(211) => '0',
      d(210) => '0',
      d(209) => '0',
      d(208) => '0',
      d(207) => '0',
      d(206) => '0',
      d(205) => '0',
      d(204) => '0',
      d(203) => '0',
      d(202) => '0',
      d(201) => '0',
      d(200) => '0',
      d(199) => '0',
      d(198) => '0',
      d(197) => '0',
      d(196) => '0',
      d(195) => '0',
      d(194) => '0',
      d(193) => '0',
      d(192) => '0',
      d(191) => '0',
      d(190) => '0',
      d(189) => '0',
      d(188) => '0',
      d(187) => '0',
      d(186) => '0',
      d(185) => '0',
      d(184) => '0',
      d(183) => '0',
      d(182) => '0',
      d(181) => '0',
      d(180) => '0',
      d(179) => '0',
      d(178) => '0',
      d(177) => '0',
      d(176) => '0',
      d(175) => '0',
      d(174) => '0',
      d(173) => '0',
      d(172) => '0',
      d(171) => '0',
      d(170) => '0',
      d(169) => '0',
      d(168) => '0',
      d(167) => '0',
      d(166) => '0',
      d(165) => '0',
      d(164) => '0',
      d(163) => '0',
      d(162) => '0',
      d(161) => '0',
      d(160) => '0',
      d(159) => '0',
      d(158) => '0',
      d(157) => '0',
      d(156) => '0',
      d(155) => '0',
      d(154) => '0',
      d(153) => '0',
      d(152) => '0',
      d(151) => '0',
      d(150) => '0',
      d(149) => '0',
      d(148) => '0',
      d(147) => '0',
      d(146) => '0',
      d(145) => '0',
      d(144) => '0',
      d(143) => '0',
      d(142) => '0',
      d(141) => '0',
      d(140) => '0',
      d(139) => '0',
      d(138) => '0',
      d(137) => '0',
      d(136) => '0',
      d(135) => '0',
      d(134) => '0',
      d(133) => '0',
      d(132) => '0',
      d(131) => '0',
      d(130) => '0',
      d(129) => '0',
      d(128) => '0',
      d(127) => '0',
      d(126) => '0',
      d(125) => '0',
      d(124) => '0',
      d(123) => '0',
      d(122) => '0',
      d(121) => '0',
      d(120) => '0',
      d(119) => '0',
      d(118) => '0',
      d(117) => '0',
      d(116) => '0',
      d(115) => '0',
      d(114) => '0',
      d(113) => '0',
      d(112) => '0',
      d(111) => '0',
      d(110) => '0',
      d(109) => '0',
      d(108) => '0',
      d(107) => '0',
      d(106) => '0',
      d(105) => '0',
      d(104) => '0',
      d(103) => '0',
      d(102) => '0',
      d(101) => '0',
      d(100) => '0',
      d(99) => '0',
      d(98) => '0',
      d(97) => '0',
      d(96) => '0',
      d(95) => '0',
      d(94) => '0',
      d(93) => '0',
      d(92) => '0',
      d(91) => '0',
      d(90) => '0',
      d(89) => '0',
      d(88) => '0',
      d(87) => '0',
      d(86) => '0',
      d(85) => '0',
      d(84) => '0',
      d(83) => '0',
      d(82) => '0',
      d(81) => '0',
      d(80) => '0',
      d(79) => '0',
      d(78) => '0',
      d(77) => '0',
      d(76) => '0',
      d(75) => '0',
      d(74) => '0',
      d(73) => '0',
      d(72) => '0',
      d(71) => '0',
      d(70) => '0',
      d(69) => '0',
      d(68) => '0',
      d(67) => '0',
      d(66) => '0',
      d(65) => '0',
      d(64) => '0',
      d(63) => '0',
      d(62) => '0',
      d(61) => '0',
      d(60) => '0',
      d(59) => '0',
      d(58) => '0',
      d(57) => '0',
      d(56) => '0',
      d(55) => '0',
      d(54) => '0',
      d(53) => '0',
      d(52) => '0',
      d(51) => '0',
      d(50) => '0',
      d(49) => '0',
      d(48) => '0',
      d(47) => '0',
      d(46) => '0',
      d(45) => '0',
      d(44) => '0',
      d(43) => '0',
      d(42) => '0',
      d(41) => '0',
      d(40) => '0',
      d(39) => '0',
      d(38) => '0',
      d(37) => '0',
      d(36) => '0',
      d(35) => '0',
      d(34) => '0',
      d(33) => '0',
      d(32) => '0',
      d(31) => '0',
      d(30) => '0',
      d(29) => '0',
      d(28) => '0',
      d(27) => '0',
      d(26) => '0',
      d(25) => '0',
      d(24) => '0',
      d(23) => '0',
      d(22) => '0',
      d(21) => '0',
      d(20) => '0',
      d(19) => '0',
      d(18) => '0',
      d(17) => '0',
      d(16) => '0',
      d(15) => '0',
      d(14) => '0',
      d(13) => '0',
      d(12) => '0',
      d(11) => '0',
      d(10) => '0',
      d(9) => '0',
      d(8) => '0',
      d(7) => '0',
      d(6) => '0',
      d(5) => '0',
      d(4) => '0',
      d(3) => '0',
      d(2) => '0',
      d(1) => '0',
      d(0) => '0',
      dpo(223 downto 0) => NLW_U0_dpo_UNCONNECTED(223 downto 0),
      dpra(4) => '0',
      dpra(3) => '0',
      dpra(2) => '0',
      dpra(1) => '0',
      dpra(0) => '0',
      i_ce => '1',
      qdpo(223 downto 0) => NLW_U0_qdpo_UNCONNECTED(223 downto 0),
      qdpo_ce => '1',
      qdpo_clk => '0',
      qdpo_rst => '0',
      qdpo_srst => '0',
      qspo(223 downto 0) => NLW_U0_qspo_UNCONNECTED(223 downto 0),
      qspo_ce => '1',
      qspo_rst => '0',
      qspo_srst => '0',
      spo(223 downto 0) => spo(223 downto 0),
      we => '0'
    );
end STRUCTURE;
