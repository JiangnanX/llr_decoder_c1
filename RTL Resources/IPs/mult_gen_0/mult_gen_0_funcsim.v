// Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2014.2 (win64) Build 932637 Wed Jun 11 13:33:10 MDT 2014
// Date        : Tue Aug 11 17:40:36 2015
// Host        : Lenovo-Laptop running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim {C:/Dropbox/TL/RTL/TL_Decoder/Projects/Lara/1272_140_5bit_SD_C1/RTL
//               Resources/IPs/mult_gen_0/mult_gen_0_funcsim.v}
// Design      : mult_gen_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "mult_gen_v12_0,Vivado 2014.2" *) (* CHECK_LICENSE_TYPE = "mult_gen_0,mult_gen_v12_0,{}" *) 
(* core_generation_info = "mult_gen_0,mult_gen_v12_0,{x_ipProduct=Vivado 2014.2,x_ipVendor=xilinx.com,x_ipLibrary=ip,x_ipName=mult_gen,x_ipVersion=12.0,x_ipCoreRevision=5,x_ipLanguage=VERILOG,C_VERBOSITY=0,C_MODEL_TYPE=0,C_OPTIMIZE_GOAL=1,C_XDEVICEFAMILY=artix7,C_HAS_CE=0,C_HAS_SCLR=0,C_LATENCY=1,C_A_WIDTH=16,C_A_TYPE=0,C_B_WIDTH=16,C_B_TYPE=0,C_OUT_HIGH=31,C_OUT_LOW=0,C_MULT_TYPE=1,C_CE_OVERRIDES_SCLR=0,C_CCM_IMP=0,C_B_VALUE=10000001,C_HAS_ZERO_DETECT=0,C_ROUND_OUTPUT=0,C_ROUND_PT=0}" *) 
(* NotValidForBitStream *)
module mult_gen_0
   (CLK,
    A,
    B,
    P);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) input CLK;
  input [15:0]A;
  input [15:0]B;
  output [31:0]P;

  wire [15:0]A;
  wire [15:0]B;
  wire CLK;
  wire [31:0]P;
  wire [47:0]NLW_U0_PCASC_UNCONNECTED;
  wire [1:0]NLW_U0_ZERO_DETECT_UNCONNECTED;

(* C_A_TYPE = "0" *) 
   (* C_A_WIDTH = "16" *) 
   (* C_B_TYPE = "0" *) 
   (* C_B_VALUE = "10000001" *) 
   (* C_B_WIDTH = "16" *) 
   (* C_CCM_IMP = "0" *) 
   (* C_CE_OVERRIDES_SCLR = "0" *) 
   (* C_HAS_CE = "0" *) 
   (* C_HAS_SCLR = "0" *) 
   (* C_HAS_ZERO_DETECT = "0" *) 
   (* C_LATENCY = "1" *) 
   (* C_MODEL_TYPE = "0" *) 
   (* C_MULT_TYPE = "1" *) 
   (* C_OPTIMIZE_GOAL = "1" *) 
   (* C_OUT_HIGH = "31" *) 
   (* C_OUT_LOW = "0" *) 
   (* C_ROUND_OUTPUT = "0" *) 
   (* C_ROUND_PT = "0" *) 
   (* C_VERBOSITY = "0" *) 
   (* C_XDEVICEFAMILY = "artix7" *) 
   (* DONT_TOUCH *) 
   (* downgradeipidentifiedwarnings = "yes" *) 
   mult_gen_0_mult_gen_v12_0__parameterized0 U0
       (.A(A),
        .B(B),
        .CE(1'b1),
        .CLK(CLK),
        .P(P),
        .PCASC(NLW_U0_PCASC_UNCONNECTED[47:0]),
        .SCLR(1'b0),
        .ZERO_DETECT(NLW_U0_ZERO_DETECT_UNCONNECTED[1:0]));
endmodule

(* ORIG_REF_NAME = "mult_gen_v12_0" *) (* C_VERBOSITY = "0" *) (* C_MODEL_TYPE = "0" *) 
(* C_OPTIMIZE_GOAL = "1" *) (* C_XDEVICEFAMILY = "artix7" *) (* C_HAS_CE = "0" *) 
(* C_HAS_SCLR = "0" *) (* C_LATENCY = "1" *) (* C_A_WIDTH = "16" *) 
(* C_A_TYPE = "0" *) (* C_B_WIDTH = "16" *) (* C_B_TYPE = "0" *) 
(* C_OUT_HIGH = "31" *) (* C_OUT_LOW = "0" *) (* C_MULT_TYPE = "1" *) 
(* C_CE_OVERRIDES_SCLR = "0" *) (* C_CCM_IMP = "0" *) (* C_B_VALUE = "10000001" *) 
(* C_HAS_ZERO_DETECT = "0" *) (* C_ROUND_OUTPUT = "0" *) (* C_ROUND_PT = "0" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module mult_gen_0_mult_gen_v12_0__parameterized0
   (CLK,
    A,
    B,
    CE,
    SCLR,
    ZERO_DETECT,
    P,
    PCASC);
  input CLK;
  input [15:0]A;
  input [15:0]B;
  input CE;
  input SCLR;
  output [1:0]ZERO_DETECT;
  output [31:0]P;
  output [47:0]PCASC;

  wire [15:0]A;
  wire [15:0]B;
  wire CE;
  wire CLK;
  wire [31:0]P;
  wire [47:0]PCASC;
  wire SCLR;
  wire [1:0]ZERO_DETECT;

(* C_A_TYPE = "0" *) 
   (* C_A_WIDTH = "16" *) 
   (* C_B_TYPE = "0" *) 
   (* C_B_VALUE = "10000001" *) 
   (* C_B_WIDTH = "16" *) 
   (* C_CCM_IMP = "0" *) 
   (* C_CE_OVERRIDES_SCLR = "0" *) 
   (* C_HAS_CE = "0" *) 
   (* C_HAS_SCLR = "0" *) 
   (* C_HAS_ZERO_DETECT = "0" *) 
   (* C_LATENCY = "1" *) 
   (* C_MODEL_TYPE = "0" *) 
   (* C_MULT_TYPE = "1" *) 
   (* C_OPTIMIZE_GOAL = "1" *) 
   (* C_OUT_HIGH = "31" *) 
   (* C_OUT_LOW = "0" *) 
   (* C_ROUND_OUTPUT = "0" *) 
   (* C_ROUND_PT = "0" *) 
   (* C_VERBOSITY = "0" *) 
   (* C_XDEVICEFAMILY = "artix7" *) 
   (* downgradeipidentifiedwarnings = "yes" *) 
   (* secure_extras = "A" *) 
   mult_gen_0_mult_gen_v12_0_viv__parameterized0 i_mult
       (.A(A),
        .B(B),
        .CE(CE),
        .CLK(CLK),
        .P(P),
        .PCASC(PCASC),
        .SCLR(SCLR),
        .ZERO_DETECT(ZERO_DETECT));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2014"
`pragma protect key_keyowner = "Cadence Design Systems.", key_keyname= "cds_rsa_key", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 64)
`pragma protect key_block
KT92+mUu23SKE2xLY7Zx+qO9+Wcx8tU/NtvYr0AC4Dzxjg6ZkTtih0/nG+rd8j34aRp3K2Ch+hi/
JdJ9Lb0YcQ==


`pragma protect key_keyowner = "Mentor Graphics Corporation", key_keyname= "MGC-VERIF-SIM-RSA-1", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 128)
`pragma protect key_block
iXRBiVnZOjzRqsob5YlnOIJUMmYI2rAug4CaXx/v3n9tHxU6i6Tpp0oIDqkJ2G/0NwVkQvVSaRwo
mBC0Uj7ZzukGJsuoRP3dtJZrGfCFjsPXbo9dgfZVl9XN2aZgw1nW/x/c4J3GIYVJSHODEJ77mNVh
+SgRybjg6fBP1br2mFo=


`pragma protect key_keyowner = "Xilinx", key_keyname= "xilinx_2014_03", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 256)
`pragma protect key_block
H+fzvMdreao8WFDRG3lpAF76v6k/OQpvZHe9Q9JfxTxt0wENax59gu0kRYv+Qe867sJAxQy+zmyk
i42iIv3gB1x0YX1yVxYFskHv+5i2LzTFycHS7yXnoSyjU1MUNR2eGdJmCeFYZXU4xahSLCGJXTjB
0brw27s4M1wGOb3wlNghMV65lugBlux/9Rqz4VTJM9c9ro+dIzhoRimH2jSKjT0+hMDRDIIMyHEP
2p1XSPoHBgm/6PZ/I+kAX//WBHFFQGOTTadI7+msxSi/0BILATOjJ9lVZ1slKWlJMgzdnLsaSMMm
AFAyPQvIq+r2c03kpan+pb2n1VCWrH7mzAKPcg==


`pragma protect key_keyowner = "Synopsys", key_keyname= "SNPS-VCS-RSA-1", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 128)
`pragma protect key_block
bWdSHFeypFyLb53QKSjNbqni4MvoFf1tSifQxHt3RX2aFgVwLwhOUV8DogMUAvRAIsk30rAOvuD4
o9evWzMbry6kSId8SK/8fBzA5GGb2Am4RCwxyRCCbANtRooGbDioZ73KhmDXWoG7AFtf6nP4GaiR
mWt4cNLBvpOX1N2WdiU=


`pragma protect key_keyowner = "Aldec", key_keyname= "ALDEC08_001", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 256)
`pragma protect key_block
LZDuJMGt4f6Gt/ak+MQYnb4VKOS2H80RqreT1VARVjF+FFxGfeuOTFu3LJ8bzZpmU5mBNIp9sWN7
2kiFrw21B/QKPpnwHVlsJhOdlQ2w0WlUzqzHJ/QTNUecTruIIkwmVzk4UvxLYW4UgMxnPIbErYUP
EkKHy0tsQGfy8cxGwNvwJlAyBQnaZgnCMJNfyWH5xwD0JxHXYfeJCb2r8LpfUI7x9Uq1DnIVLV/M
+mnU/Rk14NNPvS1tOBBpAsMTp/ku30kdGVHaMZou/XpS0l5hVmECZWv+2NznVSrcPN8g0TFpPqU8
iZoYYFlJiD8bVeYWuGmsHjJMTKJxUePbAZ5ZzQ==


`pragma protect key_keyowner = "Mentor Graphics Corporation", key_keyname= "MGC-PREC-RSA", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 256)
`pragma protect key_block
iKOtctim2iBAohpJDxBEPXO7UTKE4nu25jG9TEJi5jDTgJ02UtO0s2onrakqrnAzE60SNqfQpyA7FPokOg182Vrq+KIYl2E6Y6PcSxNlPTuVl+ebI7YRowF843TVSs/e30ks95HChbcCjIQ8cdeK2CtYarxPg9ByON+jhVYOlBvHHarzExj5jV2/W/bkUvgMtPay63GWn4VCVcXsxexithc0Lqh9e6YdbJw3LrgTGZqviosQDqOSdXaGxF6M5A4RUSVA7ylKo1q9zrvQQFtXxwY8K5yJGs/uc0Ekmo/ON3O3nAAZRRTqioF9kRKsd7weoQLvFJQgbd6jLhy3YHhh0g==


`pragma protect key_keyowner = "Synplicity", key_keyname= "SYNP05_001", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 64, bytes = 128)
`pragma protect key_block
cQWmmeiL67CPMNXnemZ3ecUSMQmXexv97jw5H9g2pAZL+YvYFhVT2vX1523ThiMHmgHDEi2IDIZ93uXZ3iFmejwlkS0QS0TcjHRvTX2zqkLFIMjlcgfzluoarnSUq/CIl9U1GjCfxSCvWgkMMZWaWhAmdXJhUJ1yriZFoKrUmmLUZpU+NPNRTAZQIJuUoaG1wW0KDhhmlKKUP+DYRJlPugmw/TVupCp61v6R8cm3ZnVYIxF1SrsGARkQ2RJM8u+rDUP6iBUpOHCWpxiM8a6IGo4RCtG8+lkE7x+IjDd4sy/DpTRzlHvc6C8kB6tdZoJwLRhPaSnlG4xast3EHCKm+w==


`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 64, bytes = 2832)
`pragma protect data_block
WDLgN3DQATO+ZLTECd2dUx6i9SPp61UsW3oOkYGgNO6FxOOGHw59ppQ8J+IKzKKnNGdY0V9aVAWo
hxGm5XqtGdYvzHbcJfr94Hw1ekhbLSSDpYXmCGcB8tah2XQ4FWCAD2O9VCFQ5XX2vufncNjU5Ynh
tg1ctSaE3OGKtOqbWr+fRuaRV9clB8AdF8fycofGC95jHOigDW9KKALQYVyxbuAR7OMC9QZ6LlbM
x1pmoUzPe0xtA3CgQDZqJakQDa6grvih3Kkr/KerEqk8EzzOQR3nROoaSjcYMbYmuALxIiKZkymX
lbsGmxrMZ1P6XQrFztZIAyHbYgLODr2fWAoe4LNQd7UNG0rKrlUltxNiAarIRu7c00JWfmOGtdmI
lTq8xzEFH520R30pc16vZ7km2a3OWPt3DfpqbmrISqqP/j8iJ28hBEuAEltsuZJmAAase2cqyffk
WY07MLxttp7OLg03JpW24odZwcxaw2LFdIPAQcl+GNItRmYn+KETAsvq7bPlu1YXZX3faDMINqLh
W3uW8d3uM2MwPm0NdW4HUAvsNB3wqVKP3SxkWv5LphETDZRvtSWzsgik78qdNe8hEB6FMH5RnhSr
QvtSPintt7Bjj5RZuYQEMeUBDESXsYOCnHo7Fla2mfGmQUH2bg81Jk3JjNyWxkTNkcgWC7IaQ5aO
5COqdE8hwasqI2pyJFpxn8hfS4curPxOQ8Mv02ukvrC4sdnq5kfD/19avrf87BQxVmNR9hh96fqJ
zXuAXM3qsP4YqDx/pHdQz4bzxEIvW/69KSGVGgfuu4ju8UEmbyVu79RSJA4OO2eNfEh3yoH7VtW0
gN7w5UM1Cj5vroFUK6cRx36j/yuFVnFFSkTPlBZ676UymE7sY2gExK+QqxcibssdZ8+zfLaSk0mS
dqZDoJCDWJtuZL9bJnz942xJREv+L6fsLkvZGqLmmIFkM/4BYFtLtrXI+ACclaLZNKUVQYKfc53f
G8eEHMczhysTVBUbPUMmNEdEViy99aLPLJNUaTgvjavVhTI5RKfdj2Wx20U80pd8LaL6UjjjIlEn
2hYc63iX0koXOuxF4z9gPG3phiS7OTH638dlIWAbqYK/oFONfi3mdTTBHmz2LtBiSkeTfBXx6y/W
GW/r1+AZzVrdrvm/fOde1uLAuRWZguxwxmqNOTF8RsMoYKPlqxo38bLB43o6gF3zUovWdDXHNyb6
E3ez1EdM5uVRwAWV4DDqm74MwL9wFx7Om88A8Lh73kfuB/R0Jp6uea7LiMQPWzVKDn7VSiez+4Xk
xC1c+Q1LBVPxa8v1ZyDz9NGC8vZvjWgawiTnAP9ZCK4UZGMuztvmLoDvA0C2njZ4PlO+peULa9T5
1jUUVknWgr0YJ0WhO7uQgpOzVQyvb3qx1b7EaVRyDwC0qw/NQyGxSePXaOFEWEPQz3oT9mERRZ23
z3QWYsMB+rcO1+vU2N14KVyHKNaj1kojAE6hvyCsnePjfVxHWk816bJQtLcVc0SAEcyWVGDxDB6m
+fMsDFQNsb2ne4K0yVKNgdWupquHJMO92cC9wNIfs0PXliB/JWakbeZLILcxS5YMEExnHLrY7sTS
wpr/q6U5SLEuMAdjbQqKOMTJ91fNCvqYrNwjr03jY77IwB1bbFQChH+iwi7b8fAZZVHoUYY6dpFd
6xRBqSUaMBLfWdht9ivXFwJTK/E6Cx1717Vhm0tt3QCXuYekXucOzOJQylDKR6ueoWAakM+iiHQk
3c5IBWFdya2dCx5gS6daYDllvjeyj583h1rUvhPm1tVNCPFbH0EMdRStioBmeR3e1K8QyCprU8BV
7kjPRbbxq52YcBrWuCXVQm+F9xm72XsnFf0IYVsrSzuEgd5hjoYFWnYdBreghJryHpKU90DL3mZ9
7WARh6kiVssJNOaxdJlf5WHJIcLVLgU1rx5tzT7CV/EhwTB14e/yp+mxW9w+aGn3SjeHt7SkFIzr
QjsvH+y9c8OxsHhG4CZz8XjAK89y6BiokRkNJpGOe88khKBDhC4yZ4meaX/pgWsIA9771GHo7XBP
BRohra3cl3kUekzm7QrLHththruwbm3RCKs0sQry5OBGMh+ycdT5xufWC0LDuzthuwgsPX5CFFkd
F+N1IhcxiYQci+/iARrVPByZQ3xs88vwNYA/Nl/BpN3gEeSE1q9+Kz7A5XrVMQcNy1Sl+TAtFAaS
ruNst61/HvXq8TqfCY4I6s39wkvzPHKJWwFtIw/taBqMSNoSLV5G6Ip14565Uv2F1b1Z4k/TiuFx
A+44DIOl7HpFPF9+D/wu+fJvgx5QqnG4Tt/1q1ihCBUIkpTTBHQHcDb0CKtOxS/uEM9u5WzsH+ld
ZtcLn/2QF96GgjAhfkJf0pIXhD1KcL1myBnhP2YlqaGL0WkhZRQXv31a2ldrpxk263o8sP3v9uqZ
unjVURCQktjsRsuRE6J+oYz4DRWabdMnZjPxF0GWOyQEwoapk+b9dmRq3V0FPvocGu7RAOpOI0aH
fsAcPDFASlEjViqERXl0YpS27y7OoD+gn9uR51K2+g43EiK7CjpT5nhosoqXK1QrN+h4s9FVuthO
o1XMe5NtRhl+ad5m989VOZZWvnOQ7jwSr1EqHaeMQO+fdXgGGIihD9OQjQubBZl1vS8tL4QqXk/l
tgQFKvQS+BJqWZLGD5aabbyBH+CVLonN/ihHKZsWHKgTdYRWHtZnhJM2njuZhnEra3hAhC/byWSH
mXlEkaCA8fR8nOiYsj+MAHQcAMEgF2aYCD31Peh+C8OQ/U9qgIchIyluKhqBsplTRQKGN34O7F4i
J/cp09sXm7ABJImOFwICVB21qNDyaimKCJA/PMB1C5xQigmOwFSicDlfcsY/Mp8KMa0D/ESlsNCU
MT10npzMddTiE45Ia1VApALTCyL07X2Fg96ij3Ujiw8tn3Fn5A+HglbAWahQ04U00UzAsPmAg8wl
vahF3Fr6Y3ya+y9FRmn0p4b7PgdD3qHsu95JkT83Aearqiezq37q4JMZSVg9jU4zOv16ERfc8eLA
fn+4LmZesqPhs37tCSJUlr8E88rE3p3HMiyPyu9VHWsCOr82VePekWEsDwfdr7/oMkMisoL22rSE
IoQf7EDubChk+5dMULkDyoEZvafW/TT0IY/z1e22AO0IUFqj2twTwnsea1z0y8EHVrJsAiiK2rb4
7BEl7qZJkegBfBmqxZOSmYtAnniXT/kZcoh5XnSZaDu61799MtGcZ4JVlGJJvOYtTeNmetU7kND9
Kw05uz9tW6fcJ8TcfIvel0rJlBgtu/Lbo0eKJYZlcSzeknU6+AXAnBgUdzD8zhsD8poaXGJNJ5n8
J8ax4bNuQnDkwKz/4uQp0w6PU69r4mbwMAdoFTHm3Kq7WCwVgO7p/g1N6BIR1HY7GRwwN2kWef2+
PCgIKC30Ebz0eHRpuwBuPPfapwJPwLRznHNSw2RGQq3G56yQi8HNUfHeHBNNaeOBTuFZJtOnfz91
mFYo4hQa+NwB95wvhrw7mTCh08p27JkOWAPbSLH8RTSOFbT7/sXPC0GjCmeMzDXRdVJWoDUTNomH
wqh0SLON2rKAgrZCiHyzpb2N2LhGwK5XLUNYPbUCb9UY8Xx8SHARx+0dI2m0tfCYV4Ho4GRL3/k2
DbRc4SeiMsLoaN4Z16bo5Cz5lRX4TNkVWLy0RjqiKWeZIbu9BkM1rGYDB8nT9IxARwBXxzc93SZT
c8cA/Sd91SonsNp28yJIZkyQJOcdfRj2uLpYJGngfCHeQedeg4hI
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2014"
`pragma protect key_keyowner = "Cadence Design Systems.", key_keyname= "cds_rsa_key", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 64)
`pragma protect key_block
KT92+mUu23SKE2xLY7Zx+qO9+Wcx8tU/NtvYr0AC4Dzxjg6ZkTtih0/nG+rd8j34aRp3K2Ch+hi/
JdJ9Lb0YcQ==


`pragma protect key_keyowner = "Mentor Graphics Corporation", key_keyname= "MGC-VERIF-SIM-RSA-1", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 128)
`pragma protect key_block
iXRBiVnZOjzRqsob5YlnOIJUMmYI2rAug4CaXx/v3n9tHxU6i6Tpp0oIDqkJ2G/0NwVkQvVSaRwo
mBC0Uj7ZzukGJsuoRP3dtJZrGfCFjsPXbo9dgfZVl9XN2aZgw1nW/x/c4J3GIYVJSHODEJ77mNVh
+SgRybjg6fBP1br2mFo=


`pragma protect key_keyowner = "Xilinx", key_keyname= "xilinx_2014_03", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 256)
`pragma protect key_block
H+fzvMdreao8WFDRG3lpAF76v6k/OQpvZHe9Q9JfxTxt0wENax59gu0kRYv+Qe867sJAxQy+zmyk
i42iIv3gB1x0YX1yVxYFskHv+5i2LzTFycHS7yXnoSyjU1MUNR2eGdJmCeFYZXU4xahSLCGJXTjB
0brw27s4M1wGOb3wlNghMV65lugBlux/9Rqz4VTJM9c9ro+dIzhoRimH2jSKjT0+hMDRDIIMyHEP
2p1XSPoHBgm/6PZ/I+kAX//WBHFFQGOTTadI7+msxSi/0BILATOjJ9lVZ1slKWlJMgzdnLsaSMMm
AFAyPQvIq+r2c03kpan+pb2n1VCWrH7mzAKPcg==


`pragma protect key_keyowner = "Synopsys", key_keyname= "SNPS-VCS-RSA-1", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 128)
`pragma protect key_block
bWdSHFeypFyLb53QKSjNbqni4MvoFf1tSifQxHt3RX2aFgVwLwhOUV8DogMUAvRAIsk30rAOvuD4
o9evWzMbry6kSId8SK/8fBzA5GGb2Am4RCwxyRCCbANtRooGbDioZ73KhmDXWoG7AFtf6nP4GaiR
mWt4cNLBvpOX1N2WdiU=


`pragma protect key_keyowner = "Aldec", key_keyname= "ALDEC08_001", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 256)
`pragma protect key_block
LZDuJMGt4f6Gt/ak+MQYnb4VKOS2H80RqreT1VARVjF+FFxGfeuOTFu3LJ8bzZpmU5mBNIp9sWN7
2kiFrw21B/QKPpnwHVlsJhOdlQ2w0WlUzqzHJ/QTNUecTruIIkwmVzk4UvxLYW4UgMxnPIbErYUP
EkKHy0tsQGfy8cxGwNvwJlAyBQnaZgnCMJNfyWH5xwD0JxHXYfeJCb2r8LpfUI7x9Uq1DnIVLV/M
+mnU/Rk14NNPvS1tOBBpAsMTp/ku30kdGVHaMZou/XpS0l5hVmECZWv+2NznVSrcPN8g0TFpPqU8
iZoYYFlJiD8bVeYWuGmsHjJMTKJxUePbAZ5ZzQ==


`pragma protect key_keyowner = "Mentor Graphics Corporation", key_keyname= "MGC-PREC-RSA", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 256)
`pragma protect key_block
iKOtctim2iBAohpJDxBEPXO7UTKE4nu25jG9TEJi5jDTgJ02UtO0s2onrakqrnAzE60SNqfQpyA7FPokOg182Vrq+KIYl2E6Y6PcSxNlPTuVl+ebI7YRowF843TVSs/e30ks95HChbcCjIQ8cdeK2CtYarxPg9ByON+jhVYOlBvHHarzExj5jV2/W/bkUvgMtPay63GWn4VCVcXsxexithc0Lqh9e6YdbJw3LrgTGZqviosQDqOSdXaGxF6M5A4RUSVA7ylKo1q9zrvQQFtXxwY8K5yJGs/uc0Ekmo/ON3O3nAAZRRTqioF9kRKsd7weoQLvFJQgbd6jLhy3YHhh0g==


`pragma protect key_keyowner = "Synplicity", key_keyname= "SYNP05_001", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 64, bytes = 128)
`pragma protect key_block
cQWmmeiL67CPMNXnemZ3ecUSMQmXexv97jw5H9g2pAZL+YvYFhVT2vX1523ThiMHmgHDEi2IDIZ93uXZ3iFmejwlkS0QS0TcjHRvTX2zqkLFIMjlcgfzluoarnSUq/CIl9U1GjCfxSCvWgkMMZWaWhAmdXJhUJ1yriZFoKrUmmLUZpU+NPNRTAZQIJuUoaG1wW0KDhhmlKKUP+DYRJlPugmw/TVupCp61v6R8cm3ZnVYIxF1SrsGARkQ2RJM8u+rDUP6iBUpOHCWpxiM8a6IGo4RCtG8+lkE7x+IjDd4sy/DpTRzlHvc6C8kB6tdZoJwLRhPaSnlG4xast3EHCKm+w==


`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 64, bytes = 8464)
`pragma protect data_block
Ciu+A3vUi5c82t4NklKsVBHvdNIJDd4MgEOF6GPZsZb4XnLhmTwRyHXLlYQhddW7c1Us2CHXBe1Q
yUOTRKMR1wSZqFVeG+OtCRU2RJ5j7zo03fs44KmxR3UmiGz/RWfJyYwM0It8qNejfbtOkV/tsS7w
8TL0EUrVpnI6A2jq5zMZ0L9Nzz+L648tCdJHIHGhkeYwxFc4hUkCC3T2JqjrUHP7eqBZrEdLbNMK
kIXU0iHzfFA0gOiUE6LWeabOeDlqw3OguAdb3tVbkvaPga6GN9B2g+JK75b3elJqMVc3+/Y7P3l0
Ddyh2BGYC/JiaJ2Wcf80hueZyfgD4DA09beSXJJJHMT8j6EaS50rKnRmjTtkDC3yrWlPih+bzdl5
ChaKE13FtukzIBph07nGVvqPWm1m36RDALdgFxDB+ppYSTuDmk6Wzf1jeJAfptucn5vtE+9YeHjx
bOAjIeBDJtvO1umvm9iXYDAssdQdh7dWOlksA2HwVjDECqGC7iG3eYlVohBJE5gSQaEY6sppPjPw
WhZTeJNF9Rsqh+H521bgnhSHZCFkJVPZOsqpwUfpCeiNU6tGbSODxQJMfYLFwHUsWkeANuTvVHBh
TxotU1jCr1l3xlN6n+nTymTMzZjk83nzkyHjlOleYIVo05oqiTjb7l5UAl6Bd+EDiQ/vL8iFEQgk
hDl8V8JIdjMqZgwNeOdRCns1g2pfrRYblmZFoRC7lMskZ/ZXXhY9XWAAY0MmgwTf2zZnWiHtX1kC
h1Q/gCTw0NdSVH4Qk8c6X66Ev0nWEFeAY/KDRxGElrsO91Z0Am9c0KclUjL5vpZfmx7YLZgOnyB6
vDZS/uv1uNYL2/eGID+JEEDp7gkDRU9ZrVxdiV+KSIXWqXwA+z/C//7gblSF1K0ePSMDm7HSSVNr
JTJFNrBsonEkAyyYPA0qaY5W0fwQKpigKuFZNXTX+ck5IM7O1DcEGLr4okVt1CbL8d6Ls4f8FAXi
SWKDh2w9R9WjZIKHfiG+bNC1WYQpEajjgH04lCSBDlVKIcH/4EnqRSHlSZ0w2WzCtjob6YFHcddw
Uly3UjA27xusvRx6f6fVURejuvfJQjT1jTpcLsjwMIe1i3w7LrQLPO4lSYFgF3BMdStVb5a4VoJY
kZkK81eJOysDcHaVf2NAODkK7BqUwxxc/Mrn1GwksTzZhlWzqMqexw06NjPOulGbHGYWwrOI3hvN
OlgvxAc2FDdqosKVo98BrpmpOuUsLOqmMgOL0PfAHLeFsitYJHUO1rLRAermC1ZzYM4wayTH7WcK
FqEoqpL5IwnzxyR0qXX0/dzqEeqWdoGfu3BoqNTYe1NQsMjFDoZb4XiKvqqa29L5pEMEIeDlrFnP
WphDkqtWHQiyhZt4qFSMCLWyxU6G+2KK1ddWTDn/jVcOKXvKhbtk6+tfYaN85NpQib+sn7toqRVM
q130zyeYCOoswQh3CgP3HxYJaggs3pZnOda2K0cD/3dXrraTnL3MjGvZgsSuUxtvFoTUPnCo7dGj
9/+VxIdlyAVHHbGpzq9XtK5RlC5SG8hb6w6LNcvXLuSml3mi8hBxX0FI+wxCBOLAcg9EY4PDubUt
QPgMZuXuNtA/TmPTKZ7KJONYVkDcSnEXbUjzKyeeSNpe5hGvluXkQNjXkawgDflMOtOrR6GQGzdp
uBbS9ryUcWrvTAK9yPumcDt3PcysBi0nCFOJbzWRiIeTZ8fY12ja5BDjwcKaeYX0iWi5+YYDDQfl
Z1Q6g2bij3ruo5r1nAEUGJ5cG3NqgM9piW5QLuGvnll8KLKMe560q0x1lfPlScC5GzA/dBI2toSs
JKlef/b2G6HsvpG755dqk8jauJVnlbyJcMNXgUbQJfLFMfpb0mwj7MEUdi8dvyBS0PnkUxSA+8eZ
FG9Wo/uI8rsigqUpkKFbmK8nJd82NZifnARRiswP64o39iNoqJyHfBkoFq5eB0/xFWtz0kldx20f
k6rp8DUEarHKSNAuKJLy7vEiigXCB5hNfGcYiMOb+taVOvE+lwfMDdHmjqvyes5XOwA8Ww/9wwj8
1HOrmpQOJs8Z5aqIFb+Y5TWQJLHx89GsR5ZPo5UJdpURGGhTFDRcFNGdnhm8eKMPuzBNUddQWRKP
a1JeP0+QgORpGRVSlY5Onm6eKm4U1rKkR/hw1uuK6Kpc4fxgzRoQ/0/UMQMpdcdCdDC4tv8IsBml
cOS6vIBBdtgIou/J8LdKZj9jlnhVqGzEqoZcUq0Xb7sSaBnFlNfdhvUomFiA95CPsRUiWjzhn2U0
LRmxhvVOOGcUnlep6Vtu0t7gi1U5by55Ak3kSbMzIqHp4UcS8dodDrDLqV2v9J53BMPf9gps1etm
3V6aztz8bX3HeoZqKk10NT9Hv3jWwfEok/WU2g8RUuZ8dU5OKgHxA8JPGSRxWKJkWcTtOHTlJpRF
t32MSeuTKc3cHCMQSgnRaWuxgnmVvi2B67r2aeRN5N4qyVkyFIAHipVYvzNdGL3ZDRj7vF0rK5HC
NdKiYq/L5ulCf0i66uV1DpoU2d7pzKAWcF7hEWEeayAmCOXXN1fUCytf3R5a5QLXqLPaOnE0rjUt
p9ckcYnwuf0OimqubsheUTn//4a90KGnJrbe65fD7sOtDvitYqBDWTHnHcYyWnZ2ZabUxZOdosaw
t1RCkrmVmumcovdUy9wBFfh1nWHmxFNDVYJB2Ud2x29pEivwcQRJ/pAdyhtrVUpKnIidIC0JqKHp
RlwJ6oO2g48/wszEt3wr0LY+Ls6EJNGMd1yzLpUL7OmTUMWYQgKkL7ob1t4VNMJVclqda12isuDz
Cxf7dpyAaYzj0EwjbrzgalNOqKX4o77WXGIy7LtJ3WIbLOvtCUTyGE4qmmiO4PMKKsDlDOIRjSK+
U21GkRjnWspAOBH8F5Tdiwhms6oPY79OoabB0YtOl2zDutaLxkl3/xyuFyqw0wJniy8QIf1tvPUt
8SDf1awyILqFMwAHz8PM5Rem4RWa8TX8GreEnwhW6kEnuMyt1icUmwSxnEa21uozjPHRTHSHJZj8
2jAgwphQE1J58d7dJekaosxnQwIxpGxKQMlXw3a/vpAQA6rAY1POrRMxOBWkp9NaqfSQNjNaYQGZ
O0u36guNNScjHvViUZDb+6nBsPLxBuX0wXl7SDU2J8Kxrqqk1//dUeWAbfvGJdMmWFz90+me6/Gp
cnw3c7nd+uoG1LsxFfALfPkcMvA7/hB8ffNviOceFsmff0euxRvWJB0HdPgT1JLCo4Qwi0tFUOAT
OJZu+V5x/vh1NHQmT0CjZ0IoBCudlfi2EVuB4Y7X57I243X541zPJw+7yjiXpwC5pMPzM4zyOlGl
3u478zxMUPPloow6Umt46vYToGGJExdR9clsW2ivU4WX3hjAHv0l01EgtZYgwm1hT83gXrlx7SF7
Db21JiQ5kwkOXOvIEFQpsKE8ZU6sAzdx7XY2qdWW6U1UJ/9lw+ZtyZL18GcZSsc4wO5i+xJH4BU5
bIap5JA5TEjk7gmXQ4mu+QxYMOP8jzMTEthOi6T5YwBOyJ66jDZxyZwGj6s+aK0+u0puXQcb7D8B
2bO/NuMXiMAMSuSxB3EvPmfmL/TJbTzyOGTMyQKTJlj26oo1z0sJCudQX+WVx2HgCcjmVN5C3gym
5YCOYOExv7lSFUTaFTMed/z6/WbcsnW1bVeQnczevE+F/LPJUv8t/wF8lDraZBCMBKIpyK7A45Qo
JXLwttR/B26v4Q6D4PIRkucBSCiJvDb90L2/T3HWWgiLnNu5u/Qb6dM2iy3Pw9RLifCG0HQJxsSH
++gWcEyrZhiIeDk9V/bA73/lgXZjKMEA2DWBy/evh8HcdyE3DpMaXR+u3tO4ldMJQBKKClzqFxVT
zdBAmIOL5z++q3Yw869WdJXd0geEF7f44PJZvyzYmz/AnXIlcv8oyvTLRQzPZs8aNhOWnA64Dkb9
gK1HDK984j9cn0tW95A7/5J2CoXUdgX4P0KRfybFgSsIwMwEF66oY3QowFydrkyfiRKj1Euvk537
f536DO22/9t8uDw6fgGjx8APLzcqpSUTwaw31PWmAJcwU53j6gBSoXOktne6vP7eAXALZBncFqO4
J6tNTB/hAjbdXegTOn4uc/jBpi2DiU2t7rM+yxVaKkLNKd2/dzyuuLIIXIomy8P2sVJnh3UdTb1w
O/ZLFz9PdHm0/IwlHgdB534JpCKadQTXQSJagpDtHd3oAXgdp/qkIhgOLIUt5rCeCs7QrO+BZE+f
qL5/c1fP8f0+Iw395yKKzo2GTQsgRpMNcK46nAqxsfOQ8DxMyGkLC6aDXvl4HS0DsI77P/3K5a1N
LMOeXNXtpOgxapEH4JjxHe1TeuuBGhbb2f2ahmBjMY8pOpWPAv8dxMz1Oahy9UXFsu9PWp5AfSuN
aj0+A4hxAYLhPPfF07UcaBwIxjRNEhO+X8JX2joXc3YP6tzbyRpUeb5hHkFmemjlSLz5FW7IIx+j
2Yx3rx/6mQdO2kCfmLpa/jA41Mj200xsyb1RDwmCfyn0QfImAhaE7gK4hH350SncTrHbQO4zd5Z3
PlPNlGQ0QAsrnbLgf//1j4cgRZy3oOdXSTjPn8imyKXvbK9Dnko99yVAyb7w3tvyR8F603MJW/uW
ZY3A5pab4zex7YQTyr5QpjkJrcdITfi/8Nf4sCi2E6xDDarV5vxQFhYsuRZzN++wOYSGXTRjkDQX
uVWl54rvkmXCYi7/r2ktMbTCEDpvG/KIOLeKzi6GDoaZbkmJ1YO9jjUt/zDSLp+nUWjcciCSaqn3
nZ98Yh5WxALNUlQmGX2OZx0ARZHkTZioTJCZmkLFaaNLkzLkHdifwoIZZCVGuSDE/0Ok90w4CZ8q
hO76PFVJyDuZQhCeahJI2uZd2gs4B6W+LC2p1lob8nIAyL1NNk5MX2U49KDGv556q+lyQTyp/SNh
2elazSx8rz+KKm7+B9LJ5c10/diB80ovxY4lYq+Q8pZesp9EzQudn5AG62/Ra8rJpIB1luAU0OL7
OnvkqQpfxx0G+EPvcFjPSE9aME6RxhkGAKFpY7q92MI91sVNwd/ge6v2QJ0PVscTt7P4W/B5Dd2z
nG7eF3ER7p1xcZzS9ToZ1kpR9/vcYGIqx8kk0Ph30hBFqeUNiQy0Mq5qge2byYcqAx+D1rYVTTXv
l3sbKf4+/EQvDa/GAm0MT+6s/XVIRrw4W5d5A5hS8JVUjkmbSYygZ+VdKm+5lcDZNgQletdIGIPs
zv8Ns692atl+XFsgm13UsArKIZi9O1G5oMA95EG57Vhis9QPIkY/2nukTvHfDwrFUHv7d59zkr/n
tmlj1Hx2ouaqUSwBX5IvjE6+f8/2eI3Tw2CWN3FCWVMTZiI65Nru8IwAhbglAIRsqmhxcE1rzBRn
KMVybMm1HetL3Jgdbom/kreu4AgsH3juhCmGi2d05bh10XmM62+FqH8YWb9qXAx2TLkCP+IaaWE7
Qbrb1XdkfRvD4tJnWgkeGCy2kfRZxyNw6QtnVV2Y8RR8hbHbwGcbeFc/BaRnMnG7reqpMFxMHVw+
98Q79IKDNwNDKQT6rEEBWQA49QDODaUKV9as2eZUqG72g53XGPGofX0TpUmRV08tferyJ32xCrty
8iZF1crKr/fT2eX46C2KaBjM60Lkpzu4vp2+CXn7sQITxrF4Xlb+KubQa+pkkKMcUGbXCSaHBaAn
RVcqrSAUVvmgCILBX+usjkRDCWX1LRYmZzeqZHgeQCw7cpo89kyoapTBs3qx8SBaM+Yvpw2MZhA0
eEzKjKpFhRtNGjz1fQyW6ICdLrcUanGtnqgjl3pAA4xmi7ILJQ722G7kfbM0fGP35v1CPeNdXo4F
rmKSIPSjy486Lg+3ZsI+PEuwjoDOGKaiuQV86RrJ9MdqYv9NRBgi3pR1sedZEhBlcu/00xkt7HDV
M2iB7XkhIrTZmHkfsgqvrQ0SLohvfj4D2xKcgSzIfEvCnMxBkWgvPz485mPyAMR+1uvUBAEJmF7w
PwkhJsgovHs7vk3me/huOPnkd3mmnqAfj9lhzwknbDF+1Fxj0GleQYQdja+ntFLhVv1ktRvY9SUX
5+ch//qJye4CsJWQt7X5eD2goRHtIwYr51ro4/z/BztquLtEj09nnmeqkWojOF5PwBPfVeendMuo
q+b45wcCSox89sgjqTFXnMu7PIBHLbO4KcHYgCTu6wvHHHKhp/qT5XRe2Nj5mGqfGkN3xgkeWgKt
tRyKwf8Xmc92iIfqVYNUUIBJp3psIVhPhekZc2EvDPia8kNL3v53hqb6xY7BpfLpWXhHyZShvWTs
xaH83hYcq02Vs3nv+3V8EJrxUX4UVWV2snti9h9mLjMnkJKYbkp0c/V4uTQfTnnIvRMAgwSL7d2h
nG/QSXsQtXmqOLk5lsCJovAZRYq7jqHM+ygU8Tj/YpJeHsb4kRUSI6zalsuR8LrLk3IkMB32u1ot
YpFCbqEhFbv3bPxf4/hTzMZhqIgWK7btbOBST8+wiJ0zZzCRTpdxX+mN2YvEtxAeuAIeAs1Bk3lS
IgK4Bui9M27JGUZk+TBHOXBICxgmpXNyjp+OkUa4sWxOc7G5pRkXeGYdJ5pbAmntaAjAKROv17T9
kqimWPf39V7BJmoeDZqHUfhiBrC2Iar/2J7Tqn/7Wh/gf2G6hsC4BxUwrODLbhdOdxMF3hga8d8d
z2Ax3JXYRAHIltw3XMkt9w5lHyFuM4GIdxZFgstBkncztoRu55lVFu2C54fpmXG9k7arRfgEn0P3
yErR6gEUWvKPoVaffL6iHmcQsTTOey4bpmOq3YdMB6Ik/pmEMhBOa2kTPW10nvXLdlgwuP44Uoya
dfNpQMyb3LVF/dqlMIuREmEyKErvTNsqDpc4O4B37JqmCas8eaJgv/75dwOfY7T8bUm30wGbh3Nt
LWbTwf9bzNSeapKF9Aw28WiTqsTbtCf/2sp/uNjqeBDYr4r1MK4rZXOfZwXfkFYWMnDfbctrZyw4
nSVdUA8KnmQ/1VcFIYYXKcfO2i0nL4q7id7ewOQsB3FGMVGN0Ri3zIIcFO/bRcnHq3MX93F5lFSr
1PulRG2Xsu3o8al98M8cGsRBWAXvVdKSEnMpCGuBrnMPzjBVN+PIaniBTHGlciwCxjVlkVg6glDf
+ytmg5prmxxCYkmODv9kcdZiUPdPOoaNhbb4lnXK+rlhWOWqpnK6wbpAAtfRRjbSC0HbqWp0SDON
oqlrWWXckj5Z4v8At2q9AxqS2SIGC99CApz+iTCOlwOvLl/A6+T+LvPCxXAxK1uycU9H+NIKED3d
cnUdo0COY13ftLdh07MHqerK7eyYHuMcmFJehHwCXyOrBMeTbtNPKOC99YFZFvMVDR6YhtTzTnST
1aMi/bpX4MtFGZR4FFif02yXr/5kwQyin3ZavkycvPh2/UeX3B1U9diCigZVcNngv1BxnapmykQZ
0kWB5sDszu7vECwXtzpv9xgBDCjGs9zFge+tSccyKoqTaLC8CRi7JiUWGWChScMknbIy3fYJLhGd
TKnQBKV/wGorH+t6wtmpksCtm6OqpzjM1CT+lZtPycFvNuMQZ74UgP8iB7+UECGilPuvfy3/KGDA
D8CNFjs8F6mjRR8MLwTYD7ucIFjaYcjGsGdm527c2APWQpeKJJIwB9iwb241bNAcsMPb91RjlAYk
CsKhNHzblzgeV8uzkc28ynbc2QKHTN4a29YGj0yuOXX09E+EHQSK0mWctAnSq1uP1nhwevYpz8aH
Ogyo6IIoLtcDds2NJvSfSJK8hJEvFZfLILwnUIywy6e2ea+yGw1moaOILJfEbtiIdBTUPClKihdP
snBXLMd4Cf8HKFQUspZZCO/Rwtnflc8th99o+6rxJP0mejIaIUBLE9T4euGwUnbCdOT0xo5yrqp8
unp75exNlN9OLMR3ug65IRPih+d2ZuRSgXOBJE4CrDCnYecJ15UyTzsTXw0xP2qssUzWr1vDt0Wo
0+01ogpDwBtJIWWy/XsTQcfEcRW1L2GXIFWTD3fyvUYCrYnzyIbWvsPu6azppeGlSX39il8+2w1e
H8vPAMw6Qzk0fhp+Iaeths+WWzOMDMyars0r7GSmDxdTPC4q26525wxA8F4K75tTBBBh9WNyCaBg
61fdHS/W1A3G2ToStztNLmmpbXedqJyn6ABHm2yzPe00MLbkdytW5OLRpNF2vdXZ7Iu7rSDEoGuK
bII+rSyy/xFNYOrHmj02E7DKc6Mi+5sN9geBo0C4Cy7vjDKODlbqKDserfLYM6eR/CJceTmnjasT
ebCrgIdE6m+ulH6BMzUGQ/0b/vmmy9GAKvuQ4TbvQgRHjVmINFz1LYd+ZBTmMN/2F5qlXxKjIiPF
brUYbQdjemhJuFpuoMLiICEvURK/+TtwcWV+QM8XcE7+JHf9tKYxw+gZ2E82EGmM5bH6BSmMtQ2c
hgWsrQ2I+Q0bcWWHBrgnKaBci3LoyOnj3aIIG9NObfv6dJF9lLud9+F2bMAOZ1AisrUV8XPtchAz
kLnRcdy0CdLa46kaaK0xqzVVHtH4DoNNrr+Hj3YfhAlDkg+vUpZnOFvDfW6AepqV58n1jvu58ReW
wJD+ySQVTxl6+yojRL1SVW0103NpBVr435GoJlVYXYF+7Pwk4yZ0IRoc6qhXoQjyJCGMGWb/PAcQ
ide+uUk4M2wfTUAqWptLO8ZHFIY0LCbf76sBx/BRJ7sNmfe1c/VvVgyYW0T610kr3JFl4Zvk4x7Y
R6ikk7+Xy8pf8Xdn1yWOY4bZPSKxnWsuuotxstdSn/3bR0yRb/EiNpPDAVySHBMHVaGFWliwZDKP
DkhXqeKbohkc//Qd8/ayLPhJlQ80oUhOaYyylKOt6usA0NdisSkzP79HtDzkzjbphH2zanJ6QDFP
uvW9AJQLSUZeH5IR/6mT3KZOjXP91nb267/KcHIXLbSDtN6HEWwrNHJy71C8fIJfrBjselFMZZ20
cBZcbGyQ0EmFxDERIIRp4ds4UYAatcngn73lW8NmevtylJhUEOr0X/ILvoS/XqmHG8ouAOyen8Mr
Q+nCXW9Ypo4SWQ45qpPHkji+Q7DyT0SB8Coo0aCe8HNudNpaXaKFYrwNGlwJvslabQpaVU90eGzD
kWXvAjpN2pMOoWtVyq/igvEg85AUT4yQwJp60iRVEh0xEMPcqF2L5sRB1G94nTLjQDaaBvSPLmAl
6Iw6ZWWAuynu1+Zte5BzB9R5QSaUPrFMXG8u+giifl6JFxOiE4LxLxogJw6gT/S6vKbFAmHtNip6
X0x+H4ru3jnDdoOV5zRgaHJcVxi9HT873MHqYj2/sONz2JKQn24v7VBDBqJQ3zAhDoHEnNC8N+t8
Aycs0z0LZEys/twMtIgIwVm6LUt26uUNAtGBPUQN0CL8A6XkjkF1OHYikpvLg3CK5Fmg4zUc7U6x
mm+DpmEyx70WyLTgUO2IXd5LWbySLpw8GKoRC2u2PypHUheaYgnlU0L+xvj82626jezbWSzTL1iK
Sh7u3md9uZZg9lVo/sLdc39IZYhMWo6bnUnoz6DBA/yJFVKerYZaMDibJHh+NeexJyLmGP3v+8IJ
O3OjqOkwcNsTnEpmBpsDfQrgY7bt4+I1Gol4EplmlMeWW/zsJi0yCOSgpqDcIRfd6NW9M+GC/TZj
gmnnYvUcpcGj1tCtg1iwQhROGFL26DRFogxhrz3XIjfdhgxKMzjNARIPu2Zh6jP27ShjDYT6bWJo
1eOm5npNVkcrzlugYFhvTYewqcxo4fnswxtZG+c1bkybN8uz5+YZzuxCyosKxFuNUD5RmoeRRfPw
cuZM0SgJ/2ynywDxIhMpbzKMfQTM2hidAqOMP7IFHOpeg+YhsaW+b0BlcHRhj4cd+r9f71YcWwI6
rY4EZRnV9qiPYPK9NKRgtbYctcFlrR5ddx+FVktWIAhh0aJ2oF0OluOOKQ9f1cIaOjfPXgNPIVHY
xHbjTsOwjex/XGym0WsYfSotW0QVvIiGUD1VNH4YlwF1su88cqeB6lvEWiwsbJsq7s116h3lSWnD
MrtxEyV26KUrJU1galZG2UeYt4vf69XtfDFJmtOB1O4LA8/6OVxZtetUtnDRD8BC2fRHQyULvYJr
kk/GFkunqBKO52Mzs5OAlITg+CavjoJ6qwgj6qUPbMnOu42yCINyECp9AOT/LuDOtTnrWkY4Scln
w3hgD0F0KRGGX/DHqf/arCMBKpHw+3RoVteslFzyez2s8nHAMYuLWLM4zONk/QlHju8ijgGd1+XE
o6YuIruj0HuFE68/xNO+0vUy0F6IS36eeirFA+S0SMUuCYBQEFq8aUIWefPBwqPkfjnag5jQyQch
bcC7aYkdyJ9v4ZaWCVpaJNKDqNE+JkKHE2iL7uQSMBMypZFOrcJwEzR6iknTuccp29aBDbJuXIN/
rwTuYa7CRgt3SdBBQn4DRbMfPgZp0rjs7GH10FC7qq7CFI9lFAN7ZvUUaSxCCSIvKKi+4GVMM77y
FgimBsMNd5jB5zJvWHPFeIKac9kYi//J0joMuRHoZfMW3Ql/sZ/Rodg/Sem1MCpuE4dDDStZ8PLu
DwfHBiqO1wzT3unHU94eoCNqcRkGgs8qUA6iOiqwae7KeSRnRUE4RjaJLsaVxNJM+ybwsfYhkaDa
fXcKzhWgVOBNKoUllPekCNCAbRoZ9CGn9oXUMORPgyyEciiSkrgf+c+BXuLpoPElItQ2IoS4wK45
EGMwxMBpxzJo9sbuBFOePYG+8JASYh7Tb/9if5iIPBYgK+TS3YNrGFgBnB4KpabldRX07YZZebyR
tYITA7IiPOGHPOQ1xfxIv0I+GCn6ZxpeXUWdkBdptfbEACZMySKPa0OAgqOeQHICAb3TLKSsmQbK
WoboAZpIyM8zT2G4lN9qvI/gtFajJUkKVVOPk6PHyEqk0uyHu82ekOPw3gETgfiKZIJz7eYUzuj9
Pwp6xgN8BtxUQenC1CNJQdzu8xXaiA+8uLXUIklE+cqCFgL2PF2v4WSU6bYdkEH02q1G2+ZYV6Cv
ebR92YojLj+B9VOoBV3oRCZs4I3dpCOr0L6Gf1q6D2Iedoqf7muEHXVnvb1Na7QHg6JHBhqsNQ4C
ya3Z8hcP4nKo6MJtFMNLT6eKQWQiZjK7E2YF8CPF73K7uqoiCNAa4j7OleoYGSmrlcR730zSGLRO
kZ8xkuINQVjPQo9Ms2ATIYDRP0sILetyzMVVe8n+IUC7Ntq8YgNduUQgHvgrLxi7Qx2Mdo0BFHkL
ElAMV5CXODKBAFmB9QvrM1aLZqOKM4nQtIj2dQ==
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2014"
`pragma protect key_keyowner = "Cadence Design Systems.", key_keyname= "cds_rsa_key", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 64)
`pragma protect key_block
KT92+mUu23SKE2xLY7Zx+qO9+Wcx8tU/NtvYr0AC4Dzxjg6ZkTtih0/nG+rd8j34aRp3K2Ch+hi/
JdJ9Lb0YcQ==


`pragma protect key_keyowner = "Mentor Graphics Corporation", key_keyname= "MGC-VERIF-SIM-RSA-1", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 128)
`pragma protect key_block
iXRBiVnZOjzRqsob5YlnOIJUMmYI2rAug4CaXx/v3n9tHxU6i6Tpp0oIDqkJ2G/0NwVkQvVSaRwo
mBC0Uj7ZzukGJsuoRP3dtJZrGfCFjsPXbo9dgfZVl9XN2aZgw1nW/x/c4J3GIYVJSHODEJ77mNVh
+SgRybjg6fBP1br2mFo=


`pragma protect key_keyowner = "Xilinx", key_keyname= "xilinx_2014_03", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 256)
`pragma protect key_block
H+fzvMdreao8WFDRG3lpAF76v6k/OQpvZHe9Q9JfxTxt0wENax59gu0kRYv+Qe867sJAxQy+zmyk
i42iIv3gB1x0YX1yVxYFskHv+5i2LzTFycHS7yXnoSyjU1MUNR2eGdJmCeFYZXU4xahSLCGJXTjB
0brw27s4M1wGOb3wlNghMV65lugBlux/9Rqz4VTJM9c9ro+dIzhoRimH2jSKjT0+hMDRDIIMyHEP
2p1XSPoHBgm/6PZ/I+kAX//WBHFFQGOTTadI7+msxSi/0BILATOjJ9lVZ1slKWlJMgzdnLsaSMMm
AFAyPQvIq+r2c03kpan+pb2n1VCWrH7mzAKPcg==


`pragma protect key_keyowner = "Synopsys", key_keyname= "SNPS-VCS-RSA-1", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 128)
`pragma protect key_block
bWdSHFeypFyLb53QKSjNbqni4MvoFf1tSifQxHt3RX2aFgVwLwhOUV8DogMUAvRAIsk30rAOvuD4
o9evWzMbry6kSId8SK/8fBzA5GGb2Am4RCwxyRCCbANtRooGbDioZ73KhmDXWoG7AFtf6nP4GaiR
mWt4cNLBvpOX1N2WdiU=


`pragma protect key_keyowner = "Aldec", key_keyname= "ALDEC08_001", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 256)
`pragma protect key_block
LZDuJMGt4f6Gt/ak+MQYnb4VKOS2H80RqreT1VARVjF+FFxGfeuOTFu3LJ8bzZpmU5mBNIp9sWN7
2kiFrw21B/QKPpnwHVlsJhOdlQ2w0WlUzqzHJ/QTNUecTruIIkwmVzk4UvxLYW4UgMxnPIbErYUP
EkKHy0tsQGfy8cxGwNvwJlAyBQnaZgnCMJNfyWH5xwD0JxHXYfeJCb2r8LpfUI7x9Uq1DnIVLV/M
+mnU/Rk14NNPvS1tOBBpAsMTp/ku30kdGVHaMZou/XpS0l5hVmECZWv+2NznVSrcPN8g0TFpPqU8
iZoYYFlJiD8bVeYWuGmsHjJMTKJxUePbAZ5ZzQ==


`pragma protect key_keyowner = "Mentor Graphics Corporation", key_keyname= "MGC-PREC-RSA", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 256)
`pragma protect key_block
iKOtctim2iBAohpJDxBEPXO7UTKE4nu25jG9TEJi5jDTgJ02UtO0s2onrakqrnAzE60SNqfQpyA7FPokOg182Vrq+KIYl2E6Y6PcSxNlPTuVl+ebI7YRowF843TVSs/e30ks95HChbcCjIQ8cdeK2CtYarxPg9ByON+jhVYOlBvHHarzExj5jV2/W/bkUvgMtPay63GWn4VCVcXsxexithc0Lqh9e6YdbJw3LrgTGZqviosQDqOSdXaGxF6M5A4RUSVA7ylKo1q9zrvQQFtXxwY8K5yJGs/uc0Ekmo/ON3O3nAAZRRTqioF9kRKsd7weoQLvFJQgbd6jLhy3YHhh0g==


`pragma protect key_keyowner = "Synplicity", key_keyname= "SYNP05_001", key_method = "rsa"
`pragma protect encoding = (enctype = "BASE64", line_length = 64, bytes = 128)
`pragma protect key_block
cQWmmeiL67CPMNXnemZ3ecUSMQmXexv97jw5H9g2pAZL+YvYFhVT2vX1523ThiMHmgHDEi2IDIZ93uXZ3iFmejwlkS0QS0TcjHRvTX2zqkLFIMjlcgfzluoarnSUq/CIl9U1GjCfxSCvWgkMMZWaWhAmdXJhUJ1yriZFoKrUmmLUZpU+NPNRTAZQIJuUoaG1wW0KDhhmlKKUP+DYRJlPugmw/TVupCp61v6R8cm3ZnVYIxF1SrsGARkQ2RJM8u+rDUP6iBUpOHCWpxiM8a6IGo4RCtG8+lkE7x+IjDd4sy/DpTRzlHvc6C8kB6tdZoJwLRhPaSnlG4xast3EHCKm+w==


`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 64, bytes = 240)
`pragma protect data_block
lYg5Ew/T41lemPS4MUFO+Gd3B91LlyL7F4SRGIBB9h5xuE4/OhRsCPFk7qdqDTLsUreQnJ8HMXal
jBvXRdyIuWKUtDqP/3oKUQKdt2BQTcJvZZxdxftEJwFBVO5UKlTetmFu66ULuJqTSw0UhDx4NOFv
eyQkAn985GEGFvKfWDs4DUVFBiaNgd82QxkYqzoeUoxMUiVQZIKFphbjww+txhX/4C2U1o/U2yNy
kS3wjSLWzMH4sFRmo1qgPxvBvttp+mQqU/bsZAt3WzbC0FP6S3fUgPsU0IrIbC63VxMDvnRn3Dzn
5DpKKQbEBkJEsIqI
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
