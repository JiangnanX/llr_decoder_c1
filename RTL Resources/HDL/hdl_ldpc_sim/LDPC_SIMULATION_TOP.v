
/* /---Confidentiality Notice Start--/
  TexasLDPC Inc Confidential.
  © Copyright 2014 to the present, TexasLDPC Inc.
  All the software contained in these files are protected by United States
  copyright law and may not be reproduced, distributed, transmitted,
  displayed, published or broadcast without the prior written permission
  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
  or other notice from copies of the content.
  /---Confidentiality Notice End--/
  
  Company      : TexasLDPC Inc
  Engineer     : Tiben Che, Osso Vahabzadeh 
  Create Date  : 10/24/2014 10:37:47 PM
  Design Name  : 
  Module Name  : LDPC_SIMULATION_TOP
  Project Name : 
  Description  : 
  Dependencies : 
  Revision     : 0.1 (01/10/2016)
  Comments     :  
  ----------------------------------*/

`include "LDPC_Simulation_Parameters.h"
`include "../hdl_gen/LDPC_Decoder_parameters.h"
`include "Codewords.h"
`include "Interface_Parameters.h"

`define  DEBUG_BUS_wl `CodeWordLength_wl+`CodeWordLength_wl+`Version_wl+`TimeSlotCounter_wl+`CodeID_wl+`H_row_wl+`iteration_wl+5

(* keep_hierarchy = "yes" *) 
module LDPC_SIMULATION_TOP
	(
    input  clk				,
    input  rst_n			,
    input  start_simulation	,
    output S_press			,
    output R_press    		);

	
	wire 											simulation_done			,
													clk_decoder				,
													clk_chipscope			,
													locked					,
													rst_clock				,
													rst_decoder				,
													data_valid				,
													d_start					,
													decoding_error			,
													decoder_busy			,
													new_frame				,
													LLR_ch_wen				,
													codeword_ren			,
													update_noise_parameters	,
													ram_we					,
													start_decode_plus		,
													decode_valid_plus		,
													decode_error_plus		,
													iterations_done			;
	wire 		[  `LLR_number*10-1							:0 ] 	awgn					;
	wire   		[ 31						:0 ]    noise_Par_ROM			;
	wire 		[ 223						:0 ] 	vol_ref_ROM				;												
	wire 		[ `LLR_Ch_wl*`LLR_number-1		:0 ] 	LLR						;
	wire 		[ `d_bandwidth-1        	:0 ] 	decoded_data			;
	wire 		[ `DEBUG_BUS_wl-1			:0 ]    DEBUG_BUS				;
	wire 		[ `ebno_range_wl-1		  	:0 ] 	ebno_idx				;
	wire 		[ `codeword_bit_idx_wl-1  	:0 ] 	codeword_bit_idx		;
	wire 		[ `frame_counter_wl-1	 	:0 ] 	frame_counter			,
													read_frame_counter		;
	wire 		[ `error_counter_wl-1	  	:0 ] 	error_counter			;
	wire 		[ `iteration_counter_wl-1 	:0 ] 	iteration_counter		,
													read_iteration_counter	;
	wire 		[ `iteration_wl-1         	:0 ] 	iterations				;
	wire 		[ `codelength-1  			:0 ]   	codeword          		;
	wire signed	[`LLR_Ch_wl*`LLR_number-1	  	:0 ]  	LLR_temp				;
	
	integer 										mon						;
	reg signed 	[  `LLR_number*10-1							:0 ] 	awgn_temp				;
 
	assign rst_clock	=~ rst_n			 ;
	assign rst_decoder	=~ start_decode_plus ;
	assign S_press		=  start_simulation	 ;
	assign R_press		=  rst_n			 ;
	assign LLR_temp 	=  LLR				 ;
	
	//assign codeword     = `codeword_zero  	 ;
	assign codeword     = `codeword_nonzero  ;
	
	parameter seed1 = `awgn_seed1;
	parameter seed2 = `awgn_seed2;
	parameter seed3 = `awgn_seed3;
	parameter seed4 = `awgn_seed4;
	parameter seed5 = `awgn_seed5;
	parameter seed6 = `awgn_seed6;
	parameter seed7 = `awgn_seed7;
	parameter seed8 = `awgn_seed8;
	parameter seeds = {seed8,seed7,seed6,seed5,seed4,seed3,seed2,seed1};
	
/* 	initial begin
		mon = $fopen("monitor.txt","w");
	end
  
	always @ (posedge clk_decoder)
		begin
		if (LLR_ch_wen) 
			begin			
			$fwrite(mon,"%d %d %d %d %d\n",
			awgn_temp									   ,
			LDPC_SIMULATION_TOP.LG1.llr_encoder_1.noise_0  ,
			LDPC_SIMULATION_TOP.LG1.llr_encoder_1.noise_1  ,
			LDPC_SIMULATION_TOP.LG1.llr_encoder_1.Y		   ,
			LLR_temp									   );
			end
		awgn_temp <= awgn;
		end */
  
		//at a later point of sim
		//#100 $fclose(mon);
 
    (* keep_hierarchy = "yes" *)
	/* wgng GN_generator
		(
		clk_chipscope ,
		rst_n		  ,
		1'b0		  ,
		1'b0		  ,
		awgn          );
		 */
	genvar k;
	generate for(k=0;k<`LLR_number;k=k+1) begin
		wgng GN_generator(clk_chipscope, rst_n, 1'b0, 1'b0,awgn[k*10 +: 10]);// seeds[k*22 +: 22], 
	end 
	endgenerate
		
    (* keep_hierarchy = "yes" *)
	LLR_Generator LG1
        (
        .clk_decoder	  		( clk_decoder  	 		  ),
        .rst_n			  		( rst_n			  	 	  ),
        .awgn			  		( awgn		  	 		  ),
        .update_noise_parameters( update_noise_parameters ),
        .noise_std_dev_ROM		( noise_Par_ROM 	 	  ),
		.vol_ref_ROM	  		( vol_ref_ROM   	 	  ),
		.codeword_ren			( codeword_ren			  ),
		.codeword				( codeword 				  ),
		.codeword_bit_idx 	  	( codeword_bit_idx 		  ),
        .LLR			  		( LLR			  	 	  ));
		
		
    (* keep_hierarchy = "yes" *)
	clk_wiz_0 clk_generator
        (
        .clk_in1 ( clk			 ),      
        .clk_out1( clk_decoder	 ),  
        .clk_out2( clk_chipscope ),
        .reset	 ( rst_clock	 ), 
        .locked  ( locked		 ));

		
    (* keep_hierarchy = "yes" *)
	Start_Simulation_Generator SSG1
    (
        .clk				  ( clk_decoder			  ),
        .start_simulation	  ( start_simulation	  ),
        .start_simulation_plus( start_simulation_plus )
        );

		
    (* keep_hierarchy = "yes" *)
	LDPC_Decoder_Top decoder
        (   
        .clk					  ( clk_decoder		),
        .reset_n				  ( rst_decoder		),
        .CodeId					  ( 3'b000			),
        .max_iterations_programmed( 7'b0001010		),
        .Disable_Convergence_logic( 1'b0			),
        .LLR_ch					  ( LLR				),
        .new_frame				  ( new_frame		),
        .LLR_ch_wen				  ( LLR_ch_wen		),
        .soi_busy				  ( 1'b0			),
        .decoded_data			  ( decoded_data	),
        .d_start				  ( d_start			),
        .data_valid				  ( data_valid		),
        .decoding_error			  ( decoding_error	),
        .decoder_busy			  ( decoder_busy	),
        .DEBUG_BUS				  ( DEBUG_BUS		),
        .iterations				  ( iterations		),
        .iterations_done		  ( iterations_done	));
		
 
    (* keep_hierarchy = "yes" *) 
	LLR_Input_Controller LLRIC1
        (
        .clk			 ( clk_decoder		 ),
        .rst_n			 ( rst_n		 	 ),
        .start_decode	 ( start_decode_plus ),
        .new_frame		 ( new_frame		 ),
        .LLR_ch_wen		 ( LLR_ch_wen		 ),
		.codeword_ren	 ( codeword_ren		 ),
		.codeword_bit_idx( codeword_bit_idx  ));
	

    (* keep_hierarchy = "yes" *)
	Sim_Controller   SC1
		(
        .clk			  		( clk_decoder			  ),
        .rst_n			  		( rst_n					  ),
        .simulation_start 		( start_simulation_plus   ),
        .decode_valid_plus		( decode_valid_plus	      ),
        .decode_error_plus		( decode_error_plus	      ),
        .iterations_done  		( iterations_done		  ),
        .iterations		  		( iterations			  ),
        .update_noise_parameters( update_noise_parameters ),
        .start_decode	  		( start_decode_plus	      ),
        .simulation_done  		( simulation_done		  ),
        .ram_we			  		( ram_we				  ),
        .frame_counter	  		( frame_counter           ),
        .ebno_idx		  		( ebno_idx			      ),
        .error_counter	  		( error_counter		      ),
        .iteration_counter		( iteration_counter	      ));
		
			
    rom_noise rom_noise_par 
		(
        .a	( ebno_idx      ),				// input wire
        .clk( clk_decoder   ),
        .spo( noise_Par_ROM ));				// output wire
        					
			
    ram_frame ram_frame_NO 
		(
        .a  ( ebno_idx			 ),			// input wire
        .d  ( frame_counter		 ),  		// input wire
        .clk( clk_decoder		 ),  
        .we ( ram_we			 ),
        .spo( read_frame_counter ));  		// output wire
        
			
    ram_iteration ram_iteration_NO 
		(
        .a  ( ebno_idx				 ), 	// input wire
        .d  ( iteration_counter		 ), 	// input wire
        .clk( clk_decoder			 ), 
        .we ( ram_we				 ), 
        .spo( read_iteration_counter ));  	// output wire
        
            
    rom_vol rom_vol_ref 
		(
        .a  ( ebno_idx    ),      			// input wire 
        .clk( clk_decoder ),
        .spo( vol_ref_ROM ));  				// output wire
		
			
    (* keep_hierarchy = "yes" *) 
	Plus_Generator  PG1
		(
        .clk			  ( clk_decoder		  ),
        .rst_n			  ( rst_n   		  ),   
        .decode_valid	  ( data_valid        ),
        .decode_error	  ( decoding_error    ),
        .decode_valid_plus( decode_valid_plus ),
        .decode_error_plus( decode_error_plus ));
        
			
    ila_0 chip_scope 
		(
        .clk   ( clk_chipscope			),  
        .probe1( clk_decoder			),  
        .probe0( simulation_done		),  
        .probe2( ebno_idx				),  
        .probe3( read_frame_counter		),  
        .probe4( frame_counter			),  
        .probe5( decode_valid_plus		),  
        .probe6( error_counter			),
        .probe7( read_iteration_counter ));
        

endmodule