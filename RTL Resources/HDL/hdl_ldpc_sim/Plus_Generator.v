`timescale 1ns / 1ps

/* /---Confidentiality Notice Start--/
  TexasLDPC Inc Confidential.
  © Copyright 2014 to the present, TexasLDPC Inc.
  All the software contained in these files are protected by United States
  copyright law and may not be reproduced, distributed, transmitted,
  displayed, published or broadcast without the prior written permission
  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
  or other notice from copies of the content.
  /---Confidentiality Notice End--/
  
  Company      : TexasLDPC Inc
  Engineer     : Tiben Che 
  Create Date  : 11/06/2014 06:02:04 PM
  Design Name  : 
  Module Name  : Plus_Generator
  Project Name : 
  Description  : 
  Dependencies : 
  Revision     : 0.01 - File Created
  Comments     :  
  ----------------------------------*/

module Plus_Generator
	(
    clk				  ,
    rst_n			  ,
    decode_valid	  ,
    decode_error	  ,
    decode_valid_plus ,
    decode_error_plus );	
    
	input 	clk			 ,
			rst_n		 ,
			decode_valid ,
			decode_error ;
    
	output	decode_valid_plus ,
			decode_error_plus ;
    
    reg 	decode_valid_buffer  ,
			decode_valid_buffer1 ,
			decode_error_buffer  ,
			decode_error_buffer1 ;
    
    always @(posedge clk)   
		
		begin
			decode_valid_buffer  <= decode_valid		;
			decode_valid_buffer1 <= decode_valid_buffer	;
		end

    always @(posedge clk)   
		
		begin
			decode_error_buffer  <= decode_error		;
			decode_error_buffer1 <= decode_error_buffer	;
		end
        
    assign decode_error_plus = decode_error_buffer&(~decode_error_buffer1);	
    assign decode_valid_plus = decode_valid_buffer1&(~decode_valid_buffer);
	
endmodule