`timescale 1ns / 1ps

/* /---Confidentiality Notice Start--/
  TexasLDPC Inc Confidential.
  © Copyright 2014 to the present, TexasLDPC Inc.
  All the software contained in these files are protected by United States
  copyright law and may not be reproduced, distributed, transmitted,
  displayed, published or broadcast without the prior written permission
  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
  or other notice from copies of the content.
  /---Confidentiality Notice End--/
  
  Company      : TexasLDPC Inc
  Engineer     : Tiben Che 
  Create Date  : 11/06/2014 03:48:16 PM
  Design Name  : 
  Module Name  : Sim_Controller
  Project Name : 
  Description  : 
  Dependencies : 
  Revision     : 0.01 - File Created
  Comments     :  
  ----------------------------------*/

`include "LDPC_Simulation_Parameters.h"
`include "../hdl_gen/LDPC_Decoder_parameters.h"

module Sim_Controller(
		clk				  		,
		rst_n				  	,
		simulation_start  		,
		decode_valid_plus 		,
		decode_error_plus 		,
		iterations_done   		,
		iterations		  		,
		update_noise_parameters	,
		start_decode	  		,
		simulation_done   		,
		ram_we			  		,
		frame_counter	  		,
		ebno_idx		  		,
		error_counter	  		,
		iteration_counter 		);
		
    input 									clk				  		,
											rst_n				  	,
											simulation_start  		,
											decode_valid_plus 		,
											decode_error_plus 		,
											iterations_done   		;
    input [`iteration_wl-1        		:0] iterations		  		;
 	
    output 									update_noise_parameters	, 
											start_decode	  		, 
											simulation_done	  		,
											ram_we			  		;
    output 	[`frame_counter_wl-1		:0] frame_counter	  		;
    output 	[`ebno_range_wl-1			:0] ebno_idx		  		;
    output 	[`error_counter_wl-1		:0] error_counter     		;
    output 	[`iteration_counter_wl-1	:0] iteration_counter 		;
    
    reg 	[`ebno_range_wl-1			:0] ebno_idx		  		;
    reg 	[`frame_counter_wl-1		:0] frame_counter	  		;
    reg 	[`error_counter_wl-1		:0] error_counter	  		;
    reg 	[`iteration_counter_wl-1	:0] iteration_counter 		;
    reg 	[3							:0] state			  		;
    reg 									update_noise_parameters , 
											start_decode	  		,
											simulation_done	  		,
											ram_we			  		;
    
    parameter idle              = 3'b000 ;
    parameter update            = 3'b001 ;
    parameter send_start_signal = 3'b010 ;
    parameter fer_compute       = 3'b011 ;
    parameter save_frame_NO     = 3'b100 ;
    parameter update_ebno       = 3'b101 ; 
    parameter read_ram          = 3'b110 ;
    
    
    always @(posedge clk or negedge rst_n )
	
    begin
    if (rst_n == 0)
		begin
			ebno_idx 				<= 10'd`ebno_first_point    ;
			frame_counter 			<= `frame_counter_wl'b0     ;
			error_counter 			<= `error_counter_wl'b0     ;
			iteration_counter 		<= `iteration_counter_wl'b0 ;
			update_noise_parameters <= 1'b0						;
			start_decode 			<= 1'b0						;
			simulation_done 		<= 1'b0						;
			ram_we 					<= 1'b0						;
			state 					<= idle						;
        end
    else 
        case (state)
            
            idle:
				
                if(simulation_start == 1'b1)
                    state <= update;
                else
					begin
						ebno_idx 				<= 10'd`ebno_first_point   	;
						frame_counter 			<= `frame_counter_wl'b0	   	;
						error_counter 			<= `error_counter_wl'b0	   	;
						iteration_counter 		<= `iteration_counter_wl'b0 ;
						state 					<= idle						;
						update_noise_parameters <= 1'b0						;
						start_decode 			<= 1'b0						;
						simulation_done 		<= 1'b0						;  
						ram_we 					<= 1'b0						;
					end
                    
            update:
				
                if (update_noise_parameters == 1'b1)
					begin
						update_noise_parameters	<= 1'b0				 		;
						state 					<= send_start_signal 		;
					end
                else
					begin
						update_noise_parameters <= 1'b1	  					;
						state 					<= update 					;
					end
                    
            send_start_signal:
				
                if (start_decode == 1'b1)
					begin
						start_decode			<= 1'b0		   				;
						state 					<= fer_compute 				;
					end
                else
					begin
						start_decode 			<= 1'b1				 		;
						state 					<= send_start_signal 		;
					end           
                   
            fer_compute:
				
                if(iterations_done == 1)
                    iteration_counter <= iteration_counter + iterations ;
                else
					if(decode_error_plus == 1)						
						error_counter <= error_counter + 1'b1 ;						
					else if(decode_valid_plus == 1)
						begin
							frame_counter <= frame_counter + 1'b1 ;
							if((error_counter == `frame_error_max) | (frame_counter == ({`max_frame_counter_wl{1'b1}}-1'b1)))
								state <= save_frame_NO;
							else
								state <= send_start_signal;
						end

                    
            save_frame_NO:
				
                if (ram_we == 1'b1) 
					begin
						frame_counter 			<= `frame_counter_wl'b0    	;
						error_counter 			<= `error_counter_wl'b0    	;
						iteration_counter		<= `iteration_counter_wl'b0	;
						ram_we 					<= 1'b0						;
						state 					<= update_ebno				;                        
					end
                else
					begin
						ram_we 					<= 1'b1						;
						state  					<= save_frame_NO			;
					end  
                    
            update_ebno:
				
                if(ebno_idx == `ebno_last_point)
					begin
						ebno_idx 				<= 10'd`ebno_first_point   	;
						frame_counter 			<= `frame_counter_wl'b0    	;
						error_counter 			<= `error_counter_wl'b0	   	;
						iteration_counter 		<= `iteration_counter_wl'b0 ;
						update_noise_parameters <= 1'b0						;
						start_decode 			<= 1'b0						;
						simulation_done 		<= 1'b1						;  
						ram_we 					<= 1'b0						;
						state 					<= read_ram					;
					end
                else
					begin
						ebno_idx 				<= ebno_idx + 1'b1 			;
						state 					<= update		   			; 
					end
                    
            read_ram:
				
                if(ebno_idx == `ebno_last_point)
					begin
						ebno_idx 				<= 10'd`ebno_first_point   	;
						frame_counter 			<= `frame_counter_wl'b0    	;
						error_counter 			<= `error_counter_wl'b0    	;
						iteration_counter 		<= `iteration_counter_wl'b0 ;
						update_noise_parameters <= 1'b0 					;
						start_decode 			<= 1'b0			 			;
						simulation_done 		<= 1'b0 					;  
						ram_we 					<= 1'b0 					;
						state 					<= idle 					;
					end
                else 
                    ebno_idx 				<= ebno_idx + 1'b1 			;
                        
            default:
				
					begin
						ebno_idx 				<= 10'd`ebno_first_point   	;
						frame_counter 			<= `frame_counter_wl'b0    	;
						error_counter 			<= `error_counter_wl'b0    	;
						iteration_counter 		<= `iteration_counter_wl'b0 ;
						update_noise_parameters <= 1'b0						;
						start_decode 			<= 1'b0						;
						simulation_done 		<= 1'b0						;  
						ram_we 					<= 1'b0						;
						state 					<= idle						;
					end
            endcase
    end
	
endmodule