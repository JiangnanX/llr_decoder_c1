`define 	In_width	35
`define 	LLR_number	2
`define 	Out_cycle   5040 //    CODE_L/`LLR_number
`define   	In_cycle	288    // CODE_L/`In_width

/* `define 	awgn_seeds32 	32'b01011001000010101110001010111011		//random seeds for AWGN 
`define 	awgn_seeds24 	24'b010110010000101011100010		//random seeds for AWGN 
`define 	awgn_seeds16 	16'b0101100100001010	//random seeds for AWGN 
`define 	awgn_seeds8 	8'b01011001		//random seeds for AWGN 
`define 	awgn_seeds4		4'b1010
`define 	awgn_seeds2		2'b01
`define 	awgn_seed1	 	1'b0		//random seeds for AWGN  */


`define 	awgn_seed1		22'b0101010101010101010011
`define 	awgn_seed2		22'b1111010011010101010101
`define 	awgn_seed3		22'b1100011111010101101100
`define 	awgn_seed4		22'b0000011111010100101010
`define 	awgn_seed5		22'b1011111000001101010101
`define 	awgn_seed6		22'b0001110101110000101010
`define 	awgn_seed7		22'b1110001111111111101011
`define 	awgn_seed8		22'b0000000000100101001011
