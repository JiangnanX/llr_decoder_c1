`timescale 1ns / 1ps

/* /---Confidentiality Notice Start--/
  TexasLDPC Inc Confidential.
  © Copyright 2014 to the present, TexasLDPC Inc.
  All the software contained in these files are protected by United States
  copyright law and may not be reproduced, distributed, transmitted,
  displayed, published or broadcast without the prior written permission
  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
  or other notice from copies of the content.
  /---Confidentiality Notice End--/
  
  Company      : TexasLDPC Inc
  Engineer     : Tiben Che 
  Create Date  : 11/01/2014 03:23:28 PM
  Design Name  : 
  Module Name  : LLR_Input_Controller
  Project Name : 
  Description  : 
  Dependencies : 
  Revision     : 0.01 - File Created
  Comments:  
  ----------------------------------*/

`include "LDPC_Simulation_Parameters.h"
`include "Interface_Parameters.h"

module LLR_Input_Controller
	(
    clk				 ,
    rst_n		 	 ,
    start_decode	 ,
    new_frame		 ,
    LLR_ch_wen		 ,
	codeword_ren	 ,
	codeword_bit_idx );
	
    input 	clk			 ,
			rst_n		 ,
			start_decode ;
			
    output 								new_frame 		 ,
										LLR_ch_wen		 ,
										codeword_ren	 ;
	output [`codeword_bit_idx_wl-1:0] 	codeword_bit_idx ;

           
    parameter idle  = 2'b00;
    parameter start = 2'b01;
    parameter count = 2'b10;
    
    reg 							new_frame		  ,
									LLR_ch_wen		  ,
									LLR_ch_wen_int	  ,
									LLR_ch_wen_int_1d ,
									codeword_ren 	  ;
    reg [15						:0] LLR_counter		  ;
    reg [1						:0] ch_wen_counter	  ;
    reg [1						:0] state			  ;
	reg [`codeword_bit_idx_wl-1	:0] codeword_bit_idx  ;
    
    wire start_decode_buffer; 

    assign start_decode_buffer = start_decode;  
    
    always @(posedge clk or negedge rst_n )
	
        begin
			if(rst_n == 1'b0)
				begin
					state               <= idle	 ;
					LLR_counter         <= 0	 ; 
					new_frame           <= 1'b0	 ;
					LLR_ch_wen          <= 1'b0	 ; 
					LLR_ch_wen_int    	<= 1'b0	 ;
					LLR_ch_wen_int_1d 	<= 1'b0	 ;
					ch_wen_counter      <= 2'b00 ;
					codeword_bit_idx 	<= 10'd0 ;
					codeword_ren 		<= 1'b0  ;
				end
			else 
				begin
					
					LLR_ch_wen_int_1d <= LLR_ch_wen_int	   ;
					LLR_ch_wen        <= LLR_ch_wen_int_1d ;
						 
					case (state)
							
						idle:
							if(start_decode_buffer == 1'b1)
								state <= start;
							else
								begin
									state            <= idle  ;
									LLR_counter      <= 0     ;
									new_frame        <= 1'b0  ;
									LLR_ch_wen_int	 <= 1'b0  ;
									ch_wen_counter   <= 2'b00 ;
									codeword_bit_idx <= 10'd0 ;
									codeword_ren 	 <= 1'b0  ;
								end
						start:
							if(new_frame == 1'b1)
								begin
									new_frame 		 <= 1'b0				   	;
									LLR_ch_wen_int	 <= 1'b1				   	;
									LLR_counter 	 <= LLR_counter 	 + `LLR_number	;
									codeword_bit_idx <= codeword_bit_idx + `LLR_number	;
									codeword_ren 	 <= 1'b1					;
									state 			 <= count				   	;	
								end
							else
								new_frame <= 1'b1;
									
						count:
								
							if (LLR_counter == 10'd`LLR_input_counter)
								begin
									LLR_counter      <= 0	 ;
									codeword_bit_idx <= 0	 ;
									codeword_ren 	 <= 1'b0 ;
									ch_wen_counter   <= 0	 ;
									LLR_ch_wen_int	 <= 1'b0 ;						
									state            <= idle ;
								end
							else
								begin
									LLR_counter      <= LLR_counter 	 + `LLR_number ;
									codeword_bit_idx <= codeword_bit_idx + `LLR_number ;
								end

						default:
							begin
								state            <= idle  ;
								LLR_counter      <= 0	  ;
								new_frame        <= 1'b0  ;
								LLR_ch_wen_int	 <= 1'b0  ;
								ch_wen_counter   <= 2'b00 ;
								codeword_bit_idx <= 10'd0 ;
								codeword_ren 	 <= 1'b0  ;
							end
					endcase
				end // end of else   
        end // end of always
endmodule