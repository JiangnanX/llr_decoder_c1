
/* /---Confidentiality Notice Start--/
  TexasLDPC Inc Confidential.
  © Copyright 2014 to the present, TexasLDPC Inc.
  All the software contained in these files are protected by United States
  copyright law and may not be reproduced, distributed, transmitted,
  displayed, published or broadcast without the prior written permission
  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
  or other notice from copies of the content.
  /---Confidentiality Notice End--/
  
  Company      : TexasLDPC Inc
  Engineer     : Tiben Che, Osso Vahabzadeh
  Create Date  :
  Design Name  : 
  Module Name  : LDPC_Simulation_Parameters.h
  Project Name : 
  Description  : 
  Dependencies :
  Revision     : 1.0 (03/04/2016)
  Comments     :
  ----------------------------------*/

`define     P                           140
`define     block_col                   72
`define     LLR_input_counter           `P*`block_col
`define     frame_error_max             2
`define     ebno_first_point   		    0   // The first Eb/No point that will be simulated
`define     ebno_last_point         	10	// The last  Eb/No point that will be simulated
`define     ebno_range_wl               5
`define		codeword_bit_idx_wl 		14
`define     frame_counter_wl            40
`define     max_frame_counter_wl        40
`define     error_counter_wl            8  // = ceil(log2(frame_error_max)) + 1
`define     iteration_counter_wl        43 // = `frame_counter_wl + ceil(log2(no of iterations)
`define		codelength					`P*`block_col