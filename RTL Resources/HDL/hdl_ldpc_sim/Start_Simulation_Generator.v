`timescale 1ns / 1ps

/* /---Confidentiality Notice Start--/
  TexasLDPC Inc Confidential.
  © Copyright 2014 to the present, TexasLDPC Inc.
  All the software contained in these files are protected by United States
  copyright law and may not be reproduced, distributed, transmitted,
  displayed, published or broadcast without the prior written permission
  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
  or other notice from copies of the content.
  /---Confidentiality Notice End--/
  
  Company      : TexasLDPC Inc
  Engineer     : Tiben Che 
  Create Date  : 11/02/2014 04:43:04 PM
  Design Name  : 
  Module Name  : Start_Simulation_Generator
  Project Name : 
  Description  : 
  Dependencies : 
  Revision     : 0.01 - File Created
  Comments     :  
  ----------------------------------*/


module Start_Simulation_Generator
	(
	clk				 ,
	start_simulation ,
	start_simulation_plus
	);
    
    reg 	start_simulation_buffer  ,
			start_simulation_buffer1 ;
    
    input 	clk				 ,
			start_simulation ;
			
    output 	start_simulation_plus;
    
    always @(posedge clk)   
	
		begin
			start_simulation_buffer  <= start_simulation		;
			start_simulation_buffer1 <= start_simulation_buffer	;
		end
    
    assign start_simulation_plus = start_simulation_buffer&(~start_simulation_buffer1) ;
	
endmodule