
/* /---Confidentiality Notice Start--/
  TexasLDPC Inc Confidential.
  © Copyright 2014 to the present, TexasLDPC Inc.
  All the software contained in these files are protected by United States
  copyright law and may not be reproduced, distributed, transmitted,
  displayed, published or broadcast without the prior written permission
  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
  or other notice from copies of the content.
  /---Confidentiality Notice End--/
  
  Company      : TexasLDPC Inc
  Engineer     : Tiben Che, Osso Vahabzadeh 
  Create Date  : 11/01/2014 02:59:46 PM
  Design Name  : 
  Module Name  : LLR_Generator
  Project Name : 
  Description  : 
  Dependencies : 
  Revision     : 1.0 (03/04/2016)
  Comments     :  
  ----------------------------------*/

`include "LDPC_Simulation_Parameters.h"
`include "../hdl_gen/LDPC_Decoder_parameters.h"
`include "Interface_Parameters.h"

module LLR_Generator
	(
    clk_decoder		  		,
    rst_n				  	,
    awgn			  		,
    update_noise_parameters	,
    noise_std_dev_ROM 		,
    vol_ref_ROM		  		,
	codeword_ren			,
	codeword				,
	codeword_bit_idx  		,
    LLR    					);
	
    input 										clk_decoder		  		,
												rst_n				  	,
												update_noise_parameters	,
												codeword_ren			;
	input 	[ `codelength-1  			: 0 ]	codeword				;
	input	[ `codeword_bit_idx_wl-1  	: 0 ]	codeword_bit_idx  		;
    input 	[ `LLR_number*10-1							: 0 ] 	awgn			  		;
    input 	[ 31						: 0 ]	noise_std_dev_ROM 		;
	input 	[ 223						: 0 ]	vol_ref_ROM		  		;
    
    output 	[ `LLR_Ch_wl*`LLR_number-1			: 0 ]  	LLR				  		;
    
    wire 	[`LLR_number-1   :0]									codeword_bit	  		;
	wire 	[`LLR_number*32-1						: 0	] 	noise_0			  		,
												noise_1			  		;
 	
	reg  	[ 15						: 0	] 	std_dev0		  		, 
												std_dev1		  		;
	reg  	[ 223						: 0	]	vol_ref			  		;	
	
	
	assign codeword_bit = 	(codeword_ren == 1) ?   
								codeword [ 10'd`codelength- codeword_bit_idx +: `LLR_number ] : 
								`LLR_number'bx;	
								
	/* assign codeword_bit = 	(codeword_ren == 1) ?   
								codeword [ 10'd`codelength-codeword_bit_idx ] : 
								1'bx 										  ;
	 */							
	
    (* keep_hierarchy = "yes" *) 
/* 	mult_gen_0 mult0
		(
        .CLK( clk_decoder					 ),  					
        .A	( {awgn[9], awgn[9:0], 5'b00000} ),		// input  wire [15 : 0] A        
        .B	( std_dev0						 ),     // input  wire [15 : 0] B
        .P	( noise_0						 ));    // output wire [31 : 0] P
		
	  
	(* keep_hierarchy = "yes" *) 
	mult_gen_0 mult1
		(
        .CLK( clk_decoder					 ),  					
        .A	( {awgn[9], awgn[9:0], 5'b00000} ),		
        .B	( std_dev1						 ),		
        .P	( noise_1						 ));
	  
	  
    (* keep_hierarchy = "yes" *) 
	LLR_Encoder llr_encoder_1
		(
		clk_decoder	 ,
		codeword_bit ,
		noise_0		 , 
		noise_1		 ,
		vol_ref		 ,
		LLR			 ); */
     
    genvar k;
	generate for(k=0;k<`LLR_number;k=k+1) begin
		mult_gen_0 mult0(clk_decoder, {awgn[k*10+9], awgn[k*10+:10], 5'b00000},std_dev0,noise_0[k*32+:32]);
		mult_gen_0 mult1(clk_decoder, {awgn[k*10+9], awgn[k*10+:10], 5'b00000},std_dev1,noise_1[k*32+:32]);
		LLR_Encoder llr_encoder(clk_decoder,codeword_bit[`LLR_number-1-k],noise_0[k*32+:32],noise_1[k*32+:32],vol_ref,LLR[k*`LLR_Ch_wl +:`LLR_Ch_wl]);
	end 
	endgenerate 
	
	always @(posedge clk_decoder or negedge rst_n )
    
        if(rst_n == 1'b0)
            begin
				std_dev0  <= 16'b0  ;
				std_dev1  <= 16'b0  ;
				vol_ref   <= 224'b0 ;
            end 
        else
            if(update_noise_parameters == 1'b1)
            begin
                std_dev0  <= noise_std_dev_ROM[15:0 ] ;
                std_dev1  <= noise_std_dev_ROM[31:16] ;
				vol_ref   <= vol_ref_ROM	  [223:0] ;
            end 
            
endmodule