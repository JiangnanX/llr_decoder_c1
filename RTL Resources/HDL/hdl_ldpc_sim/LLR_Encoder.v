`timescale 1ns / 1ps

/* /---Confidentiality Notice Start--/
  TexasLDPC Inc Confidential.
  © Copyright 2014 to the present, TexasLDPC Inc.
  All the software contained in these files are protected by United States
  copyright law and may not be reproduced, distributed, transmitted,
  displayed, published or broadcast without the prior written permission
  of TexasLDPC Inc. You may not alter or remove any trademark, copyright
  or other notice from copies of the content.
  /---Confidentiality Notice End--/
  
  Company      : TexasLDPC Inc
  Engineer     : Osso Vahabzadeh 
  Create Date  : 01/10/2016 10:46 PM
  Design Name  : 
  Module Name  : LLR_Encoder
  Project Name : 
  Description  : 
  Dependencies : 
  Revision     : 0.01 - File Created
  Comments     :  
  ----------------------------------*/

`include "LDPC_Simulation_Parameters.h"
`include "../hdl_gen/LDPC_Decoder_parameters.h"

module LLR_Encoder
	(
    input   						  clk		   ,
	input 							  codeword_bit ,
    input signed [ 31			: 0 ] noise_0  	   , 
    input signed [ 31			: 0 ] noise_1	   ,
	input        [ 223			: 0 ] vol_ref	   ,
    output reg 	 [ `LLR_Ch_wl-1	: 0 ] LLR		   );
		
	wire signed [31:0]  one_q9f22	  ,
						neg_one_q9f22 ,	
						vr_3t		  , 
						vr_2t		  , 
						vr_1t		  , 
						vr_0t		  , 
						vr_1pt		  , 
						vr_2pt		  , 
						vr_3pt 		  ;
	wire signed [32:0]	vr_3 		  , 
						vr_2  		  ,
						vr_1  		  ,
						vr_0  		  ,
						vr_1p  		  ,
						vr_2p  		  ,
						vr_3p  		  ;

	wire        [6 :0]  cond_onehot   ;
	
	reg	signed  [32:0] 	Y			  ;		
	reg                	cond_3	      , 
						cond_2		  , 
						cond_1		  , 
						cond_0		  , 
						cond_1p 	  , 
						cond_2p 	  , 
						cond_3p 	  ;
						
		
	assign one_q9f22	 = 32'b00000000010000000000000000000000				;
	assign neg_one_q9f22 = 32'b11111111110000000000000000000000				;
	
    assign { vr_3t, vr_2t, vr_1t, vr_0t, vr_1pt, vr_2pt, vr_3pt} = vol_ref	;
    assign vr_3  = { vr_3t [31], vr_3t  }						     		;
    assign vr_2  = { vr_2t [31], vr_2t  }							   		;
    assign vr_1  = { vr_1t [31], vr_1t  }									;
    assign vr_0	 = { vr_0t [31], vr_0t  }									;
    assign vr_1p = { vr_1pt[31], vr_1pt }									;
    assign vr_2p = { vr_2pt[31], vr_2pt }									;
    assign vr_3p = { vr_3pt[31], vr_3pt }									;
	
    
    always @(posedge clk)
    
	begin		
		if (codeword_bit == 1'b0)
			Y <= one_q9f22     			+ noise_0 ;
			//Y <= { 1'b0, one_q9f22 		+ 32'b0	 };
		else if (codeword_bit == 1'b1)
			Y <= neg_one_q9f22 			+ noise_1 ;
			//Y <= { 1'b1, neg_one_q9f22 	+ 32'b0  };
				
		cond_3	<= Y > vr_3  ; 
		cond_2	<= Y > vr_2  ;
		cond_1	<= Y > vr_1  ;
		cond_0  <= Y > vr_0  ;
		cond_1p	<= Y > vr_1p ;
		cond_2p	<= Y > vr_2p ;
		cond_3p	<= Y > vr_3p ;
	end    
	
	assign cond_onehot = { cond_3, cond_2, cond_1, cond_0, cond_1p, cond_2p, cond_3p };
                
	always @ (*)
	
	begin                
		casex(cond_onehot)
			7'b1xxxxxx : LLR = 5'b01101 ;
			7'b01xxxxx : LLR = 5'b01001 ; 
			7'b001xxxx : LLR = 5'b00101 ;
			7'b0001xxx : LLR = 5'b00010 ;
			7'b00001xx : LLR = 5'b11110 ;
			7'b000001x : LLR = 5'b11011 ;
			7'b0000001 : LLR = 5'b10111 ;
			7'b0000000 : LLR = 5'b10011 ;
			default    : LLR = 5'b00000 ;
		endcase
	end    
	
endmodule