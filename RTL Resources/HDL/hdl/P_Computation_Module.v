/*
  
  /---Confidentiality Notice Start--/
  This document contains information proprietary to Texas A&M University, College Station.
  Any disclosure or use is expressly prohibited, except upon explicit
  written permission by Texas A&M University.
  Texas A&M University Confidential.
  Contact: Office of Technology Commercialization.
  The material contained in this software and the associated papers
  have features which are contained in a patent disclosure for LDPC decoding architectures 
  filed by Gwan Choi, John Junkins , Kiran Gunnam. 
  For more information about the licensing process, please contact:
  Alex Garcia
  Department: Licensing & Intellectual Property Management
  Title: Licensing Associate
  Email: alegarza@tamu.edu
  Phone: (979) 458-2640
  Fax: (979) 845-2684
  /---Confidentiality Notice End--/
  
  Kiran Gunnam 
  Texas A&M University, December 2005  
  File Name              : P_Computation_Module.v 
  Module Name            : P_Computation_Module
  Module Attributes      : 
  Design                 : Data path and Control Paths, Vector processing
  Parametrizable         : Yes
  Functionality          : 
  This module instantiates the P adder array and R select logic for Rnew computation.
  P = Qold + Rnew;
  Rnew is computed on-the-fly from FS memory in an out-of-order fashion.
  This module is an implementation of the layered decoding of QC-LDPC codes
  based on "On-the-fly" computation paradigm.
  Matlab Model           :	Block_LDPC_Decoder.m
  Revision History       : v1.0, Kiran Gunnam,Texas A&M University, December 2005.
*/

`include "../hdl_gen/LDPC_Decoder_Parameters.h"
module P_Computation_Module
                       (
							   clk,
							   Qs_old,
								FS_new,
//								Qsign_new,
								block_num,
								set_Rnew_to_zero,
								P
								);

//Inputs and Outputs
input                                  clk;
input   [`Qs_memory_bandwidth-1   : 0] Qs_old;
input   [`FS_memory_bandwidth-1   : 0] FS_new;
//input   [`Qsign_memory_bandwidth-1: 0] Qsign_new;
input   [`dc_wl-1                 : 0] block_num;
input                                  set_Rnew_to_zero;
output  [`Qs_memory_bandwidth-1   : 0] P;          

//Internal wires and pipeline registers. 
wire    [`R_bandwidth-1           : 0] R_new;
reg     [`R_bandwidth-1           : 0] R_new_L;
reg     [`Qs_memory_bandwidth-1   : 0] Qs_old_L;          
wire    [`Qs_wl-1:0]                   Qs_old_wire[`HD_blocksize-1:0];//for debug easy view
//reg     [`FS_memory_bandwidth-1   : 0] FS_new_L;
//reg     [`Qsign_memory_bandwidth-1: 0] Qsign_new_L;
wire     [`Qsign_memory_bandwidth-1: 0] Qsign_new_int;

//R_Selection_array RSA_Rnew_ref(clk,FS_new_L,Qsign_new_L,block_num,set_Rnew_to_zero,R_new);
//R_Selection_array RSA_Rnew(clk,FS_new_L,Qsign_new_int,block_num,set_Rnew_to_zero,R_new); //if pd=13
R_Selection_array RSA_Rnew(clk,FS_new,Qsign_new_int,block_num,set_Rnew_to_zero,R_new);
P_adder_array     PAA(P,Qs_old_L,R_new_L);

always@(posedge clk)
 begin
 R_new_L      <= R_new;
 Qs_old_L     <= Qs_old;
// FS_new_L     <= FS_new; //if pd=13
 //Qsign_new_L  <= Qsign_new;
 end
 
 genvar k;
 generate
  for(k=0; k<`HD_blocksize; k=k+1)
    begin:Sign_Extraction
    assign Qs_old_wire[k]   = Qs_old[k*`Qs_wl+`Qs_wl-1:k*`Qs_wl];//for debug easy view
    assign Qsign_new_int[k] = Qs_old[k*`Qs_wl+`Qs_wl-1];
    end 
 endgenerate
 
endmodule       
