/*
  /---Confidentiality Notice Start--/
  Texas A&M University Confidential.
  Contact: Office of Technology Commercialization.
  The material contained in this software and the associated papers
  have features which are contained in a patent disclosure for LDPC decoding architectures 
  filed by Gwan Choi, John Junkins , Kiran Gunnam. 
  For more information about the licensing process, please contact:
  Alex Garcia
  Department: Licensing & Intellectual Property Management
  Title: Licensing Associate
  Email: alegarza@tamu.edu
  Phone: (979) 458-2640
  Fax: (979) 845-2684
   � Copyright 2005 to the present, Texas A&M University System. 
  All the software contained in these files are protected by United States copyright law
  and may not be reproduced, distributed, transmitted, displayed, published or broadcast 
  without the prior written permission of Texas A&M University System. 
  You may not alter or remove any trademark, copyright or other notice from copies 
  of the content.
  /---Confidentiality Notice End--/

  Kiran Gunnam 
  Texas A&M University
  File Name              : hd_Mem_control.v 
  Module Attributes      : Control for the Memory module for hard decision memory
  Design                 : 
  Parametrizable         : Yes
  Functionality          : Supplies the control signals for the hard decison memory for the decoder 
  More than Double buffering/additionl buffering can be added
  based on the application requirements such as to cater
  average throughput case and low energy throttling
  Matlab Model           :	See the Block_LDPC_Decoder.m 
  Revision History       : v1.2 
  
  v1.0, Kiran Gunnam, Dec 2005: soi_busy controls the d_start and d_start indicates the start of streaming output.
  soi_busy serves as an active low read signal to read  the decoded bits. 
  one vector of 128 bits is read out one clock cycle after the assertion of
  soi_busy to low. There is no guard time required between successive reads. However when the soi_busy is 
  asserted high, this interface still writes 7 vectors each of length 128 bits.
  So in other words, the latency for the control through soi_busy is 7 clock cyles.
  v1.1: This is changed such that the HD buffer writes the data only when
  the soi_busy goes from high to low. Between such successive events, there should
  be a minimum of 7 clock cycles.--This is a temp. change.
  v1.2: soi_busy now does not control the d_start as d_start now simply indicates the
  availabilty of a frame for streaming. soi_busy serves as an active low read signal to read
  the decoded bits. one vector of 128 bits is read out one clock cycle after the assertion of
  soi_busy to low. There is no guard time required between successive reads. Note that data_valid_soi is a dont care signal 
  for now.
  
*/


`include "../hdl_gen/LDPC_Decoder_Parameters.h"

module HD_Buffer(
                     //decoder clock
							clk,
							//global reset
							reset_n,
							//Number of Block columns in the H matrix. From OFC ROM
							H_col,
                            H_row,
							soi_busy,//stream_out_interface_busy,
							         //busy signal from output interface.
							//decoder interface to hd memory
                            hd_wr_addr, 
							hd_buf_wen, 
                            d_block_s2_to_hd_Mem, 
                            SHIFT_COEFF_hd,
   						//control signals from decoder
							decoding_start,
							decoder_busy,
							decoding_done,
                            decoding_error,
   						//From cht module 
							hd_rd_addr_cht,
   						//To cht module  
	                        d_block_s2,
   						//Note that the read address
							//for decoded_data_out data is generated internally
							//within this module.
                            d_start_soi,
							decoded_data_out_soi,
							data_valid_soi,
                            hdb_frame_error,
							//control signal to the Input interface
							hd_Mem_busy,
                            //indicator signal to the debug interface
                            hd_Mem_avl_debug,
                            hd_Mem_avl_flag 
							);

input                                  clk;
input                                  reset_n;
input   [`MaxH_col_wl-1          : 0]  H_col;
input   [`MaxH_row_wl-1          : 0]  H_row;
input                                  soi_busy;
input   [`HD_memory_addr_wl-1 : 0]     hd_wr_addr;
input                                  hd_buf_wen;
input   [`HD_blocksize-1 : 0]          d_block_s2_to_hd_Mem;
input   [`Shift_wl-1:0]                SHIFT_COEFF_hd;
input                                  decoding_start,
                                       decoder_busy,
									   decoding_done,
                                       decoding_error;
input   [`HD_memory_addr_wl-1 : 0]     hd_rd_addr_cht;
output  [`HD_blocksize-1 : 0]          d_block_s2;
output                                 d_start_soi;
output   [`HD_blocksize-1 : 0]         decoded_data_out_soi;
output                                 data_valid_soi;
output                                 hdb_frame_error;
output                                 hd_Mem_busy;
output                                 hd_Mem_avl_flag;
output [`Number_HD_Buffers_wl-1     : 0]  hd_Mem_avl_debug; 
wire [`Number_HD_Buffers_wl-1     : 0]  hd_Mem_avl_debug;


//Internal signals and registers
//signal that depend on the assumption that we use double buffering
reg                                    hdbr,hdbw;
//signal that are generic for any number of buffers
reg [`Number_HD_Buffers-1        : 0]  hd_wen;    
reg [`Number_HD_Buffers_wl-1     : 0]  hd_Mem_avl;
reg                                    hd_Mem_busy;
reg                                    data_valid;
reg [`HD_memory_addr_wl-1        : 0]  hd_rd_address1,hd_rd_address2,
                                       hd_rd_address_soi;
reg                                    d_start,buffer_streaming,hd_streaming_done,hd_streaming_done_1D;      
reg                                    soi_streaming;
reg                                    d_start_1D,d_start_2D,d_start_3D,d_start_4D,
                                       d_start_5D,d_start_6D,d_start_7D,d_start_8D;
reg                                    buffer_streaming_1D,buffer_streaming_2D,buffer_streaming_3D,
                                       buffer_streaming_4D,buffer_streaming_5D,buffer_streaming_6D,
                                       buffer_streaming_7D;
reg                                    soi_busy_1D,soi_busy_2D,soi_busy_3D;                                       

//internal registers
reg   [`Shift_wl-1:0]                  SHIFT_COEFF_hd_OUT;
reg   [`HD_blocksize-1 : 0]            decoded_data_out_soi;
wire  [`Shift_wl-1:0]                  shift_out1,shift_out2;
wire  [`HD_blocksize-1 : 0]            d_out1,d_out2,decoded_data_out;
reg   [`HD_blocksize-1 : 0]            decoded_data_out_pf;
reg   [`HD_blocksize-1  : 0]		   d_block_s2,
									   decoded_data_shifted;
wire [`HD_blocksize-1  : 0]            decoded_data_shifted_flip,decoded_data_out_flip; 
reg  [`MaxH_col_wl-1   : 0]            num_vectors_sent;                                      
reg hdb0_error,hdb1_error,hdb_frame_error;
reg prefetch_vectors,prefetch_vectors_1D;
													
/*************START OF LOGIC- dependent on double buffering. ************************************************/
//since double buffering is used this simple flip logic is sufficent to manage the read and write control
//for the hd memory.
always@(posedge clk or negedge reset_n)
 begin
 if(reset_n == 1'b0)
  begin
  hdbw               <= 1'b1;
  hdbr               <= 1'b0;
  hd_wen             <= 2'b00;
  hdb0_error         <= 0;
  hdb1_error         <= 0;
  end
  else
   begin
	 /*WRITE LOGIC**********************/
	 //Ping-pong between two buffers for every new frame and when there is a buffer available to be written
      if(decoding_start==1)//the conditon to  hd_Mem_busy==0 is already checked in Qs_Mem_Control.v before invoking the decoding_start=1. 
		hdbw        <= ~hdbw; //so no need to place the condition hd_Mem_busy==0 again along with if(decoding_start==1). If
                                  //extra caution is needed, then please use if(decoding_start==1 & hd_Mem_busy_1D==1)
                                  //where hd_Mem_busy_1D is 1 cycle delayed version of hd_Mem_busy

		//Maintain the multiplexer logic for address and data for the double buffering	
		if(hdbw==1'b0)
		 begin
		 hd_wen[0]  <= hd_buf_wen;
		 hd_wen[1]  <= 1'b0;
		 end
		 else
		 begin
		 hd_wen[0]  <= 1'b0;
		 hd_wen[1]  <= hd_buf_wen;
		 end
      
	/*READ LOGIC for data into cht module**********************/
		//assign read enable for the hd memory for reading into the cht module
		//if more than double buffering is used, need to change this logic.
		 d_block_s2         <= (hdbw==1'b0)? d_out1:d_out2;
		  //need to check this once again.
		 if(decoder_busy)
		  begin
		  hd_rd_address1     <=(hdbw==1'b0)?hd_rd_addr_cht:
                                         ((soi_busy==1'b0 | prefetch_vectors == 1'b1)?hd_rd_address_soi:hd_rd_address1);
		  hd_rd_address2     <=(hdbw==1'b1)?hd_rd_addr_cht:
                                         ((soi_busy==1'b0 | prefetch_vectors == 1'b1)?hd_rd_address_soi:hd_rd_address2);
		  end
		  else
		  begin
		   if(hdbr==1'b0)
		    hd_rd_address1     <=(soi_busy==1'b0 | prefetch_vectors == 1'b1)?hd_rd_address_soi:hd_rd_address1;
		    else
		    hd_rd_address2     <=(soi_busy==1'b0 | prefetch_vectors == 1'b1)?hd_rd_address_soi:hd_rd_address2;
		  end
      
		
  
	/*READ LOGIC for streaming out buffer**********************/
		//Ping-pong between two buffers for every new frame when the streaming for the previous frame is done.
      if(hd_streaming_done_1D==1'b1)
		hdbr       <= ~hdbr;
		//assign read enable for the double buffering. 
		//if more than one buffer is used, need to change this logic.
         if(soi_busy_1D==1'b0 | prefetch_vectors_1D==1'b1)
         begin
         decoded_data_shifted <= (hdbr==1'b0)? d_out1:d_out2;
		 SHIFT_COEFF_hd_OUT   <= (hdbr==1'b0)? shift_out1:shift_out2;
         end
         
    //Latch the decoder error signals
     //latch the decoder error signal when the signal iterations_done is 1.
        
        if(decoding_done & hdbw==1'b0)
         hdb0_error  <= decoding_error;
       //note that the following statement is MUTEX wrt above
        if(decoding_done & hdbw==1'b1)
         hdb1_error      <= decoding_error;       
         
         //Mark the outgoing buffer with the error flag.
         hdb_frame_error <= ((hdbr==1'b0)?hdb0_error:hdb1_error)& buffer_streaming_6D;
  end //end of else of if(reset_n == 1'b0) block
 end//end of always block 
/*************END OF LOGIC- dependent on double buffering. ************************************************/
	
/* START OF GENERIC LOGIC that maintains the buffer control. this is independent of double buffering********/
always@(posedge clk or negedge reset_n)
 begin
 if(reset_n == 1'b0)
  begin
  hd_rd_address_soi    <= 0;
  hd_Mem_avl           <= 0;
  hd_Mem_busy          <= 1'b0;
  buffer_streaming     <= 1'b0;          
  soi_streaming        <= 1'b0;  
  hd_streaming_done    <= 1'b0;
  hd_streaming_done_1D <= 1'b0;
  d_start              <= 1'b0;
  d_start_1D           <= 1'b0;
  d_start_2D           <= 1'b0;
  d_start_3D           <= 1'b0;
  d_start_4D           <= 1'b0;
  d_start_5D           <= 1'b0;
  d_start_6D           <= 1'b0;
  d_start_7D           <= 1'b0;
  d_start_8D           <= 1'b0;
  data_valid           <= 1'b0;
  soi_busy_1D          <= 1'b0;
  soi_busy_2D          <= 1'b0;
  soi_busy_3D          <= 1'b0;
  prefetch_vectors     <= 1'b0;
  prefetch_vectors_1D  <= 1'b0;
  num_vectors_sent     <= 1'b0;
  buffer_streaming_1D  <= 1'b0;
  buffer_streaming_2D  <= 1'b0;
  buffer_streaming_3D  <= 1'b0;
  buffer_streaming_4D  <= 1'b0;
  buffer_streaming_5D  <= 1'b0;
  buffer_streaming_6D  <= 1'b0;
  buffer_streaming_7D  <= 1'b0;
  
  end
  else
   begin
        
         soi_busy_1D <= soi_busy;
         soi_busy_2D <= soi_busy_1D;
         soi_busy_3D <= soi_busy_2D;
         prefetch_vectors_1D <= prefetch_vectors;
         
        if(soi_busy_1D==1'b0 | prefetch_vectors_1D==1'b1 ) 
         decoded_data_out_soi <= decoded_data_out_pf;
        if(soi_busy_1D==1'b0 | prefetch_vectors_1D==1'b1 ) 
         decoded_data_out_pf <= decoded_data_out;
         
		//maintain the read address logic to stream out the data
         if(d_start==1'b1)
          begin
          hd_rd_address_soi    <= 0;
          buffer_streaming     <= 1'b1;
          end
		 else if(buffer_streaming == 1'b1 & (soi_busy==1'b0 | prefetch_vectors==1'b1)  //&&soi_busy_1D==1'b1 
		          & hd_rd_address_soi  < H_col-H_row-1)
		 hd_rd_address_soi    <= hd_rd_address_soi+1;
		 
         if(d_start_6D==1'b1)
          soi_streaming    <= 1'b1;
         else if(soi_streaming == 1'b1 & soi_busy==1'b0 & num_vectors_sent<H_col-H_row)
           num_vectors_sent <= num_vectors_sent+1;
         else if(num_vectors_sent==H_col-H_row)
		  num_vectors_sent    <= 0;
           

         
          //data_valid       <= buffer_streaming_5D & buffer_streaming_6D & (!soi_busy);
          data_valid       <= soi_streaming & (!soi_busy)&(!d_start_6D);
        
         
          d_start_1D           <= d_start; //look for the d-start in the logic following this.
          d_start_2D           <= d_start_1D;
          d_start_3D           <= d_start_2D;
          d_start_4D           <= d_start_3D;
          d_start_5D           <= d_start_4D;
          d_start_6D           <= d_start_5D;
          d_start_7D           <= d_start_6D;
          d_start_8D           <= d_start_7D;
          
          
          buffer_streaming_1D  <= buffer_streaming;
          buffer_streaming_2D  <= buffer_streaming_1D;
          buffer_streaming_3D  <= buffer_streaming_2D;
          buffer_streaming_4D  <= buffer_streaming_3D;
          buffer_streaming_5D  <= buffer_streaming_4D;
          buffer_streaming_6D  <= buffer_streaming_5D;
          buffer_streaming_7D  <= buffer_streaming_6D;
          
                    
          hd_streaming_done_1D <= hd_streaming_done;
         
         

		 
		//Maintain the memory availability flag. this denotes the number of available buffers for the decoder
		//Note that this takes care of the rare event that a new hd frame and buffer streaming complete
		//at the same clock cycle. Note that the following logic is an unsigned logic. So make sure
		//not to declare any of these signals as singed.
		
		if(hd_rd_address_soi>0 & num_vectors_sent==H_col-H_row-1 & hd_rd_address_soi== H_col-H_row-1)
		begin
		  buffer_streaming    <= 1'b0;
          soi_streaming       <= 1'b0;
		  hd_Mem_avl          <= (hd_Mem_avl+decoding_done)-1;
		end
		else
		  hd_Mem_avl <= hd_Mem_avl + decoding_done;
          
        
		
        
		//Maintain the d_start and buffer_streaming flag		  
		 //if(d_start==1'b0 & buffer_streaming==1'b0 & hd_Mem_avl > 0 & soi_busy ==1'b0)
         if(d_start==1'b0 & buffer_streaming==1'b0 & buffer_streaming_7D ==1'b0 & hd_Mem_avl > 0)//soi_busy now does not control the d_start
                              //as d_start now simply indicates the availabilty of a frame for streaming
          begin                              
		  d_start              <= 1'b1;
          prefetch_vectors     <= 1'b1;
          end
		  else
		  d_start       <= 1'b0;
          
          if(d_start_6D == 1'b1)
           prefetch_vectors     <= 1'b0;

		 //Maintain an internal signal to show the state of the hd memory
		if(d_start)
		hd_streaming_done   <= 1'b0;
		//else if(hd_rd_address_soi==H_col-H_row)
        else if(num_vectors_sent==H_col-H_row)
		hd_streaming_done   <= 1'b1;
		else
		hd_streaming_done   <= 1'b0;
		
		//Mainitain a signal to the interface to show the state of hd_Mem. If this signal 
		//is 1, then this module can not accept any new frames.
		if(hd_Mem_avl==`Number_HD_Buffers)
		hd_Mem_busy <= 1'b1;
		else
		hd_Mem_busy <= 1'b0;		  
		
  end //end of else of if(reset_n == 1'b0) block
 end//end of always block 

assign d_start_soi    = buffer_streaming_7D & buffer_streaming;//d_start_7D;
//assign d_start_soi    = d_start_7D;
assign data_valid_soi = data_valid;//data_valid | d_start_6D;
assign  hd_Mem_avl_flag = |hd_Mem_avl; //indicator that atleast one hard decision buffer is not empty.
                                       //note that this flag is for indicative purposes. And should
                                       //not be used in any control within the decoder core.
assign hd_Mem_avl_debug = hd_Mem_avl;                                        
                                       
/* END OF GENERIC LOGIC that maintains the buffer control. this is independent of double buffering********/

/**********************************************************************************/
//Instantiate the hard decision memory with two buffers
hd_mem hd_mem(
    .rd_addr1(hd_rd_address1),
	.rd_addr2(hd_rd_address2),
    .wr_addr(hd_wr_addr), 
    .clk(clk), 
    .d_in(d_block_s2_to_hd_Mem), 
    .shift_in(SHIFT_COEFF_hd), 
	.d_out1(d_out1),
    .d_out2(d_out2), 
    .shift_out1(shift_out1), 
	.shift_out2(shift_out2), 
    .wen(hd_wen)
    );
/**********************************************************************************/

/*Undo the shift on the hard decison data before sending out*/
/*Note that the router has some latency usually 1,2,3 clock cycles depending on
paralleization factor M*/

genvar k;
 generate
  for(k=0; k<`HD_blocksize; k=k+1)
	begin:Bit1_as_MSB
	assign decoded_data_shifted_flip[k]  = decoded_data_shifted[`HD_blocksize-1-k];
	end 
 endgenerate
 
//Routers and Multiplexers
HD_shifter d_shifter(
    .clk(clk), 
    .X(decoded_data_shifted_flip), 
    .Xs(decoded_data_out_flip), 
    .Shift(SHIFT_COEFF_hd_OUT),
    .freeze_flag(soi_busy_1D & prefetch_vectors_1D==1'b0)//freeze_flag_2D
   );

genvar k1;
 generate
  for(k1=0; k1<`HD_blocksize; k1=k1+1)
	begin:Bit1_as_LSB
	assign decoded_data_out[k1]  = decoded_data_out_flip[`HD_blocksize-1-k1];
	end 
 endgenerate

endmodule











