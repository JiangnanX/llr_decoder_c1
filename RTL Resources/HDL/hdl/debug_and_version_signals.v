/*
Kiran Gunnam
File Name     : debug_and_version_signals.v
Module        : debug_and_version_signals
Functionality : This module sends internal control and decoding status signals of the decoder 
                It also adds a revision tag for the LDPC decoder core.
                If need to debug the other longer data buses, please use ChipScope on Xilinx
                FPGAs or similar untility on ASIC emulation platforms.

Module Attributes      : Debug Interface unit
Parametrizable         : Yes. Can add more debug signals as needed
                          by changing the DEBUG_BUS_wl

*/                
`include "../hdl_gen/LDPC_Decoder_Parameters.h"

`define DEBUG_BUS_wl `CodeWordLength_wl+`CodeWordLength_wl+`Version_wl+`dc_wl+`CodeID_wl+`H_row_wl+`iteration_wl+5
module debug_and_version_signals(
                                 input clk,
                                 input Qs_Mem_avl_flag,
                                 input hd_Mem_avl_flag,
                                 input [`iteration_wl-1:0]       iterations_CC,
                                 input [`H_row_wl-1:0]           layer_CC,
                                 input [`CodeID_wl-1:0]          CodeId,
                                 input [`CodeWordLength_wl-1:0]    CodeWordLength,
                                 input [`CodeWordLength_wl-1:0]    DataWordLength,
                                 input [`dc_wl-1:0]                block_num_CC,
                                 input iterations_done, 
                                 input Disable_CC_logic,
                                 output [`DEBUG_BUS_wl-1:0]       DEBUG_BUS);

//number_of_scalar_values, clk,Qs_Mem_avl_flag,hd_Mem_avl_flag,iterations_done,Disable_CC_logic
parameter ns = 5;                                 

assign DEBUG_BUS[0]                    = clk;
assign DEBUG_BUS[1]                    = Qs_Mem_avl_flag;
assign DEBUG_BUS[2]                    = hd_Mem_avl_flag;
assign DEBUG_BUS[3]                    = iterations_done;                                       
assign DEBUG_BUS[4]                    = Disable_CC_logic;
assign DEBUG_BUS[`iteration_wl+ns-1:ns]  = iterations_CC;
assign DEBUG_BUS[`H_row_wl +`iteration_wl+ns-1:`iteration_wl+ns]  
                                       = layer_CC;
assign DEBUG_BUS[`CodeID_wl+`H_row_wl +`iteration_wl+ns-1 :`H_row_wl +`iteration_wl+ns]   
                                       = CodeId;
assign DEBUG_BUS[`dc_wl+`CodeID_wl+`H_row_wl +`iteration_wl+ns-1 :`CodeID_wl+`H_row_wl +`iteration_wl+ns]  
                                       = block_num_CC;
assign DEBUG_BUS[`Version_wl+`dc_wl+`CodeID_wl+`H_row_wl +`iteration_wl+ns-1:`dc_wl+`CodeID_wl+`H_row_wl +`iteration_wl+ns]
                                       = `Version;
assign DEBUG_BUS[`CodeWordLength_wl+`Version_wl+`dc_wl+`CodeID_wl+`H_row_wl +`iteration_wl+ns-1: `Version_wl+`dc_wl+`CodeID_wl+`H_row_wl +`iteration_wl+ns]
                                           = CodeWordLength;                                       

assign DEBUG_BUS[`CodeWordLength_wl+`CodeWordLength_wl+`Version_wl+`dc_wl+`CodeID_wl+`H_row_wl +`iteration_wl+ns-1: `CodeWordLength_wl+`Version_wl+`dc_wl+`CodeID_wl+`H_row_wl +`iteration_wl+ns]
                                           = DataWordLength;                                       

endmodule