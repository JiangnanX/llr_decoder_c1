/*
/---Confidentiality Notice Start--/
  Texas A&M University Confidential.
  Contact: Office of Technology Commercialization.
  The material contained in this software and the associated papers
  have features which are contained in a patent disclosure for LDPC decoding architectures 
  filed by Gwan Choi, John Junkins , Kiran Gunnam. 
  For more information about the licensing process, please contact:
  Alex Garcia
  Department: Licensing & Intellectual Property Management
  Title: Licensing Associate
  Email: alegarza@tamu.edu
  Phone: (979) 458-2640
  Fax: (979) 845-2684
   � Copyright 2005 to the present, Texas A&M University System. 
  All the software contained in these files are protected by United States copyright law
  and may not be reproduced, distributed, transmitted, displayed, published or broadcast 
  without the prior written permission of Texas A&M University System. 
  You may not alter or remove any trademark, copyright or other notice from copies 
  of the content.
  /---Confidentiality Notice End--/
*/  
`include "../hdl_gen/LDPC_Decoder_Parameters.h"  
module fs_mem_regular(layer_CPA,layer,DL,clk,reset_n,FS_in,FS_old,FS_new,wen,FS_old_ren,FS_new_ren,fs_new_rw_cond1);

parameter FS_memory_bandwidth = `FS_memory_bandwidth;//1792;//;
parameter FS_memory_depth     = `FS_memory_depth;
parameter FS_memory_addr_wl   = `FS_memory_addr_wl;

/*parameter FS_memory_bandwidth = `FS_memory_bandwidth;
parameter FS_memory_depth     = `FS_memory_depth;
parameter FS_memory_addr_wl   = `FS_memory_addr_wl;*/

input [FS_memory_addr_wl-1 : 0] layer_CPA,layer,DL;
input clk;
input reset_n;
input  [FS_memory_bandwidth-1  : 0] FS_in;
output [FS_memory_bandwidth-1  : 0]  FS_old;
output [FS_memory_bandwidth-1  : 0] FS_new;
input wen;
input FS_old_ren,FS_new_ren;
input fs_new_rw_cond1;  

wire  [FS_memory_addr_wl-1 : 0] layer_rw;
reg  [FS_memory_addr_wl-1 : 0] layer_CPA_1D,DL_1D;
reg [FS_memory_bandwidth-1  : 0] FS_new_reg;
wire [FS_memory_bandwidth-1  : 0] FS_new_m;
reg wen_1D;
wire rw_cond1;
reg rw_cond;
generate
if(`FS_MEM_TYPE==1)
 begin: FS_MEM_RW1P_R1P
  
//FS_old is same as previus FS_out when wen=1.  
assign layer_rw=wen?layer_CPA:layer;    //can move this to previous pipeline stage for timing, if tool does not do retiming automatically

dpram_rw1p_r1p #(FS_memory_bandwidth,FS_memory_depth,FS_memory_addr_wl)
                fs_mem(layer_rw,DL,clk,FS_in,FS_old,FS_new,wen,FS_old_ren,FS_new_ren);
                
end
else if(`FS_MEM_TYPE==2)
 begin: FS_MEM_R2P_W1P
 dpram_r2p_w1p #(FS_memory_bandwidth,FS_memory_depth,FS_memory_addr_wl)
                 fs_mem(layer_CPA,layer,DL,clk,FS_in,FS_old,FS_new,wen);
 end
 else if(`FS_MEM_TYPE==3)
   begin: FS_MEM_WCAHCE_OPT
   //move the code for FS_MEM_WCACHE_OPT HERE
   //FS_old is same as previus FS_out when wen=1.  
   assign layer_rw=wen?layer_CPA:layer;    //can move this to previous pipeline stage for timing, if tool does not do retiming automatically
   
   dpram_rw1p_r1p #(FS_memory_bandwidth,FS_memory_depth,FS_memory_addr_wl)
                   fs_mem(layer_rw,DL,clk,FS_in,FS_old,FS_new_m,wen,FS_old_ren,FS_new_ren);
   
   
  always@(posedge clk) begin
    if(reset_n==1'b0) begin
     wen_1D <=1'b0;
     layer_CPA_1D  <= 0;
     rw_cond<=1'b0;
    end
    else begin
     wen_1D<=wen;
     layer_CPA_1D<=layer_CPA;
     if(wen)
      FS_new_reg <=FS_in;
      
     rw_cond<=(layer_CPA==DL) & wen;
         
    end
  end //end of always
  
   //take care of read of fs_new while write for fs_new 
   assign  FS_new = (rw_cond)?FS_new_reg: FS_new_m;                
   //then later..take care of directly connecting FS_in to FS_new
                   
   end
else if(`FS_MEM_TYPE==4)
  begin: FS_MEM_WCAHCE_OPT
  //move the code for FS_MEM_WCACHE_OPT HERE
  //FS_old is same as previus FS_out when wen=1.  
  assign layer_rw=wen?layer_CPA:layer;    //can move this to previous pipeline stage for timing, if tool does not do retiming automatically
  
   dpram_rw1p_r1p #(FS_memory_bandwidth,FS_memory_depth,FS_memory_addr_wl)
                    fs_mem(layer_rw,DL,clk,FS_in,FS_old,FS_new_m,wen,FS_old_ren,FS_new_ren);
    
  
  
 always@(posedge clk) begin
   if(reset_n==1'b0) begin
    wen_1D <=1'b0;
    layer_CPA_1D  <= 0;
    DL_1D<=0;
    rw_cond<=1'b0;
   end
   else begin
    wen_1D<=wen;
    layer_CPA_1D<=layer_CPA;
    DL_1D<=DL;
    if(wen)
     FS_new_reg <=FS_in;
   end
   rw_cond <=(layer_CPA==DL) & wen;
 end //end of always
  assign  rw_cond1= fs_new_rw_cond1;  
  //above signal from LUM_control for better timing into the pipelined registers using the eariler versions of layer_CPA and DL and wen signals.
  assign  FS_new=  rw_cond1?FS_in: 
                   rw_cond?FS_new_reg:
                            FS_new_m;
                  
  end


endgenerate 
  
                
endmodule


