  
/*
  File Name: shifter_mux2_1.v
  Module Name: shifter_mux2_1
  Kiran Gunnam
  Texas A&M University
  Module Attributes      : 2 to 1 multiplexer. Word length is given by shifter word length
  Design                 : A simple mutiplexer
  Parametrizable         : Yes
  Revision History       : v1.0
*/
module shifter_mux2_1(A,B,Y,S);
parameter shifter_wl = 1;

    input  [shifter_wl-1:0] A,B;
    output [shifter_wl-1:0] Y;
    input   S;

    assign Y= S?B:A;

endmodule



					