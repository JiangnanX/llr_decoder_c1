/*Kiran Gunnam 
  Texas A&M University
  File Name              : sii_module.v 
  Module Attributes      : Streaming Input Interface Unit 
  Design                 : Simple control logic
  Parametrizable         : Yes
  Functionality          : 
  This is the  Streaming Input Interface Unit for LDPC decoding architecture based on the on-the-fly
  computation paradigm.
  Note that this unit need to be changed based on the input bandwidth.
  Matlab Model           :	See the Block_LDPC_Decoder.m
  Revision History       : v1.0
*/

`include "../hdl_gen/LDPC_Decoder_Parameters.h"
module sii_module(clk,
                  reset_n,
                  LLR_ch,
                  new_frame,
                  H_col,
                  LLR_ch_wen,
                  LLR_ch_block,
                  LLR_ch_block_wen
                 );

//Inputs and Outputs
input                              clk;
input                              reset_n;
input  [`LLR_Ch_bandwidth-1   : 0] LLR_ch;
input                              new_frame;
input  [`MaxH_col_wl-1         :0] H_col;
input                              LLR_ch_wen;
output [`Qs_memory_bandwidth-1  :0]LLR_ch_block;
output                             LLR_ch_block_wen;


parameter number_of_words_per_stream_vector = `LLR_Ch_sub_streams ;
//Internal registers
//SII State machine variables
reg       SII_state;
parameter SII_IDLE             = 1'b0;
parameter SII_PROC             = 1'b1;

reg [`LLR_bandwidth-1  :0]        LLR_ch_block_temp;
wire [`Qs_memory_bandwidth-1  :0] LLR_ch_block;
reg                               LLR_ch_block_wen;
reg [`MaxH_col_wl-1         :0]   block_count;
reg [`LLR_Ch_sub_streams_wl-1:0]  word_count;
wire signed [`LLR_Ch_wl-1:0]      a[`Parallelization-1:0];                                                  
wire signed [`P_wl-1:0]           a_se[`Parallelization-1:0];  


always@ (posedge clk)
begin
  if (reset_n ==0)
   begin
     SII_state <= SII_IDLE;
     block_count <=0;
     word_count <= 0;
     LLR_ch_block_wen <= 1'b0;
   end
  else
  begin
  case (SII_state)
   SII_IDLE:begin
    if (new_frame)
     begin 
      SII_state <= SII_PROC;
      block_count <=0;
      word_count <= 0;
      LLR_ch_block_wen <= 1'b0;
     end
    else
     begin
       block_count <=0;
       word_count <= 0;
       LLR_ch_block_wen <= 1'b0;
     end
    end
   SII_PROC:
   begin
     if (new_frame)
      begin 
       SII_state <= SII_PROC;
       block_count <=0;
       word_count <= 0;
       LLR_ch_block_wen <= 1'b0;
      end
     else if(block_count>=(H_col) && (word_count==(number_of_words_per_stream_vector-1)) )//for continuous llr_ch_wen, should be H_col - 1;
      begin
       SII_state <= SII_IDLE;
       block_count <= 0;//for continuous llr_ch_wen, should delete this line;
      end
      
            
      if (LLR_ch_wen)
       begin
       LLR_ch_block_temp[(((word_count+1)*`LLR_Ch_bandwidth)-1)-:`LLR_Ch_bandwidth] <=LLR_ch;
        if (word_count==(number_of_words_per_stream_vector-1))
          begin 
            word_count <= 0;
            LLR_ch_block_wen <= 1;
          end
         else
          begin   
            word_count <= word_count + 1;
            LLR_ch_block_wen <= 0;
          end
       end
       else
       begin
         LLR_ch_block_wen <= 0;
       end
      
      if (LLR_ch_block_wen)
        begin
         if (block_count==(H_col)) //for continuous llr_ch_wen, should be H_col - 1;
           begin 
             block_count <= 0;
           end
           else 
             block_count <= block_count + 1;
        end
   end //end of SII_PROC:begin
   endcase
  end
 end  
       

genvar k;
 generate
 for (k=0; k < `Parallelization; k=k+1) 
     begin: vector_Sign_Extension
        //a_se is of wordlength `P_wl. a is of word length `LLR_Ch_wl.Since `P_wl >= `LLR_Ch_wl
        //this is a sign extension. Just make sure to declare a_se and a as the signed
        //with the word lengths `P_wl and `LLR_Ch_wl.
        //no sign extension on this line yet as both word lengths are equal
        assign a[k]            =  LLR_ch_block_temp[(k+1)*`LLR_Ch_wl-1:k*`LLR_Ch_wl];
//        // sign extension on this line
        assign a_se[k]         = a[k];  
//        //Now regroup them as a vector.
        assign LLR_ch_block[(k+1)*`P_wl-1:k*`P_wl]= a_se[k];     
    end
endgenerate

endmodule