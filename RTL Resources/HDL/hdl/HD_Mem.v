/*
  /---Confidentiality Notice Start--/
  Texas A&M University Confidential.
  Contact: Office of Technology Commercialization.
  The material contained in this software and the associated papers
  have features which are contained in a patent disclosure for LDPC decoding architectures 
  filed by Gwan Choi, John Junkins , Kiran Gunnam. 
  For more information about the licensing process, please contact:
  Alex Garcia
  Department: Licensing & Intellectual Property Management
  Title: Licensing Associate
  Email: alegarza@tamu.edu
  Phone: (979) 458-2640
  Fax: (979) 845-2684
   � Copyright 2005 to the present, Texas A&M University System. 
  All the software contained in these files are protected by United States copyright law
  and may not be reproduced, distributed, transmitted, displayed, published or broadcast 
  without the prior written permission of Texas A&M University System. 
  You may not alter or remove any trademark, copyright or other notice from copies 
  of the content.
  /---Confidentiality Notice End--/

  Kiran Gunnam 
  Texas A&M University
  File Name              : HD_Mem.v 
  Module Attributes      : Memory module for storing shifted versions of hard 
                           decisions
  Design                 : 
  Parametrizable         : Yes
  Functionality          : Serves as hard decision memory for the decoder and 
  stores the shifted version of hard decisions. 
  Double buffering is used. Based on the application requirements 
  such as to cater average throughput case and low energy throttling, more buffering
  can be added.
  Matlab Model           :	See the Block_LDPC_Decoder.m 
  Revision History       : v1.0 
  
*/

`include "../hdl_gen/LDPC_Decoder_Parameters.h"

module hd_mem(rd_addr1,rd_addr2,wr_addr,clk,d_in,shift_in,d_out1,d_out2,
              shift_out1,shift_out2,wen);

input  [`HD_memory_addr_wl-1 : 0] rd_addr1,rd_addr2,wr_addr;
input                             clk;
input  [`HD_blocksize-1 : 0]      d_in;
input  [`Shift_wl-1:0]            shift_in;
output [`HD_blocksize-1 : 0]      d_out1,d_out2;
output [`Shift_wl-1:0]            shift_out1,shift_out2;
input  [`Number_HD_Buffers-1:0]   wen;

parameter HD_memory_depth    =  `HD_memory_depth;
parameter HD_memory_addr_wl  =  `HD_memory_addr_wl;
parameter HD_memory_bandwidth =  `HD_memory_bandwidth;

wire  [`HD_memory_bandwidth-1:0] dina,douta,doutb;


assign {shift_out1,d_out1}        =  douta;
assign {shift_out2,d_out2}        =  doutb;
assign {dina}                     =  {shift_in,d_in};


//            #(width,depth,ceil(log2(depth)))
dpram_r1p_w1p #(HD_memory_bandwidth,HD_memory_depth,HD_memory_addr_wl)
               hd_mem1(wr_addr,rd_addr1,clk,dina,douta,wen[0],1'b1);
dpram_r1p_w1p #(HD_memory_bandwidth,HD_memory_depth,HD_memory_addr_wl)
               hd_mem2(wr_addr,rd_addr2,clk,dina,doutb,wen[1],1'b1);


endmodule


