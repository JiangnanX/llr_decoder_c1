 //Kiran Gunnam  
 //Dual port RAM. 2 port Read. 1 port write. Based on Xilinx template.
 //dout1 is same as previous dout1 when write_enable=1.  
 module dpram_rw1p_r1p(wr_address,read_address2,clock,din,dout1,dout2,write_enable,read_enable1, read_enable2);//synthesis attribute keep_hierarchy of dpram_r2p_w1p is yes
  parameter RAM_WIDTH = 36, RAM_DEPTH = 512, RAM_ADDR_BITS = 9; 
    
   input  [RAM_ADDR_BITS-1:0] wr_address,read_address2;
   input  clock;
   input  [RAM_WIDTH-1:0] din;
   output [RAM_WIDTH-1:0] dout1,dout2;   
   input  write_enable;
   input  read_enable1;
   input  read_enable2;
   
   

   reg   [RAM_WIDTH-1:0] dpram [RAM_DEPTH-1:0];
   reg   [RAM_WIDTH-1:0] dout1,dout2;   
   
   always @(posedge clock) begin
        //read first mode for port1
        if (write_enable) begin
		   dpram[wr_address] <= din;
         //  dout1 <= din;
        end
        else if(read_enable1)
           dout1 <= dpram[wr_address];         
       //write first mode for port2
       if(read_enable2)
       dout2 <= dpram[read_address2];
   end
   
   
endmodule