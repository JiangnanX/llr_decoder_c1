/*
  /---Confidentiality Notice Start--/
  This document contains information proprietary to Texas A&M University, College Station.
  Any disclosure or use is expressly prohibited, except upon explicit
  written permission by Texas A&M University.
  Texas A&M University Confidential.
  Contact: Office of Technology Commercialization.
  The material contained in this software and the associated papers
  have features which are contained in a patent disclosure for LDPC decoding architectures 
  filed by Gwan Choi, John Junkins , Kiran Gunnam. 
  For more information about the licensing process, please contact:
  Alex Garcia
  Department: Licensing & Intellectual Property Management
  Title: Licensing Associate
  Email: alegarza@tamu.edu
  Phone: (979) 458-2640
  Fax: (979) 845-2684
   � Copyright 2005 to the present, Texas A&M University System. 
  All the software contained in these files are protected by United States copyright law
  and may not be reproduced, distributed, transmitted, displayed, published or broadcast 
  without the prior written permission of Texas A&M University System. 
  You may not alter or remove any trademark, copyright or other notice from copies 
  of the content.
  /---Confidentiality Notice End--/


  Kiran Gunnam 
  Texas A&M University 
  File Name              : CNU_PS.v
  Module Attributes      : Datapath 
  Design                 : Data path, Scalar processing
  Parametrizable         : Yes 
  Functionality          : 
  Does the partial state processing on Q messages. This is based on 
  min-sum processing.
  Matlab Model           :	CNU_PS_PS.m, Data path, Vector processing
  Revision History       : v1.0
*/

`include "../hdl_gen/LDPC_Decoder_Parameters.h"

module CNU_PS(clk,valid_entry,Q,block_num,PS);

//Inputs and Outputs
input                               clk;
input                               valid_entry;
input  [`Q_wl-1:0]                  Q;
input  [`dc_wl-1:0]                 block_num;
output [`PS_wl-1:0]                 PS;


parameter PS_wl = `PS_wl;
parameter M_wl =  `M_wl;


//Internal signals based on the inputs

wire                             Q_sign;
wire [`M_wl-1               :0]  Q_mag;



//Internal micro-architecture state , PS, partial state
reg   [`M_wl-1:0]             M1,M2;
reg   [`dc_wl-1:0]            M1_index;
reg                           Q_cum_sign;




//Combine the information entities into a single entity
assign PS[PS_wl-1:PS_wl-M_wl]                        = M1;
assign PS[(PS_wl-M_wl-1):((PS_wl)-2*(M_wl))]         = M2;
assign PS[(PS_wl-2*(M_wl)-1):1]                      = M1_index;
assign PS[0]                                         = Q_cum_sign;

//Separate the inputs into bit fields
assign Q_sign          = Q[`Q_wl-1];
assign Q_mag           = Q[`Q_wl-2:0];

always @(posedge clk)
begin
    if (valid_entry)
	  begin
		  if(block_num == 0) //assumption that the block_num=0 is in TimeSlot_Counter=0 and 
		                     //has valid_entry=1
		    begin
				 M1            <= Q_mag;
				 M2            <= `M_Inf;
				 M1_index      <= 0;
				 Q_cum_sign    <= Q_sign;
                 
		    end
		  else
		  begin
           if(Q_mag<M1)
			    begin
				  M2 <= M1;
				  M1 <= Q_mag;
				  M1_index <= block_num;
				 end
				else if(Q_mag<M2)
			   M2 <= Q_mag;
				 
			   Q_cum_sign    <= Q_cum_sign ^ Q_sign;
	     end
	end // end of if (valid_entry) block
end //end of always process

endmodule


