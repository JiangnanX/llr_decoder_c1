/*
  /---Confidentiality Notice Start--/
  Texas A&M University Confidential.
  Contact: Office of Technology Commercialization.
  The material contained in this software and the associated papers
  have features which are contained in a patent disclosure for LDPC decoding architectures 
  filed by Gwan Choi, John Junkins , Kiran Gunnam. 
  For more information about the licensing process, please contact:
  Alex Garcia
  Department: Licensing & Intellectual Property Management
  Title: Licensing Associate
  Email: alegarza@tamu.edu
  Phone: (979) 458-2640
  Fax: (979) 845-2684
  /---Confidentiality Notice End--/

  Kiran Gunnam 
  Texas A&M University
  File Name              : Circulant_Scaling.v
  Module Attributes      : Datapath 
  Design                 : Data path, Scalar processing
  Parametrizable         : Yes
  Functionality          : Implements Ciruclant scaling that works for
                           correction method 3 and correction method 6.
  Notes                  : 
  Correction methods for LDPC codes
  
  Method 1:OMS/NMS For regular QC-LDPC codes, it is sufficent to apply the correction for R values 
            or Q values. See Jinghu Chen's paper on offset min-sum
  
  Method 2: 2-D OMS/2-D NMS: For irregular QC-LDPC codes, The normal practice is to apply the correction for
  R messages and Q messages in two steps. We can use either offset or scaling method. See
  the Fossorier's paper on 2-D normalization.

  Method 3: 2-D NMS-gamma: Does the scaling operation to reduce the over-estimated reliabity values for the 
  irregular LDPC codes.
  The scaling factor cirualant_gamma is a mutiple of R scaling factor alpha and Q scaling factor 
  gamma for each circulant. Each block row has a different alpha. Each block column has a 
  different beta. See the Fossirier's paper on 2-D normalization to obtain the scaling coefficents alpha 
  and beta. So each circulant has a different scaling factor gamma- and this scaling 
  is applied on Q messages except when they the Q messsages directly come from the channel LLR 
  values. Please see the document for transformation of the dataflow graph.
  See the file Circulant_Scaling.v
    
  Method 4: 2-D NMS-gamma_offset:This is exactly similar to Method 3. However a correction factor gamma_offset 
  that is  derived from gamma(or in a different manner either based on density evolution or experimental trials)
  can be applied as an offset for the Q messages instead of a scaling factor. However for this method,
  the quantization need to be of uniform quantization with step size the integer mutiples of all different
  offsets. See the file Circulant_correction_offset.v

  Method 5: NMS value-reuse/OMS value-resue: For regular QC-LDPC codes, if we choose to do the correction on the output of check node processing
  (R messages), the scaling/offset correction needed to be done for only two values(Min1, Min2 ). So for the
  case of  regualar QC-LDPC, this is taken care in the CNU processing labelled as FS(Final State) processing.
  
  Method 6:  1-D NMS-gamma, BN irregular: For check node regular and bit-node irregular QC-LDPC codes,it is sufficent
  to apply the correction for R values based on the block column. Since we need to do scaling on R_old and R_new,
  it is easier to apply the algorithm transformation such that we can apply the scaling on Q messages.
  So each block column has a different scaling factor gamma- and this scaling is applied on Q messages 
  except when they the Q messsages directly come from the channel LLR values(i.e. for some circulants in 
  the first iteration) . Please see the document for transformation of the dataflow graph. This is 
  essentially similar to Method 3 in terms of dataflow graph except that gamma values are directly given by
  beta values instead of alpha * beta. 

  Method 7:  1-D NMS-gamma_offset,BN irregular: For check node regular and bit-node irregular QC-LDPC codes,it is 
  sufficent to apply the  correction for R values (as an offset correction) based on the block column. Similar to 
  method 6 except that the gamma_offset is used as the offset correction in stead of using gamma as the scaling 
  factor. In the implementaiton, method 7 and method 4 are similar except for the way to calculate the gamma_offset
  parameters.

  Method 8: NMS-alpha,CN irregular: For check node irregular and bit-node regular QC-LDPC codes,it is sufficent to apply the 
  correction for R values/Q values depending on the block row(i.e check node profile). This correction is scaling factor alpha. 
  For this kind of check node irregular QC-LDPC codes,if we choose to do the correction on the output of check node processing 
  (R messages), the scaling correction needed to be done for only two values(Min1, Min2 ). So for this case,this is taken care
  in the CNU processing labelled as FS(Final State) processing. In the implemenation, method 8 is similar to method 5 except
  the correction factor varies based on the block row.
 
  Method 9: NMS-alpha_offset,CN irregular: For check node irregular and bit-node regular QC-LDPC codes,it is sufficent
  to apply the correction(offset correction) for R values/Q values depending on the block row(i.e check node profile). 
  This correction is  offset based on alpha. For this kind of check node irregular QC-LDPC codes,if we choose to do
  the correction on the output of check node processing (R messages), the offset correction needed to be done for
  only two values(Min1, Min2 ). So for this case,this is taken care in the CNU processing labelled as FS(Final State)
  processing. In the implemenation, method 9 is similar to method 5 except the correction factor varies based on
  the block row.
  
  
  Novelty and Advantages            :
  Methods 1 and 2 are the standard ones.
  Methods 3,4,6,7,8 and 9 are novel. They are best suited for irregular LDPC codes.
  Method  5 is novel. This is best suited for regular LDPC codes.

  Advantages:

  Methods 3,4, 6 and 7 are similar in data flow graph. The correction needs to be applied on only one 
  type of messages as the algorithm transformation makes two-step one-time 2-D correction as one-step 
  one-time 2-D correction. In fact, the main advantage is in letting the use of compressed messages (Min1 and Min2)
  for R messages without any correction as the correction is done on Q messages.These Q messages 
  are computed on-the-fly based on R_old and R_new. Had the correction needed to be done on the R messages,
  then the correction should have been applied two times, one for the R_old and 
  another time for R_new messages.

  Methods 3 and 6 are similar in data flow graph and in implementaiton using a new scaling method based on gamma.
  Methods 4 and 7 are similar in data flow graph and in implementaiton using an offset correction based on gamma.
  Methods 1, 8 and 9 are similar in data flow graph and in implementaion using the FS processing to apply the 
  correction. The correction can be scaling or offset and needs to be applied only on 2 values.
  

  Revision History       : v1.0
*/

`include "../hdl_gen/LDPC_Decoder_Parameters.h"

module Circulant_Scaling(clk,Q,gamma,Q_Scaled);

parameter Q_BitWidth                     = `Qs_wl;
parameter Q_Scaled_BitWidth              = `Q_wl;
parameter Qs_Min                         = `Qs_Min;
parameter Qs_Max                         = `Qs_Max;



//Inputs and Outputs
input                                      clk;
input  signed [Q_BitWidth-1:0]             Q;
input         [`GammaV_wl-1:0]             gamma;
output        [Q_Scaled_BitWidth-1:0]      Q_Scaled;

reg          [Q_Scaled_BitWidth-1:0]      Q_Scaled;
reg	       [Q_BitWidth-1:0]             Q_S;
wire         [Q_BitWidth-1:0]             Qmag;//Qmag is signed mag representation of Q
wire         [Q_BitWidth+`GammaV_wl-2:0]  Qmag_Scaled;
wire         [Q_BitWidth-2:0]  		      Qmag_Scaled_and_Truncated;
wire         [Q_Scaled_BitWidth-2:0]      Qmag_SRS;//Scaled and Rounded 
                                            //and Saturated;
reg         [Q_BitWidth+`GammaV_wl-2:0]  Qmag_Scaled_temp1 /* synthesis syn_dspstyle = "logic" */;
wire         [Q_BitWidth-1:0]             temp2;
wire         [Q_BitWidth+`GammaV_wl-2:0]  Qmag_Scaled_temp0 /* synthesis syn_dspstyle = "logic" */;
reg          Q_sign_Scaled;
    

//Do the 2's complement ==> to signed number conversion
always @(Q)
 begin
  if(Q[Q_BitWidth-1])
	  Q_S = -Q;
  else
	  Q_S = Q;
 end

assign temp2                    = Q;
//Note that Qs_Min is a Negative Weird Number. So while converting from 2's complement
//to signed magnitude form, special care should be taken.
//For instance for a 6 bit 2scomplement number QsMin is -32 and Qs_Max is 31.
//If 2's complement is taken on -32, then it will be -32. This happens due to overflow.
//So we will saturate the result to Qs_Max i.e. 31.
assign Qmag                     = (temp2== Qs_Min)? Qs_Max:{1'b0,Q_S[Q_BitWidth-2:0]};

//multiplier_4b_7b multiplier_4b_7b(clk,Qmag[Q_BitWidth-2:0],gamma,Qmag_Scaled_temp1);

assign Qmag_Scaled_temp0 = Qmag[Q_BitWidth-2:0]* gamma;

generate
 if(`CORRECTION_UNIT_PIPELINE==2)
 begin: CU_PD2
  always@(posedge clk)
   begin
    Qmag_Scaled_temp1 <= 	Qmag_Scaled_temp0;
    Q_sign_Scaled     <=   Q[Q_BitWidth-1];
    Q_Scaled          <= {Q_sign_Scaled,Qmag_SRS};
   end
  end
  else
  begin: CU_PD1
  always@(posedge clk)
     begin
      Q_Scaled          <= {Q[Q_BitWidth-1],Qmag_SRS};
     end
  end
endgenerate


generate
  if(`CORRECTION_UNIT_PIPELINE==2)
    begin: CU_PD2_assign
     assign Qmag_Scaled = Qmag_Scaled_temp1;
     end
   else
    begin: CU_PD1_assign
    assign Qmag_Scaled = Qmag_Scaled_temp0;
    end
endgenerate 

//Note that MSB of Qmag_Scaled is always zero due to the restriction on Qmag range.
//Do the rounding on the last bits of Qmag.
//assign Qmag_Scaled_and_Truncated = Qmag_Scaled[Q_BitWidth+`GammaV_wl-2:Q_BitWidth+`GammaV_wl-Q_Scaled_BitWidth];
//assign Qmag_Scaled_and_Rounded   = Qmag_Scaled_and_Truncated+Qmag_Scaled[Q_BitWidth+`GammaV_wl-Q_Scaled_BitWidth-1];
//assign Q_Scaled                 = {Q_sign_Scaled,Qmag_Scaled_and_Rounded};
//

assign Qmag_Scaled_and_Truncated           = Qmag_Scaled[Q_BitWidth+`GammaV_wl-2:`GammaV_wl];
satu_adder_v2_ccoa  #(Q_Scaled_BitWidth-1,Q_BitWidth-1,1) 
                      sa_unsigned
				          (Qmag_SRS,Qmag_Scaled_and_Truncated,Qmag_Scaled[`GammaV_wl-1]);



/* Note that 2's complement to SM conversion logic needs to be added, if the following code
needs to be used. However note that the above code of using a 5x4 bit integer * fraction 
multiplier is more efficient. So the following code is not used. 
wire [Q_BitWidth-1:1]  Q_dividedBy2;
wire [Q_BitWidth-1:2]  Q_dividedBy4;
wire [Q_BitWidth-1:3]  Q_dividedBy8;
wire [Q_BitWidth-1:4]  Q_dividedBy16;

reg  [Q_BitWidth-1:0]         Q_2_4;
reg  [Q_BitWidth-1:2]         Q_8_16;


wire [Q_BitWidth-1:0]    Q_2_4_8_16;

//Internal signals derived from the inputs
assign Q_dividedBy2  = Q[Q_BitWidth-1:1]; //=R/2
assign Q_dividedBy4  = Q[Q_BitWidth-1:2]; //=R/4
assign Q_dividedBy8  = Q[Q_BitWidth-1:3]; //=R/8
assign Q_dividedBy16 = Q[Q_BitWidth-1:4]; //=R/16



//Do the R scaling operation using a tree adder
always @(Q_dividedBy8,Q_dividedBy16,gamma[0],gamma[1])
begin
 case(gamma[1:0])
   2'b00 : Q_8_16   <= 0;
   2'b01 : Q_8_16   <= Q_dividedBy16;
	2'b10 : Q_8_16   <= Q_dividedBy8;
	2'b11 : Q_8_16   <= Q_dividedBy8+Q_dividedBy16;
 endcase
end

always @(Q_dividedBy4,Q_dividedBy2,gamma[3],gamma[2])
begin
 case(gamma[3:2])
   2'b00 : Q_2_4   <= 0;
   2'b01 : Q_2_4   <= Q_dividedBy4;
	2'b10 : Q_2_4   <= Q_dividedBy2;
	2'b11 : Q_2_4   <= Q_dividedBy2+Q_dividedBy4;
 endcase
end

assign Q_2_4_8_16 = Q_8_16 + Q_2_4;
assign Q_Scaled = (gamma == 4'b0000)? Q[Q_BitWidth-1:1]:Q_2_4_8_16[Q_BitWidth-1:1]; 
//if vector representation for gamma is all zeros, then take the scaling as one.
//note that Q_scaled ignores the LSB of the result.
*/


endmodule



