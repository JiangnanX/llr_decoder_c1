 //Kiran Gunnam  
 //Dual port RAM. 2 port Read. 1 port write. Based on Xilinx template.
 module dpram_r2p_w1p(wr_address,read_address1,read_address2,clock,din,dout1,dout2,write_enable);//synthesis attribute keep_hierarchy of dpram_r2p_w1p is yes
  parameter RAM_WIDTH = 36, RAM_DEPTH = 512, RAM_ADDR_BITS = 9; 
    
   input  [RAM_ADDR_BITS-1:0] wr_address,read_address1,read_address2;
   input  clock;
   input  [RAM_WIDTH-1:0] din;
   output [RAM_WIDTH-1:0] dout1,dout2;   
   input  write_enable;
   

   reg   [RAM_WIDTH-1:0] dpram [RAM_DEPTH-1:0];
   reg   [RAM_WIDTH-1:0] dout1,dout2;   
   //write first mode
   always @(posedge clock) begin
        if (write_enable) 
		   dpram[wr_address] <= din;
       dout1 <= dpram[read_address1];         
       dout2 <= dpram[read_address2];
   end
   
   
endmodule