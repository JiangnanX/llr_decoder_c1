/*
  /---Confidentiality Notice Start--/
  This document contains information proprietary to Texas A&M University, College Station.
  Any disclosure or use is expressly prohibited, except upon explicit
  written permission by Texas A&M University.
  (C) Copyright 2007
  Texas A&M University Confidential.
  Contact: Office of Technology Commercialization.
  The material contained in this software and the associated papers
  have features which are contained in a patent disclosure for LDPC decoding architectures 
  filed by Gwan Choi, John Junkins , Kiran Gunnam. 
  For more information about the licensing process, please contact:
  Alex Garcia
  Department: Licensing & Intellectual Property Management
  Title: Licensing Associate
  Email: alegarza@tamu.edu
  Phone: (979) 458-2640
  Fax: (979) 845-2684
   � Copyright 2005 to the present, Texas A&M University System. 
  All the software contained in these files are protected by United States copyright law
  and may not be reproduced, distributed, transmitted, displayed, published or broadcast 
  without the prior written permission of Texas A&M University System. 
  You may not alter or remove any trademark, copyright or other notice from copies 
  of the content.
  /---Confidentiality Notice End--/
  Kiran Gunnam
  Texas A&M University 
  File Name              : Convergence_Check.v
  Module Attributes      : Datapath 
  Design                 : Data path, Vector processing
  Parametrizable         : Yes
  Functionality          : Checks the convergence of the decoding process and
  signals to stop the stop the iterations as soon as the convergence reached or 
  when the iterations are equal to the maximum number of iterations.		 
  Matlab Model           : Block_LDPC_Decoder.m	 Data path, Vector processing
  Revision History       :  
  
 v1.1:  Added the accurate reset control on layer_cht. 
 v1.0:  Kiran Gunnam,Texas A&M University, December 2005 
*/
`include "../hdl_gen/LDPC_Decoder_Parameters.h"

module Convergence_Check(
                         clk,
					     reset_n,
					     H_row,
					     dc_in,
					     ts_max_in,
					     valid_entry_in,
                         d_block_s1_in,
					     d_block_s2_in,
                         d_block_s2_to_CC_in,
					     layer_in,
                         DL_in,
                         UCVF_in,
					     iterations_in,
                         iterations_PCU,
					     max_iterations,
                         Disable_CC_logic,
					     block_num_in,			
                         iterations_done, 
					     decoding_error
					 ); 
                
input clk,reset_n;
input [`MaxH_row_wl-1         :0] H_row;
input [`dc_wl-1               :0] dc_in;
input [`TimeSlotCounter_wl-1 : 0] ts_max_in;
input                             valid_entry_in;
input [`HD_blocksize-1       : 0] d_block_s1_in,d_block_s2_in, d_block_s2_to_CC_in;
input [`dc_wl-1              : 0] block_num_in;
input [`H_row_wl-1           : 0] layer_in,DL_in;			
input                             UCVF_in;
input [`iteration_wl-1       : 0] iterations_in,iterations_PCU;
input [`iteration_wl-1       : 0] max_iterations;
input                             Disable_CC_logic;//==1, enables the convergence check logic
output                            iterations_done, decoding_error;


//input pipeline registers
reg [`dc_wl-1              : 0] dc;
reg [`TimeSlotCounter_wl-1 : 0] ts_max;
reg [`HD_blocksize-1       : 0] d_block_s1,d_block_s2, d_block_s2_to_CC;
reg [`HD_blocksize-1       : 0] d_block_s2_to_CC_tmp;
reg [`dc_wl-1              : 0] block_num;
reg [`H_row_wl-1           : 0] layer,DL;			
reg                             UCVF;
reg [`iteration_wl-1       : 0] iterations;


//internal registers for re-alignment of control signals
reg UCVF_1D,UCVF_2D,UCVF_3D,UCVF_4D,UCVF_5D;
reg  [`dc_wl-1              : 0] block_num_1D, block_num_2D, block_num_3D, 
                                 block_num_4D,block_num_5D,block_num_6D,
											block_num_7D;
reg                              valid_entry,valid_entry_1D,valid_entry_2D,valid_entry_3D,valid_entry_4D,
								 valid_entry_5D,valid_entry_6D,valid_entry_7D;			
reg [`dc_wl-1               : 0] dc_1D,dc_2D,dc_3D,dc_4D,dc_5D,dc_6D,dc_7D;
reg [`TimeSlotCounter_wl-1 : 0 ] ts_max_1D,ts_max_2D,ts_max_3D,ts_max_4D,ts_max_5D,ts_max_6D,ts_max_7D; 
reg [`H_row_wl-1            : 0] layer_1D,layer_2D,layer_3D,layer_4D,layer_5D,layer_6D,layer_7D;
reg [`iteration_wl-1        : 0] iterations_1D,iterations_2D,iterations_3D,
                                 iterations_4D,iterations_5D,iterations_6D,
											iterations_7D;
//internal wires and registers for layer_cht logic
wire signed [`H_row_wl     : 0] layer_signed,DL_signed; 
reg  signed [`H_row_wl     : 0] layer_cht_spec1_pipeline_stage1,layer_cht_spec1_pipeline_stage2;
reg         [`H_row_wl-1   : 0] layer_cht_spec1_pipeline_stage3,layer_cht_spec1_pipeline_stage4,
                                layer_cht_spec1_pipeline_stage5,
                                layer_cht_spec1_pipeline_stage5_prev;
reg         [`H_row_wl-1   : 0] layer_cht_1D;


reg  [`H_row_wl-1            : 0] layer_cht;			
reg  [`HD_blocksize-1        : 0] sign_flip_violation_vector; 
reg  [`Num_cc_sign_flip_or-1 : 0] sign_flip_violation_block;
                                  

//reg                               sign_flip_violation_block1;
reg                               sign_flip_violation;                                  
reg                               sign_flip_violation_layer;   
reg                               sign_flip_violation_layer_prev;                               
//internal registers for cht logic
reg  [`HD_blocksize-1        : 0] cht_accumulator;
reg                               cht_fail_flag_layer_int;
reg [`MaxH_row-1              : 0]  cht_fail_flag_layer;
reg                               cht_fail_flag_frame;
reg                               cht_fail_flag_frame_int;
reg                               iterations_done,decoding_error;

genvar                            k;

//First set the inputs into the input pipeline registers. Since there are 5 pipeline stages internally, the latency of
//this module is 6 clock cycles.
always @(posedge clk) 
begin
if(reset_n == 0) 
    valid_entry <= 1'b0;
 else
  begin
     valid_entry      <= valid_entry_in;
     dc               <= dc_in;
     ts_max           <= ts_max_in;
     d_block_s1       <= d_block_s1_in;
     d_block_s2       <= d_block_s2_in;
     block_num  <= block_num_in;
     layer            <= layer_in;
     DL               <= DL_in;
     UCVF             <= UCVF_in;
     iterations       <= iterations_in;
  end
end

generate
if(`P_SHIFTER_PIPELINE==1) 
 begin: PS_PD1
    always @(posedge clk) 
    begin
    d_block_s2_to_CC <= d_block_s2_to_CC_in;
    end
 end
else
 begin
    always @(posedge clk) //this is to align the hard decisions with rest of the CC logic so that same logic in CC that is used for 
                          //P_SHIFTER_PIPELINE 1 can be reused for and P_SHIFTER_PIPELINE 0. 
                           //if the rest of the control logic in convergence check in LUM_control are modified for P_SHIFTER_PIPELINE=0, 
    begin                 //one more cycle in convergence logic as well number of flops equal to parallelization can be saved. to do optimization.  
    d_block_s2_to_CC_tmp <= d_block_s2_to_CC_in;
    d_block_s2_to_CC <= d_block_s2_to_CC_tmp;
    end
 end
endgenerate

assign layer_signed = {1'b0,layer};
assign DL_signed    = {1'b0,DL};

//Now do cht computation
always @(posedge clk)
 begin
  if(reset_n == 0) begin
  	 layer_cht             <= 0;
     layer_cht_spec1_pipeline_stage5 <=0;
     cht_accumulator       <= 0;
	 /*Note that some of the bits of this register may be never used 
	 in operation. This happens when the supported code has less
	 number of layers than the MaxH_row. However this statement is
	 necessary as we want to use the condition
	 cht_fail_flag_layer==0 in run time to determine convergence*/
	 cht_fail_flag_layer   <= 0;

	 cht_fail_flag_frame   <= 0;
	 decoding_error        <= 0;
	 iterations_done       <= 1'b0;
    
    
     valid_entry_1D <= 1'b0;
     valid_entry_2D <= 1'b0;
     valid_entry_3D <= 1'b0;
     valid_entry_4D <= 1'b0;
     valid_entry_5D <= 1'b0;
     valid_entry_6D <= 1'b0;
     valid_entry_7D <= 1'b0;
	end
	else
	 begin
	 
     /**First do re-alignment of control signals for different pipeline stages**/
     UCVF_1D            <= UCVF;        
     UCVF_2D            <= UCVF_1D;
     UCVF_3D            <= UCVF_2D;
     UCVF_4D            <= UCVF_3D;
     UCVF_5D            <= UCVF_4D;
    
     block_num_1D <= block_num;
     block_num_2D <= block_num_1D;
     block_num_3D <= block_num_2D;
     block_num_4D <= block_num_3D;
     block_num_5D <= block_num_4D;
     block_num_6D <= block_num_5D;
	 block_num_7D <= block_num_6D;
	 
	 valid_entry_1D <= valid_entry;
     valid_entry_2D <= valid_entry_1D;
     valid_entry_3D <= valid_entry_2D;
     valid_entry_4D <= valid_entry_3D;
     valid_entry_5D <= valid_entry_4D;
     valid_entry_6D <= valid_entry_5D;
     valid_entry_7D <= valid_entry_6D;
     
 
     layer_cht_1D       <= layer_cht;
   
     dc_1D              <= dc;
     dc_2D              <= dc_1D;
     dc_3D              <= dc_2D;
     dc_4D              <= dc_3D;
     dc_5D              <= dc_4D;
     dc_6D              <= dc_5D;
	 dc_7D              <= dc_6D;
	 
	 ts_max_1D              <= ts_max;
     ts_max_2D              <= ts_max_1D;
     ts_max_3D              <= ts_max_2D;
     ts_max_4D              <= ts_max_3D;
     ts_max_5D              <= ts_max_4D;
     ts_max_6D              <= ts_max_5D;
     ts_max_7D              <= ts_max_6D;
     
     layer_1D           <= layer;
     layer_2D           <= layer_1D;
     layer_3D           <= layer_2D;
     layer_4D           <= layer_3D;
     layer_5D           <= layer_4D;
     layer_6D           <= layer_5D;
	 layer_7D           <= layer_6D;
     
     iterations_1D      <= iterations;
     iterations_2D      <= iterations_1D; 
     iterations_3D      <= iterations_2D;
     iterations_4D      <= iterations_3D;
     iterations_5D      <= iterations_4D;
     iterations_6D      <= iterations_5D;
	 iterations_7D      <= iterations_6D;
       
	 
     /**********************************************************************************************************/
       /*********************************First Parallel Computation. layer_cht logic*************************/
	   //Parallel compuataion:Vector XOR bit-wise comparator and OR gate for sign flip check.
	    // pipeline stage 1
         //do this sign check only when UCVF =0
         //pipelined path of sign_flip_violation. 3 clock cycle delay. pipelined path is introduced to OR gate 
         //(which is used to calculate >0)in sign_flip_violation_int 
         if(UCVF==1'b0 & valid_entry)
		    sign_flip_violation_vector         <= (d_block_s1 ^d_block_s2);
            
        // pipeline stage 2(disable the parallel tree incase low fan in.)
        //note that `Num_cc_sign_flip_or*Max_FanIn_of_convergence_logic should be equal to circulant size.
        //sign_flip_violation_block[`Num_cc_sign_flip_or-2:0] are computed in for-generate at the end of the file 
        sign_flip_violation_block[`Num_cc_sign_flip_or-1]    <= 
                               |(sign_flip_violation_vector[`HD_blocksize-1: (`Num_cc_sign_flip_or-1)*`Max_FanIn_of_convergence_logic]);
          //collect remaiing bits of sign_flip_violation_vector here and do the OR gate.
          //note that |(unsigned_number) is same as (unsigned_number)>0
          //also note that msb or lsb order does not matter for this OR logic.                

         // pipeline stage 3.
         sign_flip_violation                   <= |(sign_flip_violation_block);
//                                             
        //Exact value of layer_cht is the number of "good" rows that don't have the sign violation
        
        //Speculative computation 1 for new value of layer_cht. This value will be committed to layer_cht
        //only when sign_flip_violation is 1. Note that this speculative computation also takes 3 clock
        //cycle delay
         layer_cht_spec1_pipeline_stage1   <= layer_signed-DL_signed-1; //make sure to declare layer_cht_spec_pipeline_stage1
                                                                     //as signed
         if(layer_cht_spec1_pipeline_stage1<0)
           layer_cht_spec1_pipeline_stage2 <= layer_cht_spec1_pipeline_stage1+H_row;
         else//copy the value from the previous pipeline stage
           layer_cht_spec1_pipeline_stage2 <= layer_cht_spec1_pipeline_stage1;         
          
           layer_cht_spec1_pipeline_stage3 <= layer_cht_spec1_pipeline_stage2[`H_row_wl-1: 0];
           layer_cht_spec1_pipeline_stage4 <= layer_cht_spec1_pipeline_stage3;
         // take the minimum number of good rows for all the blocks. this is done in a serial fashion , one block at a time.
         //if(block_num_4D==0)
         if(block_num_4D==0 & valid_entry_4D)
          begin
          layer_cht_spec1_pipeline_stage5_prev <= layer_cht_spec1_pipeline_stage5;
          if(sign_flip_violation)
           layer_cht_spec1_pipeline_stage5 <= layer_cht_spec1_pipeline_stage3; //3 instead of 4 for now.
           else
           //layer_cht_spec1_pipeline_stage5 <= {`H_row_wl{1'b1}};
             layer_cht_spec1_pipeline_stage5 <= H_row-1;//be careful with signed operations here
          end 
         // else if(block_num_4D<=dc_4D & sign_flip_violation)
         else if(block_num_4D<=dc_4D & valid_entry_4D & sign_flip_violation)
         layer_cht_spec1_pipeline_stage5 <= (layer_cht_spec1_pipeline_stage3< layer_cht_spec1_pipeline_stage5)?
                                                     layer_cht_spec1_pipeline_stage3:layer_cht_spec1_pipeline_stage5;
        

          
        // pipeline stage 4. use the result of sign_flip_violation,layer_cht_spec1_pipeline_stage3 and
        //layer_cht_spec2_pipeline_stage3 for the 4th  pipeline stage
        //Now is the time to commmit the speculative value of layer_cht calculated in pipe line stage 3 to layer_cht if
        //there is a sign flip violation for that block and that block is not using the channel value for the first time.
        //Make sure to align UCVF by 3 clock cycles. 
         //if(block_num_4D==0)
         if(block_num_4D==0 & valid_entry_4D)
          begin
           sign_flip_violation_layer      <= sign_flip_violation;
           sign_flip_violation_layer_prev <= sign_flip_violation_layer;
          end
         //else if(block_num_4D<=dc_4D)
         else if(block_num_4D<=dc_4D & valid_entry_4D)
           sign_flip_violation_layer <= sign_flip_violation | sign_flip_violation_layer;
           
           
           
        // if(sign_flip_violation_layer_prev & UCVF_5D==1'b0 & block_num_5D==0)
         if(sign_flip_violation_layer_prev & UCVF_5D==1'b0 & block_num_5D==0 & valid_entry_5D)
            layer_cht <= (layer_cht_spec1_pipeline_stage5_prev<layer_cht)?
                                layer_cht_spec1_pipeline_stage5_prev:layer_cht; 
         // else if(layer_cht<H_row-1& block_num_5D==dc_5D-1)//ts_max_5D)//CHG
          else if(layer_cht<H_row-1& block_num_5D==dc_5D-1 & valid_entry_5D)
            layer_cht <= layer_cht+1;          
            
            
           
   /*********************************END OF First Parallel Computation. layer_cht logic*************************/      
   /**********************************************************************************************************/      
	/*Note that the pipe lines for first parallel computation and second parallel computation occur at different times
      though they may overlap. 
       1)pipeline stages 2,3 and 4 in Second parallel computation,  
       2)the pipeline stage 1 in second parallel computation
       3)Speculative computation 2  in first parallel computation,
       are invoked for each layer.
      and all the pipeline stages for speculative computation 1 of first parallel computation are invoked for 
      every block/ciruclant/each valid time slot counter value*/      
    /*********************************Second Parallel Computation. layer convergence logic*************************/
	   //Parallel computaion: Do the matrix vector multiplcation for cht	 
	    // pipeline stage 1
		 //if(block_num_3D==0) 
		 if(block_num_3D==0 & valid_entry_3D)
		 cht_accumulator <= d_block_s2_to_CC;
		 //else if(block_num_3D<=dc_3D)//note that dc is stored as decimal value-1. 
							        //so need <= expression.
		   else if(block_num_3D<=dc_3D & valid_entry_3D)
     		 cht_accumulator <= cht_accumulator ^ d_block_s2_to_CC;

		 // pipeline stage 2
		//cht_accumulator tells whether a layer passed the cht test by having a value of 0
		//cht_fail flag keeps track of whether all the layers in the code passed the cht test. 
		 //if(block_num_3D==0)	 
		 if(block_num_3D==0 & valid_entry_3D)
		 begin
		 cht_fail_flag_layer_int <= (cht_accumulator>0);				
		 end
        // pipeline stage 3. This intentionally creates a 2-cycle path for cht_fail_flag_layer
		  //as long as cht_fail_flag_layer_int is not used else where.
        //if(block_num_4D==0)
        if(block_num_4D==0 & valid_entry_4D)		 
         cht_fail_flag_layer[layer_4D]   <=  cht_fail_flag_layer_int;

		//if(block_num_5D==0)
		if(block_num_5D==0 & valid_entry_5D)	
             cht_fail_flag_frame_int <= (cht_fail_flag_layer==0)?0:1;
           
           //if(!(sign_flip_violation_layer_prev & UCVF_5D==1'b0) & block_num_5D==0) //if there is no sign flip violation, use the forwarding
             if(!(sign_flip_violation_layer_prev & UCVF_5D==1'b0) & block_num_5D==0 & valid_entry_5D) //if there is no sign flip violation, use the forwarding
               cht_fail_flag_frame <= (cht_fail_flag_layer==0)?0:1;
           else
               cht_fail_flag_frame <= cht_fail_flag_frame_int;

/*********************************END OF Second Parallel Computation. layer convergence logic***/      
/********************************************************************************************/      
	

 /*********************************Final check on convergence using sign flip violation
 and cht_fail_flag_frame. This is the pipeline stage 6*************************/
		  //signal to stop the iterations as soon as the convergence reached or when we are
		  //tried and tired with the maximum number of iterations.
		  
		  //if((block_num_7D==dc_7D |block_num_4D==dc_4D) & iterations_PCU>0)
		    if(((block_num_7D==dc_7D & valid_entry_7D) |(block_num_4D==dc_4D&valid_entry_4D)) & iterations_PCU>0)
		   begin
            //layer_cht is aligned with block_num_6D. so use the 1D version of layer_cht
            //to align with block_num_7D
			 //if((block_num_7D==dc_7D & cht_fail_flag_frame==0 & layer_cht_1D==H_row-1 & Disable_CC_logic==1'b0)| 
			 //     (block_num_4D==dc_4D & (iterations_4D == max_iterations) & (layer_4D == H_row-2)))
			//   if((block_num_7D==dc_7D & cht_fail_flag_frame==0 & cht_fail_flag_frame_int==0 & layer_cht_1D==H_row-1 & Disable_CC_logic==1'b0)| 
			//     (block_num_4D==dc_4D & (iterations_4D == max_iterations) & (layer_4D == H_row-2)))//for more robust operation, 
               if((block_num_7D==dc_7D & valid_entry_7D & cht_fail_flag_frame==0 & cht_fail_flag_frame_int==0 & layer_cht_1D==H_row-1 & Disable_CC_logic==1'b0)| 
                  (block_num_4D==dc_4D & valid_entry_4D & (iterations_4D == max_iterations) & (layer_4D == H_row-2)))//for more robust operation, 
                                                                                                                              //check on cht_fail_flag_frame_int also
			  begin
			  iterations_done <= 1'b1; 
			  decoding_error  <= cht_fail_flag_frame;
   		      layer_cht       <= 0;
			  layer_cht_1D    <= 0;
			  layer_cht_spec1_pipeline_stage3 <=0;
			  layer_cht_spec1_pipeline_stage5 <=0;
			  sign_flip_violation             <=0;
			  end
			 
			 
		 end	 //if(block_num_7D==dc_7D)
/***********************************************************************************/
         
         if(iterations_done == 1'b1)
		    iterations_done <= 1'b0;
 

	 end //else of reset_n
 end	 //for always


 generate
   for (k=0; k < `Num_cc_sign_flip_or-1; k=k+1) 
       begin: CC_limit_fanout
         always @(posedge clk)
           begin
              sign_flip_violation_block[k]    <= 
                   |(sign_flip_violation_vector[k*`Max_FanIn_of_convergence_logic+:`Max_FanIn_of_convergence_logic]);
                   //note that |(unsigned_number) is same as (unsigned_number)>0
                   //also note that msb or lsb order does not matter for this OR logic.
           end     
       end
 endgenerate

endmodule


