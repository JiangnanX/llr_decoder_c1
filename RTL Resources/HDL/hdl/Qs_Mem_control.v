/*
  /---Confidentiality Notice Start--/
  Texas A&M University Confidential.
  Contact: Office of Technology Commercialization.
  The material contained in this software and the associated papers
  have features which are contained in a patent disclosure for LDPC decoding architectures 
  filed by Gwan Choi, John Junkins , Kiran Gunnam. 
  For more information about the licensing process, please contact:
  Alex Garcia
  Department: Licensing & Intellectual Property Management
  Title: Licensing Associate
  Email: alegarza@tamu.edu
  Phone: (979) 458-2640
  Fax: (979) 845-2684
   � Copyright 2005 to the present, Texas A&M University System. 
  All the software contained in these files are protected by United States copyright law
  and may not be reproduced, distributed, transmitted, displayed, published or broadcast 
  without the prior written permission of Texas A&M University System. 
  You may not alter or remove any trademark, copyright or other notice from copies 
  of the content.
  /---Confidentiality Notice End--/


  Kiran Gunnam 
  Texas A&M University
  File Name              : Qs_Mem_control.v 
  Module Attributes      : Control for the Memory module for shifted version of Q
  Design                 : 
  Parametrizable         : Yes
  Functionality          : Supplies the control signals for the Qs memory for the decoder 
  Double buffering/additionl buffering can be added
  based on the application requirements such as to cater
  average throughput case and low energy throttling
  Matlab Model           :	See the Block_LDPC_Decoder.m 
  Revision History       : v1.1
  v1.1:  Added the double buffering
  v1.0:  Kiran Gunnam,Texas A&M University, December 2005 
  
*/


`include "../hdl_gen/LDPC_Decoder_Parameters.h"

module Qs_Mem_control(
                     //decoder clock
							clk,
							//global reset
							reset_n,
							//Number of Block columns in the H matrix. From OFC ROM
							H_col,
							//LLR data from channel and the corresponding signals. Note that the write address
							//for LLR_ch data is generated internally within this module.
                            LLR_ch,
                            new_frame,
                            LLR_ch_wen,
							//Qs read interface
							Qs_rd_addr,
                            Qs_out,
						   //Qs write interface
							Qs_wr_addr,
                            Qs_in,
                            Qs_wen_from_decoder,
							//control signal to the decoder
                            decoding_start,
							//control signal from decoder
							decoding_done,
							//control signal from decoder's hard decision buffer
							HD_Buffer_busy,
							//control signal to the LLR_ch interface
							Qs_Mem_busy,
                            //indicator signal to the debug interface
                            Qs_Mem_avl_debug,
                            Qs_Mem_avl_flag 
							);

input                                  clk;
input                                  reset_n;
input   [`MaxH_col_wl-1          : 0]  H_col;
input   [`Qs_memory_bandwidth-1  : 0]  LLR_ch;
input                                  new_frame;
input                                  LLR_ch_wen;
input   [`Qs_memory_addr_wl-1    : 0]  Qs_rd_addr;
output  [`Qs_memory_bandwidth-1  : 0]  Qs_out;
input   [`Qs_memory_addr_wl-1    : 0]  Qs_wr_addr;
input   [`Qs_memory_bandwidth-1  : 0]  Qs_in;
input                                  Qs_wen_from_decoder;
output                                 decoding_start;
input                                  decoding_done;
input                                  HD_Buffer_busy;
output                                 Qs_Mem_busy;
output                                 Qs_Mem_avl_flag;  

output [`Number_Qs_Buffers_wl-1     : 0]  Qs_Mem_avl_debug; 
wire [`Number_Qs_Buffers_wl-1     : 0]  Qs_Mem_avl_debug;
                                       

//Internal singals and registers
//signal that depend on the assumption that we use double buffering
reg                                    qbr;
reg                                    qbw_buff_num,qbw_decoder_buff_num;
//signal that are generic for any number of buffers
reg [`Number_Qs_Buffers-1        : 0]  qbw,qbw_decoder;
reg [`Number_Qs_Buffers-1        : 0]  Qs_wen,Qs_r_en;    
reg [`Number_Qs_Buffers_wl-1     : 0]  Qs_Mem_avl;
reg                                    decoding_start,decoder_busy,Qs_Mem_busy;
reg [`Qs_memory_addr_wl-1        : 0]  Qs_wr_addr1,Qs_wr_addr2,LLR_ch_wr_address;
reg [`Qs_memory_bandwidth-1      : 0]  Qs_in1,Qs_in2;
reg                                    fill_buffer,fill_buffer_1D; 
reg                                    decoding_done_1D;


/*************START OF LOGIC- dependent on double buffering. ************************************************/
//since double buffering is used this simple flip logic is sufficent to manage the read and write control
//for the Qs memory.
always@(posedge clk)
 begin
 if(reset_n == 1'b0)
  begin
  qbw[0]             <= 1'b0;
  qbw[1]             <= 1'b0;
  qbw_decoder[0]     <= 1'b0;
  qbw_decoder[1]     <= 1'b0;
  qbw_buff_num       <= 1'b0;
  qbw_decoder_buff_num <= 1'b0;
  qbr               <= 1'b0;
  Qs_wen            <= 2'b00;
  Qs_r_en[0]        <= 1'b1;
  Qs_r_en[1]        <= 1'b0;
  
  end
  else
   begin
	 /*WRITE LOGIC**********************/
	 //Ping-pong between two buffers for every new frame and when there is a buffer available to be written
      if(new_frame==1 & Qs_Mem_busy==0) 
        begin
		qbw[qbw_buff_num]  <= 1'b1;
		qbw[~qbw_buff_num] <= 1'b0;
		qbw_buff_num       <= ~qbw_buff_num;
        end
        else if(fill_buffer_1D<=1'b1 & fill_buffer==1'b0)
        begin
                qbw[0] <= 1'b0;
                qbw[1] <= 1'b0;
        end
        
     //Ping-pong between two buffers when ever decoding newframe
     if(decoding_start==1)
      begin
       qbw_decoder[qbw_decoder_buff_num]  <=1'b1;
       qbw_decoder[~qbw_decoder_buff_num] <=1'b0;
       qbw_decoder_buff_num               <= ~qbw_decoder_buff_num;
      end
    else if(decoding_done==1) 
       begin
        qbw_decoder[0]     <= 1'b0;
	qbw_decoder[1]     <= 1'b0;
       end
  
	 //Maintain the multiplexer logic for address and data for the double buffering	
	 //Make sure to be more explicit in differentiating the four possible events
	 //At any time, there may be one or two writes to the two buffers of Qs
      if(qbw[0]==1&qbw[1]==0) 
		begin
			Qs_in1          <= LLR_ch;
			Qs_wr_addr1     <= LLR_ch_wr_address;
			Qs_wen[0]       <= LLR_ch_wen;           
		end
       else if(qbw_decoder[0]==1&qbw_decoder[1]==0) 
		begin
			Qs_in1          <= Qs_in;
			Qs_wr_addr1     <= Qs_wr_addr;
			Qs_wen[0]       <= Qs_wen_from_decoder;
		end
		
	if(qbw[0]==0&qbw[1]==1)
		begin
			Qs_in2          <= LLR_ch;
			Qs_wr_addr2     <= LLR_ch_wr_address;
			Qs_wen[1]       <= LLR_ch_wen;           
		 end
        else if(qbw_decoder[0]==0&qbw_decoder[1]==1) 
		begin
			Qs_in2          <= Qs_in;
			Qs_wr_addr2     <= Qs_wr_addr;
			Qs_wen[1]       <= Qs_wen_from_decoder;
		 end
     
	
     
		/*READ LOGIC**********************/
		//Ping-pong between two buffers for every new frame when the decoding for the previous frame is done.
      if(decoding_done==1 & Qs_Mem_avl > 0) //note that this is very precise timing as Qs_Mem_avl is decremented by 1 if decoding_done pulse happens.
		begin
	        qbr   <= ~qbr;
		//assign read enable for the double buffering. if more than one buffer is used, need to change this logic.
		Qs_r_en[0] <=  qbr; 
		Qs_r_en[1] <= ~qbr; 
		end

  end //end of else of if(reset_n == 1'b0) block
 end//end of always block 
/*************END OF LOGIC- dependent on double buffering. ************************************************/
	
/* START OF GENERIC LOGIC that maintains the buffer control. this is independent of double buffering********/
always@(posedge clk)
 begin
 if(reset_n == 1'b0)
  begin
  LLR_ch_wr_address    <= 0;
  Qs_Mem_avl           <= 0;
  decoding_start       <= 1'b0;
  decoder_busy         <= 1'b0;
  Qs_Mem_busy          <= 1'b0;
  fill_buffer          <= 1'b0;
  fill_buffer_1D       <= 1'b0;
  decoding_done_1D     <= 1'b0;
  end
  else
   begin
		
		fill_buffer_1D          <=fill_buffer;
            decoding_done_1D        <=decoding_done;
		//maintain the write address logic to write the LLR_ch data into the Qs memory.
		if(new_frame==1'b1 & Qs_Mem_busy==0)
		 begin
		 LLR_ch_wr_address    <= 0;
		 fill_buffer          <=1'b1;
		 end
		else if(fill_buffer == 1'b1 & LLR_ch_wen==1'b1 & Qs_Mem_busy==1'b0 & 
		          LLR_ch_wr_address < H_col-1)
		 LLR_ch_wr_address    <= LLR_ch_wr_address+1;
		 else if(LLR_ch_wr_address==H_col-1 &  LLR_ch_wen==1'b1)
		  LLR_ch_wr_address    <= 0;

		 
		//Maintain the memory availability flag. this denotes the number of available buffers for the decoder
		//Note that this takes care of the rare event that a new frame and decoding_done happens
		//at the same clock cycle. Note that the following logic is an unsigned logic. So make sure
		//not to declare any of these signals as singed.
		
		if(LLR_ch_wr_address==H_col-1 & LLR_ch_wen==1'b1)
		begin
		  fill_buffer       <= 1'b0;
		  Qs_Mem_avl        <= Qs_Mem_avl+1-decoding_done;
		end
		else
		  Qs_Mem_avl <= Qs_Mem_avl -decoding_done;
		  
		//Maintain the decoder_start flag		  
		 if( decoding_start==1'b0 & decoder_busy==1'b0 & Qs_Mem_avl > 0 
		    & HD_Buffer_busy==1'b0)
		  decoding_start       <= 1'b1;
		  else
		  decoding_start       <= 1'b0;

		//Maintain an internal signal to show the state of the decoder
		if(decoding_start)
		decoder_busy <= 1'b1;
		else if(decoding_done_1D)//delay the onset of decoding_start by atleast one clock cycle so that HD_Buffer_busy has chance to show its state
		decoder_busy <= 1'b0;



		//Mainitain a signal to the interface to show the state of Qs_Mem. If this signal 
		//is 1, then this module can not accept any new frames.
		if(Qs_Mem_avl==`Number_Qs_Buffers)
		Qs_Mem_busy <= 1'b1;
		else
		Qs_Mem_busy <= 1'b0;		  
		
  end //end of else of if(reset_n == 1'b0) block
 end//end of always block 

assign  Qs_Mem_avl_flag = |Qs_Mem_avl; //indicator that atleast one hard decision buffer is not empty.
                                       //note that this flag is for indicative purposes. And should
                                       //not be used in any control within the decoder core.
assign Qs_Mem_avl_debug = Qs_Mem_avl;                                                                              
/* END OF GENERIC LOGIC that maintains the buffer control. this is independent of double buffering********/


/**********************************************************************************/
//Instantiate the Qs memory with two buffers
Qs_Mem Qs_Mem(Qs_rd_addr,Qs_wr_addr1,Qs_wr_addr2,clk,Qs_in1,Qs_in2,Qs_out,Qs_wen,Qs_r_en);
/**********************************************************************************/

endmodule











