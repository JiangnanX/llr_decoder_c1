/*
  /---Confidentiality Notice Start--/
  This document contains information proprietary to Texas A&M University, College Station.
  Any disclosure or use is expressly prohibited, except upon explicit
  written permission by Texas A&M University.
  Texas A&M University Confidential.
  Contact: Office of Technology Commercialization.
  The material contained in this software and the associated papers
  have features which are contained in a patent disclosure for LDPC decoding architectures 
  filed by Gwan Choi, John Junkins , Kiran Gunnam. 
  For more information about the licensing process, please contact:
  Alex Garcia
  Department: Licensing & Intellectual Property Management
  Title: Licensing Associate
  Email: alegarza@tamu.edu
  Phone: (979) 458-2640
  Fax: (979) 845-2684
   � Copyright 2005 to the present, Texas A&M University System. 
  All the software contained in these files are protected by United States copyright law
  and may not be reproduced, distributed, transmitted, displayed, published or broadcast 
  without the prior written permission of Texas A&M University System. 
  You may not alter or remove any trademark, copyright or other notice from copies 
  of the content.
  /---Confidentiality Notice End--/
  
  Kiran Gunnam 
  Texas A&M University, December 2005 
  File Name              : Layered_Update_Module.v 
  Module Name            : Layered_Update_Module
  Module Attributes      : 
  Design                 : Data path and Control Paths, Vector processing
  Parametrizable         : Yes
  Functionality          : 
  This module does the Layered Update on Q messages. Instantiates P computation module,
  Q computation module, P shifter.
  P       = Qs_old+ R_new;
  P_new   = shift(P,shift_coefficent)
  Qs      = P_new - R_old;
  This module is an implementation of the layered decoding of QC-LDPC codes
  based on "On-the-fly" computation paradigm.
  Matlab Model           :	Block_LDPC_Decoder.m
  Revision History       : v1.0, Kiran Gunnam,Texas A&M University, December 2005.
*/

`include "../hdl_gen/LDPC_Decoder_Parameters.h"
module Layered_Update_Module
                       (
							   //decoder clock
								clk,
								//global reset
								reset_n,
								//Qs message vector from Qs memory
							   Qs_old,
								//Final State vector from FS memory
								FS_old,
                        FS_new,
								//Qsign message vectors from Qsign memory
								//Qsign_new,
								Qsign_old,
								//control signals from LDPC_PCU module
								decoding_processor_busy,
								//control signals from LDPC_PCU module after re-alignment
								iterations,
                        layer,
                        DL,
                        TimeSlotCounter,
                        block_num,
                        dc,
                        ts_max,
                        valid_entry,
					         UCVF,
								//control signals from OFC module after re-alignment
                        BC,
                        BC_HD_mem,
						      SHIFT,
						      SHIFT_UCVF1,
						      DELTA_SHIFT_UCVF0,
                        CI,
								DB,
                        GAMMA,    
								//Outputs
								Qs_to_QsMem,   //shifted version of Q
                        Qs_wr_addr,
                        Qs_rd_addr,
								Qs_wen,     //write enable to Qs_Mem
								Qsign,      //sign bits of Qs
                        Qsign_old_rd_addr,
                       // Qsign_new_rd_addr,
                        Qsign_wr_addr,
								Qsign_wen,  //write enable to Qsign_Mem
								Q,          //Q message to CNU.
								d_block_s1, //sign bits of P before the shifter, to convergence check module
								d_block_s2,//sign bits of P after the shifter,to the HD memory 
								SHIFT_COEFF_HD, //absolute shift coefficeint that is applied 
								//on P(and so the d_block_s2). this is nothing but a re-aligned version of SHIFT
                        HD_rd_addr,
								HD_wr_addr,
								HD_wen, //write enable to HD memory
                        layer_FSold,
                                block_num_CC,
                        dc_CC, 
                        ts_max_CC,
                        valid_entry_CC,
                        layer_CC, 
                        DL_CC, 
                        UCVF_CC, 
                        iterations_CC, 
                        valid_entry_CNU_PS,
                        layer_CNU_PS,
								TimeSlotCounter_CNU_PS,
								block_num_CNU_PS,
                        dc_CNU_PS,
                        layer_CPA,
                        FS_Wen,
                        FS_old_ren,
                        fs_new_rw_cond1 
   							);

//Inputs and Outputs
input                                  clk;
input                                  reset_n;
input   [`Qs_memory_bandwidth-1   : 0] Qs_old;
input   [`FS_memory_bandwidth-1   : 0] FS_old,FS_new;
//input   [`Qsign_memory_bandwidth-1: 0] Qsign_new,Qsign_old;
input   [`Qsign_memory_bandwidth-1: 0] Qsign_old;
input                                  decoding_processor_busy;
input   [`iteration_wl-1          : 0] iterations;
input   [`H_row_wl-1              : 0] layer;                      
input   [`H_row_wl-1              : 0] DL;     
input   [`TimeSlotCounter_wl-1    : 0] TimeSlotCounter;
input   [`dc_wl-1                 : 0] block_num;
input   [`dc_wl-1                 : 0] dc;
input   [`TimeSlotCounter_wl-1    : 0] ts_max;
input                                  valid_entry;
input                                  UCVF;
input   [`H_col_wl-1              : 0] BC,BC_HD_mem;
input   [`Shift_wl-1              : 0] SHIFT,SHIFT_UCVF1,DELTA_SHIFT_UCVF0;//Binary encoded shift coefficient
input   [`ci_wl-1                 : 0] CI;
input   [`dc_wl-1                 : 0] DB;
input   [`Gamma_wl-1              : 0] GAMMA;
output  [`Qs_memory_bandwidth-1   : 0] Qs_to_QsMem;
output  [`Qs_memory_addr_wl-1     : 0] Qs_wr_addr,Qs_rd_addr;
output                                 Qs_wen; 
output  [`Qsign_memory_bandwidth-1 : 0] Qsign;
//output  [`Qsign_memory_addr_wl-1   : 0] Qsign_old_rd_addr,Qsign_new_rd_addr,Qsign_wr_addr;
output  [`Qsign_memory_addr_wl-1   : 0] Qsign_old_rd_addr,Qsign_wr_addr;
output                                 Qsign_wen;         
output  [`Q_bandwidth-1           : 0] Q;
output  [`HD_blocksize-1          : 0] d_block_s1,d_block_s2;
output   [`Shift_wl-1             : 0] SHIFT_COEFF_HD;//Binary encoded shift coefficient
output  [`HD_memory_addr_wl-1     : 0] HD_wr_addr,HD_rd_addr;
output                                 HD_wen;
output  [`H_row_wl-1              : 0] layer_FSold, layer_CNU_PS;
output  [`TimeSlotCounter_wl-1    : 0] TimeSlotCounter_CNU_PS;
output  [`dc_wl-1                 : 0] block_num_CNU_PS;
output  [`dc_wl-1                 : 0] dc_CNU_PS;
output  [`dc_wl-1                 : 0] block_num_CC;
output  [`dc_wl-1                 : 0] dc_CC;
output  [`TimeSlotCounter_wl-1    : 0] ts_max_CC;
output                                 valid_entry_CC;
output  [`H_row_wl-1              : 0] layer_CC,DL_CC;
output  [`iteration_wl-1          : 0] iterations_CC;
output                                 valid_entry_CNU_PS;
output                                 UCVF_CC; 
output [`H_row_wl-1          :0]       layer_CPA;
output                                 FS_Wen;
output                                 FS_old_ren;
output                                 fs_new_rw_cond1; 

//Internal wires 
wire    [`Shift_wl-1              : 0] SHIFT_COEFF_P,SHIFT_COEFF_HD;
wire                                   HD_wen;
wire    [`TimeSlotCounter_wl-1    : 0] TimeSlotCounter_CNU_PS;
wire    [`dc_wl-1                 : 0] block_num_CNU_PS,block_num_PCM,block_num_QCM;
wire    [`Gamma_wl-1              : 0] GAMMA_CCOA;
wire                                   set_Rnew_to_zero_PCM,set_Rold_to_zero_QCM;
wire                                   Qs_wen;
wire    [`Qs_memory_bandwidth-1   : 0] P,P_new; 
wire                                   Qsign_wen;  
wire   [`HD_blocksize-1          : 0]  d_block_s2;
wire   [`Qs_memory_bandwidth-1   : 0]  Qs;
reg    [`Qs_memory_bandwidth-1   : 0] Qs_to_QsMem;
wire [`H_row_wl-1                  : 0] layer_CPA;  
wire                                    FS_Wen;
wire                                    FS_old_ren;
wire                                    fs_new_rw_cond1; 

//Instantiate the control module for layered update
LUM_Control LUM_Ctrl (
    .clk(clk), 
    .reset_n(reset_n), 
	 .decoding_processor_busy(decoding_processor_busy),
    .iterations(iterations), 
    .layer(layer), 
    .DL(DL), 
    .TimeSlotCounter(TimeSlotCounter), 
    .block_num(block_num), 
    .dc(dc), 
    .ts_max(ts_max),
    .valid_entry(valid_entry),
    .UCVF(UCVF), 
    .BC(BC), 
    .BC_HD_mem(BC_HD_mem),
    .SHIFT(SHIFT), 
    .SHIFT_UCVF1(SHIFT_UCVF1), 
    .DELTA_SHIFT_UCVF0(DELTA_SHIFT_UCVF0), 
    .CI(CI), 
    .DB(DB), 
    .GAMMA(GAMMA), 
    .TimeSlotCounter_CNU_PS(TimeSlotCounter_CNU_PS), 
    .block_num_CC(block_num_CC), 
    .dc_CC(dc_CC), 
    .ts_max_CC(ts_max_CC),
    .valid_entry_CC(valid_entry_CC), 
    .layer_CC(layer_CC), 
    .DL_CC(DL_CC), 
    .layer_CNU_PS(layer_CNU_PS),
    .layer_FSold(layer_FSold),
    .UCVF_CC(UCVF_CC), 
    .iterations_CC(iterations_CC), 
    .valid_entry_CNU_PS(valid_entry_CNU_PS), 
    .block_num_PCM(block_num_PCM), 
    .block_num_QCM(block_num_QCM), 
    .block_num_CNU_PS(block_num_CNU_PS), 
    .dc_CNU_PS(dc_CNU_PS), 
    .Qs_rd_addr(Qs_rd_addr), 
    .Qs_wr_addr(Qs_wr_addr), 
    .SHIFT_COEFF_P(SHIFT_COEFF_P), 
    .SHIFT_COEFF_HD(SHIFT_COEFF_HD), 
    //.Qsign_new_rd_addr(Qsign_new_rd_addr), 
    .Qsign_old_rd_addr(Qsign_old_rd_addr), 
    .Qsign_wr_addr(Qsign_wr_addr), 
    .HD_rd_addr(HD_rd_addr), 
    .HD_wr_addr(HD_wr_addr), 
    .HD_wen(HD_wen), 
    .GAMMA_CCOA(GAMMA_CCOA), 
    .set_Rnew_to_zero_PCM(set_Rnew_to_zero_PCM), 
    .set_Rold_to_zero_QCM(set_Rold_to_zero_QCM), 
    .Qs_wen(Qs_wen), 
    .Qsign_wen(Qsign_wen),
    .layer_CPA(layer_CPA),
    .FS_Wen(FS_Wen),
    .FS_old_ren(FS_old_ren),
    .fs_new_rw_cond1(fs_new_rw_cond1)  
    );

 
//Instantiate the data processing modules here
//P computation module
//P_Computation_Module PCM(clk,Qs_old,FS_new,Qsign_new,block_num_PCM,set_Rnew_to_zero_PCM,P); 
P_Computation_Module PCM(clk,Qs_old,FS_new,block_num_PCM,set_Rnew_to_zero_PCM,P);
//Routers and Multiplexers
P_shifter P_shifter(clk,P,P_new,SHIFT_COEFF_P);
//Q computation module
Q_Computation_Module QCM(clk,P_new,FS_old,Qsign_old,block_num_QCM,set_Rold_to_zero_QCM,Qs,d_block_s2);
//Correction on Q messages using 2-D gamma correction(offset or scaling).
Circulant_correction_array CCOA(clk,Qs_to_QsMem,GAMMA_CCOA,Q);

genvar k;
generate
  for(k=0; k<`HD_blocksize; k=k+1)
	begin:Sign_Extraction
	assign d_block_s1[k]     = P[k*`P_wl+`P_wl-1];
	//assign d_block_s2[k]     = P_new[k*`P_wl]; //This is moved to QCM to make sure that the memory input is after a register.
  	assign Qsign[k]          = Qs_to_QsMem[k*`P_wl+`P_wl-1];
	end 
endgenerate

//register the Qs before sending to the Qs memory and to the correction unit.
always @(posedge clk)
 begin
  Qs_to_QsMem <= Qs;
 end



endmodule       
