/*This is a saturated adder for unsigned numbers
  Kiran Gunnam 
  Texas A&M University
*/  

module satu_adder_v2_ccoa(q,a,b); 

parameter q_wl = 5;  
parameter a_wl = 7;
parameter b_wl = 1;
parameter o_wl = a_wl+b_wl-q_wl;

input  [a_wl-1:0] a;//these are unsigned
input  [b_wl-1:0] b;
output [q_wl-1:0] q;

wire 	[q_wl-1:0] y;
wire    [o_wl-1:0] OVFL_bits;
wire    [q_wl-1:0] temp;

//satu_adder_v2x_q5b_a7b_b1b_unsigned sa(a,b,{OVFL_bits,y});
assign {OVFL_bits,y} = a + b;

genvar k;
generate
 for (k=0; k <= q_wl-1; k=k+1) 
   begin: saturate_mag
     assign temp[k] = 1'b1;
    end
endgenerate


assign q = (OVFL_bits>0)?temp:y;
  
endmodule