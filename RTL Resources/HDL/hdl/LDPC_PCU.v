/*
  
  /---Confidentiality Notice Start--/
  This document contains information proprietary to Texas A&M University, College Station.
  Any disclosure or use is expressly prohibited, except upon explicit
  written permission by Texas A&M University.
  Texas A&M University Confidential.
  Contact: Office of Technology Commercialization.
  The material contained in this software and the associated papers
  have features which are contained in a patent disclosure for LDPC decoding architectures 
  filed by Gwan Choi, John Junkins , Kiran Gunnam. 
  For more information about the licensing process, please contact:
  Alex Garcia
  Department: Licensing & Intellectual Property Management
  Title: Licensing Associate
  Email: alegarza@tamu.edu
  Phone: (979) 458-2640
  Fax: (979) 845-2684
  � Copyright 2005 to the present, Texas A&M University System. 
      All the software contained in these files are protected by United States copyright law
      and may not be reproduced, distributed, transmitted, displayed, published or broadcast 
      without the prior written permission of Texas A&M University System. 
      You may not alter or remove any trademark, copyright or other notice from copies 
      of the content.
  /---Confidentiality Notice End--/
  
  Kiran Gunnam 
  Texas A&M University
  File Name              : LDPC_PCU.v 
  Module Attributes      : Control Unit 
  Design                 : Simple control logic
  Parametrizable         : Yes
  Functionality          : 
  This is the process control unit for LDPC decoding architecture based on the on-the-fly
  computation paradigm. This unit generates the control signals for the pipeline execution.
  Matlab Model           :	See the Block_LDPC_Decoder.m
  Revision History       : v1.1
  v1.1:  Changes for no idle slots.
  v1.0:  Kiran Gunnam,Texas A&M University, December 2005 
*/

`include "../hdl_gen/LDPC_Decoder_Parameters.h"

module LDPC_PCU(     clk,
                     reset_n,
					 new_frame,			//From input buffer control
                     max_iterations_programmed, //From streaming input interface, can be programmed 
                                               //for each new frame before the new_frame gets asserted
					 Disable_Convergence_logic,
                     H_row,				//From OFC ROM
					 dc_next,			//From OFC ROM
					 dc_next_init,    //From OFC ROM
					 ts_max_next,		//From OFC ROM
                     ts_max_next_init,    //From OFC ROM
                     valid_entry_next,   //From OFC ROM
                     valid_entry_next_init1,  //From OFC ROM
                     valid_entry_next_init2,  //From OFC ROM
                     cci, //From OFC ROM
					 iterations_done, //From cht unit
                     layer_PCU,
					 next_layer_PCU,
					 max_iterations,	//To cht unit
                     Disable_CC_logic,
					 iterations,
					 ci_inorder,
					 valid_entry_rom_address,
					 TimeSlotCounter,
					 dc_PCU,
					 ts_max_PCU,
					 valid_entry_PCU,
					 block_num,
                     decoding_processor_busy
					 );

input  clk;
input  reset_n;
input  new_frame;
input  [`iteration_wl-1             :0] max_iterations_programmed;
input                                   Disable_Convergence_logic;
input  [`MaxH_row_wl-1              :0] H_row; 
input  [`dc_wl-1                    :0] dc_next,dc_next_init;
input  [`TimeSlotCounter_wl-1       :0] ts_max_next,ts_max_next_init;
input  [`cci_wl-1                   :0] cci;
input                                   valid_entry_next, valid_entry_next_init1,valid_entry_next_init2;
input  iterations_done;

output [`H_row_wl-1                 :0] layer_PCU,next_layer_PCU;
output [`iteration_wl-1             :0] max_iterations; 
output                                  Disable_CC_logic;
output [`iteration_wl-1             :0] iterations; 
output [`ci_wl-1                    :0] ci_inorder;
output [`cci_wl-1                   :0] valid_entry_rom_address;
output [`TimeSlotCounter_wl-1       :0] TimeSlotCounter;
output [`TimeSlotCounter_wl-1       :0] ts_max_PCU;
output [`dc_wl-1                    :0] dc_PCU,block_num;
output                                  valid_entry_PCU;
output                                  decoding_processor_busy;
reg    [`iteration_wl-1             :0] max_iterations;     
reg                                     Disable_CC_logic;          
reg    [`H_row_wl-1                 :0] layer, next_layer; 
reg    [`H_row_wl-1                 :0] layer_PCU,next_layer_PCU ; 
reg    [`iteration_wl-1             :0] iterations; 
reg    [`ci_wl-1                    :0] ci_inorder;
wire   [`cci_wl-1                   :0] valid_entry_rom_address;
reg    [`TimeSlotCounter_wl-1       :0] TimeSlotCounter_int,TimeSlotCounter_1D;
reg    [`TimeSlotCounter_wl-1       :0] ts_max;
reg    [`TimeSlotCounter_wl-1       :0] ts_max_PCU;
reg                                     valid_entry;
reg                                     valid_entry_PCU;
reg    [`cci_wl-1                   :0] vrom_cntr;
reg    [`dc_wl-1                    :0] block_num,dc;
reg    [`dc_wl-1                    :0] dc_PCU;
reg    [`PIPELINE_DEPTH_wl-1   :0]     prologue;   
//PCU State machine variables
reg    [1:0]   PCU_state;
parameter PCU_IDLE             = 2'b00;
parameter PCU_PROC             = 2'b01;
parameter PCU_PIPELINE_FLUSH   = 2'b10;
reg decoding_processor_busy; 




always@ (posedge clk)
 begin
 if (reset_n ==0)
  begin
      PCU_state <= PCU_IDLE;
      prologue <= 0;
      decoding_processor_busy <= 1'b0;                 
      max_iterations       <= max_iterations_programmed;
      Disable_CC_logic     <= Disable_Convergence_logic; 
    //to debug the decoder by making it perform maximum number of iterations
      layer_PCU            <= 0;
      next_layer_PCU       <= 0;
      layer                <= 0;
      next_layer           <= 0;
      dc                   <= 0;
      ts_max               <= 0;
      valid_entry          <= 0;
      ts_max_PCU          <= 0;
      valid_entry_PCU     <= 0;
      next_layer_PCU      <= 0;
      iterations           <= 0;
      ci_inorder           <= 0;
      vrom_cntr                  <= 0;
      TimeSlotCounter_int  <= `Freeze_Slot;
      TimeSlotCounter_1D   <= `Freeze_Slot;
      block_num            <= 0;
      
      
  end
 else
  begin
  case (PCU_state)
      PCU_IDLE : begin
		//Note that the decoder should start in this state before starting a new frame
		                prologue                <= 0;
                        decoding_processor_busy <= 1'b0;                 
                        max_iterations       <= max_iterations_programmed;
                        Disable_CC_logic     <= Disable_Convergence_logic; 
                       //to debug the decoder by making it perform maximum number of iterations

                        layer_PCU            <= 0;
						next_layer_PCU       <= 1;
						layer                <= 0;
						next_layer           <= 1;
						dc                   <= dc_next_init;
						ts_max               <= ts_max_next_init;
						valid_entry          <= 1'b0;//valid_entry_next_init1; //2 clk cycle latency on valid_entry_ROM.
						//so may need 2 inits
	                    ts_max_PCU          <= 0;
                        valid_entry_PCU     <= 0;
                        next_layer_PCU      <= 0;
                                               
	                    iterations           <= 0;
	                    ci_inorder           <= 0;
	                    vrom_cntr            <= 0;
	                    TimeSlotCounter_int  <= `Freeze_Slot;
						TimeSlotCounter_1D   <= `Freeze_Slot;
	                    block_num            <= 0;
                       	if(new_frame)
                         PCU_state <= PCU_PIPELINE_FLUSH;
                         else
						 PCU_state <= PCU_IDLE;
                    
                 end
     PCU_PIPELINE_FLUSH : 
                 begin      
		                decoding_processor_busy <= 1'b0;                 
                     if(prologue<`PIPELINE_DEPTH-1)
                             prologue <= prologue+1;
         		    	if(prologue==`PIPELINE_DEPTH-1)
                        begin
                         prologue  <= 0;
						 PCU_state <= PCU_PROC;
                        end
						 else
						 PCU_state <= PCU_PIPELINE_FLUSH;
                         
                 end
      PCU_PROC : begin
		                decoding_processor_busy <= 1'b1;
                        layer_PCU           <= layer;
                        dc_PCU              <= dc;
                        ts_max_PCU          <= ts_max;
                        valid_entry_PCU     <= valid_entry;
						next_layer_PCU      <= next_layer;
		                TimeSlotCounter_1D  <= TimeSlotCounter_int;
						TimeSlotCounter_int <= (TimeSlotCounter_int<ts_max)?TimeSlotCounter_int+1:0;
						vrom_cntr           <= (vrom_cntr<cci-1)?vrom_cntr+1:0;
						valid_entry         <= (vrom_cntr>2)? valid_entry_next :
						                          (vrom_cntr==1)?valid_entry_next_init2:valid_entry_next_init1;
						//may need two valid_entry_next_inits here as there is 2 clock cycle latency on this memory
						
						//Layer counter
						if(TimeSlotCounter_int==ts_max) 
						 begin
						 layer           <=  (layer <H_row-1)? layer+1:0;
						 next_layer      <=  (next_layer <H_row-1)? next_layer+1:0;
						 dc              <=   dc_next;
						 ts_max          <=   ts_max_next;
						 end
						 //Iteration counter
					   if(layer==0 & TimeSlotCounter_int==0) 
						  begin
						   iterations      <= iterations+1;
	                       ci_inorder      <= 0;
						  end
						
						//if(TimeSlotCounter_int<=dc)//note that dc is stored as decimal value-1. 
						                       //so need <= expression. assuming stall cycles happen at the end.
						if(TimeSlotCounter_int<=ts_max & valid_entry)//
						  begin
						   block_num      <= TimeSlotCounter_int;
							if(!(layer==0 & TimeSlotCounter_int==0))
							 ci_inorder     <= ci_inorder+1;
						  end
				
						 //reset logic based in iterations_done flag from cht module
						 if(iterations_done)
						   PCU_state <= PCU_IDLE;
						  else
						   PCU_state <= PCU_PROC;
              end
   endcase
 end
end


assign TimeSlotCounter = TimeSlotCounter_1D;   
assign valid_entry_rom_address =vrom_cntr;
endmodule

