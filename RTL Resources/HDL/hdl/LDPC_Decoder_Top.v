/*
  
  /---Confidentiality Notice Start--/
  This document contains information proprietary to Texas A&M University, College Station.
  Any disclosure or use is expressly prohibited, except upon explicit
  written permission by Texas A&M University.
  Texas A&M University Confidential.
  Contact: Office of Technology Commercialization.
  The material contained in this software and the associated papers
  have features which are contained in a patent disclosure for LDPC decoding architectures 
  filed by Gwan Choi, John Junkins , Kiran Gunnam. 
  For more information about the licensing process, please contact:
  Alex Garcia
  Department: Licensing & Intellectual Property Management
  Title: Licensing Associate
  Email: alegarza@tamu.edu
  Phone: (979) 458-2640
  Fax: (979) 845-2684
  � Copyright 2005 to the present, Texas A&M University System. 
  All the software contained in these files are protected by United States copyright law
  and may not be reproduced, distributed, transmitted, displayed, published or broadcast 
  without the prior written permission of Texas A&M University System. 
  You may not alter or remove any trademark, copyright or other notice from copies 
  of the content.
  /---Confidentiality Notice End--/
  
  Kiran Gunnam 
  Texas A&M University, December 2005 
  File Name              : LDPC_Decoder_Top.v
  Module Name            : LDPC_Decoder_Top
  Module Attributes      : Top level
  Design                 : Data path and Control Paths, Vector processing
  Parametrizable         : Yes
  Functionality          : 
  This module is an implementation of the layered decoding of QC-LDPC codes
  based on "On-the-fly" computation paradigm.This  does the instantiation of all the necessary modules
  for the decoder of irregular LDPC codes.Does the partial state processing on Q messages. This is based on 
  min-sum processing.
  Matlab Model           :	Block_LDPC_Decoder.m
  Revision History       : v1.0, Kiran Gunnam,Texas A&M University, December 2005.
  This is the toplevel template implementation for Block LDPC codes/ irregular LDPC codes for 
  IEEE 802.11n and IEEE802.16e. Please retain this header and mark the revision.
  Note that the decoder accepts new buffer after the completion of current frame.
  Double bufferinng can be added. Check the LDPC_Decoder_xxx_Top.v for actual implementation file.
  
  Revision History       : v1.1
  1. Increase of vector data path pipeline width from 5 to 9 for better frequency. 
     The 802.11n decoder design P&R frequency is around 100 MHz. 
     The expected final frequency for the new design will be around 125-130 MHz.
     The total pipeline depth including the control is 15. 
  2. Double buffering on the Input buffer(Qs Memory)
  3. Improvement in convergence logic.Added the accurate reset control on layer_cht. 
     Iteration savings of around 0.5 are expected.
  4. Replacement of offset module with scaling module. 

*/

`include "../hdl_gen/LDPC_Decoder_Parameters.h"
`define DEBUG_BUS_wl `CodeWordLength_wl+`CodeWordLength_wl+`Version_wl+`dc_wl+`CodeID_wl+`H_row_wl+`iteration_wl+5

module LDPC_Decoder_Top(clk,
                        reset_n,
                        CodeId,
                        max_iterations_programmed,
                        Disable_Convergence_logic,
                        LLR_ch,
                        new_frame,
                        LLR_ch_wen,
                        soi_busy,
                        decoded_data,
                        d_start,
                        data_valid,
                        decoding_error,
                        decoder_busy,
                        DEBUG_BUS,
                        iterations,
                        iterations_done
                        );

//Inputs and Outputs
input                              clk;
input                              reset_n;
input  [`CodeID_wl-1:0]            CodeId;
input  [`iteration_wl-1       : 0] max_iterations_programmed;
input                              Disable_Convergence_logic;
input  [`LLR_Ch_bandwidth-1   : 0] LLR_ch;
input                              new_frame,LLR_ch_wen;
input                              soi_busy;
output [`d_bandwidth-1        : 0] decoded_data;
output                             data_valid;
output                             d_start;
output                             decoding_error;
output                             decoder_busy;
output [`DEBUG_BUS_wl-1:0]         DEBUG_BUS;
output [`iteration_wl-1        :0] iterations;
output                             iterations_done;


////////////////////////////////////////////////////////////////////////////
/*reg [`LLR_Ch_bandwidth-1     :0] LLR_ch_L_tmp8,LLR_ch_L_tmp7,LLR_ch_L_tmp6, LLR_ch_L_tmp5, 
                                 LLR_ch_L_tmp4, LLR_ch_L_tmp3, LLR_ch_L_tmp2, LLR_ch_L_tmp1;*/
wire [`Qs_memory_bandwidth-1  :0]LLR_ch_block;
wire                             LLR_ch_block_wen;
wire signed [`LLR_Ch_wl-1:0]      a[`Parallelization-1:0];                                                  
wire signed [`P_wl-1:0]           a_se[`Parallelization-1:0];  


wire [`iteration_wl-1         :0] max_iterations_programmed,max_iterations;
wire                              Disable_CC_logic;
//d_shifter (clk,d_out,decoded_data,SHIFT_COEFF_HD_OUT);
//wire  [128-1:0] decoded_data ;//Shifted message vector

//PCU LDPC_PCU PCU(clk,reset_n,new_frame,H_row, dc,iterations_done,
//             reset_layer_cht,layer,next_layer,layer_cht,iterations,linear_index,
// 				 TimeSlotCounter, block_num);
wire [`dc_wl-1              :0] dc;
wire [`TimeSlotCounter_wl-1  :0] ts_max;
wire                             valid_entry;
wire [`cci_wl-1              :0] valid_entry_rom_address;
wire  iterations_done;
wire [`H_row_wl-1            :0] layer,layer_PCU,layer_CNU_PS,layer_CPA,next_layer; 
wire [`iteration_wl-1        :0] iterations,iterations_PCU; 
wire [`ci_wl-1               :0] linear_index;
wire [`TimeSlotCounter_wl-1  :0] TimeSlotCounter,TimeSlotCounter_PCU;
wire [`dc_wl-1               :0] block_num,block_num_PCU;


    
//wire                             UCVF_PCU;
//OFC_ROM (clk,reset_n,CodeId,layer,linear_index,block_num,dc,UCVF,BC,
//                 SHIFT,SHIFT_UCVF1,DELTA_SHIFT_UCVF0,CI,DL,DB,GAMMA,H_row,H_col,Z);

wire [`dc_wl-1                 :0] dc_next,dc_next_init,dc_PCU,dc_CNU_PS;
wire  [`cci_wl-1               :0] cci;
wire [`TimeSlotCounter_wl-1  :0]   ts_max_next,ts_max_next_init, ts_max_PCU;
wire                               valid_entry_next, valid_entry_next_init1,valid_entry_next_init2,valid_entry_PCU;
wire                               UCVF_OFC;
wire [`H_col_wl-1              :0] BC_OFC;
wire [`Shift_wl-1              :0] SHIFT_OFC,SHIFT_UCVF1_OFC,
                                   DELTA_SHIFT_UCVF0_OFC;
wire [`ci_wl-1                 :0] CI_OFC;//,DCI_OFC;
wire [`H_row_wl-1              :0] DL_OFC;
wire [`dc_wl-1                 :0] DB_OFC;//is not used outside OFC ROM.
wire [`Gamma_wl-1              :0] GAMMA_OFC;


wire                              UCVF;
wire [`H_col_wl-1             :0] BC,BC_HD_mem;
wire [`Shift_wl-1             :0] SHIFT,SHIFT_UCVF1,DELTA_SHIFT_UCVF0;
wire [`ci_wl-1                :0] CI;
wire [`H_row_wl-1             :0] DL,DL_fs_mem;
wire [`dc_wl-1                :0] DB;
wire [`Gamma_wl-1             :0] GAMMA;
wire [`MaxH_row_wl-1          :0] H_row;
wire [`MaxH_col_wl-1          :0] H_col;
wire [`Z_wl-1                 :0] Z;
wire [`CodeWordLength_wl-1    :0] CodeWordLength,DataWordLength ;




//Layered_Update_Module  (clk,reset_n,Qs_old,FS,Qsign_new,Qsign_old,TimeSlotCounter,block_num,iterations, 
// SHIFT,SHIFT_UCVF1,DELTA_SHIFT_UCVF0,GAMMA,UCVF,Qs,Qs_wen,Qsign,Qsign_wen, 
//   Q,d_block_s1,d_block_s2,SHIFT_COEFF_HD,d_block_s2_wen,TimeSlotCounter_CNU_PS,block_num_CNU_PS);
wire [`TimeSlotCounter_wl-1  :0] TimeSlotCounter_CNU_PS;
wire [`dc_wl-1               :0] block_num_CC,block_num_CNU_PS,dc_CC;
wire [`TimeSlotCounter_wl-1  :0] ts_max_CC;
wire                             valid_entry_CC;
wire                             d_block_s2_wen;
wire [`H_row_wl-1            :0] layer_CC,DL_CC,layer_FSold;
wire [`iteration_wl-1        :0] iterations_CC;
wire                             UCVF_CC; 

//CNU_PS_array(clk,Q,TimeSlotCounter_CNU_PS,block_num_CNU_PS,dc,FS,FS_Wen);
wire [`Q_bandwidth-1  :0]  Q;
wire FS_Wen;
wire [`FS_memory_bandwidth-1  :0] FS;
//CC (clk,reset_n, H_row,dc, d_block_s1,d_block_s2,layer_cht,
//					   iterations, max_iterations_programmed,TimeSlotCounter,			
//                iterations_done, decoding_error, reset_layer_cht); 
wire [`HD_blocksize-1         :0] d_block_s1,d_block_s2,d_block_s2_to_HD_Mem;
wire                              decoding_error_CC;
//fs_mem (FS_addr,clk,FS,FS_out,FS_Wen);
//reg [`FS_memory_addr_wl-1 : 0] FS_addr;
wire [`FS_memory_bandwidth-1:0]  FS_old,FS_new;
wire FS_old_ren,FS_new_ren;
wire fs_new_rw_cond1;  
//hd_mem (HD_rd_addr,HD_wr_addr,clk,d_in,SHIFT_COEFF_HD,d_out,SHIFT_COEFF_HD_OUT,HD_wen,HD_r_en);
wire [`HD_memory_addr_wl-1    :0] HD_rd_addr,HD_wr_addr;
wire  [`Shift_wl-1            :0] SHIFT_COEFF_HD;
wire                              HD_Buffer_busy;
wire                              decoding_processor_busy;
wire                              hd_Mem_avl_flag;

//Qs_Mem_control Qs_Mem(clk,reset_n,H_col,LLR_ch,new_frame,LLR_ch_wen,Qs_rd_addr,Qs_old,
//						   Qs_wr_addr,Qs,Qs_wen,decoding_start,iterations_done,decoder_busy);
wire [`Qs_memory_bandwidth-1  :0] Qs_old,Qs;
wire  Qs_wen;
wire [`Qs_memory_addr_wl-1:0] Qs_wr_addr,Qs_rd_addr;
wire  Qs_Mem_avl_flag; 
//Qsign_mem (Qsign_old_rd_addr1,Qsign_new_rd_addr2,Qsign_wr_addr,
//                 clk,Qsign,Qsign_wen,Qsign_old,Qsign_new);

//wire  [`Qsign_memory_addr_wl-1  :0]    Qsign_old_rd_addr,Qsign_new_rd_addr,Qsign_wr_addr;
wire  [`Qsign_memory_addr_wl-1  :0]    Qsign_old_rd_addr,Qsign_wr_addr;
wire  [`Qsign_memory_bandwidth-1:0]    Qsign;
wire                                   Qsign_wen;
//wire [`Qsign_memory_bandwidth-1 :0]    Qsign_old,Qsign_new;
wire [`Qsign_memory_bandwidth-1 :0]    Qsign_old;

wire [`Number_HD_Buffers_wl-1     : 0]  hd_Mem_avl_debug;
wire [`Number_Qs_Buffers_wl-1     : 0]  Qs_Mem_avl_debug;

////////////////////////////////////////////////////////////////////////////


//Control Units
LDPC_PCU PCU (
    .clk(clk), 
    .reset_n(reset_n), 
    .new_frame(decoding_start), 
    .max_iterations_programmed(max_iterations_programmed),
    .Disable_Convergence_logic(Disable_Convergence_logic),
    .H_row(H_row), 
    .dc_next(dc_next), //send the dc_i from OFC ROM as soon as it is available to PCU
	.dc_next_init(dc_next_init),
	.cci(cci), 
	.ts_max_next(ts_max_next), //send the ts_max_i from OFC ROM as soon as it is available to PCU
    .ts_max_next_init(ts_max_next_init), 
    .valid_entry_next(valid_entry_next),
    .valid_entry_next_init1(valid_entry_next_init1),
    .valid_entry_next_init2(valid_entry_next_init2),
	.iterations_done(iterations_done), 
    .layer_PCU(layer_PCU), 
    .next_layer_PCU(next_layer), 
    .max_iterations(max_iterations),
    .Disable_CC_logic(Disable_CC_logic),
    .iterations(iterations_PCU), 
    .ci_inorder(linear_index), 
    .valid_entry_rom_address(valid_entry_rom_address),
    .TimeSlotCounter(TimeSlotCounter_PCU), 
	.dc_PCU(dc_PCU),
	.ts_max_PCU(ts_max_PCU),
	.valid_entry_PCU(valid_entry_PCU),
    .block_num(block_num_PCU), 
    .decoding_processor_busy(decoding_processor_busy) 
    );

OFC_ROM OFC_ROM(
    .clk(clk), 
    .reset_n(reset_n), 
    .CodeId(CodeId), 
    .valid_entry_rom_address(valid_entry_rom_address),
    .layer(layer_PCU), 
    .linear_index(linear_index), 
    .block_num(block_num_PCU), 
    .dc_next(dc_next), 
	.dc_next_init(dc_next_init),
	.cci(cci),
	.ts_max_next(ts_max_next), 
    .ts_max_next_init(ts_max_next_init),
    .valid_entry_next(valid_entry_next),
    .valid_entry_next_init1(valid_entry_next_init1),
    .valid_entry_next_init2(valid_entry_next_init2),
    .UCVF(UCVF_OFC),
	.BC(BC_OFC), 
    .SHIFT(SHIFT_OFC), 
    .SHIFT_UCVF1(SHIFT_UCVF1_OFC), 
    .DELTA_SHIFT_UCVF0(DELTA_SHIFT_UCVF0_OFC), 
    .CI(CI_OFC), 
    .DL(DL_OFC), 
    .DB(DB_OFC), 
	.GAMMA(GAMMA_OFC), 
    .H_row(H_row), 
    .H_col(H_col), 
    .Z(Z),
    .CodeWordLength(CodeWordLength),
    .DataWordLength(DataWordLength) 
    );  
        
           
OFC_signal_realignment OFC_SR(
    .clk(clk),
    .reset_n(reset_n), 
    .TimeSlotCounter_PCU(TimeSlotCounter_PCU), 
	.layer_PCU(layer_PCU),  
    .dc_PCU(dc_PCU),
    .ts_max_PCU(ts_max_PCU),  
    .valid_entry_PCU(valid_entry_PCU),
	.block_num_PCU(block_num_PCU),
    .iterations_PCU(iterations_PCU),
    .UCVF_i(UCVF_OFC), 
    .BC_i(BC_OFC), 
    .SHIFT_i(SHIFT_OFC), 
    .SHIFT_UCVF1_i(SHIFT_UCVF1_OFC), 
    .DELTA_SHIFT_UCVF0_i(DELTA_SHIFT_UCVF0_OFC), 
    .CI_i(CI_OFC), 
    .DL_i(DL_OFC), 
    .DB_i(DB_OFC), 
    .GAMMA_i(GAMMA_OFC), 
	.TimeSlotCounter(TimeSlotCounter), 
    .layer(layer),  
    .dc(dc), 
    .ts_max(ts_max),
    .valid_entry(valid_entry),
	.block_num(block_num),
    .iterations(iterations),
    .UCVF(UCVF), 
    .BC(BC), 
    .BC_HD_mem(BC_HD_mem),
    .SHIFT(SHIFT), 
    .SHIFT_UCVF1(SHIFT_UCVF1), 
    .DELTA_SHIFT_UCVF0(DELTA_SHIFT_UCVF0), 
    .CI(CI), 
    .DL_fs_mem(DL_fs_mem), 
    .DL(DL), 
    .DB(DB), 
    .GAMMA(GAMMA)
    );
					  

//Decoding Processing
/*Layered_Update_Module LUM(clk,reset_n,Qs_old,FS_old,FS_new,Qsign_new,Qsign_old,TimeSlotCounter,block_num,iterations, 
   dc,UCVF,BC,SHIFT,SHIFT_UCVF1,DELTA_SHIFT_UCVF0,CI,GAMMA,
   Qs,Qs_wr_addr,Qs_rd_addr,Qs_wen,Qsign,Qsign_old_rd_addr,Qsign_new_rd_addr,Qsign_wr_addr,Qsign_wen, 
   Q,d_block_s1,d_block_s2,SHIFT_COEFF_HD,HD_wr_addr,d_block_s2_wen,TimeSlotCounter_CNU_PS,block_num_CNU_PS,dc_CNU_PS);*/

Layered_Update_Module LUM(
    .clk(clk), 
    .reset_n(reset_n), 
    .Qs_old(Qs_old), 
    .FS_old(FS_old), 
    .FS_new(FS_new), 
    //.Qsign_new(Qsign_new), 
    .Qsign_old(Qsign_old),
	 .decoding_processor_busy(decoding_processor_busy), 
    .iterations(iterations), 
    .layer(layer), 
    .DL(DL), 
    .TimeSlotCounter(TimeSlotCounter), 
    .block_num(block_num), 
    .dc(dc), 
    .ts_max(ts_max),
    .valid_entry(valid_entry),
    .UCVF(UCVF), 
    .BC(BC), 
    .BC_HD_mem(BC_HD_mem),
    .SHIFT(SHIFT), 
    .SHIFT_UCVF1(SHIFT_UCVF1), 
    .DELTA_SHIFT_UCVF0(DELTA_SHIFT_UCVF0), 
    .CI(CI), 	 //Equivalent use: linear index from PCU after re-alignment. 
	             //So no need to use this memory output. Do this optimization
					 //at a  later time.
	.DB(DB),
    .GAMMA(GAMMA), 
	 //outputs
    .Qs_to_QsMem(Qs), 
    .Qs_wr_addr(Qs_wr_addr), 
    .Qs_rd_addr(Qs_rd_addr), 
    .Qs_wen(Qs_wen), 
    .Qsign(Qsign), 
    .Qsign_old_rd_addr(Qsign_old_rd_addr), 
    //.Qsign_new_rd_addr(Qsign_new_rd_addr), 
    .Qsign_wr_addr(Qsign_wr_addr), 
    .Qsign_wen(Qsign_wen), 
    .Q(Q), 
    .d_block_s1(d_block_s1), 
	.d_block_s2(d_block_s2_to_HD_Mem), 
    .SHIFT_COEFF_HD(SHIFT_COEFF_HD), 
	.HD_rd_addr(HD_rd_addr),
    .HD_wr_addr(HD_wr_addr), 
    .HD_wen(d_block_s2_wen),
    .layer_FSold(layer_FSold),
    .block_num_CC(block_num_CC), 
    .dc_CC(dc_CC), 
    .ts_max_CC(ts_max_CC),
    .valid_entry_CC(valid_entry_CC),
    .layer_CC(layer_CC), 
    .DL_CC(DL_CC), 
    .UCVF_CC(UCVF_CC), 
    .iterations_CC(iterations_CC),  
    .valid_entry_CNU_PS(valid_entry_CNU_PS),   
    .layer_CNU_PS(layer_CNU_PS),
    .TimeSlotCounter_CNU_PS(TimeSlotCounter_CNU_PS), 
    .block_num_CNU_PS(block_num_CNU_PS), 
    .dc_CNU_PS(dc_CNU_PS),
    .layer_CPA(layer_CPA),
    .FS_Wen(FS_Wen),
    .FS_old_ren(FS_old_ren),
    .fs_new_rw_cond1(fs_new_rw_cond1)  
    );

CNU_PS_array CPA(
    .clk(clk), 
    .valid_entry(valid_entry_CNU_PS),
    .Q(Q), 
    .block_num(block_num_CNU_PS), 
    .PS(FS) 
    );
    
Convergence_Check CC (
    .clk(clk), 
    .reset_n(reset_n), 
    .H_row(H_row), 
    .dc_in(dc_CC),
    .ts_max_in(ts_max_CC), 
    .valid_entry_in(valid_entry_CC),
    .d_block_s1_in(d_block_s1), 
    .d_block_s2_in(d_block_s2), 
    .d_block_s2_to_CC_in(d_block_s2_to_HD_Mem),
    .layer_in(layer_CC), 
    .DL_in(DL_CC), 
    .UCVF_in(UCVF_CC), 
    .iterations_in(iterations_CC), 
    .iterations_PCU(iterations_PCU),
    .max_iterations(max_iterations),
    .Disable_CC_logic(Disable_CC_logic),    
    .block_num_in(block_num_CC), 
    .iterations_done(iterations_done), 
    .decoding_error(decoding_error_CC)
    );    


assign FS_new_ren= valid_entry;

//Memories
//fs_mem fs_mem(layer,clk,FS,FS_out,FS_Wen);
fs_mem_regular fs_mem(
    .layer_CPA(layer_CPA), 
    .layer(layer_FSold),
    .DL(DL), 
 //   .DL(DL_fs_mem), 
    .clk(clk), 
    .reset_n(reset_n),
    .FS_in(FS), 
    .FS_old(FS_old), 
    .FS_new(FS_new), 
    .wen(FS_Wen),
    .FS_old_ren(FS_old_ren),
    .FS_new_ren(FS_new_ren),
    .fs_new_rw_cond1(fs_new_rw_cond1)  
    );


HD_Buffer HD_Buffer(
    .clk(clk), 
    .reset_n(reset_n), 
    .H_col(H_col),
    .H_row(H_row),    
    .soi_busy(soi_busy),
    .hd_wr_addr(HD_wr_addr), 
    .hd_buf_wen(d_block_s2_wen), 
    .d_block_s2_to_hd_Mem(d_block_s2_to_HD_Mem), 
    .SHIFT_COEFF_hd(SHIFT_COEFF_HD), 
    .decoding_start(decoding_start), 
    .decoder_busy(decoding_processor_busy), 
    .decoding_done(iterations_done), 
    .decoding_error(decoding_error_CC),
    .hd_rd_addr_cht(HD_rd_addr), 
    .d_block_s2(d_block_s2), 
    .d_start_soi(d_start), 
    .decoded_data_out_soi(decoded_data), 
    .data_valid_soi(data_valid), 
    .hdb_frame_error(decoding_error),
    .hd_Mem_busy(HD_Buffer_busy),
    .hd_Mem_avl_debug(hd_Mem_avl_debug),
    .hd_Mem_avl_flag(hd_Mem_avl_flag)
    );

    
//Qs Memory
Qs_Mem_control Qs_Mem(
    .clk(clk), 
    .reset_n(reset_n), 
    .H_col(H_col), 
    .LLR_ch(LLR_ch_block), 
    //.LLR_ch(LLR_ch), 
    .new_frame(new_frame), 
    .LLR_ch_wen(LLR_ch_block_wen), 
    //.LLR_ch_wen(LLR_ch_wen), 
    .Qs_rd_addr(Qs_rd_addr), 
    .Qs_out(Qs_old), 
    .Qs_wr_addr(Qs_wr_addr), 
    .Qs_in(Qs), 
    .Qs_wen_from_decoder(Qs_wen), 
    .decoding_start(decoding_start), 
    .decoding_done(iterations_done),
    .HD_Buffer_busy(HD_Buffer_busy),
    .Qs_Mem_busy(decoder_busy),
    .Qs_Mem_avl_debug(Qs_Mem_avl_debug),
    .Qs_Mem_avl_flag(Qs_Mem_avl_flag)
    );
    

Qsign_mem Qsign_mem(
    .rd_addr1(Qsign_old_rd_addr), 
   // .rd_addr2(Qsign_new_rd_addr), 
    .wr_addr(Qsign_wr_addr), 
    .clk(clk), 
    .Qsign_in(Qsign), 
    .wen(Qsign_wen), 
    .Qsign_out1(Qsign_old) 
   // .Qsign_out2(Qsign_new)
    );

//Interfaces
generate
 if (`LLR_Ch_sub_streams==1)
     begin: No_SII_Module
        //assign LLR_ch_block     = LLR_ch;
        assign LLR_ch_block_wen = LLR_ch_wen;
        genvar k;
         for (k=0; k < `Parallelization; k=k+1) 
             begin: vector_Sign_Extension
                //a_se is of wordlength `P_wl. a is of word length `LLR_Ch_wl.Since `P_wl >= `LLR_Ch_wl
                //this is a sign extension. Just make sure to declare a_se and a as the signed
                //with the word lengths `P_wl and `LLR_Ch_wl.
                //no sign extension on this line yet as both word lengths are equal
                assign a[k]            =  LLR_ch[(k+1)*`LLR_Ch_wl-1:k*`LLR_Ch_wl];
        //        // sign extension on this line
                assign a_se[k]         = a[k];  
        //        //Now regroup them as a vector.
                assign LLR_ch_block[(k+1)*`P_wl-1:k*`P_wl]= a_se[k];     
            end
      end
      else if (`LLR_Ch_sub_streams>1) begin: SII_Module
        //Streaming Input interface Module
         sii_module sii_module(
            .clk(clk), 
            .reset_n(reset_n), 
            .LLR_ch(LLR_ch), 
            .new_frame(new_frame), 
            .H_col(H_col),
            .LLR_ch_wen(LLR_ch_wen), 
            .LLR_ch_block(LLR_ch_block), 
            .LLR_ch_block_wen(LLR_ch_block_wen)
            );
            
      /*sii_module  sii_module(
                  .clk(clk),
                  .reset_n(reset_n), 
                  .LLR_ch_i(LLR_ch), 
                  .new_frame_i(new_frame), 
                  .LLR_ch_wen_i(LLR_ch_wen), 
                  .LLR_ch_block_o(LLR_ch_block), 
                  .LLR_ch_block_wen_o(LLR_ch_block_wen)
                 );
       */     
   
      end
   endgenerate
   
debug_and_version_signals debug_and_version_signals(
    .clk(clk), 
    .Qs_Mem_avl_flag(Qs_Mem_avl_flag), 
    .hd_Mem_avl_flag(hd_Mem_avl_flag), 
    .iterations_CC(iterations_CC), 
    .layer_CC(layer_CC), 
    .CodeId(CodeId), 
    .CodeWordLength(CodeWordLength),//this value is also placed the DEBUG bus-and it controls the 
                                    //input interface
    .DataWordLength(DataWordLength),//this value is also placed the DEBUG bus-and it controls the 
                                        //input interface
    
    .block_num_CC(block_num_CC), 
    .iterations_done(iterations_done), 
    .Disable_CC_logic(Disable_CC_logic),
    .DEBUG_BUS(DEBUG_BUS)
    );   

endmodule       
