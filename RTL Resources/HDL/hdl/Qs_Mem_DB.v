/*Kiran Gunnam 
  Texas A&M University
  File Name              : Qs_Mem.v 
  Module Attributes      : Memory module for shifted version of Q
  Design                 : 
  Parametrizable         : Yes
  Functionality          : Serves as Qs memory for the decoder and stores the
  shifted version of Q messages.
  Double buffering/additionl buffering can be added
  based on the application requirements such as to cater
  average throughput case and low energy throttling
  Matlab Model           :	See the Block_LDPC_Decoder.m 
  Revision History       : v1.0
  
*/


`include "../hdl_gen/LDPC_Decoder_Parameters.h"

module Qs_Mem(rd_addr,wr_addr1,wr_addr2,clk,Qs_in1,Qs_in2,Qs_out,wen,r_en);

input  [`Qs_memory_addr_wl-1 : 0] rd_addr,wr_addr1,wr_addr2;
input  clk;
input  [`Qs_memory_bandwidth-1  : 0] Qs_in1,Qs_in2;
output [`Qs_memory_bandwidth-1  : 0] Qs_out;
input  [`Number_Qs_Buffers-1:0]   wen,r_en;


parameter Qs_memory_depth     =  `Qs_memory_depth;
parameter Qs_memory_addr_wl   =  `Qs_memory_addr_wl;
parameter Qs_memory_bandwidth =  `Qs_memory_bandwidth;

wire [`Qs_memory_bandwidth-1  : 0] Qs_out1,Qs_out2;
assign Qs_out                    = (r_en[0]==1)? Qs_out1:Qs_out2;

//            #(width,depth,ceil(log2(depth)))
dpram_r1p_w1p #(Qs_memory_bandwidth,Qs_memory_depth,Qs_memory_addr_wl)
                Qs_Mem1(wr_addr1,rd_addr,clk,Qs_in1,Qs_out1,wen[0],r_en[0]);
dpram_r1p_w1p #(Qs_memory_bandwidth,Qs_memory_depth,Qs_memory_addr_wl)
                Qs_Mem2(wr_addr2,rd_addr,clk,Qs_in2,Qs_out2,wen[1],r_en[1]);


endmodule











