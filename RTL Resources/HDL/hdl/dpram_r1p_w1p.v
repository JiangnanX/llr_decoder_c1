 //Kiran Gunnam  
 //Dual port RAM. 1 port Read. 1 port write. Based on Xilinx template.
 module dpram_r1p_w1p(write_address,read_address,clock,din,dout,write_enable,read_enable);//synthesis attribute keep_hierarchy of dpram_r1p_w1p is yes
  parameter RAM_WIDTH = 36, RAM_DEPTH = 512, RAM_ADDR_BITS = 9;

 
   
   input  [RAM_ADDR_BITS-1:0] write_address,read_address;
	input  clock;
   input  [RAM_WIDTH-1:0] din;
	output [RAM_WIDTH-1:0] dout;   
	input  write_enable;
	input  read_enable;

   reg   [RAM_WIDTH-1:0] dpram [RAM_DEPTH-1:0];
   reg   [RAM_WIDTH-1:0] dout;   
   //read first mode
   always @(posedge clock) begin
      if (write_enable)
         dpram[write_address] <= din;
      if(read_enable)  
         dout <= dpram[read_address];
   end
   
          
endmodule