/*

  /---Confidentiality Notice Start--/
  This document contains information proprietary to Texas A&M University, College Station.
  Any disclosure or use is expressly prohibited, except upon explicit
  written permission by Texas A&M University.
  Texas A&M University Confidential.
  Contact: Office of Technology Commercialization.
  The material contained in this software and the associated papers
  have features which are contained in a patent disclosure for LDPC decoding architectures 
  filed by Gwan Choi, John Junkins , Kiran Gunnam. 
  For more information about the licensing process, please contact:
  Alex Garcia
  Department: Licensing & Intellectual Property Management
  Title: Licensing Associate
  Email: alegarza@tamu.edu
  Phone: (979) 458-2640
  Fax: (979) 845-2684
   � Copyright 2005 to the present, Texas A&M University System. 
  All the software contained in these files are protected by United States copyright law
  and may not be reproduced, distributed, transmitted, displayed, published or broadcast 
  without the prior written permission of Texas A&M University System. 
  You may not alter or remove any trademark, copyright or other notice from copies 
  of the content.
  /---Confidentiality Notice End--/
  
  Kiran Gunnam  
  Texas A&M University
  File Name              : R_Selection.v 
  Module Attributes      : Datapath 
  Design                 : Data path, Scalar processing
  Parametrizable         : Yes
  Functionality          : 
  Does the R selection operation by comparing the index of Q with the index of the minimum
  of the Q messages and R sign. R sign is computed based on the cumulative sign of Q messages 
  and the corresponding Q sign.
  Matlab Model           :	R_Selection.m, Data path, Vector processing
  Revision History       : v1.0
*/
`include "../hdl_gen/LDPC_Decoder_Parameters.h"

module R_Selection(clk,FS,Qsign,block_num,set_R_to_zero,R);

//Inputs and Outputs
input                            clk;        
input  [`FS_wl-1 : 0]            FS;

input                            Qsign;
input  [`dc_wl-1 : 0]            block_num;
input                            set_R_to_zero;
output signed [`R_wl-1  : 0]     R;



//Internal signals derived from the inputs
wire   [`M_wl-1:0]             M1,M2;
wire   [`dc_wl-1:0]            M1_index;
wire                           Q_cum_sign;

//Internal signals generated
wire                           Rsign;
wire 	 [`M_wl-1:0]             R_mag;
wire signed  [`R_wl-1:0]       temp,R_tmp;




parameter FS_wl  =  `FS_wl;
parameter M_wl   =  `M_wl;
parameter dc_wl  =  `dc_wl;


//Separate the FS information into respective information entities
assign M1         = FS[FS_wl-1: (FS_wl-M_wl)]; 
assign M2         = FS[(FS_wl-M_wl-1):(FS_wl-(2*M_wl))];
assign M1_index   = FS[(FS_wl-(M_wl*2)-1):(FS_wl-(2*M_wl)-dc_wl)];
assign Q_cum_sign = FS[0];


//Do the R select processing
assign Rsign     = Q_cum_sign ^ Qsign;
assign R_mag     = (block_num == M1_index)?M2:M1;
assign temp      = {1'b0,R_mag};
assign R_tmp     = (Rsign)?-temp:temp;
assign R         = (set_R_to_zero)?0:R_tmp;

endmodule


