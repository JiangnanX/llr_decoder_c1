/*
  
  /---Confidentiality Notice Start--/
  This document contains information proprietary to Texas A&M University, College Station.
  Any disclosure or use is expressly prohibited, except upon explicit
  written permission by Texas A&M University.
  Texas A&M University Confidential.
  Contact: Office of Technology Commercialization.
  The material contained in this software and the associated papers
  have features which are contained in a patent disclosure for LDPC decoding architectures 
  filed by Gwan Choi, John Junkins , Kiran Gunnam. 
  For more information about the licensing process, please contact:
  Alex Garcia
  Department: Licensing & Intellectual Property Management
  Title: Licensing Associate
  Email: alegarza@tamu.edu
  Phone: (979) 458-2640
  Fax: (979) 845-2684
   � Copyright 2005 to the present, Texas A&M University System. 
  All the software contained in these files are protected by United States copyright law
  and may not be reproduced, distributed, transmitted, displayed, published or broadcast 
  without the prior written permission of Texas A&M University System. 
  You may not alter or remove any trademark, copyright or other notice from copies 
  of the content.
  /---Confidentiality Notice End--/
  
  Kiran Gunnam 
  Texas A&M University, December 2005 
  File Name              : Q_Computation_Module.v
  Module Name            : Q_Computation_Module 
  Module Attributes      : 
  Design                 : Data path and Control Paths, Vector processing
  Parametrizable         : Yes
  Functionality          : 
  This module instantiates the Q subtractor array and R select logic for Rold computation.
  Qs = Pnew - Rold;
  Rold is computed on-the-fly from FS memory. 
  This module is an implementation of the layered decoding of QC-LDPC codes
  based on "On-the-fly" computation paradigm.
  Matlab Model           :	Block_LDPC_Decoder.m
  Revision History       : v1.0, Kiran Gunnam,Texas A&M University, December 2005.
*/

`include "../hdl_gen/LDPC_Decoder_Parameters.h"
module Q_Computation_Module
                       (
							   clk,
							   P_new,
							   FS_old,
							   Qsign_old,
							   block_num,
							   set_Rold_to_zero,
							   Qs,
                               d_block_s2
								);

//Inputs and Outputs
input                                  clk;
input   [`Qs_memory_bandwidth-1   : 0] P_new;
input   [`FS_memory_bandwidth-1   : 0] FS_old;
input   [`Qsign_memory_bandwidth-1: 0] Qsign_old;
input   [`dc_wl-1                 : 0] block_num;
input                                  set_Rold_to_zero;
output  [`Qs_memory_bandwidth-1   : 0] Qs;          
output  [`HD_blocksize-1          : 0]  d_block_s2;
//Internal wires and pipeline registers. 
reg     [`Qs_memory_bandwidth-1   : 0] P_new_L;          
reg     [`FS_memory_bandwidth-1   : 0] FS_old_L;
wire    [`R_bandwidth-1           : 0] R_old;
reg     [`R_bandwidth-1           : 0] R_old_L;
reg     [`Qsign_memory_bandwidth-1: 0] Qsign_old_L;
wire   [`HD_blocksize-1          : 0]  d_block_s2;
wire   [`P_wl-1:0]                    P_new_L_wire[`HD_blocksize-1:0];//for debug easy view

generate
if(`P_SHIFTER_PIPELINE==1) 
begin: PS_PD1
R_Selection_array RSA_Rold(clk,FS_old_L,Qsign_old_L,block_num,set_Rold_to_zero,R_old);//R_old = f(FS_old);
end
else 
begin
R_Selection_array RSA_Rold(clk,FS_old,Qsign_old,block_num,set_Rold_to_zero,R_old);//R_old = f(FS_old);
end
endgenerate
Q_subtractor_array QSA(P_new_L,R_old_L,Qs);//Qs= P_new_L- R_old_L;

always@(posedge clk)
 begin
 P_new_L      <= P_new;
 R_old_L      <= R_old;
 Qsign_old_L  <= Qsign_old;
 //Remember that the non-zero blocks are numbered from 0 to dc-1 and the first block is 0.
 //delayed version of block_num is used in R Select Unit for Rold
// if(block_num==0) //this kind of condition needed when we try to use single port for 
                 //FS_new write and FS_old read
   FS_old_L <= FS_old;
 end
 
 genvar k;
 generate
  for(k=0; k<`HD_blocksize; k=k+1)
	begin:Sign_Extraction
    assign P_new_L_wire[k]   = P_new_L[k*`P_wl+`P_wl-1:k*`P_wl];//for debug easy view
	assign d_block_s2[k]     = P_new_L[k*`P_wl+`P_wl-1];
	end 
 endgenerate

endmodule       
