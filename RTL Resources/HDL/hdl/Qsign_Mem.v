/*
  /---Confidentiality Notice Start--/
  Texas A&M University Confidential.
  Contact: Office of Technology Commercialization.
  The material contained in this software and the associated papers
  have features which are contained in a patent disclosure for LDPC decoding architectures 
  filed by Gwan Choi, John Junkins , Kiran Gunnam. 
  For more information about the licensing process, please contact:
  Alex Garcia
  Department: Licensing & Intellectual Property Management
  Title: Licensing Associate
  Email: alegarza@tamu.edu
  Phone: (979) 458-2640
  Fax: (979) 845-2684
   � Copyright 2005 to the present, Texas A&M University System. 
  All the software contained in these files are protected by United States copyright law
  and may not be reproduced, distributed, transmitted, displayed, published or broadcast 
  without the prior written permission of Texas A&M University System. 
  You may not alter or remove any trademark, copyright or other notice from copies 
  of the content.
  /---Confidentiality Notice End--/


  Kiran Gunnam 
  Texas A&M University 
  File Name              : Qsign_Mem.v
  Module Attributes      : Memory module for storing sign of Qs
  Design                 : 
  Parametrizable         : Yes
  Functionality          : Write Port: Sign of Qs. Read Ports 1 :    Qsign_old 
                           
Sign of Qs of dependent block is needed for Rnew: Qsign(dci)=Qs(bc) (in the case of layered decoder for regular LDPC codes, Qs is implemented as FIFO instead of memory)
Sign of Qs of current block is needed for Rold: Qsign(ci) (this can be computed as Rsign(ci) and can be stored in sign fifo for the layered decoder for regular LDPC codes.)

 Option 1: For layered decoder for regular LDPC codes, we use FIFO for storing Q and use Rsign memory to supply the sign of R old messages.
Sign of R new messages are computed in the check node unit by readin the sign of Q (i.e. the out put of Q FIFO which is the delayed version of Q input to 
CNU) and using the xor of Qsign with Q_cum_sign.
So in this case:     
                                                     For Rnew, read the Qs sign as sign(Q(bc)) (i.e. simply Q FIFO output)
                                                     For Rold, retrive the sign of R message from Rsign FIFO. 

 Option 2 :For both regular and irregular layerd decoder 
                           Qsign is stored in 1-port write and 2-port read Qsign memory.
                           For Rnew, read the Qs sign as Qsign(dci)
                           For Rold, read the Qs sign as Qsign(ci)
 Option 3: For both regular and irregular layerd decoder (preferred)
                           Note that Qsign(dci) is same as sign(Qs(bc)). So Qsign is stored in 1-port write and 1-port read Qsign memory.
                                                      For Rnew, read the Qs sign as sign(Qs(bc) (which is same as Qsign(dci))
                                                      For Rold, read the Qs sign as Qsign(ci)
  Matlab Model           :	See the Block_LDPC_Decoder.m 
  Revision History       : v1.0
  
*/

`include "../hdl_gen/LDPC_Decoder_Parameters.h"

module Qsign_mem(rd_addr1,wr_addr,clk,Qsign_in,wen,Qsign_out1);

input  [`Qsign_memory_addr_wl-1 : 0]    rd_addr1,wr_addr;
input                                   clk;
input  [`Qsign_memory_bandwidth-1 : 0]  Qsign_in;
input                                   wen;
output [`Qsign_memory_bandwidth-1 : 0]  Qsign_out1;

parameter Qsign_memory_depth    =  `Qsign_memory_depth;
parameter Qsign_memory_addr_wl  =  `Qsign_memory_addr_wl;
parameter Qsign_memory_bandwidth = `Qsign_memory_bandwidth;


//            #(width,depth,ceil(log2(depth)))
dpram_r1p_w1p #(Qsign_memory_bandwidth,Qsign_memory_depth,Qsign_memory_addr_wl)
               Qsign_mem1(wr_addr,rd_addr1,clk,Qsign_in,Qsign_out1,wen,1'b1);

endmodule


