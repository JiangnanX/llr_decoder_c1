/*
  
  /---Confidentiality Notice Start--/
  This document contains information proprietary to Texas A&M University, College Station.
  Any disclosure or use is expressly prohibited, except upon explicit
  written permission by Texas A&M University.
  Texas A&M University Confidential.
  Contact: Office of Technology Commercialization.
  The material contained in this software and the associated papers
  have features which are contained in a patent disclosure for LDPC decoding architectures 
  filed by Gwan Choi, John Junkins , Kiran Gunnam. 
  For more information about the licensing process, please contact:
  Alex Garcia
  Department: Licensing & Intellectual Property Management
  Title: Licensing Associate
  Email: alegarza@tamu.edu
  Phone: (979) 458-2640
  Fax: (979) 845-2684
   � Copyright 2005 to the present, Texas A&M University System. 
  All the software contained in these files are protected by United States copyright law
  and may not be reproduced, distributed, transmitted, displayed, published or broadcast 
  without the prior written permission of Texas A&M University System. 
  You may not alter or remove any trademark, copyright or other notice from copies 
  of the content.
  /---Confidentiality Notice End--/
  
  Kiran Gunnam 
  Texas A&M University, December 2005 
  File Name              : LUM_Control.v 
  Module Name            : LUM_Control
  Module Attributes      : 
  Design                 : Control Unit for Layered_Update_Module.v
  Parametrizable         : Yes.
                           However, need to change the re-alignment of control signals if the latencies of
									data processing blocks are changed
  Functionality          : 
  This module provides control signals to Layered_Update_Module.
  Layered_Update_Module does the Layered Update on Q messages. Instantiates P computation module,
  Q computation module, P shifter.
  P       = Qs_old+ R_new;
  P_new   = shift(P,shift_coefficent)
  Qs      = P_new - R_old;
  This module is an implementation of the layered decoding of QC-LDPC codes
  based on "On-the-fly" computation paradigm.
  Matlab Model           :	Block_LDPC_Decoder.m
  Revision History       : v1.1
  v1.1:  Changes for no idle slots.
  v1.0:  Kiran Gunnam,Texas A&M University, December 2005 
*/

`include "../hdl_gen/LDPC_Decoder_Parameters.h"
module LUM_Control
                      (
                       //decoder clock
                        clk,
                        //global reset
                        reset_n,
                        //control signals from LDPC_PCU module
								decoding_processor_busy,
                        iterations,
                        layer,
                        DL,
                        TimeSlotCounter,
                        block_num,
                        dc,
                        ts_max,
                        valid_entry,
					         UCVF,
                        //control signals from OFC module
                        BC,
                        BC_HD_mem,
					         SHIFT,
					         SHIFT_UCVF1,
					         DELTA_SHIFT_UCVF0,
                        CI,
						      DB,
                        GAMMA,    
					    //Outputs
					         TimeSlotCounter_CNU_PS,
                        block_num_CC,
                        dc_CC,
                        ts_max_CC, 
                        valid_entry_CC,
                        layer_CC, 
                        DL_CC, 
                        layer_CNU_PS,
                        layer_FSold,
                        UCVF_CC, 
                        iterations_CC, 
                        valid_entry_CNU_PS,  
					         block_num_PCM,
					         block_num_QCM,
					         block_num_CNU_PS,
                        dc_CNU_PS,
                        Qs_rd_addr, //based on BC
                        Qs_wr_addr, //based on BC
					         SHIFT_COEFF_P, //absolute shift coefficient that is applied 
					         SHIFT_COEFF_HD, //absolute shift coefficeint that is applied 
						//on P(and so the d_block_s2). this is nothing but a re-aligned version of SHIFT
                        Qsign_old_rd_addr,//based on CI
                        Qsign_wr_addr,//based on CI
                        HD_rd_addr, //based on BC
					         HD_wr_addr, //based on BC
						      HD_wen, //write enable to HD memory
						      GAMMA_CCOA,      //GAMMA to Circulant Correction array
						      set_Rnew_to_zero_PCM,
						      set_Rold_to_zero_QCM,
						      Qs_wen,
						      Qsign_wen,
						      layer_CPA,
						      FS_Wen,
						      FS_old_ren,
						      fs_new_rw_cond1  
   							);

//Inputs and Outputs
input                                  clk;
input                                  reset_n;

input                                  decoding_processor_busy;
input   [`iteration_wl-1          : 0] iterations;
input   [`H_row_wl-1              : 0] layer;                      
input   [`H_row_wl-1              : 0] DL;                      
input   [`TimeSlotCounter_wl-1    : 0] TimeSlotCounter;
input   [`dc_wl-1                 : 0] block_num;
input   [`dc_wl-1                 : 0] dc;
input   [`TimeSlotCounter_wl-1  :0]    ts_max;
input                                  valid_entry;
input                                  UCVF;
input   [`H_col_wl-1              : 0] BC,BC_HD_mem;
input   [`Shift_wl-1              : 0] SHIFT,SHIFT_UCVF1,DELTA_SHIFT_UCVF0;//Binary encoded shift coefficient
input   [`ci_wl-1                 : 0] CI;
input   [`dc_wl-1                 : 0] DB;
input   [`Gamma_wl-1              : 0] GAMMA;

				                       
output  [`TimeSlotCounter_wl-1    : 0] TimeSlotCounter_CNU_PS;
output  [`dc_wl-1                 : 0] block_num_CC;
output  [`dc_wl-1                 : 0] dc_CC;
output  [`TimeSlotCounter_wl-1  :0]    ts_max_CC;
output                                 valid_entry_CC;
output  [`H_row_wl-1              : 0] layer_CC,DL_CC,layer_CNU_PS,layer_FSold;
output  [`iteration_wl-1          : 0] iterations_CC;
output                                 valid_entry_CNU_PS;
output                                 UCVF_CC; 
output  [`dc_wl-1                 : 0] block_num_PCM,block_num_QCM,block_num_CNU_PS;
output  [`dc_wl-1                 : 0] dc_CNU_PS;
output  [`Qs_memory_addr_wl-1     : 0] Qs_wr_addr,Qs_rd_addr;
output  [`Shift_wl-1              : 0] SHIFT_COEFF_P,SHIFT_COEFF_HD;
//output  [`Qsign_memory_addr_wl-1  : 0] Qsign_old_rd_addr,Qsign_new_rd_addr,Qsign_wr_addr;
output  [`Qsign_memory_addr_wl-1  : 0] Qsign_old_rd_addr,Qsign_wr_addr;
output  [`HD_memory_addr_wl-1     : 0] HD_rd_addr,HD_wr_addr;
output                                 HD_wen;
output  [`Gamma_wl-1              : 0] GAMMA_CCOA;
output                                 set_Rnew_to_zero_PCM,set_Rold_to_zero_QCM;
output                                 Qs_wen,Qsign_wen;
output [`H_row_wl-1          :0]       layer_CPA;
output                                 FS_Wen;
output                                 FS_old_ren;
output                                 fs_new_rw_cond1;  
reg                                    HD_wen,Qs_wen;

//Internal wires and pipeline registers for control and data
reg    [`TimeSlotCounter_wl-1     : 0] TimeSlotCounter_1D,TimeSlotCounter_2D,TimeSlotCounter_3D,
                                       TimeSlotCounter_4D,TimeSlotCounter_5D,TimeSlotCounter_6D,
													TimeSlotCounter_7D,TimeSlotCounter_8D,TimeSlotCounter_9D;
reg    [`dc_wl-1                  : 0] DB_1D,block_num_1D,block_num_2D,block_num_3D,block_num_4D,
                                       block_num_5D,block_num_6D,block_num_7D,block_num_8D,
													block_num_9D; 
reg    [`Gamma_wl-1               : 0] GAMMA_1D,GAMMA_2D,GAMMA_3D,GAMMA_4D,GAMMA_5D,
                                       GAMMA_6D; 

reg                                    set_Rnew_to_zero; 
reg 								   set_Rnew_to_zero_1D;
reg                                    set_Rold_to_zero;
reg                                    set_Rold_to_zero_1D,set_Rold_to_zero_2D,
                                       set_Rold_to_zero_3D,set_Rold_to_zero_4D;
reg    [`dc_wl-1                  : 0] dc_1D,dc_2D,dc_3D,dc_4D,dc_5D,dc_6D,dc_7D,dc_8D,dc_9D;
reg    [`TimeSlotCounter_wl-1     : 0] ts_max_1D,ts_max_2D,ts_max_3D;
reg                                    valid_entry_1D,valid_entry_2D,valid_entry_3D;
reg                                    valid_entry_4D,valid_entry_5D,valid_entry_6D;
reg                                    valid_entry_7D,valid_entry_8D,valid_entry_9D;
reg    [`H_col_wl-1               : 0] BC_1D,BC_2D,BC_3D,BC_4D,BC_5D,BC_6D,BC_7D;      
reg    [`ci_wl-1                  : 0] CI_1D,CI_2D,CI_3D,CI_4D,CI_5D,CI_6D,CI_7D;

reg    [`Shift_wl-1               : 0] SHIFT_UCVF_COEFF_1D,SHIFT_UCVF_COEFF_2D,
                                       SHIFT_UCVF_COEFF_3D;
reg    [`Shift_wl-1               : 0] SHIFT_1D,SHIFT_2D,SHIFT_3D,SHIFT_4D,SHIFT_5D,SHIFT_6D;
reg [`H_row_wl-1                  : 0] layer_1D,layer_2D,layer_3D,
                                       layer_4D,layer_5D,layer_6D,
                                       layer_7D,layer_8D,layer_9D;
reg [`iteration_wl-1              : 0] iterations_1D,iterations_2D,iterations_3D;
reg                                    UCVF_1D,UCVF_2D,UCVF_3D;
reg [`H_row_wl-1                  : 0] DL_1D,DL_2D,DL_3D;
reg [`H_row_wl-1                  : 0] layer_CPA,layer_FSold;  
reg                                    FS_Wen; 
reg                                    FS_old_ren;    
reg                                    fs_new_rw_cond1;                                

always@(posedge clk)
    begin
     if(reset_n == 0) 	
	   begin	 //Initialize the TimeSlotCounter to Freeze_Slot.
		TimeSlotCounter_1D <= `Freeze_Slot;
		TimeSlotCounter_2D <= `Freeze_Slot;
		TimeSlotCounter_3D <= `Freeze_Slot;
		TimeSlotCounter_4D <= `Freeze_Slot;
		TimeSlotCounter_5D <= `Freeze_Slot;
		TimeSlotCounter_6D <= `Freeze_Slot;
		TimeSlotCounter_7D <= `Freeze_Slot;
		TimeSlotCounter_8D <= `Freeze_Slot;
		TimeSlotCounter_9D <= `Freeze_Slot;
	    set_Rold_to_zero   <=  1'b0;
	    set_Rnew_to_zero   <=  1'b0;
	    valid_entry_1D <= 1'b0;
        valid_entry_2D <= 1'b0;
        valid_entry_3D <= 1'b0;
        valid_entry_4D <= 1'b0;
        valid_entry_5D <= 1'b0;
        valid_entry_6D <= 1'b0;
        valid_entry_7D <= 1'b0;
        valid_entry_8D <= 1'b0;
        valid_entry_9D <= 1'b0;
	 	end
	else 
	begin
	 TimeSlotCounter_1D  <= TimeSlotCounter;
	 TimeSlotCounter_2D  <= TimeSlotCounter_1D;
	 TimeSlotCounter_3D  <= TimeSlotCounter_2D;
	 TimeSlotCounter_4D  <= TimeSlotCounter_3D;
	 TimeSlotCounter_5D  <= TimeSlotCounter_4D;
	 TimeSlotCounter_6D  <= TimeSlotCounter_5D;
	 TimeSlotCounter_7D  <= TimeSlotCounter_6D;
	 TimeSlotCounter_8D  <= TimeSlotCounter_7D;
	 TimeSlotCounter_9D  <= TimeSlotCounter_8D;
 
 
          
                     
	 block_num_1D        <= block_num; 
	 block_num_2D        <= block_num_1D;
	 block_num_3D        <= block_num_2D;
	 block_num_4D        <= block_num_3D;
	 block_num_5D        <= block_num_4D;
	 block_num_6D        <= block_num_5D;
	 block_num_7D        <= block_num_6D;
	 block_num_8D        <= block_num_7D;
	 block_num_9D        <= block_num_8D;
 
 	 DB_1D               <= DB;

	 set_Rnew_to_zero    <= (UCVF==1);
	 
	 set_Rold_to_zero     <= (iterations<2);
	 set_Rold_to_zero_1D <= set_Rold_to_zero;
	 set_Rold_to_zero_2D <= set_Rold_to_zero_1D;
	 set_Rold_to_zero_3D <= set_Rold_to_zero_2D;
     set_Rold_to_zero_4D <= set_Rold_to_zero_3D;

	 dc_1D               <= dc;
	 dc_2D               <= dc_1D;
	 dc_3D               <= dc_2D;
	 dc_4D               <= dc_3D;
	 dc_5D               <= dc_4D;
	 dc_6D               <= dc_5D;
	 dc_7D               <= dc_6D;
	 dc_8D               <= dc_7D;
	 dc_9D               <= dc_8D;
	 
	 ts_max_1D            <= ts_max;
	 ts_max_2D            <= ts_max_1D;
	 ts_max_3D            <= ts_max_2D;
	 
	 valid_entry_1D       <= valid_entry;
	 valid_entry_2D       <= valid_entry_1D;
     valid_entry_3D       <= valid_entry_2D;
     valid_entry_4D       <= valid_entry_3D;
     valid_entry_5D       <= valid_entry_4D;
     valid_entry_6D       <= valid_entry_5D;
     valid_entry_7D       <= valid_entry_6D;
     valid_entry_8D       <= valid_entry_7D;
     valid_entry_9D       <= valid_entry_8D;
     
     
     layer_1D            <= layer;
     layer_2D            <= layer_1D;
     layer_3D            <= layer_2D;
     layer_4D            <= layer_3D;
     layer_5D            <= layer_4D;
     layer_6D            <= layer_5D;
     layer_7D            <= layer_6D;
     layer_8D            <= layer_7D;
     layer_9D            <= layer_8D;
          
     
     DL_1D               <= DL;
     DL_2D               <= DL_1D;
     DL_3D               <= DL_2D;
     
     UCVF_1D             <= UCVF;
     UCVF_2D             <= UCVF_1D;
     UCVF_3D             <= UCVF_2D;
     
     iterations_1D       <= iterations;
     iterations_2D       <= iterations_1D;
     iterations_3D       <= iterations_2D;

	 //BC is read address for the Qs before entering the first pipeline
	 //BC is the write address for the Qs at the end of 7th pipeline stage(i.e BC_7D).
	 BC_1D               <= BC; 
	 BC_2D               <= BC_1D;
	 BC_3D               <= BC_2D;
	 BC_4D               <= BC_3D;
	 BC_5D               <= BC_4D;
	 BC_6D               <= BC_5D;
     BC_7D               <= BC_6D;

     //Note that control signal Shift need to be asserted along with the vector data 
     // Since the vector data is changed in 4th pipeline stage,
    // SHIFT_UCVF_COEFF should be supplied at the end of 3rd pipeline stage(i.e _3D)
	 SHIFT_UCVF_COEFF_1D    <= (UCVF==1)? SHIFT_UCVF1:DELTA_SHIFT_UCVF0;
     SHIFT_UCVF_COEFF_2D    <=  SHIFT_UCVF_COEFF_1D;
     SHIFT_UCVF_COEFF_3D    <=  SHIFT_UCVF_COEFF_2D;

	 SHIFT_1D            <=  SHIFT;
	 SHIFT_2D            <=  SHIFT_1D;
	 SHIFT_3D            <=  SHIFT_2D;
	 SHIFT_4D            <=  SHIFT_3D;
	 SHIFT_5D            <=  SHIFT_4D;
	 SHIFT_6D            <=  SHIFT_5D;

	 //CI is read address in the 3rd pipeline stage for Qsign
	 //(i.e CI_2D).
	 //CI is the write address for the Qs at the end of 7th pipeline stage for Qsign memory
	 //(i.e. CI_7D)
	 CI_1D               <=  CI;
	 CI_2D               <=  CI_1D;
	 CI_3D               <=  CI_2D;
	 CI_4D               <=  CI_3D;
	 CI_5D               <=  CI_4D;
	 CI_6D               <=  CI_5D;
     CI_7D               <=  CI_6D;

	 GAMMA_1D            <=  GAMMA;
	 GAMMA_2D            <=  GAMMA_1D;
	 GAMMA_3D            <=  GAMMA_2D;
	 GAMMA_4D            <=  GAMMA_3D;
	 GAMMA_5D            <=  GAMMA_4D;
	 GAMMA_6D            <=  GAMMA_5D;
	end
end

generate
if(`P_SHIFTER_PIPELINE==1)
    begin: PSHIFTER_PD1_wen_logic 
    always@(posedge clk)
        begin
         if(reset_n == 0)     
         begin
          HD_wen <= 1'b0;
          Qs_wen <= 1'b0;
         end
         else 
         begin
         //align the write enable signal for HD memory to align with the shifter output
          HD_wen <=valid_entry_3D& decoding_processor_busy;
          //align the write enable signal for Qs memory to Q computation module output
          Qs_wen <=valid_entry_5D & decoding_processor_busy;
         end
       end
   end
else
  begin: PSHIFTER_PD0_wen_logic
    always@(posedge clk)
        begin
         if(reset_n == 0)     
         begin
          HD_wen <= 1'b0;
          Qs_wen <= 1'b0;
         end
         else 
         begin
         //align the write enable signal for HD memory to align with the shifter output
         HD_wen <=valid_entry_2D& decoding_processor_busy;
         //align the write enable signal for Qs memory to Q computation module output
         Qs_wen <=valid_entry_4D & decoding_processor_busy;
                  
         end
       end
   end
 endgenerate        

generate
if(`CORRECTION_UNIT_PIPELINE==2 && `P_SHIFTER_PIPELINE==1) 
begin: CU_PD2_CNU_logic

 always@(posedge clk)
    begin
     if(reset_n == 0) 	
	   begin	 //Initialize the TimeSlotCounter to Freeze_Slot.
	    FS_Wen             <=  1'b0;
	    FS_old_ren         <=  1'b0;
        fs_new_rw_cond1<= 1'b0;
	 	end
	else 
	begin
	 if(block_num_8D==0)
	   layer_CPA     <= layer_CNU_PS;//This is write address of fs_mem
                          //latched for the layer that is being processed.FS_Wen signal 
                          //is generated when the last block is processed if fs_mem is
                          //a dual port RAM. 
      if(block_num_8D==dc_8D & valid_entry_8D) //take care of arbitrary insertion of idle cycles by using block_num instead
      //of TimeSlotCounter
      begin
        FS_Wen      <= 1'b1;
        fs_new_rw_cond1 <=(layer_CNU_PS==DL);
      end
        else begin
        FS_Wen      <= 1'b0;
        fs_new_rw_cond1 <=1'b0;
        end
          if((block_num==0 & valid_entry) & (block_num_7D==dc_7D & valid_entry_7D))
             begin
             FS_old_ren      <= 1'b1;
             layer_FSold      <= layer;
             end
           else if ((block_num_1D==0 & valid_entry_1D) &  !FS_old_ren)
             begin
             FS_old_ren       <= 1'b1;
             layer_FSold      <= layer_1D;
             end
            else
             FS_old_ren      <= 1'b0;
    end
 end
end
else if(`CORRECTION_UNIT_PIPELINE==1 && `P_SHIFTER_PIPELINE==1) 
begin: CU_PD1_CNU_logic
always@(posedge clk)
    begin
     if(reset_n == 0) 	
	   begin	 //Initialize the TimeSlotCounter to Freeze_Slot.
	    FS_Wen             <=  1'b0;
	    FS_old_ren         <=  1'b0;
        fs_new_rw_cond1<= 1'b0;
	 	end
	else 
	begin
	 if(block_num_7D==0)
	   layer_CPA     <= layer_CNU_PS;//This is write address of fs_mem
                          //latched for the layer that is being processed.FS_Wen signal 
                          //is generated when the last block is processed if fs_mem is
                          //a dual port RAM. 
      if(block_num_7D==dc_7D & valid_entry_7D) //take care of arbitrary insertion of idle cycles by using block_num instead
      //of TimeSlotCounter
      begin
        FS_Wen      <= 1'b1;
        fs_new_rw_cond1 <=(layer_CNU_PS==DL);
      end
        else begin
        FS_Wen      <= 1'b0;
        fs_new_rw_cond1 <=1'b0;
        end
          if((block_num==0 & valid_entry) & (block_num_6D==dc_6D & valid_entry_6D))
             begin
             FS_old_ren      <= 1'b1;
             layer_FSold      <= layer;
             end
           else if ((block_num_1D==0 & valid_entry_1D) &  !FS_old_ren)
             begin
             FS_old_ren       <= 1'b1;
             layer_FSold      <= layer_1D;
             end
            else
             FS_old_ren      <= 1'b0;
    end
 end
end
else if(`CORRECTION_UNIT_PIPELINE==1 && `P_SHIFTER_PIPELINE==0) 
begin: CU_PD1_PS_PD0_CNU_logic
always@(posedge clk)
    begin
     if(reset_n == 0) 	
	   begin	 //Initialize the TimeSlotCounter to Freeze_Slot.
	    FS_Wen             <=  1'b0;
	    FS_old_ren         <=  1'b0;
        fs_new_rw_cond1<= 1'b0;
	 	end
	else 
	begin
	 if(block_num_6D==0)
	   layer_CPA     <= layer_CNU_PS;//This is write address of fs_mem
                          //latched for the layer that is being processed.FS_Wen signal 
                          //is generated when the last block is processed if fs_mem is
                          //a dual port RAM. 
      if(block_num_6D==dc_6D & valid_entry_6D) //take care of arbitrary insertion of idle cycles by using block_num instead
      //of TimeSlotCounter
      begin
        FS_Wen      <= 1'b1;
        fs_new_rw_cond1 <=(layer_CNU_PS==DL);
      end
        else begin
        FS_Wen      <= 1'b0;
        fs_new_rw_cond1 <=1'b0;
        end
          if((block_num==0 & valid_entry) & (block_num_5D==dc_5D & valid_entry_5D))
             begin
             FS_old_ren      <= 1'b1; 
             layer_FSold      <= layer;
             end
           else if ((block_num_1D==0 & valid_entry_1D) &  !FS_old_ren)
             begin
             FS_old_ren       <= 1'b1;
             layer_FSold      <= layer_1D;
             end
            else
             FS_old_ren      <= 1'b0;
    end
 end
end
endgenerate

generate
if(`CORRECTION_UNIT_PIPELINE==2 && `P_SHIFTER_PIPELINE==1) 
begin: CU_PD2
    assign layer_CNU_PS           = layer_8D;                        
    assign TimeSlotCounter_CNU_PS = TimeSlotCounter_8D;
    assign valid_entry_CNU_PS     = valid_entry_8D;
    assign block_num_CNU_PS       = block_num_8D;
    assign dc_CNU_PS              = dc_8D;
end
else if(`CORRECTION_UNIT_PIPELINE==1 && `P_SHIFTER_PIPELINE==1) 
begin: CU_PD1
    assign layer_CNU_PS           = layer_7D;                        
    assign TimeSlotCounter_CNU_PS = TimeSlotCounter_7D;
    assign valid_entry_CNU_PS     = valid_entry_7D;
    assign block_num_CNU_PS       = block_num_7D;
    assign dc_CNU_PS              = dc_7D;
end
else if(`CORRECTION_UNIT_PIPELINE==1 &&`P_SHIFTER_PIPELINE==0) 
begin: CU_PD1_PS_PD0
    assign layer_CNU_PS           = layer_6D;                        
    assign TimeSlotCounter_CNU_PS = TimeSlotCounter_6D;
    assign valid_entry_CNU_PS     = valid_entry_6D;
    assign block_num_CNU_PS       = block_num_6D;
    assign dc_CNU_PS              = dc_6D;
end

endgenerate

generate
if(`P_SHIFTER_PIPELINE==1)
    begin: PSHIFTER_PD1 
    assign block_num_QCM          = block_num_4D;
    assign set_Rold_to_zero_QCM   = set_Rold_to_zero_3D;
    assign GAMMA_CCOA             = GAMMA_5D;
    assign Qs_wr_addr             = BC_6D;
    assign Qsign_wr_addr          = CI_6D;
    assign HD_wr_addr             = BC_5D;
    assign SHIFT_COEFF_HD         = SHIFT_5D; 
    end
    else
    begin: PSHIFTER_PD0 
    assign block_num_QCM          = block_num_3D;
    assign set_Rold_to_zero_QCM   = set_Rold_to_zero_2D;
    assign GAMMA_CCOA             = GAMMA_4D;
    assign Qs_wr_addr             = BC_5D;
    assign Qsign_wr_addr          = CI_5D;
    assign HD_wr_addr             = BC_4D;
    assign SHIFT_COEFF_HD         = SHIFT_4D; 
    end
endgenerate
//assign TimeSlotCounter_CC     = TimeSlotCounter_3D;
assign dc_CC                  = dc_2D;
assign ts_max_CC              = ts_max_2D;
assign valid_entry_CC         = valid_entry_2D;
assign layer_CC               = layer_2D;
assign DL_CC                  = DL_2D;
assign iterations_CC          = iterations_2D;
assign UCVF_CC                = UCVF_2D;
assign block_num_CC           = block_num_2D;
assign block_num_PCM          = DB_1D;
assign Qs_rd_addr             = BC;
assign SHIFT_COEFF_P          = SHIFT_UCVF_COEFF_2D; 
assign Qsign_old_rd_addr      = CI_2D;
assign HD_rd_addr             = BC_HD_mem;//note that there is 2-clock cycle latency through HD Buffer. So need to 
                                      //issue the read address as BC_HD_mem (earlier version of BC by 1 clock cycle)

assign set_Rnew_to_zero_PCM   = set_Rnew_to_zero;
assign Qsign_wen              = Qs_wen&decoding_processor_busy; 


endmodule       