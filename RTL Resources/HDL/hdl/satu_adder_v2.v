/*This is a saturated adder for 2's compliment numbers
  Kiran Gunnam 
  Texas A&M University
*/  
module satu_adder_v2(q,b,a);

parameter q_wl = 6;   
parameter b_wl = 6;
parameter a_wl = 5;

input  signed  [a_wl-1:0] a;
input  signed  [b_wl-1:0] b;
output signed  [q_wl-1:0] q;

wire   signed  [q_wl-1:0] y;
wire           [q_wl-2:0] temp;

//satu_adder_v2x sa(a,b,OVFL,y);
assign y = a + b;
assign OVFL = ~(a[a_wl-1] ^ b[b_wl-1]) & (a[a_wl-1] ^ y[q_wl-1]);

genvar k;
generate
 for (k=0; k < q_wl-1; k=k+1) 
   begin: saturate_mag
     assign temp[k] = ~a[a_wl-1];
    end
endgenerate

assign q = (OVFL)?{a[a_wl-1],temp}:y;
  
endmodule