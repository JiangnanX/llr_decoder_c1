-------------------------------------------------------------------------------
--                                                                           --
-- Fichier         : rom_pkg.vhd                                             --
-- Fichiers utiles :                                                         --
--                                                                           --
--                                                                           --
-- Auteurs         : J_L DANGER H LAAMARI                                    --
-- Projet          : WGNG                                                    --
--                                                                           --
-- date de creation: 10/04/2000                                              --
--                                                                           --
-- Description     : Definition of the ROMs of the design                    --
--                                                                           --
-------------------------------------------------------------------------------
-- Modification : J-L Danger contenu des ROM ln(x) 23/8/00                   -- 
-------------------------------------------------------------------------------
-- Copyrigth LESTER, Universite de Bretagne Sud, 2001                        --
--           ENST Paris                                                      --
-- This software is distributed under a GPL licence                          --
-------------------------------------------------------------------------------

LIBRARY IEEE;
USE ieee.std_logic_1164.all;


PACKAGE rom_pkg is 

-------------------------------------------------------------------------------
-- Declaration of the rom_x of the design. The content of the ROM are        --
-- given by the matlab source file "gauss_param.m"                           --
-------------------------------------------------------------------------------

CONSTANT Null_vector :  std_logic_vector(14 DOWNTO 0) := "000000000000000";

FUNCTION rom_1(adr : std_logic_vector(3 DOWNTO 0)) RETURN std_logic_vector;

FUNCTION rom_2(adr : std_logic_vector(3 DOWNTO 0)) RETURN std_logic_vector;

FUNCTION rom_3(adr : std_logic_vector(3 DOWNTO 0)) RETURN std_logic_vector;

FUNCTION rom_4(adr : std_logic_vector(3 DOWNTO 0)) RETURN std_logic_vector;

FUNCTION rom_5(adr : std_logic_vector(3 DOWNTO 0)) RETURN std_logic_vector;

FUNCTION rom_cosine(adr : std_logic_vector(7 DOWNTO 0)) RETURN std_logic_vector;

END rom_pkg;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- PACKAGE BODY                                                              --
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
PACKAGE BODY rom_pkg IS


FUNCTION rom_1(adr : std_logic_vector(3 DOWNTO 0)) RETURN std_logic_vector IS
  VARIABLE v_log : std_logic_vector(8 downto 0);
 
  BEGIN
        CASE adr IS
          
          WHEN "0001" => v_log := "011000101";  
          WHEN "0010" => v_log := "010101111";
          WHEN "0011" => v_log := "010011110";
          WHEN "0100" => v_log := "010010000";
          WHEN "0101" => v_log := "010000100";
          WHEN "0110" => v_log := "001111001";
          WHEN "0111" => v_log := "001101111";
          WHEN "1000" => v_log := "001100110";
          WHEN "1001" => v_log := "001011100";
          WHEN "1010" => v_log := "001010011";
          WHEN "1011" => v_log := "001001001";
          WHEN "1100" => v_log := "000111111";
          WHEN "1101" => v_log := "000110101";
          WHEN "1110" => v_log := "000101000";
          WHEN "1111" => v_log := "000010111";
          WHEN OTHERS => v_log := "000000000";
     
        END CASE;
        RETURN v_log;
   END;

FUNCTION rom_2(adr : std_logic_vector(3 DOWNTO 0)) RETURN std_logic_vector IS

VARIABLE v_log : std_logic_vector(8 downto 0);

     BEGIN

        CASE adr IS
          
          WHEN "0001" => v_log := "100100010";
          WHEN "0010" => v_log := "100010011";
          WHEN "0011" => v_log := "100001001";
          WHEN "0100" => v_log := "100000001";
          WHEN "0101" => v_log := "011111011";
          WHEN "0110" => v_log := "011110101";
          WHEN "0111" => v_log := "011110000";
          WHEN "1000" => v_log := "011101100";
          WHEN "1001" => v_log := "011101000";
          WHEN "1010" => v_log := "011100100";
          WHEN "1011" => v_log := "011100001";
          WHEN "1100" => v_log := "011011110";
          WHEN "1101" => v_log := "011011011";
          WHEN "1110" => v_log := "011011000";
          WHEN "1111" => v_log := "011010110";
          WHEN OTHERS => v_log := "000000000";
     
        END CASE;
        RETURN v_log;
   END;     

FUNCTION rom_3(adr : std_logic_vector(3 DOWNTO 0)) RETURN std_logic_vector IS
VARIABLE v_log : std_logic_vector(8 downto 0);

 BEGIN
        CASE adr IS
          
          WHEN "0001" => v_log := "101101000";
          WHEN "0010" => v_log := "101011100";
          WHEN "0011" => v_log := "101010100";
          WHEN "0100" => v_log := "101001110";
          WHEN "0101" => v_log := "101001001";
          WHEN "0110" => v_log := "101000101";
          WHEN "0111" => v_log := "101000001";
          WHEN "1000" => v_log := "100111110";
          WHEN "1001" => v_log := "100111011";
          WHEN "1010" => v_log := "100111000";
          WHEN "1011" => v_log := "100110110";
          WHEN "1100" => v_log := "100110100";
          WHEN "1101" => v_log := "100110010";
          WHEN "1110" => v_log := "100110000";
          WHEN "1111" => v_log := "100101110";
          WHEN OTHERS => v_log := "000000000";
     
        END CASE;
        RETURN v_log;
   END;

FUNCTION rom_4(adr : std_logic_vector(3 DOWNTO 0)) RETURN std_logic_vector IS
VARIABLE v_log : std_logic_vector(8 downto 0);

 BEGIN
        CASE adr IS
          
          WHEN "0001" => v_log := "110100010";
          WHEN "0010" => v_log := "110011000";
          WHEN "0011" => v_log := "110010001";
          WHEN "0100" => v_log := "110001100";
          WHEN "0101" => v_log := "110001000";
          WHEN "0110" => v_log := "110000100";
          WHEN "0111" => v_log := "110000001";
          WHEN "1000" => v_log := "101111111";
          WHEN "1001" => v_log := "101111100";
          WHEN "1010" => v_log := "101111010";
          WHEN "1011" => v_log := "101111000";
          WHEN "1100" => v_log := "101110110";
          WHEN "1101" => v_log := "101110100";
          WHEN "1110" => v_log := "101110011";
          WHEN "1111" => v_log := "101110001";
          WHEN OTHERS => v_log := "000000000";
     
        END CASE;
        RETURN v_log;
   END;

FUNCTION rom_5(adr : std_logic_vector(3 DOWNTO 0)) RETURN std_logic_vector IS
VARIABLE v_log : std_logic_vector(8 downto 0);

 BEGIN
        CASE adr IS
          
          WHEN "0001" => v_log := "111010101";
          WHEN "0010" => v_log := "111001100";
          WHEN "0011" => v_log := "111000110";
          WHEN "0100" => v_log := "111000010";
          WHEN "0101" => v_log := "110111110";
          WHEN "0110" => v_log := "110111011";
          WHEN "0111" => v_log := "110111000";
          WHEN "1000" => v_log := "110110110";
          WHEN "1001" => v_log := "110110100";
          WHEN "1010" => v_log := "110110010";
          WHEN "1011" => v_log := "110110000";
          WHEN "1100" => v_log := "110101111";
          WHEN "1101" => v_log := "110101101";
          WHEN "1110" => v_log := "110101100";
          WHEN "1111" => v_log := "110101010";
          WHEN OTHERS => v_log := "000000000";
     
        END CASE;
        RETURN v_log;
   END;

FUNCTION rom_cosine(adr : std_logic_vector(7 DOWNTO 0)) RETURN std_logic_vector IS

   VARIABLE v_cos : std_logic_vector(7 downto 0);
  
  BEGIN 
          
	       CASE adr IS

          WHEN "00000001" => v_cos :=  "10110101";
          WHEN "00000010" => v_cos :=  "10110101";
          WHEN "00000011" => v_cos :=  "10110100";
          WHEN "00000100" => v_cos :=  "10110100";
          WHEN "00000101" => v_cos :=  "10110100";
          WHEN "00000110" => v_cos :=  "10110100";
          WHEN "00000111" => v_cos :=  "10110100";
          WHEN "00001000" => v_cos :=  "10110100";
          WHEN "00001001" => v_cos :=  "10110100";
          WHEN "00001010" => v_cos :=  "10110100";
          WHEN "00001011" => v_cos :=  "10110100";
          WHEN "00001100" => v_cos :=  "10110100";
          WHEN "00001101" => v_cos :=  "10110100";
          WHEN "00001110" => v_cos :=  "10110100";
          WHEN "00001111" => v_cos :=  "10110100";
          WHEN "00010000" => v_cos :=  "10110100";
          WHEN "00010001" => v_cos :=  "10110100";
          WHEN "00010010" => v_cos :=  "10110011";
          WHEN "00010011" => v_cos :=  "10110011";
          WHEN "00010100" => v_cos :=  "10110011";
          WHEN "00010101" => v_cos :=  "10110011";
          WHEN "00010110" => v_cos :=  "10110011";
          WHEN "00010111" => v_cos :=  "10110011";
          WHEN "00011000" => v_cos :=  "10110011";
          WHEN "00011001" => v_cos :=  "10110010";
          WHEN "00011010" => v_cos :=  "10110010";
          WHEN "00011011" => v_cos :=  "10110010";
          WHEN "00011100" => v_cos :=  "10110010";
          WHEN "00011101" => v_cos :=  "10110010";
          WHEN "00011110" => v_cos :=  "10110010";
          WHEN "00011111" => v_cos :=  "10110001";
          WHEN "00100000" => v_cos :=  "10110001";
          WHEN "00100001" => v_cos :=  "10110001";
          WHEN "00100010" => v_cos :=  "10110001";
          WHEN "00100011" => v_cos :=  "10110000";
          WHEN "00100100" => v_cos :=  "10110000";
          WHEN "00100101" => v_cos :=  "10110000";
          WHEN "00100110" => v_cos :=  "10110000";
          WHEN "00100111" => v_cos :=  "10101111";
          WHEN "00101000" => v_cos :=  "10101111";
          WHEN "00101001" => v_cos :=  "10101111";
          WHEN "00101010" => v_cos :=  "10101111";
          WHEN "00101011" => v_cos :=  "10101110";
          WHEN "00101100" => v_cos :=  "10101110";
          WHEN "00101101" => v_cos :=  "10101110";
          WHEN "00101110" => v_cos :=  "10101110";
          WHEN "00101111" => v_cos :=  "10101101";
          WHEN "00110000" => v_cos :=  "10101101";
          WHEN "00110001" => v_cos :=  "10101101";
          WHEN "00110010" => v_cos :=  "10101100";
          WHEN "00110011" => v_cos :=  "10101100";
          WHEN "00110100" => v_cos :=  "10101100";
          WHEN "00110101" => v_cos :=  "10101011";
          WHEN "00110110" => v_cos :=  "10101011";
          WHEN "00110111" => v_cos :=  "10101010";
          WHEN "00111000" => v_cos :=  "10101010";
          WHEN "00111001" => v_cos :=  "10101010";
          WHEN "00111010" => v_cos :=  "10101001";
          WHEN "00111011" => v_cos :=  "10101001";
          WHEN "00111100" => v_cos :=  "10101001";
          WHEN "00111101" => v_cos :=  "10101000";
          WHEN "00111110" => v_cos :=  "10101000";
          WHEN "00111111" => v_cos :=  "10100111";
          WHEN "01000000" => v_cos :=  "10100111";
          WHEN "01000001" => v_cos :=  "10100111";
          WHEN "01000010" => v_cos :=  "10100110";
          WHEN "01000011" => v_cos :=  "10100110";
          WHEN "01000100" => v_cos :=  "10100101";
          WHEN "01000101" => v_cos :=  "10100101";
          WHEN "01000110" => v_cos :=  "10100100";
          WHEN "01000111" => v_cos :=  "10100100";
          WHEN "01001000" => v_cos :=  "10100011";
          WHEN "01001001" => v_cos :=  "10100011";
          WHEN "01001010" => v_cos :=  "10100010";
          WHEN "01001011" => v_cos :=  "10100010";
          WHEN "01001100" => v_cos :=  "10100001";
          WHEN "01001101" => v_cos :=  "10100001";
          WHEN "01001110" => v_cos :=  "10100000";
          WHEN "01001111" => v_cos :=  "10100000";
          WHEN "01010000" => v_cos :=  "10011111";
          WHEN "01010001" => v_cos :=  "10011111";
          WHEN "01010010" => v_cos :=  "10011110";
          WHEN "01010011" => v_cos :=  "10011110";
          WHEN "01010100" => v_cos :=  "10011101";
          WHEN "01010101" => v_cos :=  "10011101";
          WHEN "01010110" => v_cos :=  "10011100";
          WHEN "01010111" => v_cos :=  "10011100";
          WHEN "01011000" => v_cos :=  "10011011";
          WHEN "01011001" => v_cos :=  "10011010";
          WHEN "01011010" => v_cos :=  "10011010";
          WHEN "01011011" => v_cos :=  "10011001";
          WHEN "01011100" => v_cos :=  "10011001";
          WHEN "01011101" => v_cos :=  "10011000";
          WHEN "01011110" => v_cos :=  "10011000";
          WHEN "01011111" => v_cos :=  "10010111";
          WHEN "01100000" => v_cos :=  "10010110";
          WHEN "01100001" => v_cos :=  "10010110";
          WHEN "01100010" => v_cos :=  "10010101";
          WHEN "01100011" => v_cos :=  "10010100";
          WHEN "01100100" => v_cos :=  "10010100";
          WHEN "01100101" => v_cos :=  "10010011";
          WHEN "01100110" => v_cos :=  "10010011";
          WHEN "01100111" => v_cos :=  "10010010";
          WHEN "01101000" => v_cos :=  "10010001";
          WHEN "01101001" => v_cos :=  "10010001";
          WHEN "01101010" => v_cos :=  "10010000";
          WHEN "01101011" => v_cos :=  "10001111";
          WHEN "01101100" => v_cos :=  "10001111";
          WHEN "01101101" => v_cos :=  "10001110";
          WHEN "01101110" => v_cos :=  "10001101";
          WHEN "01101111" => v_cos :=  "10001100";
          WHEN "01110000" => v_cos :=  "10001100";
          WHEN "01110001" => v_cos :=  "10001011";
          WHEN "01110010" => v_cos :=  "10001010";
          WHEN "01110011" => v_cos :=  "10001010";
          WHEN "01110100" => v_cos :=  "10001001";
          WHEN "01110101" => v_cos :=  "10001000";
          WHEN "01110110" => v_cos :=  "10000111";
          WHEN "01110111" => v_cos :=  "10000111";
          WHEN "01111000" => v_cos :=  "10000110";
          WHEN "01111001" => v_cos :=  "10000101";
          WHEN "01111010" => v_cos :=  "10000101";
          WHEN "01111011" => v_cos :=  "10000100";
          WHEN "01111100" => v_cos :=  "10000011";
          WHEN "01111101" => v_cos :=  "10000010";
          WHEN "01111110" => v_cos :=  "10000001";
          WHEN "01111111" => v_cos :=  "10000001"; 
          WHEN "10000000" => v_cos :=  "10000000"; 
          WHEN "10000001" => v_cos :=  "01111111"; 
          WHEN "10000010" => v_cos :=  "01111110"; 
          WHEN "10000011" => v_cos :=  "01111110"; 
          WHEN "10000100" => v_cos :=  "01111101"; 
          WHEN "10000101" => v_cos :=  "01111100"; 
          WHEN "10000110" => v_cos :=  "01111011"; 
          WHEN "10000111" => v_cos :=  "01111010"; 
          WHEN "10001000" => v_cos :=  "01111001"; 
          WHEN "10001001" => v_cos :=  "01111001"; 
          WHEN "10001010" => v_cos :=  "01111000"; 
          WHEN "10001011" => v_cos :=  "01110111"; 
          WHEN "10001100" => v_cos :=  "01110110"; 
          WHEN "10001101" => v_cos :=  "01110101"; 
          WHEN "10001110" => v_cos :=  "01110100"; 
          WHEN "10001111" => v_cos :=  "01110100"; 
          WHEN "10010000" => v_cos :=  "01110011"; 
          WHEN "10010001" => v_cos :=  "01110010"; 
          WHEN "10010010" => v_cos :=  "01110001"; 
          WHEN "10010011" => v_cos :=  "01110000"; 
          WHEN "10010100" => v_cos :=  "01101111"; 
          WHEN "10010101" => v_cos :=  "01101110"; 
          WHEN "10010110" => v_cos :=  "01101110"; 
          WHEN "10010111" => v_cos :=  "01101101"; 
          WHEN "10011000" => v_cos :=  "01101100"; 
          WHEN "10011001" => v_cos :=  "01101011"; 
          WHEN "10011010" => v_cos :=  "01101010"; 
          WHEN "10011011" => v_cos :=  "01101001"; 
          WHEN "10011100" => v_cos :=  "01101000"; 
          WHEN "10011101" => v_cos :=  "01100111"; 
          WHEN "10011110" => v_cos :=  "01100110"; 
          WHEN "10011111" => v_cos :=  "01100101"; 
          WHEN "10100000" => v_cos :=  "01100101"; 
          WHEN "10100001" => v_cos :=  "01100100";
          WHEN "10100010" => v_cos :=  "01100011";
          WHEN "10100011" => v_cos :=  "01100010";
          WHEN "10100100" => v_cos :=  "01100001";
          WHEN "10100101" => v_cos :=  "01100000";
          WHEN "10100110" => v_cos :=  "01011111";
          WHEN "10100111" => v_cos :=  "01011110";
          WHEN "10101000" => v_cos :=  "01011101";
          WHEN "10101001" => v_cos :=  "01011100";
          WHEN "10101010" => v_cos :=  "01011011";
          WHEN "10101011" => v_cos :=  "01011010";
          WHEN "10101100" => v_cos :=  "01011001";
          WHEN "10101101" => v_cos :=  "01011000";
          WHEN "10101110" => v_cos :=  "01010111";
          WHEN "10101111" => v_cos :=  "01010110";
          WHEN "10110000" => v_cos :=  "01010101";
          WHEN "10110001" => v_cos :=  "01010100";
          WHEN "10110010" => v_cos :=  "01010011";
          WHEN "10110011" => v_cos :=  "01010010";
          WHEN "10110100" => v_cos :=  "01010001";
          WHEN "10110101" => v_cos :=  "01010000";
          WHEN "10110110" => v_cos :=  "01001111";
          WHEN "10110111" => v_cos :=  "01001110";
          WHEN "10111000" => v_cos :=  "01001101";
          WHEN "10111001" => v_cos :=  "01001100";
          WHEN "10111010" => v_cos :=  "01001011";
          WHEN "10111011" => v_cos :=  "01001010";
          WHEN "10111100" => v_cos :=  "01001001";
          WHEN "10111101" => v_cos :=  "01001000";
          WHEN "10111110" => v_cos :=  "01000111";
          WHEN "10111111" => v_cos :=  "01000110";
          WHEN "11000000" => v_cos :=  "01000101";
          WHEN "11000001" => v_cos :=  "01000100";
          WHEN "11000010" => v_cos :=  "01000011";
          WHEN "11000011" => v_cos :=  "01000010";
          WHEN "11000100" => v_cos :=  "01000001";
          WHEN "11000101" => v_cos :=  "01000000";
          WHEN "11000110" => v_cos :=  "00111111";
          WHEN "11000111" => v_cos :=  "00111110";
          WHEN "11001000" => v_cos :=  "00111101";
          WHEN "11001001" => v_cos :=  "00111100";
          WHEN "11001010" => v_cos :=  "00111011";
          WHEN "11001011" => v_cos :=  "00111010";
          WHEN "11001100" => v_cos :=  "00111001";
          WHEN "11001101" => v_cos :=  "00111000";
          WHEN "11001110" => v_cos :=  "00110111";
          WHEN "11001111" => v_cos :=  "00110110";
          WHEN "11010000" => v_cos :=  "00110101";
          WHEN "11010001" => v_cos :=  "00110100";
          WHEN "11010010" => v_cos :=  "00110010";
          WHEN "11010011" => v_cos :=  "00110001";
          WHEN "11010100" => v_cos :=  "00110000";
          WHEN "11010101" => v_cos :=  "00101111";
          WHEN "11010110" => v_cos :=  "00101110";
          WHEN "11010111" => v_cos :=  "00101101"; 
          WHEN "11011000" => v_cos :=  "00101100"; 
          WHEN "11011001" => v_cos :=  "00101011"; 
          WHEN "11011010" => v_cos :=  "00101010"; 
          WHEN "11011011" => v_cos :=  "00101001"; 
          WHEN "11011100" => v_cos :=  "00101000";
          WHEN "11011101" => v_cos :=  "00100111";
          WHEN "11011110" => v_cos :=  "00100110";
          WHEN "11011111" => v_cos :=  "00100100";
          WHEN "11100000" => v_cos :=  "00100011";
          WHEN "11100001" => v_cos :=  "00100010";
          WHEN "11100010" => v_cos :=  "00100001";
          WHEN "11100011" => v_cos :=  "00100000";
          WHEN "11100100" => v_cos :=  "00011111";
          WHEN "11100101" => v_cos :=  "00011110";
          WHEN "11100110" => v_cos :=  "00011101";
          WHEN "11100111" => v_cos :=  "00011100";
          WHEN "11101000" => v_cos :=  "00011011";
          WHEN "11101001" => v_cos :=  "00011010";
          WHEN "11101010" => v_cos :=  "00011000";
          WHEN "11101011" => v_cos :=  "00010111";
          WHEN "11101100" => v_cos :=  "00010110";
          WHEN "11101101" => v_cos :=  "00010101";
          WHEN "11101110" => v_cos :=  "00010100";
          WHEN "11101111" => v_cos :=  "00010011";
          WHEN "11110000" => v_cos :=  "00010010";
          WHEN "11110001" => v_cos :=  "00010001";
          WHEN "11110010" => v_cos :=  "00010000";
          WHEN "11110011" => v_cos :=  "00001110";
          WHEN "11110100" => v_cos :=  "00001101";
          WHEN "11110101" => v_cos :=  "00001100";
          WHEN "11110110" => v_cos :=  "00001011";
          WHEN "11110111" => v_cos :=  "00001010";
          WHEN "11111000" => v_cos :=  "00001001";
          WHEN "11111001" => v_cos :=  "00001000";
          WHEN "11111010" => v_cos :=  "00000111";
          WHEN "11111011" => v_cos :=  "00000110";
          WHEN "11111100" => v_cos :=  "00000100";
          WHEN "11111101" => v_cos :=  "00000011";
          WHEN "11111110" => v_cos :=  "00000010";
          WHEN "11111111" => v_cos :=  "00000001"; 
          WHEN OTHERS     => v_cos :=  "00000000"; 

    END CASE;
    RETURN v_cos;
END;

END rom_pkg;


