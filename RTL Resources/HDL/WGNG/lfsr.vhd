-------------------------------------------------------------------------------
--                                                                           --
-- File            : lfsr.vhd                                                --
-- Related file    : lfsr_pkg.vhd                                            --
--                                                                           --
--                                                                           --
-- Auteurs         : J-L DANGER, E. Boutillon                                --
-- Projet          : WGNG                                                    --
--                                                                           --
-- date de creation: 6/04/2000                                               --
--                                                                           --
-- Description     : lfsr generator                                          --
--                                                                           --
-------------------------------------------------------------------------------
-- Modification :  E. Boutillon, Oct 2001                                    --
--                 Addition of signals config and seed                       --
-------------------------------------------------------------------------------
-- Copyrigth LESTER, Universite de Bretagne Sud, 2001                        --
--           ENST Paris                                                      --
-- This software is distributed under a GPL licence                          --
-------------------------------------------------------------------------------
LIBRARY IEEE;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE work.lfsr_pkg.all;

 
ENTITY  lfsr  IS 
	PORT(
		clk        : IN  std_logic;
		rst        : IN  std_logic;
		config     : IN  std_logic;                     -- if '1', loading of the seed
		seed       : IN  std_logic;                     -- input seed
       	rbv        : OUT std_logic_vector(28 DOWNTO 0)  -- random binary variable
	);

END  lfsr;

-------------------------------------------------------------------------------
--  Architecture                                                             --
-------------------------------------------------------------------------------
ARCHITECTURE rtl OF lfsr IS


-- LFSRs with there size

SIGNAL lfsr_cos1     : std_logic_vector(21 DOWNTO 0);
SIGNAL lfsr_cos2     : std_logic_vector(20 DOWNTO 0);
SIGNAL lfsr_ln1      : std_logic_vector(19 DOWNTO 0);
SIGNAL lfsr_ln2      : std_logic_vector(16 DOWNTO 0);
SIGNAL lfsr_ln3      : std_logic_vector(12 DOWNTO 0);
SIGNAL lfsr_ln4      : std_logic_vector(6 DOWNTO 0);
SIGNAL lfsr_ln5      : std_logic_vector(4 DOWNTO 0);
SIGNAL lfsr_signe    : std_logic_vector(14 DOWNTO 0);


BEGIN

generation : PROCESS(clk, rst)

BEGIN

IF rst='0' THEN
	lfsr_cos1  <= "1111000010100000101110";
	lfsr_cos2  <= "010110100111010000001";
	lfsr_ln1   <= "10011111110001000110";
	lfsr_ln2   <= "00000001000010111";
	lfsr_ln3   <= "1111101100000";
	lfsr_ln4   <= "0100110";
	lfsr_ln5   <= "00010";
	lfsr_signe <= "010000100000100";
	 
ELSIF (clk'event and clk='1') THEN

lfsr_cos1 <= gen_lfsr(
      pol      => lfsr_cos1,
	en       => '1',
      nb_iter  => 4,
	config   => config,
	seed     => seed);

lfsr_cos2 <= gen_lfsr(
      pol        => lfsr_cos2,
	en       => '1',
      nb_iter  => 4,
	config   => config,
	seed     => lfsr_cos1(lfsr_cos1'length - 1));

lfsr_ln1 <= gen_lfsr(
      pol      => lfsr_ln1,
	en       => '1',
      nb_iter  => 4,
	config   => config,
	seed     => lfsr_cos2(lfsr_cos2'length - 1));

lfsr_ln2 <= gen_lfsr(
      pol      => lfsr_ln2,
	en       => '1',
      nb_iter  => 4,
	config   => config,
	seed     => lfsr_ln1(lfsr_ln1'length - 1));

lfsr_ln3 <= gen_lfsr(
      pol      => lfsr_ln3,
	en       => '1',
      nb_iter  => 4,
	config   => config,
	seed     => lfsr_ln2(lfsr_ln2'length - 1));

lfsr_ln4 <= gen_lfsr(
      pol      => lfsr_ln4,
	en       => '1',
      nb_iter  => 4,
	config   => config,
	seed     => lfsr_ln3(lfsr_ln3'length - 1));

lfsr_ln5 <= gen_lfsr(
      pol      => lfsr_ln5,
	en       => '1',
      nb_iter  => 4,
	config   => config,
	seed     => lfsr_ln4(lfsr_ln4'length - 1));

lfsr_signe <= gen_lfsr(
      pol      => lfsr_signe,
	en       => '1',
      nb_iter  => 1,
	config   => config,
	seed     => lfsr_ln5(lfsr_ln5'length - 1));

END IF;

END PROCESS generation;

rbv(3 DOWNTO 0)   <= lfsr_cos1(lfsr_cos1'length-1 DOWNTO lfsr_cos1'length-4);
rbv(7 DOWNTO 4)   <= lfsr_cos2(lfsr_cos2'length-1 DOWNTO lfsr_cos2'length-4);
rbv(11 DOWNTO 8)  <= lfsr_ln1(lfsr_ln1'length-1 DOWNTO lfsr_ln1'length-4);
rbv(15 DOWNTO 12) <= lfsr_ln2(lfsr_ln2'length-1 DOWNTO lfsr_ln2'length-4);
rbv(19 DOWNTO 16) <= lfsr_ln3(lfsr_ln3'length-1 DOWNTO lfsr_ln3'length-4);
rbv(23 DOWNTO 20) <= lfsr_ln4(lfsr_ln4'length-1 DOWNTO lfsr_ln4'length-4);
rbv(27 DOWNTO 24) <= lfsr_ln5(lfsr_ln5'length-1 DOWNTO lfsr_ln5'length-4);
rbv(28)           <= lfsr_signe(lfsr_signe'length-1);

END rtl;




