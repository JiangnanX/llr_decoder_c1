-------------------------------------------------------------------------------
--                                                                           --
--  FILE          :  wgng.vhd                                                --
--  Related files :  lfsr.vhd, rom_cos.vhd, rom_ln.vhd, mult9x8.vhd, accu.vhd--
--                     rom_pkg.vhd, lfsr_pkg.vhd                             --
--                                                                           --
--  Author(s)     :  J-L Danger E. Boutillon                                 --
--                                                                           --
--  Project       :  WGNG                                                    --
--                                                                           --
--  Creation Date :  15-03-00                                                --
--                                                                           --
--  Description   :  Top level of the White Gaussian Noise Generator         --
-------------------------------------------------------------------------------
--  Modifications :  E. Boutillon, Nov. 2001                                 --
--                   addition of signals to set a specific seed to the LFSR  --
--                       When config = '1', the LFSRs are wired as a shift   --
--                       register with the input seed                        --
--                       When config = '0', one gaussian sample is given     --
--                       every 4 cycles on output port "wgn"                 --  
-------------------------------------------------------------------------------
-- Copyrigth LESTER, Universite de Bretagne Sud, 2001                        --
--           ENST Paris                                                      --
-- This software is distributed under a GPL licence                          --
-------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.all; 
USE IEEE.std_logic_arith.all;
USE work.lfsr_pkg.all;
USE work.rom_pkg.all;


ENTITY wgng IS       
       PORT (
  	         clk         : IN  std_logic;
		   rst         : IN  std_logic;
		   config      : IN  std_logic;
		   seed        : IN  std_logic;
         	   wgn         : OUT std_logic_vector(9 downto 0)
		 );

END wgng;
             
                                                 
-------------------------------------------------------------------------------
--  Architecture                                                             --
-------------------------------------------------------------------------------
ARCHITECTURE rtl OF wgng IS

SIGNAL rbv        : std_logic_vector(28 downto 0);  -- random binary variables
SIGNAL Q_cos_v    : std_logic_vector( 7 downto 0);  -- Quantized cosine value
SIGNAL Q_ln_v     : std_logic_vector( 8 downto 0);  -- Quantized logarithm value
SIGNAL wgn_s      : std_logic_vector( 9 downto 0);  -- One Quantized gaussian sample

COMPONENT   lfsr
	PORT(
		clk        : IN  std_logic;
		rst        : IN  std_logic;
		config     : IN  std_logic;
		seed       : IN  std_logic;
       	rbv        : OUT std_logic_vector(28 downto 0)
	);
END  COMPONENT;

COMPONENT rom_cos  
	PORT(
		clk      : IN  std_logic;
		rst      : IN  std_logic;
       	rbv      : IN std_logic_vector(7 DOWNTO 0);
            Q_cos_v  : OUT std_logic_vector(7 DOWNTO 0)
	    ); 
END COMPONENT;

COMPONENT rom_ln  
	PORT(
		clk      : IN  std_logic;
		rst      : IN  std_logic;
       	rbv      : IN std_logic_vector(19 DOWNTO 0);
            Q_ln_v   : OUT std_logic_vector(8 DOWNTO 0)
	    ); 
END COMPONENT;

COMPONENT mult9x8 
	PORT( 
		clk      : IN  std_logic;
		rst      : IN  std_logic;
		Q_cos_v : IN std_logic_vector(7 DOWNTO 0);
		Q_ln_v  : IN std_logic_vector(8 DOWNTO 0);
		wgn_s   : OUT std_logic_vector(9 DOWNTO 0)
		);
END COMPONENT;

COMPONENT  accu
	PORT(
		rst    : IN std_logic;
		clk    : IN std_logic;
		sign   : IN std_logic;
     	      wgn    : IN std_logic_vector(9 DOWNTO 0);
            wgn_a  : OUT std_logic_vector(9 DOWNTO 0)
	    ); 
END  COMPONENT;


        
BEGIN
   
INST_lfsr   : lfsr  PORT MAP 
					(clk    => clk,
					 rst    => rst,
					 config => config,
					 seed   => seed,
					 rbv    => rbv);

inst_rom_ln : rom_ln PORT MAP 
					(clk    => clk,
					 rst    => rst,
					 rbv    => rbv(27 DOWNTO 8),
					 Q_ln_v => Q_ln_v);

inst_rom_cos : rom_cos PORT MAP 
					(clk    => clk,
					 rst    => rst,
					 rbv    => rbv(7 DOWNTO 0),
					 Q_cos_v => Q_cos_v);


inst_mult    : mult9x8 PORT MAP 
					(clk     => clk, 
					 rst     => rst,
					 Q_cos_v => Q_cos_v,
					 Q_ln_v  => Q_ln_v,
					 wgn_s   => wgn_s);
    

inst_accu  : accu PORT MAP
					(rst    => rst,
					 clk    => clk,
					 sign   => rbv(28),
					 wgn    => wgn_s,
			             wgn_a  => wgn); 

END rtl;














