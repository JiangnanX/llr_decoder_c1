-------------------------------------------------------------------------------
--                                                                           --
-- Fichier         : lfsr_pkg.vhd                                            --
-- Fichiers utiles :                                                         --
--                                                                           --
--                                                                           --
-- Auteurs         : J-L Danger                                             --
-- Projet          : noise generator                                         --
--                                                                           --
-- date de creation: 6/04/2000                                               --
--                                                                           --
-- Description     : package of lfsr generator                               --
--                                                                           --
-------------------------------------------------------------------------------
-- Modification :                                                            --
-------------------------------------------------------------------------------
-- Copyrigth LESTER, Universite de Bretagne Sud, 2001                        --
--           ENST Paris                                                      --
-- This software is distributed under a GPL licence                          --
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;

PACKAGE lfsr_pkg IS


-------------------------------------------------------------------------------
-- Function gen_lfsr                                                         --
--    Input :                                                                -- 
--          pol     : input polynomial                                       --
--          en      : enable, null function if en = '0'                      --
--          nb_iter : number of iteration                                    --
--			     	=> the function performs pol * X^nb_iter MOD pol_gen -- 
--		config  : configuration of the seed                              --
--                      => the function performs pol<<1 & seed               --
--          seed    : input seed to give a new value to the polynomial       --  
-------------------------------------------------------------------------------
FUNCTION gen_lfsr(
            pol      : std_logic_vector;      
	      en       : std_logic;             
            nb_iter  : natural;             
	      config   : std_logic;
	      seed     :  std_logic) 		RETURN std_logic_vector;
END lfsr_pkg; 


PACKAGE BODY lfsr_pkg IS


FUNCTION gen_lfsr(
          	pol      : std_logic_vector;
	      en       : std_logic;             
            nb_iter  : natural;             
	      config   : std_logic;
	      seed     :  std_logic) 		RETURN std_logic_vector IS


    	VARIABLE pol_int : std_logic_vector(pol'length-1 DOWNTO 0);  -- tempory polynomial
	VARIABLE pol_gen : std_logic_vector(pol'length-1 DOWNTO 0);  -- generator polynomial
 	VARIABLE dummy   : std_logic := '1';                           -- for seed procedure
 
BEGIN
    -- selection of the generator polynomial as a function of the length of "pol".
    CASE pol'length is
          when 31 => pol_gen := "0000000000000000000000000001001"; -- the polymomial is x^31 + x^3 + 1
          when 30 => pol_gen := "000000000000000000000001010011";
	    when 29 => pol_gen := "00000000000000000000000000101";
	    when 28 => pol_gen := "0000000000000000000000001001";
	    when 27 => pol_gen := "000000000000000000000100111";
	    when 26 => pol_gen := "00000000000000000001000111";
	    when 25 => pol_gen := "0000000000000000000001001";
	    when 24 => pol_gen := "000000000000000000011011";
	    when 23 => pol_gen := "00000000000000000100001";
	    when 22 => pol_gen := "0000000000000000000011";
	    when 21 => pol_gen := "000000000000000000101";
	    when 20 => pol_gen := "00000000000000001001";
	    when 19 => pol_gen := "0000000000000100111";
	    when 18 => pol_gen := "000000000010000001";
          when 17 => pol_gen := "00000000000001001";
	    when 16 => pol_gen := "0000000000101101";
	    when 15 => pol_gen := "000000000000011";
	    when 14 => pol_gen := "00000000101011";
	    when 13 => pol_gen := "0000000011011";
	    when 12 => pol_gen := "000001010011";
	    when 11 => pol_gen := "00000000101";
	    when 10 => pol_gen := "0000001001";
	    when  9 => pol_gen := "000010001";
	    when  8 => pol_gen := "00011101";--x^8 + x^4 + x^3 + x^2 + 1
	    when  7 => pol_gen := "0000011";
	    when  6 => pol_gen := "000011";
	    when  5 => pol_gen := "00101"; -- x^5 + x^2 + 1
	    when  4 => pol_gen := "0011"; -- x^4 + x + 1
	    when  3 => pol_gen := "011"; -- x^3 + x + 1 
	    when  others => pol_gen := "11"; --x^2 + x + 1
     END CASE;

-- affectation of the tempory variable pol_int
pol_int := pol;

iteration : FOR i IN 1 TO nb_iter LOOP
	IF ((en = '1') OR (config = '1')) THEN
		IF pol_int(pol'length-1)='1' AND config = '0' THEN
			pol_int := (pol_int(pol'length-2 DOWNTO 0)&'0') xor pol_gen;
		ELSE
		    	pol_int := pol_int(pol'length-2 DOWNTO 0)&(config AND (seed XOR dummy ));
		END IF;
	ELSE
      	pol_int := pol_int;      
	END IF;
	dummy := NOT dummy;  -- to avoid a seed with nb_iter 0
END LOOP;
 
RETURN (pol_int);

END gen_lfsr;



END lfsr_pkg;




























