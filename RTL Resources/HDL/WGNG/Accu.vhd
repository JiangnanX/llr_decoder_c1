-------------------------------------------------------------------------------
--                                                                           --
-- File            : accu.vhd                                                --
-- Related files   :                                                         --
--                                                                           --
--                                                                           --
-- Authors         : J-L Danger H. Laamari                                   --
-- Projet          : WGNG                                                    --
--                                                                           --
-- date de creation: 18/04/2000                                              --
--                                                                           --
-- Description     : accumulation of 4 white gaussian noise samples in       --
--                   order to obtain a good gaussien distribution (central   --
--                   limit theorem). The output of the function is on 10 bits--
--                                                                           --
-------------------------------------------------------------------------------
-- Modification : 23/8/00                                                    --
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;


ENTITY  accu  IS

	PORT(
		rst    : IN std_logic;
		clk    : IN std_logic;
		sign   : IN std_logic;
     	      wgn    : IN std_logic_vector(9 DOWNTO 0);
            wgn_a  : OUT std_logic_vector(9 DOWNTO 0)
	    ); 
END  accu;

-------------------------------------------------------------------------------
--  Architecture                                                             --
-------------------------------------------------------------------------------
ARCHITECTURE rtl OF accu IS


BEGIN  

accumulation : PROCESS(rst, clk) 

	CONSTANT Offset : signed(11 DOWNTO 0) := "000000000010";     
	VARIABLE A      : signed(11 DOWNTO 0); -- Variable intermédiaire 
	VARIABLE B      : std_logic_vector(11 DOWNTO 0); -- Variable intermédiaire 
        VARIABLE Comp   : natural range 0 TO 3;
       
      BEGIN 
             
	IF rst ='0' THEN
		Comp  := 0;
            A     := (OTHERS => '0');
		wgn_a <= (OTHERS => '0');
	ELSIF (clk'event and clk='1') THEN
		IF sign = '0' THEN
			B := wgn(9)& wgn(9) & wgn(9 DOWNTO 0);
		ELSE
			B := NOT( wgn(9)& wgn(9) & wgn(9 DOWNTO 0));
		END IF;
                            
      	IF Comp = 0 THEN     -- Initialisation
               	A := Offset + signed(B);
            ELSE                 -- accumulation
	            A := A + signed(B); 
            END IF;

            IF comp = 3 THEN  -- fin de l'accumulation
            	wgn_a <= conv_std_logic_vector(A(11 DOWNTO 2),10);
			comp := 0;                               
	      ELSE	
			comp := comp +1;
		END IF;
	END IF;
END PROCESS accumulation;
END rtl;

                  











