`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/12/2016 06:47:45 PM
// Design Name: 
// Module Name: rom_vol
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module rom_vol(
    input [4:0] a,
    input clk,
    output reg [223:0] spo
    );
	
	reg [223:0] mem [0:36];
	
	initial begin
		$readmemb("vol.txt",mem);
	end
	
	always @(posedge clk) spo <= mem[a];
	
	
endmodule
