`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/12/2016 05:57:04 PM
// Design Name: 
// Module Name: rom_noise
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module rom_noise(
    input [4:0] a,
    input clk,
    output reg [31:0] spo
    );
	
	reg [31:0] mem [0:36];
	
	initial begin
		$readmemb("noise.txt",mem);
	end
	
	always @(posedge clk) spo <= mem[a];
	
	
endmodule
