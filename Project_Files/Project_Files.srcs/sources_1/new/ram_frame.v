`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/12/2016 03:19:35 PM
// Design Name: 
// Module Name: ram_frame
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ram_frame(
    input [4:0] a,
    input [39:0] d,
    input clk,
    input we,
    output reg [39:0] spo
    );
	
	reg [39:0] mem [0:31];
	
	always @(posedge clk or posedge we) begin
		if(we) begin 
			mem [a] <= d;
			spo <= mem[a];
		end
		else spo <= mem[a];
	end	
	
	
endmodule
