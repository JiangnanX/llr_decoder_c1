`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/12/2016 04:18:03 PM
// Design Name: 
// Module Name: ram_iteration
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ram_iteration(
    input [4:0] a,
    input [42:0] d,
    input clk,
    input we,
    output reg [42:0] spo
    );
	
	reg [42:0] mem [0:31];
	
	always @(posedge clk or posedge we) begin
		if(we) begin 
			mem [a] <= d;
			spo <= mem[a];
		end
		else spo <= mem[a];
	end	
	
	
endmodule
