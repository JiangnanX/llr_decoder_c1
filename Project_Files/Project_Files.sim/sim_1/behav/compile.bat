@echo off
rem  Vivado(TM)
rem  compile.bat: a Vivado-generated XSim simulation Script
rem  Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.

set PATH=%XILINX%\lib\%PLATFORM%;%XILINX%\bin\%PLATFORM%;C:/Xilinx/Vivado/2014.2/ids_lite/ISE/bin/nt64;C:/Xilinx/Vivado/2014.2/ids_lite/ISE/lib/nt64;C:/Xilinx/Vivado/2014.2/bin;%PATH%
set XILINX_PLANAHEAD=C:/Xilinx/Vivado/2014.2

xelab -m64 --debug typical --relax --include "C:/Users/jiangnan/Desktop/C1/RTL Resources/HDL/hdl_gen" --include "C:/Users/jiangnan/Desktop/C1/RTL Resources/HDL/hdl_ldpc_sim" --include "c:/Users/jiangnan/Desktop/C1/RTL Resources/IPs/ila_0/labtools_general_components_lib_v2_0/hdl/verilog" --include "c:/Users/jiangnan/Desktop/C1/RTL Resources/IPs/ila_0/labtools_xsdb_slave_lib_v3_0/hdl/verilog" --include "c:/Users/jiangnan/Desktop/C1/RTL Resources/IPs/ila_0/ila_v4_0/hdl/verilog" -L xil_defaultlib -L dist_mem_gen_v8_0 -L xbip_utils_v3_0 -L xbip_pipe_v3_0 -L xbip_bram18k_v3_0 -L mult_gen_v12_0 -L unisims_ver -L unimacro_ver -L secureip --snapshot LDPC_SIMULATION_TOP_behav --prj C:/Users/jiangnan/Desktop/C1/Project_Files/Project_Files.sim/sim_1/behav/LDPC_SIMULATION_TOP.prj   xil_defaultlib.LDPC_SIMULATION_TOP   xil_defaultlib.glbl
if errorlevel 1 (
   cmd /c exit /b %errorlevel%
)
