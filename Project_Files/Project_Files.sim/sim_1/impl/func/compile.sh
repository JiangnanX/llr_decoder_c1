#!/bin/sh
# Vivado(TM)
# compile.sh: Vivado-generated Script for launching XSim application
# Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
# 
if [ -z "$PATH" ]; then
  PATH=%XILINX%\lib\%PLATFORM%;%XILINX%\bin\%PLATFORM%:C:/Xilinx/Vivado/2014.2/ids_lite/ISE/bin/nt64;C:/Xilinx/Vivado/2014.2/ids_lite/ISE/lib/nt64
else
  PATH=%XILINX%\lib\%PLATFORM%;%XILINX%\bin\%PLATFORM%:C:/Xilinx/Vivado/2014.2/ids_lite/ISE/bin/nt64;C:/Xilinx/Vivado/2014.2/ids_lite/ISE/lib/nt64:$PATH
fi
export PATH

if [ -z "$LD_LIBRARY_PATH" ]; then
  LD_LIBRARY_PATH=:
else
  LD_LIBRARY_PATH=::$LD_LIBRARY_PATH
fi
export LD_LIBRARY_PATH

#
# Setup env for Xilinx simulation libraries
#
XILINX_PLANAHEAD=C:/Xilinx/Vivado/2014.2
export XILINX_PLANAHEAD
ExecStep()
{
   "$@"
   RETVAL=$?
   if [ $RETVAL -ne 0 ]
   then
       exit $RETVAL
   fi
}
XELAB_1="C:/Users/jiangnan/Desktop/v1.3_sim_instructions-20161005T221036Z/C1/RTL Resources/HDL/hdl_gen"
XELAB_2="C:/Users/jiangnan/Desktop/v1.3_sim_instructions-20161005T221036Z/C1/RTL Resources/HDL/hdl_ldpc_sim"
XELAB_3="c:/Users/jiangnan/Desktop/v1.3_sim_instructions-20161005T221036Z/C1/RTL Resources/IPs/ila_0/labtools_general_components_lib_v2_0/hdl/verilog"
XELAB_4="c:/Users/jiangnan/Desktop/v1.3_sim_instructions-20161005T221036Z/C1/RTL Resources/IPs/ila_0/labtools_xsdb_slave_lib_v3_0/hdl/verilog"
XELAB_5="c:/Users/jiangnan/Desktop/v1.3_sim_instructions-20161005T221036Z/C1/RTL Resources/IPs/ila_0/ila_v4_0/hdl/verilog"

ExecStep xelab -m64 --debug typical --relax --include "$XELAB_1" --include "$XELAB_2" --include "$XELAB_3" --include "$XELAB_4" --include "$XELAB_5" -L xil_defaultlib -L unisims_ver -L secureip --snapshot LDPC_SIMULATION_TOP_func_impl --prj C:/Users/jiangnan/Desktop/v1.3_sim_instructions-20161005T221036Z/C1/Project_Files/Project_Files.sim/sim_1/impl/func/LDPC_SIMULATION_TOP.prj   xil_defaultlib.LDPC_SIMULATION_TOP   xil_defaultlib.glbl
